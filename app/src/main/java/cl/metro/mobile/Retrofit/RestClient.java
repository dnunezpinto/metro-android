package cl.metro.mobile.Retrofit;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import cl.metro.mobile.Utilities.Utils;
import retrofit.RestAdapter;

/**
 * Created by Daniel on 03-03-2015.
 */
public class RestClient {


    private static ApiService REST_CLIENT;

    static {
        RestClient();
    }

    public static ApiService get() {
        return REST_CLIENT;
    }

    public static ApiService getDesarrollo() {
        return REST_CLIENT;
    }

    public static void RestClient() {
        Gson gson = new GsonBuilder()
                .serializeNulls()
                .create();

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(Utils.URL_BASE_INTERMOVIL)
                .build();


        REST_CLIENT = restAdapter.create(ApiService.class);
    }

    public static void RestClientDesarrollo() {
        Gson gson = new GsonBuilder()
                .serializeNulls()
                .create();

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(Utils.URL_DESARROLLO)
                .build();


        REST_CLIENT = restAdapter.create(ApiService.class);
    }

    public static void RestTarjetaBip() {

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(Utils.BASE_URL_BIP)
                .build();


        REST_CLIENT = restAdapter.create(ApiService.class);
    }
}
