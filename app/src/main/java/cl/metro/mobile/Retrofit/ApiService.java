package cl.metro.mobile.Retrofit;

import java.util.List;

import cl.metro.mobile.Models.EquipaminetoEstacion.Equipamiento;
import cl.metro.mobile.Models.EquipaminetoEstacion.EquipamientoResponse;
import cl.metro.mobile.Models.EstadoRed.EstadoRed;
import cl.metro.mobile.Models.Horario.TrenParse;
import cl.metro.mobile.Models.ListaClubMetro.ListClub;
import cl.metro.mobile.Models.LugaresInteresEntretencion.ListLugaresInteresEntretencion;
import cl.metro.mobile.Models.Paraderos.ListParaderos;
import cl.metro.mobile.Models.ServiciosCercanos.ServiciosCercanos;
import cl.metro.mobile.Models.TarifasServicio.Tarifas;
import cl.metro.mobile.Models.Tweets.Tweets;
import retrofit.Callback;
import retrofit.client.Response;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by Daniel on 03-03-2015.
 */
public interface ApiService {

//    @GET("/includes/ws/getEstadoLinea/{id}")
//    void getEstadoLinea(@Path("id") String estacionID, Callback<ModelEstadoLinea> callback);

//    metro.intermovil.cl/metro/cache/getEstacion/pedro-de-valdivia-l1

    //    http://www.metro.cl/includes/ws/getEstadoEstacion/las-mercedes
    @GET("/cache/getEstacion/{id}")
    void getEstadoEstacion(@Path("id") String estacionID, Callback<Response> callback);

    @GET("/cache/getEstacion/{id}")
    void getEquipEstacion(@Path("id") String estacionID, Callback<Equipamiento> callback);

    @GET("/cache/getEstacion/{id}")
    void getServiciosCercanos(@Path("id") String estacionID, Callback<ServiciosCercanos> callback);

    @GET("/cache/getEstacion/{id}")
    void equipamientoList(@Path("id") String estacionID, Callback<List<EquipamientoResponse>> cb);

    @GET("/cache/getTarifario/es")
    void getTarifario(Callback<Tarifas> cb);

// ANTIGUO REQUEST HORARIO
// @GET("/cache/getHorarios/{id}")
//    void getHorarios(@Path("id") String estacionID, Callback<TrenParse> callback);

    @GET("/horariosEstacion.php")
    void getHorarios(@Query("cod") String estacionID, Callback<TrenParse> callback);

    @GET("/cache/status_{id}")
    void getStatusRed(@Path("id") String estacionID, Callback<EstadoRed> callback);

    @GET("/cache/getTweets/{id}")
    void getTweets(@Path("id") String tweetID, Callback<Tweets> callback);

    @GET("/includes/ws/planificador/{estacionOrigen}/{estacionDestino}/{dia}/{hora}")
    void setPlanificar(@Path("estacionOrigen") String estacionOrigen, @Path("estacionDestino") String estacionDestino,
                       @Path("dia") String dia, @Path("hora") String hora, Callback<Response> callback);

    @GET("/planificador.php")
    void getPlanificador(@Query("estacionInicio") String estacionOrigen, @Query("estacionFin") String estacionDestino, Callback<Response> callback);


    @FormUrlEncoded
    @POST("/PortalCAE-WAR-MODULE/SesionPortalServlet")
    void getSaldoBip(@Field("accion") String accion,
                     @Field("NumDistribuidor") String numDistribuidor,
                     @Field("NomUsuario") String nomUsuario,
                     @Field("NumTarjeta") String numTarjeta,
                     @Field("RutUsuario") String rutUsuario,
                     @Field("NomHost") String nomHost,
                     @Field("NomDominio") String nomDominio,
                     @Field("bloqueable") String bloqueable,
                     @Field("Trx") String _Trx, Callback<Response> callback);


    //    http://qamt.agenciacatedral.cl/clubmetro/includes/wsMobile/login.php?email=negroku@gmail.com&pass=OXUDRR
    @GET("/clubMetroLogin.php")
    void login(@Query("email") String mEmail, @Query("pass") String mPassword, Callback<Response> callback);

//    http://qamt.agenciacatedral.cl/includes/wsMobile/clubMetroLista.php

    @GET("/clubMetroLista.php")
    void getListadoClubMetro(Callback<ListClub> callback);

    //    http://qamt.agenciacatedral.cl/includes/wsMobile/clubMetroDetalle.php?id=112&tipo=promocion
    @GET("/clubMetroDetalle.php")
    void getDetalleLista(@Query("id") int mId, @Query("tipo") String mTipo, Callback<Response> callback);

    //    http://qamt.agenciacatedral.cl/includes/wsMobile/estacionesExpreso.php?cod=vespucio-norte
    @GET("/estacionesExpreso.php")
    void getEstacionExpreso(@Query("cod") String mCod, Callback<Response> callback);

    //    http://qamt.agenciacatedral.cl/includes/wsMobile/estacionesPatrimonio.php?cod=pedro-de-valdivia-
    @GET("/estacionesEventos.php")
    void getEventosByEstacion(@Query("cod") String mCod, Callback<ListLugaresInteresEntretencion> callback);

    //    http://qamt.agenciacatedral.cl/includes/wsMobile/estacionesEventos.php?cod=la-moneda
    @GET("/estacionesPatrimonio.php")
    void getPatrimoniosByEstacion(@Query("cod") String mCod, Callback<ListLugaresInteresEntretencion> callback);

    //    http://qamt.agenciacatedral.cl/includes/wsMobile/estacionesListaEventos.php
    @GET("/estacionesListaEventos.php")
    void getListaEventosDelMes(Callback<ListLugaresInteresEntretencion> callback);

    //    http://qamt.agenciacatedral.cl/includes/wsMobile/estacionesParadero.php?cod=pedro-de-valdivia
    @GET("/estacionesParadero.php")
    void getParaderos(@Query("cod") String mCod, Callback<ListParaderos> callback);

    //    http://qamt.agenciacatedral.cl/includes/wsMobile/paraderosLista.php?paradero=PJ149
    @GET("/paraderosLista.php")
    void getParaderosDetalle(@Query("paradero") String mCodigoParadero, Callback<Response> callback);


    @GET("/paraderosRecorrido.php")
    void getParaderosRecorridos(@Query("paradero") String mCodigoParadero, @Query("recorrido") String mRecorrido, Callback<Response> callback);


    @GET("/wsmessage.php?action=register")
    void setConfigNotificacionesLineas(@Query("android_id") String mAndroidID,
                                       @Query("registration_id") String mRegistrationID,
                                       @Query("linea_1") String mLinea1,
                                       @Query("linea_2") String mLinea2,
                                       @Query("linea_4") String mLinea4,
                                       @Query("linea_4a") String mLinea4a,
                                       @Query("linea_5") String mLinea5,
                                       @Query("type_device") String mTypeDevice,
                                       Callback<Response> callback);

    @GET("/wsmessage.php?action=register")
    void setConfigNotificacionesHorarios(@Query("android_id") String mAndroidID,
                                         @Query("registration_id") String mRegistrationID,
                                         @Query("horario_tramo_1") String mTramo1,
                                         @Query("horario_tramo_2") String mTramo2,
                                         @Query("horario_tramo_3") String mTramo3,
                                         @Query("horario_tramo_4") String mTramo4,
                                         @Query("horario_tramo_5") String mTramo5,
                                         @Query("horario_tramo_6") String mTramo6,
                                         @Query("type_device") String mTypeDevice,
                                         Callback<Response> callback);


    @GET("/wsmessage.php?action=register")
    void setConfigNotificacionesDiasSemana(@Query("android_id") String mAndroidID,
                                           @Query("registration_id") String mRegistrationID,
                                           @Query("dia_lunes") String mDiaLunes,
                                           @Query("dia_martes") String mDiaMartes,
                                           @Query("dia_miercoles") String mDiaMiercoles,
                                           @Query("dia_jueves") String mDiaJueves,
                                           @Query("dia_viernes") String mDiaViernes,
                                           @Query("dia_sabado") String mDiaSabado,
                                           @Query("dia_domingo") String mDiaDomingo,
                                           @Query("type_device") String mTypeDevice,
                                           Callback<Response> callback);

    @GET("/serviciosPush.php")
    void getURLServicios(Callback<Response> callback);

    @GET("/clubMetroCuponesLista.php")
//    http://qamt.agenciacatedral.cl/includes/wsMobile/clubMetroCuponesLista.php?idUsuario=409338
    void getListVoucher(@Query("idUsuario") String idUsuario, @Query("random") int random, Callback<Response> callback);

    @GET("/clubMetroCuponesVerificar.php")
//    http://qamt.agenciacatedral.cl/includes/wsMobile/clubMetroCuponesVerificar.php?idUsuario=409338&voucher=584460338568121409
    void verificarCupon(@Query("idUsuario") String idUsuario, @Query("voucher") String voucher, Callback<Response> callback);


    @GET("/clubMetroRegistro.php")
//    http://qamt.agenciacatedral.cl/includes/wsMobile/clubMetroRegistro.php?nombre=Rene&paterno=valla&materno=ide&sexo=1&cel=73783925&pass=12345&email=negroku@gmail.com&rut=13890356-7
    void clubMetroRegistro(@Query("nombre") String _Nombre,
                           @Query("paterno") String _Paterno,
                           @Query("materno") String _Materno,
                           @Query("sexo") String _Sexo,
                           @Query("cel") String _Cel,
                           @Query("pass") String _Pass,
                           @Query("email") String _Email,
                           @Query("rut") String _Rut,
                           Callback<Response> callback);

    @GET("/clubMetroCuponesQuemar.php")
//    http://qamt.agenciacatedral.cl/includes/wsMobile/clubMetroCuponesQuemar.php?idUsuario=409338&idPromocion=120&codigo=asieslavidaenmetro
    void canjearCupon(@Query("idUsuario") String _idUsuario,
                      @Query("idPromocion") int _idPromocion,
                      @Query("codigo") String _Codigo,
                      Callback<Response> callback);

    @GET("/clubMetroCuponesAgregarBip.php")
//    http://qamt.agenciacatedral.cl/includes/wsMobile/clubMetroCuponesAgregarBip.php?idUsuario=409338&chip=1495771721&bip=7436781
    void agregarBip(@Query("idUsuario") String _idUsuario,
                    @Query("chip") String _Chip,
                    @Query("bip") String _Bip,
                    Callback<Response> callback);

    @GET("/clubMetroBipLista.php")
//    http://qamt.agenciacatedral.cl/includes/wsMobile/clubMetroBipLista.php?idUsuario=409338
    void getBipLista(@Query("idUsuario") String _idUsuario,
                     @Query("random") int random,
                     Callback<Response> callback);


}
