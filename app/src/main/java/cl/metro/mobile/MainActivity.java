package cl.metro.mobile;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gcm.GCMRegistrar;
import com.loopj.android.http.AsyncHttpClient;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Stack;

import cl.metro.mobile.Database.DBHelperDataBase;
import cl.metro.mobile.Fragments.BipFragment;
import cl.metro.mobile.Fragments.ClubMetro.Actividades.ActividadesFragment;
import cl.metro.mobile.Fragments.ClubMetro.ClubMetroFragment;
import cl.metro.mobile.Fragments.ClubMetro.Evento.EventosFragment;
import cl.metro.mobile.Fragments.ClubMetro.Promociones.PromocionesFragment;
import cl.metro.mobile.Fragments.Emergencias1411_Fragment;
import cl.metro.mobile.Fragments.FavoritosFragment;
import cl.metro.mobile.Fragments.FragmentServicios.ServiciosFragment;
import cl.metro.mobile.Fragments.MapaMetroActivity;
import cl.metro.mobile.Fragments.Notificaciones.NotificacionesFragment;
import cl.metro.mobile.Fragments.PlanificadorDetalleFragment;
import cl.metro.mobile.Fragments.PlanificadorFragment;
import cl.metro.mobile.Fragments.TweetsLineasFragment;
import cl.metro.mobile.Models.Estaciones;
import cl.metro.mobile.Retrofit.ApiService;
import cl.metro.mobile.Utilities.AdapterListMenu;
import cl.metro.mobile.Utilities.EnunSharedPreference;
import cl.metro.mobile.Utilities.GPSTracker;
import cl.metro.mobile.Utilities.InitApp;
import cl.metro.mobile.Utilities.SharedPreferenceMetro;
import cl.metro.mobile.Utilities.Utils;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener,
        EventosFragment.OnFragmentInteractionListener,
        PromocionesFragment.OnFragmentInteractionListener,
        ActividadesFragment.OnFragmentInteractionListener,
        PlanificadorDetalleFragment.OnFragmentInteractionListener {

    public static final String PREFS_NAME = "MyPrefsFile";
    public static final String FIRST_RUN = "FirstRun";
    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */

    public static MainActivity mainActivity;
    public static Toolbar mToolbar;
    public static DrawerLayout mDrawerLayout;
    public static GPSTracker gpsTracker;
    public static AsyncHttpClient client = new AsyncHttpClient();
    public static InitApp initApp;
    public static ApiService service;
    public static Spinner spinnerEstacionesDialog;
    public static Spinner spinnerLineaDialog;
    public static ArrayList<Estaciones> arrayEstaciones;
    public static ListView mDrawerList;
    public static ImageLoader imageLoader = ImageLoader.getInstance();
    public static DisplayImageOptions options;
    public static SharedPreferences sharedPreferences;
    private static Context mContext;
    final private int REQUEST_CODE_ASK_PERMISSIONS = 123;
    int[] myImageList = new int[]{R.drawable.item_menu_metro, R.drawable.item_menu_place,
            R.drawable.item_menu_favorite, R.drawable.item_menu_info_bip,
            R.drawable.item_menu_service, R.drawable.item_menu_plano,
            R.drawable.item_menu_club_metro};
    Fragment newFragment;
    FragmentTransaction transaction;
    private ActionBarDrawerToggle mDrawerToggle;
    private int mCurrentSelectedPosition = 0;
    private CharSequence mTitle;
    private Stack<Fragment> stack;
    private int mPosition;
    private boolean mNavigationItemClicked;

    public static void initImageLoader(Context context) {

        File cacheDir = new File(context.getCacheDir(), "imgcachedir");
        if (!cacheDir.exists())
            cacheDir.mkdir();

        options = new DisplayImageOptions.Builder()
                .resetViewBeforeLoading(true)
                .imageScaleType(ImageScaleType.IN_SAMPLE_INT)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .cacheOnDisk(true)
                .displayer(new FadeInBitmapDisplayer(300))
                .build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
                .threadPriority(Thread.MIN_PRIORITY + 3)
                .denyCacheImageMultipleSizesInMemory()
                .memoryCache(new WeakMemoryCache())
                .defaultDisplayImageOptions(options)
                .build();
        ImageLoader.getInstance().init(config);
    }

    public static String getTokenId(Context ctx) {
        String regId = null;
        try {
            regId = GCMRegistrar.getRegistrationId(ctx);
            GCMRegistrar.checkDevice(ctx);
            GCMRegistrar.checkManifest(ctx);

            if (regId.equals("")) {
                GCMRegistrar.register(ctx, Utils.SENDER_ID);
            } else {
                Log.d("Id Registration", regId);
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

        return regId;
    }

    public static void checkAppStart(String regId) {

        sharedPreferences = mContext.getSharedPreferences(PREFS_NAME, 0);

        // Verificar si está abierta por primera vez
        if (sharedPreferences.getBoolean(FIRST_RUN, true)) {
            setNotificationPushFirstTime(regId);
            SharedPreferences.Editor editere = sharedPreferences.edit();
            editere.putBoolean(FIRST_RUN, false);
            editere.commit();
        }

    }

    public static void setNotificationPushFirstTime(String regId) {

        // Todas las notificaciones están activas por defecto.

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.NONE)
                .setEndpoint(Utils.URL_BASE_INTERMOVIL)
                .build();

        ApiService apiService = restAdapter.create(ApiService.class);

        final SharedPreferenceMetro preferences_metro = new SharedPreferenceMetro((Activity) mContext);
        final SharedPreferences preferences = preferences_metro.getPrefenrenceMetro();


        // Notificaciones Día de la Semana
        apiService.setConfigNotificacionesDiasSemana(MainActivity.initApp.android_id,
                regId, "1", "1", "1", "1", "1", "1", "1",
                "android", new Callback<Response>() {
                    @Override
                    public void success(Response response, Response response2) {

//                        Log.i("App", Utils.convertResponseBody(response));

                        preferences.getBoolean(
                                EnunSharedPreference.diaLunes.getText(), true);
                        preferences.getBoolean(
                                EnunSharedPreference.diaMartes.getText(), true);
                        preferences.getBoolean(
                                EnunSharedPreference.diaMiercoles.getText(), true);
                        preferences.getBoolean(
                                EnunSharedPreference.diaJueves.getText(), true);
                        preferences.getBoolean(
                                EnunSharedPreference.diaViernes.getText(), true);
                        preferences.getBoolean(
                                EnunSharedPreference.diaSabado.getText(), true);
                        preferences.getBoolean(
                                EnunSharedPreference.diaDomingo.getText(), true);
                    }

                    @Override
                    public void failure(RetrofitError error) {


                    }
                });

        // Notificaciones Lineas
        apiService.setConfigNotificacionesLineas(MainActivity.initApp.android_id,
                regId, "1", "1", "1", "1", "1",
                "android", new Callback<Response>() {
                    @Override
                    public void success(Response response, Response response2) {

//                        Log.i("App", Utils.convertResponseBody(response));
                        preferences.getBoolean(
                                EnunSharedPreference.linea1.getText(), true);
                        preferences.getBoolean(
                                EnunSharedPreference.linea2.getText(), true);
                        preferences.getBoolean(
                                EnunSharedPreference.linea4.getText(), true);
                        preferences.getBoolean(
                                EnunSharedPreference.linea4a.getText(), true);
                        preferences.getBoolean(
                                EnunSharedPreference.linea5.getText(), true);

                    }

                    @Override
                    public void failure(RetrofitError error) {

                    }
                });

        // Notificaciones Horarios
        apiService.setConfigNotificacionesHorarios(MainActivity.initApp.android_id,
                regId, "1", "1", "1", "1", "1", "1",
                "android", new Callback<Response>() {
                    @Override
                    public void success(Response response, Response response2) {
//                        Log.i("App", Utils.convertResponseBody(response));

                        preferences.getBoolean(EnunSharedPreference.horarioTramo1.getText(), true);
                        preferences.getBoolean(EnunSharedPreference.horarioTramo2.getText(), true);
                        preferences.getBoolean(EnunSharedPreference.horarioTramo3.getText(), true);
                        preferences.getBoolean(EnunSharedPreference.horarioTramo4.getText(), true);
                        preferences.getBoolean(EnunSharedPreference.horarioTramo5.getText(), true);
                        preferences.getBoolean(EnunSharedPreference.horarioTramo6.getText(), true);
                    }

                    @Override
                    public void failure(RetrofitError error) {

                    }
                });


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mToolbar = findViewById(R.id.toolbar);
        mDrawerLayout = findViewById(R.id.drawer_layout);
        setSupportActionBar(mToolbar);
        stack = new Stack<>();

        mContext = this;
        mainActivity = this;

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, mToolbar, R.string.app_name, R.string.app_name) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);


                if (mNavigationItemClicked) {
                    onInitFragment(mPosition);
                    onSectionAttached(mPosition);
                }
                mNavigationItemClicked = false;
            }
        };


        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerLayout.setDrawerTitle(Gravity.LEFT, "Metro");
        mDrawerList = findViewById(R.id.left_drawer);


        //Inicializa el menu.
        AdapterListMenu adapterListMenu = new AdapterListMenu(getSupportActionBar().getThemedContext(),
                (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) ?
                        R.layout.drawer_list_item :
                        R.layout.list_item_menulow,
                R.id.title, new String[]{
                getString(R.string.title_section1),
                getString(R.string.title_section2),
                getString(R.string.title_section3),
                getString(R.string.title_section4),
                getString(R.string.title_section5),
                getString(R.string.title_section6),
                getString(R.string.title_section7)},
                myImageList);
        mDrawerList.setAdapter(adapterListMenu);
        mDrawerList.setOnItemClickListener(this);
        mDrawerList.setItemChecked(mCurrentSelectedPosition, true);

        initImageLoader(mContext);

        //Crea la Base de datos con el archivo SQlite
        createDataBase();

//        if (ActivityCompat.checkSelfPermission(MainActivity.this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//
//            ActivityCompat.requestPermissions(MainActivity.this,
//                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
//                    REQUEST_CODE_ASK_PERMISSIONS);
//        } else {
//            gpsTracker = new GPSTracker(MainActivity.this);
//        }

        //Inicia y Valida los permisos de GPS y luego busca la estación mas cercana
        initApp();

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_PERMISSIONS:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Permission Granted
                    Log.d("App Metro", "Permission Granted");

                    gpsTracker = new GPSTracker(this);
                    initApp.searchEstacionMasCercana(gpsTracker.location, TweetsLineasFragment.txtEstacionCercana, TweetsLineasFragment.txtEstacionDistancia, mContext);

                    Utils.hasLocationPermission = true;

//                    getEstadoLineas();


                } else {

                    createDialog();
//                    Utils.showGPSDisabledAlertToUser(mContext);

                    // Permission Denied
                    Toast.makeText(MainActivity.this, "ACCESS_LOCATION Denied", Toast.LENGTH_SHORT)
                            .show();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void onBackPressed() {

        int count = getFragmentManager().getBackStackEntryCount();

        if (count == 1) {


            //additional code
        } else {
            getFragmentManager().popBackStack();
            removeCurrentFragment();
        }

        super.onBackPressed();
    }

    public void removeCurrentFragment() {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        Fragment currentFrag = getSupportFragmentManager().findFragmentById(R.id.container);


        String fragName = "NONE";

        if (currentFrag != null)
            fragName = currentFrag.getClass().getSimpleName();


        if (currentFrag != null)
            transaction.remove(currentFrag);

        transaction.commit();

    }

    @Override
    protected void onStart() {
        super.onStart();

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://www.metro.cl/api")
                .build();

        service = restAdapter.create(ApiService.class);

        showNotification();

    }

    public void onInitFragment(int position) {
        //Actualiza la vista de la app con fragments.
        transaction = getSupportFragmentManager().beginTransaction();
        int colorStatusBar = 0;

        switch (position) {
            case 0:

                Fragment f = getSupportFragmentManager().findFragmentById(R.id.container);
                if (f != null && f instanceof TweetsLineasFragment) {
                    return;
                } else {
                    newFragment = new TweetsLineasFragment();
                }
                colorStatusBar = getResources().getColor(R.color.my_awesome_darker_color);
                break;
            case 1:
                newFragment = new PlanificadorFragment();
                transaction.addToBackStack(null);
                colorStatusBar = getResources().getColor(R.color.my_awesome_darker_color);
                break;
            case 2:
                newFragment = new FavoritosFragment();
                transaction.addToBackStack(null);
                colorStatusBar = getResources().getColor(R.color.my_awesome_darker_color);
                break;
            case 3:
                newFragment = new BipFragment();
                transaction.addToBackStack(null);
                colorStatusBar = getResources().getColor(R.color.my_awesome_darker_color);
                break;
            case 4:
                newFragment = new ServiciosFragment();
                transaction.addToBackStack(null);
                ServiciosFragment.verServicio = false;
                colorStatusBar = getResources().getColor(R.color.my_awesome_darker_color);
                break;
            case 5:
                newFragment = new MapaMetroActivity();
                transaction.addToBackStack(null);
                colorStatusBar = getResources().getColor(R.color.my_awesome_darker_color);
                break;
            case 6:
                newFragment = new ClubMetroFragment();
                transaction.addToBackStack(null);
                colorStatusBar = getResources().getColor(R.color.my_awesome_darker_color);
//                asdfColor = getResources().getColor(R.color.club_metro_color_dark);
                break;

        }


        //Cambia de color el statusBar en android >= 21 (Lolipop)
        Utils.changeStatusBarColor(colorStatusBar, this);
        //Cambia de color rojo el actionbar/toolbar
        Utils.toolbarRed(this);

        transaction.replace(R.id.container, newFragment);
        transaction.commit();


    }

    public void onSectionAttached(int number) {
        switch (number) {

            // Obtiene el titulo de cada fragment en la vista seleccionada

            case 0:
                mTitle = getString(R.string.title_section1);
                break;
            case 1:
                mTitle = getString(R.string.title_section2);
                break;
            case 2:
                mTitle = getString(R.string.title_section3);
                break;
            case 3:
                mTitle = getString(R.string.title_section4);
                break;
            case 4:
                mTitle = getString(R.string.title_section5);
                break;
            case 5:
                mTitle = getString(R.string.title_section6);
                break;
            case 6:
                mTitle = getString(R.string.title_section7);
                break;

        }

        if (mTitle != null && mToolbar != null) {
            mToolbar.setTitle(mTitle);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

        mPosition = position;
        mNavigationItemClicked = true;
        mDrawerLayout.closeDrawer(mDrawerList);

//        mDrawerToggle.setDrawerIndicatorEnabled(true);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        pushFragment(getFragment(position), true);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
//        if (!mNavigationDrawerFragment.isDrawerOpen()) {
//            // Only show items in the action bar relevant to this screen
//            // if the drawer is not showing. Otherwise, let the drawer
//            // decide what to show in the action bar.
//            getMenuInflater().inflate(R.menu.main, menu);
//            restoreActionBar();
//            return true;
//        }
        getMenuInflater().inflate(R.menu.main, menu);


        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.isDrawerIndicatorEnabled() && mDrawerToggle.onOptionsItemSelected(item)) {
            return true;

        } else if (item.getItemId() == android.R.id.home &&
                getSupportFragmentManager().popBackStackImmediate()) {
            return true;
        }

        Fragment fragment = null;
        FragmentManager fragmentManager = getSupportFragmentManager();

        switch (item.getItemId()) {

            case R.id.action_call:

                if (fragment != null && fragment instanceof Emergencias1411_Fragment) {

                } else {
                    fragment = new Emergencias1411_Fragment();
                    fragmentManager.beginTransaction()
                            .replace(R.id.container, fragment)
                            .addToBackStack(null)
                            .commit();
                }


                return true;

            case R.id.action_notificaciones:

                if (fragment != null && fragment instanceof NotificacionesFragment) {

                } else {
                    fragment = new NotificacionesFragment();
                    fragmentManager.beginTransaction()
                            .replace(R.id.container, fragment)
                            .addToBackStack(null)
                            .commit();
                }


                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    private void createDataBase() {
        DBHelperDataBase dbHelper = new DBHelperDataBase(this);
        try {
            dbHelper.createDataBase();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void initApp() {

        initApp = new InitApp();
        initApp.obtenerLineasEstaciones();

        initApp.android_id = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        initApp.gcm_registration_id = getTokenId(mContext);


        if (Utils.isEstacionSelected) {
            //Busca la estacion mas cercana
            initApp.searchEstacionMasCercanaChoosen(Utils.estacion, this);

            //Inicia la app con el primer fragment, "TweetsLineasFragment"
            onInitFragment(mCurrentSelectedPosition);
            return;
        }

        if (ActivityCompat.checkSelfPermission(MainActivity.this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(MainActivity.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    REQUEST_CODE_ASK_PERMISSIONS);
        } else {
            gpsTracker = new GPSTracker(MainActivity.this);
        }

        if (gpsTracker != null && gpsTracker.location != null) {
            initApp.searchEstacionMasCercana(gpsTracker.location, TweetsLineasFragment.txtEstacionCercana, TweetsLineasFragment.txtEstacionDistancia, mContext);
        }

        //Inicia la app con el primer fragment, "TweetsLineasFragment"
        onInitFragment(mCurrentSelectedPosition);


    }

    private void createDialog() {

        final AlertDialog builder = new AlertDialog.Builder(this).create();
        LayoutInflater inflater = this.getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_estaciones, null);

        Button btnAceptar = v.findViewById(R.id.button5);
        btnAceptar.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                builder.dismiss();
                finish();

//                Utils.estacion = MainActivity.initApp.getEstacionForNombreEstacionAndIdLinea(spinnerLineaDialog.getSelectedItem().toString(), spinnerEstacionesDialog.getSelectedItem().toString());

                Utils.isEstacionSelected = true;
                Intent intent = new Intent(mContext, MainActivity.class);
                startActivity(intent);

            }
        });

        spinnerEstacionesDialog = v.findViewById(R.id.spinner);
        spinnerEstacionesDialog.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (arrayEstaciones != null) {


                    Log.i("App", arrayEstaciones.get(position).getNombreEstacion());
                    Log.i("App", "idLinea: " + arrayEstaciones.get(position).getIdLinea() + "");
//                    for(int i= 0; i < arrayEstaciones.size(); i++){
//                        arrayEstaciones.get(position).getIdLinea();
//                    }

                    Utils.estacion = arrayEstaciones.get(position);

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerLineaDialog = (Spinner) v.findViewById(R.id.spinner1);
        spinnerLineaDialog.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {

                Utils.setActionListenerForSpinner(spinnerEstacionesDialog, arg0, 0, (Activity) mContext);

            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {

            }
        });

        builder.setCanceledOnTouchOutside(false);
        builder.setCancelable(false);
        builder.setView(v);
        builder.show();
//        return builder.show();
    }

    @Override
    public void onFragmentInteraction(int id) {

    }


    public void showDrawerToggle(boolean showDrawerIndicator) {
    }

    public void showNotification() {
        Bundle extras = getIntent().getExtras();
        if (extras != null && extras.getString("notification_string") != null && extras.getString("notification_string") != "") {

            Utils.createAlertDialog((Activity) mContext, extras.getString("notification_string"));

        }
    }


}
