package cl.metro.mobile;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import cl.metro.mobile.Database.DBHelperDataBase;
import cl.metro.mobile.Retrofit.ApiService;
import cl.metro.mobile.Utilities.Utils;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class Splash extends ActionBarActivity {

    public static final String PREFS_NAMEDB = "MyPrefsFileDB";
    public static final String FIRST_RUNDB = "FirstRunDB";
    public static final String PREFS_URL = "MyPrefsURL";
    public static final String FIRST_URL = "FirstURL";
    public static SharedPreferences sharedPreferencesDB;
    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);


        updateDB();
        checkURL();

//        Thread timer = new Thread() {
//            public void run() {
//                try {
//                    sleep(2000);
//                } catch (InterruptedException e) {
//                    // TODO Auto-generated catch block
//                    e.printStackTrace();
//                } finally {
//                    Intent i = new Intent(Splash.this, MainActivity.class);
//                    finish();
//                    startActivity(i);
//                }
//            }
//        };
//        timer.start();

    }

    public void checkURL() {

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(Utils.URL_BASE_METRO)
                .build();

        ApiService apiService = restAdapter.create(ApiService.class);

        apiService.getURLServicios(new Callback<Response>() {
            @Override
            public void success(Response response, Response response2) {

                String result = Utils.convertResponseBody(response);

                try {

                    JSONObject resultJSON = new JSONObject(result);
                    Utils.URL_BASE_INTERMOVIL = resultJSON.getString("url");
                  //  Utils.URL_BASE_INTERMOVIL = "http://www.metro.cl/api/";


                    float density = Splash.this.getResources().getDisplayMetrics().density;
                    if (density >= 4.0) {
                        Utils.BannerITTURL = resultJSON.getString("XXHDPI");
                    } else if (density >= 3.0) {
                        Utils.BannerITTURL = resultJSON.getString("XXHDPI");
                    } else if (density >= 2.0) {
                        Utils.BannerITTURL = resultJSON.getString("XHDPI");
                    } else if (density >= 1.5) {
                        Utils.BannerITTURL = resultJSON.getString("HDPI");
                    } else if (density >= 1.0) {
                        Utils.BannerITTURL = resultJSON.getString("MDPI");
                    }

//                    Utils.BannerITTURL = "";
                    Utils.linkBanner = resultJSON.getString("link");
                    if(resultJSON.has("linkFooter")){
                        Utils.linkBannerFooter = resultJSON.getString("linkFooter");
                    }
                    Utils.footerBanner = resultJSON.getString("footer");

                    if (Utils.BannerITTURL.equals("") || Utils.BannerITTURL.isEmpty()) {
                        intent = new Intent(Splash.this, MainActivity.class);
                        Bundle extras = getIntent().getExtras();

                        if (extras != null && extras.getString("notification_string") != null && extras.getString("notification_string") != "") {

                            intent.putExtra("notification_string", extras.getString("notification_string"));
                        }
                    } else {
                        intent = new Intent(Splash.this, BannerITT.class);
                    }

//                    if(Utils.BannerITTURL != null || !Utils.BannerITTURL.equals("") || !Utils.BannerITTURL.isEmpty() || Utils.BannerITTURL != ""){
//                        intent = new Intent(Splash.this, BannerITT.class);
//                    }else{
//                        intent = new Intent(Splash.this, MainActivity.class);
//                        Bundle extras = getIntent().getExtras();
//
//                        if (extras != null && extras.getString("notification_string") != null && extras.getString("notification_string") != "") {
//
//                            intent.putExtra("notification_string", extras.getString("notification_string"));
//                        }
//                    }

                    sharedPreferencesDB = getSharedPreferences(PREFS_URL, 0);

                    Log.i("App", "App abierta por PRIMERA vez");
                    SharedPreferences.Editor editere = sharedPreferencesDB.edit();
                    editere.putString(FIRST_URL, resultJSON.getString("url"));
                    editere.commit();

                    finish();
                    startActivity(intent);

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void failure(RetrofitError error) {

                Utils.URL_BASE_INTERMOVIL = Utils.URL_BASE_METRO;

                finish();
                intent = new Intent(Splash.this, MainActivity.class);
                startActivity(intent);


            }
        });

    }

    public void updateDB() {

        sharedPreferencesDB = getSharedPreferences(PREFS_NAMEDB, 0);

        //if (sharedPreferencesDB.getBoolean(FIRST_RUNDB, true)) {

            Log.i("App", "App abierta por PRIMERA vez");

            DBHelperDataBase dbHelper = new DBHelperDataBase(this);
            SQLiteDatabase db = dbHelper.getWritableDatabase();
            dbHelper.onUpgrade(db, 7, 8);
            db.close();

            SharedPreferences.Editor editere = sharedPreferencesDB.edit();
            editere.putBoolean(FIRST_RUNDB, false);
            editere.commit();

        //}


    }


//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_splash, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }
}
