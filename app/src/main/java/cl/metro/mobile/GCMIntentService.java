package cl.metro.mobile;


import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;

import com.google.android.gcm.GCMBaseIntentService;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import java.text.SimpleDateFormat;

import cl.metro.mobile.Utilities.Utils;

public class GCMIntentService extends GCMBaseIntentService {


    public GCMIntentService() {
        super(Utils.SENDER_ID);
        Log.i("METRO", "Inicio servicio GCM");
    }


    @Override
    protected void onRegistered(Context context, String registrationId) {
        Log.i("METRO", "Registrado !!!");
        //Log.i("METRO", "GCM Registrado: regId = " + registrationId);
        MainActivity.checkAppStart(registrationId);
        MainActivity.initApp.gcm_registration_id = registrationId;
    }


    @Override
    protected void onUnregistered(Context context, String registrationId) {
        Log.i("METRO", "GCM Desregistrado: regId = " + registrationId);
    }


    @Override
    protected void onMessage(Context context, Intent intent) {

        Bundle extras = intent.getExtras();
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
        // The getMessageType() intent parameter must be the intent you received
        // in your BroadcastReceiver.
        String messageType = gcm.getMessageType(intent);

        if (!extras.isEmpty()) {  // has effect of unparcelling Bundle
            /*
             * Filter messages based on message type. Since it is likely that GCM
             * will be extended in the future with new message types, just ignore
             * any message types you're not interested in, or that you don't
             * recognize.
             */
            if (GoogleCloudMessaging.
                    MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {
                //   sendNotification("Send error: " + extras.toString());
            } else if (GoogleCloudMessaging.
                    MESSAGE_TYPE_DELETED.equals(messageType)) {
                //      sendNotification("Deleted messages on server: " +
                //     extras.toString());
                // If it's a regular GCM message, do some work.
            } else if (GoogleCloudMessaging.
                    MESSAGE_TYPE_MESSAGE.equals(messageType)) {

                Log.i("METRO", "GCM Llego mensaje !!! ");
                String mensaje = intent.getStringExtra("message");
                if (mensaje != null) {

//                    Log.i("METRO", "GCM contenido: " + mensaje);
                    NotificationCompat.Builder mBuilder =
                            new NotificationCompat.Builder(this)
//                                    .setSmallIcon(R.drawable.ic_launcher)
                                    .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.ic_launcher))
                                    .setSmallIcon(R.drawable.ic_notif_metrowhite_statusbar)
                                    .setContentTitle("Metro")
                                    .setContentText(mensaje)
                                    .setStyle(new NotificationCompat.BigTextStyle().bigText(mensaje));

                    // Creates an explicit intent for an Activity in your app
                    Intent resultIntent = new Intent(this, Splash.class);
                    resultIntent.putExtra("notification_string", mensaje);
                    // The stack builder object will contain an artificial back stack for the
                    // started Activity.
                    // This ensures that navigating backward from the Activity leads out of
                    // your application to the Home screen.
                    TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
                    // Adds the back stack for the Intent (but not the Intent itself)
                    stackBuilder.addParentStack(Splash.class);
                    // Adds the Intent that starts the Activity to the top of the stack
                    stackBuilder.addNextIntent(resultIntent);
                    PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
                    mBuilder.setContentIntent(resultPendingIntent);
                    mBuilder.setAutoCancel(true);


                    if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {

                    } else {
                        // Lollipop specific setColor method goes here.
                        mBuilder.setColor(Color.parseColor("#ff252c"));

                    }

//                    Notification notif = new NotificationCompat.Builder(getApplicationContext())
//                            .setLargeIcon(notificationLargeIconBitmap)
//                              .setSmallIcon(getNotificationIcon())
//                                    .setContentIntent(resultPendingIntent)
//                            .setAutoCancel(true)
//                            .setContentTitle("Metro")
//                            .setContentText(mensaje)
//                            .build();


                    NotificationManager mNotificationManager =
                            (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

                    int idRandom = Integer.valueOf(new SimpleDateFormat("HHmmss").format(new java.util.Date()));
                    // mId allows you to update the notification later on.
                    mNotificationManager.notify(idRandom, mBuilder.build());
                }
            }
        }


    }


    @Override
    protected void onError(Context context, String errorID) {
        Log.i("METRO", "GCM errorID: " + errorID);
    }

    private int getNotificationIcon() {

        boolean whiteIcon = Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP;

        return whiteIcon ? R.drawable.ic_notif_metro_statusbar : R.drawable.ic_launcher;
    }


}
