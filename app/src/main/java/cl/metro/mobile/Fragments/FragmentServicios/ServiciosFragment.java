package cl.metro.mobile.Fragments.FragmentServicios;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;

import java.util.ArrayList;

import cl.metro.mobile.MainActivity;
import cl.metro.mobile.Models.Estaciones;
import cl.metro.mobile.Models.Lineas;
import cl.metro.mobile.R;
import cl.metro.mobile.Splash;
import cl.metro.mobile.Utilities.Utils;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ServiciosFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ServiciosFragment extends Fragment implements View.OnClickListener, AdapterView.OnItemSelectedListener {


    public static Spinner spinnerEstaciones;
    public static Spinner spinnerLinea;

    public static boolean verServicio = false;
    public static boolean verFromMap = false;
//    public static int idLinea;

    public static ArrayList<Estaciones> arrayList;
    public static Estaciones estacion;
    public static int lastIdLinea;
    private static int currentPositionEstacion = 0;
    private MenuItem agregarFavorito;
    private boolean isActivity;


    public ServiciosFragment() {
        // Required empty public constructor
    }

    public static ServiciosFragment newInstance(String param1, String param2) {
        ServiciosFragment fragment = new ServiciosFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View rootView = inflater.inflate(R.layout.fragment_servicios, container, false);

        //Cambia de color rojo el actionbar/toolbar
        Utils.toolbarRed(getActivity());

        spinnerLinea = (Spinner) rootView.findViewById(R.id.spinnerLinea);
        spinnerEstaciones = (Spinner) rootView.findViewById(R.id.spinnerEstaciones);
        spinnerLinea.setOnItemSelectedListener(this);
        spinnerEstaciones.setOnItemSelectedListener(this);

        Button button = (Button) rootView.findViewById(R.id.btnInfoEstacion);
        Button btnComercioEntorno = (Button) rootView.findViewById(R.id.btnComercioEntorno);
        Button btnExpreso = (Button) rootView.findViewById(R.id.btnExpreso);
        Button btnParaderos = (Button) rootView.findViewById(R.id.btnParaderos);
        Button btnLugaresInteresEntretencion = (Button) rootView.findViewById(R.id.btnLugaresInteres);


        button.setOnClickListener(this);
        btnComercioEntorno.setOnClickListener(this);
        btnExpreso.setOnClickListener(this);
        btnLugaresInteresEntretencion.setOnClickListener(this);
        btnParaderos.setOnClickListener(this);

//        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
//        MainActivity.mDrawerLayout.setLayoutParams(layoutParams);
//        MainActivity.mToolbar.setBackgroundColor(getActivity().getResources().getColor(android.R.color.transparent));

//        MainActivity.mToolbar.animate().translationY(-MainActivity.mToolbar.getBottom()).setInterpolator(new AccelerateInterpolator()).start();


        return rootView;
    }


    @Override
    public void onResume() {
        super.onResume();

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Servicios");

//        Log.i("onResume", "mCurrentPositionEstacions: " + currentPositionEstacion);

        Splash.sharedPreferencesDB = getActivity().getSharedPreferences(Splash.PREFS_URL, 0);
        Splash.sharedPreferencesDB.getString(Splash.FIRST_URL, Utils.URL_BASE_INTERMOVIL);

        if (estacion != null) {
            spinnerEstaciones.setSelection(estacion.getOrden());
            spinnerLinea.setSelection(estacion.getIdLinea() - 1);
        } else {
            Log.d("onResume", "Estacion NULL");
        }


    }


    //    @Override
//    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
//
//        inflater.inflate(R.menu.global_favoritos, menu);
//
//        agregarFavorito = menu.findItem(R.id.action_favorite);
//
//        MainActivity.initApp.validarFavorito(agregarFavorito);
//
//        super.onCreateOptionsMenu(menu, inflater);
//    }
//
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//
//        int id = item.getItemId();
//
//
//        if (id == R.id.action_settings) {
//            return true;
//        }
//
//        if(id == R.id.action_favorite){
//
//            Estaciones estacion = MainActivity.initApp.getEstacionForNombreEstacionAndIdLinea("" + MainActivity.initApp.currentSelectedEstacion.getIdLinea(),
//                    MainActivity.initApp.currentSelectedEstacion.getNombreEstacion());
//
//            MainActivity.initApp.setFavorito("" + estacion.getIdLinea(), estacion.getNombreEstacion(), true, agregarFavorito);
//
//        }
//
//        return super.onOptionsItemSelected(item);
//    }

    @Override
    public void onClick(View v) {

        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        Fragment fragment = null;
        Bundle args = new Bundle();
        String mCodigoEstacion = null;
        Lineas lineas = Utils.getLineaForByID(String.valueOf(estacion.getIdLinea()), getActivity().getApplicationContext());

        MainActivity.initApp.currentSelectedEstacion = estacion;

//        if (!estacion.isCombinacion()) {
//            strCode = estacionFormat(estacion.getCodigoEstacion(), lineas);
//        } else {
            mCodigoEstacion = estacion.getCodigoEstacion();
//        }

        lastIdLinea = estacion.getIdLinea();

        switch (v.getId()) {

            case R.id.btnInfoEstacion:
                verServicio = true;
                fragment = new ServicioInfoFragment();
                args.putString("codigoEstacion", mCodigoEstacion);
                args.putString("nombreEstacion", estacion.getNombreEstacion());
                args.putInt("idLinea", estacion.getIdLinea());
                fragment.setArguments(args);


                break;
            case R.id.btnComercioEntorno:
                verServicio = true;
                fragment = new ServicioMapaFragment();
//                fragment = new ServicioComercioCategoriasFragment();
//                MainActivity.initApp.arrayFiltroMapa.clear();
                args.putString("codigoEstacion", mCodigoEstacion);
                args.putString("nombreEstacion", estacion.getNombreEstacion());
                args.putInt("idLinea", estacion.getIdLinea());
                fragment.setArguments(args);

                break;

            case R.id.btnExpreso:
                verServicio = true;
                fragment = new ServicioOperacionExpreso();
                args.putString("codigoEstacion", estacion.getCodigoEstacion());
                args.putString("nombreEstacion", estacion.getNombreEstacion());
                args.putInt("idLinea", estacion.getIdLinea());
                fragment.setArguments(args);

                break;
            case R.id.btnParaderos:
                verServicio = true;
//                Intent intent= new Intent(getActivity(), ParaderosRecorridos.class);
//                startActivity(intent);
//                isActivity = true;

                fragment = new ServicioParaderosRecorridosFragment();
                args.putString("codigoEstacion", estacion.getCodigoEstacion());
                args.putString("nombreEstacion", estacion.getNombreEstacion());
                args.putInt("idLinea", estacion.getIdLinea());
                fragment.setArguments(args);

                break;

            case R.id.btnLugaresInteres:
                verServicio = true;
                fragment = new MainLugaresInteresEntretencion();
                MainLugaresInteresEntretencion.mHashMap.clear();
                args.putString("codigoEstacion", estacion.getCodigoEstacion());
                args.putString("nombreEstacion", estacion.getNombreEstacion());
                args.putInt("idLinea", estacion.getIdLinea());
                fragment.setArguments(args);

                break;


        }

//        if(!isActivity){
        fragmentManager.beginTransaction()
                .addToBackStack(null)
                .replace(R.id.container, fragment)
                .commit();
//        }


    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch (parent.getId()) {

            case R.id.spinnerLinea:

                if (verServicio && position == lastIdLinea - 1) {
//                    spinnerLinea.setSelection(estacion.getIdLinea() - 1);
                    Log.i("Spinner", "verServicio TRUE, lastIDLinea IGUALES");
                    Utils.setActionListenerForSpinner(spinnerEstaciones, parent, estacion.getOrden(), getActivity());
                } else {

                    Log.i("Spinner", "verServicio FALSE, lastIDLinea FALSE");
                    Utils.setActionListenerForSpinner(spinnerEstaciones, parent, 0, getActivity());
                }

                break;
            case R.id.spinnerEstaciones:
//                spinnerEstaciones.setSelection(Utils.pos_estacion_inicio);

//                currentPositionEstacion = position;


                Log.i("App", "verServicio " + verServicio);
                estacion = arrayList.get(position);

                MainActivity.initApp.currentSelectedEstacion = arrayList.get(position);

                break;
        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
