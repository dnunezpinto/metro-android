package cl.metro.mobile.Fragments;


import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import cl.metro.mobile.Fragments.FragmentServicios.ServiciosFragment;
import cl.metro.mobile.MainActivity;
import cl.metro.mobile.Models.Bip;
import cl.metro.mobile.Models.Estaciones;
import cl.metro.mobile.Models.PlanificadorMetro;
import cl.metro.mobile.R;
import cl.metro.mobile.Utilities.AdapterFavoritos.AdapterListFavoritosEstacion;
import cl.metro.mobile.Utilities.AdapterFavoritos.AdapterListFavoritosRecorridos;
import cl.metro.mobile.Utilities.AdapterFavoritos.AdapterListFavoritosTarjetasBip;
import cl.metro.mobile.Utilities.GPSTracker;
import cl.metro.mobile.Utilities.Utils;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FavoritosFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FavoritosFragment extends Fragment {

    public static AdapterListFavoritosTarjetasBip adapterListFavoritosTarjetasBip;
    public static TextView txtEstacionCercana;
    public static TextView txtEstacionDistancia;
    private static ListView contentTarjetasBip;
    private static Activity mActivity;
    private static TextView emptyRecorridos, emptyBip;
    private ListView contentEstacionesFavoritas;
    private ListView contentRecorridosFavoritos;
    private List<Estaciones> listEstaciones = new ArrayList<>();

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
//    private static final String ARG_PARAM1 = "param1";
//    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
//    private String mParam1;
//    private String mParam2;


    public FavoritosFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static FavoritosFragment newInstance(String param1, String param2) {
        FavoritosFragment fragment = new FavoritosFragment();
//        Bundle args = new Bundle();
//        args.putString(ARG_PARAM1, param1);
//        args.putString(ARG_PARAM2, param2);
//        fragment.setArguments(args);
        return fragment;
    }

    public static void getTarjetasBip() {

        List<Bip> bipList;
        bipList = MainActivity.initApp.getAllBip();

        adapterListFavoritosTarjetasBip = new AdapterListFavoritosTarjetasBip(mActivity,
                R.layout.list_bip_favorita, bipList, "FavoritosFragment");
        contentTarjetasBip.setAdapter(adapterListFavoritosTarjetasBip);
        if (bipList.size() == 0) {
            emptyBip.setVisibility(View.GONE);
        }


        Utils.setListViewHeightBasedOnChildren(contentTarjetasBip);

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        if (getArguments() != null) {
//            mParam1 = getArguments().getString(ARG_PARAM1);
//            mParam2 = getArguments().getString(ARG_PARAM2);
//        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        mActivity = activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_favoritos, container, false);

        //Cambia de color rojo el actionbar/toolbar
        Utils.toolbarRed(getActivity());

        txtEstacionCercana = (TextView) rootView.findViewById(R.id.txtEstacionCercana);
        txtEstacionDistancia = (TextView) rootView.findViewById(R.id.textView51);
        emptyRecorridos = (TextView) rootView.findViewById(R.id.emptyRecorridos);
        emptyBip = (TextView) rootView.findViewById(R.id.emptyBip);


        if (Utils.isEstacionSelected) {

            MainActivity.initApp.searchEstacionMasCercanaChoosen(Utils.estacion, getActivity());

        } else {
            MainActivity.gpsTracker = new GPSTracker(getActivity());
//
            if (MainActivity.gpsTracker.location != null && MainActivity.initApp.estacionMasCercana != null) {

                MainActivity.initApp.searchEstacionMasCercana(MainActivity.gpsTracker.location, txtEstacionCercana, txtEstacionDistancia, getActivity());

            }
        }

        contentEstacionesFavoritas = (ListView) rootView.findViewById(R.id.contentEstacionesFavoritas);
        contentEstacionesFavoritas.setEmptyView(rootView.findViewById(R.id.emptyEstaciones));
        contentRecorridosFavoritos = (ListView) rootView.findViewById(R.id.contentRecorridosFavoritos);
        contentTarjetasBip = (ListView) rootView.findViewById(R.id.contentTarjetaBip);
        contentTarjetasBip.setEmptyView(rootView.findViewById(R.id.emptyBip));

        RelativeLayout estacionCercana = (RelativeLayout) rootView.findViewById(R.id.ll_EstacionCercana);
        estacionCercana.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Fragment fragment = new EstacionCercanaFragment();

                Bundle args = new Bundle();
                args.putInt("estadoEstacion", TweetsLineasFragment.estadoEstacion);
                args.putString("tweetLinea", TweetsLineasFragment.strTweet);
                fragment.setArguments(args);


                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.container, fragment)
                        .addToBackStack(null)
                        .commit();

            }
        });

        TextView verEstaciones = (TextView) rootView.findViewById(R.id.tvVerEstaciones);
//        contentTarjetasBip.setEmptyView(verEstaciones);
        verEstaciones.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FragmentTransaction transaction;
                transaction = getActivity().getSupportFragmentManager().beginTransaction();
                Fragment newFragment = new ServiciosFragment();
                transaction.replace(R.id.container, newFragment);
                transaction.commit();

            }
        });

        TextView verRecorridos = (TextView) rootView.findViewById(R.id.tvVerRecorridos);
        contentRecorridosFavoritos.setEmptyView(emptyRecorridos);
        verRecorridos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FragmentTransaction transaction;
                transaction = getActivity().getSupportFragmentManager().beginTransaction();
                Fragment newFragment = new PlanificadorFragment();
                transaction.replace(R.id.container, newFragment);
                transaction.commit();

            }
        });

        TextView verTarjetaBip = (TextView) rootView.findViewById(R.id.tvVerBip);
        verTarjetaBip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FragmentTransaction transaction;
                transaction = getActivity().getSupportFragmentManager().beginTransaction();
                Fragment newFragment = new BipFragment();
                transaction.replace(R.id.container, newFragment);
                transaction.commit();

            }
        });


        getFavoritosEstaciones();
        getRecorridosFavoritos();
        getTarjetasBip();

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(getString(R.string.title_section3));
        MainActivity.mDrawerList.setItemChecked(2, true);
    }

    public void getFavoritosEstaciones() {


        listEstaciones = MainActivity.initApp.getFavoritosEstaciones(true);

        contentEstacionesFavoritas.setAdapter(new AdapterListFavoritosEstacion(getActivity(),
                R.layout.list_item_favoritos,
                R.id.title, listEstaciones, getFragmentManager()));
        Utils.setListViewHeightBasedOnChildren(contentEstacionesFavoritas);

//        contentEstacionesFavoritas.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//
//                Log.i("App", listEstaciones.get(position).getNombreEstacion());
//
//            }
//        });

//        contentEstacionesFavoritas

//        for(Estaciones estaciones : listEstaciones){

//            ViewFavoritos favoritos =  new ViewFavoritos(getActivity());

//            favoritos.txtNombreFavorito.setText(estaciones.getNombreEstacion());
//            MainActivity.initApp.setEstacionImage(estaciones.getIdLinea(),favoritos.txtNombreFavorito, getActivity());

//            contentEstacionesFavoritas.addView(favoritos);


//        }
    }

    public void getRecorridosFavoritos() {

        List<PlanificadorMetro> planificadorMetroList;
        planificadorMetroList = MainActivity.initApp.getAllPlanificadoresMetro();

        contentRecorridosFavoritos.setAdapter(new AdapterListFavoritosRecorridos(getActivity(),
                R.layout.list_item_favoritos,
                R.id.title, planificadorMetroList, getFragmentManager()));

//        if(planificadorMetroList.size() > 0 ){
//            emptyRecorridos.setVisibility(View.GONE);
//        }
        Utils.setListViewHeightBasedOnChildren(contentRecorridosFavoritos);


    }

}
