package cl.metro.mobile.Fragments.FragmentServicios;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

import cl.metro.mobile.MainActivity;
import cl.metro.mobile.Models.LugaresInteresEntretencion.DatosLugaresInteresEntretencion;
import cl.metro.mobile.Models.LugaresInteresEntretencion.ListLugaresInteresEntretencion;
import cl.metro.mobile.R;
import cl.metro.mobile.Retrofit.ApiService;
import cl.metro.mobile.Utilities.AdapterListLugaresInteresEntretencion;
import cl.metro.mobile.Utilities.Utils;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.converter.GsonConverter;

/**
 * A simple {@link Fragment} subclass.
 */
public class LugaresEventosMes extends Fragment implements AbsListView.OnItemClickListener {

    private static List<DatosLugaresInteresEntretencion> datosLugaresInteresEntretencions;
    private ApiService apiService;
    private AbsListView mListView;
    private AdapterListLugaresInteresEntretencion adapterListLugaresInteresEntretencion;
    private boolean isVerRubros = false;
    private ProgressBar progressBar;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        Gson gson = new GsonBuilder()
//                .excludeFieldsWithoutExposeAnnotation()
                .create();

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setConverter(new GsonConverter(gson))
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(Utils.URL_BASE_METRO)
                .build();

        apiService = restAdapter.create(ApiService.class);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_lugares_eventos_mes, container, false);

        mListView = (AbsListView) rootView.findViewById(R.id.list);
        mListView.setOnItemClickListener(this);

        progressBar = (ProgressBar) rootView.findViewById(R.id.progressBar9);

        TextView tvNombreEstacion = (TextView) rootView.findViewById(R.id.nombreEstacion);
        tvNombreEstacion.setText(MainLugaresInteresEntretencion.nombreEstacion);
        MainActivity.initApp.setEstacionImage(MainLugaresInteresEntretencion.idLinea, tvNombreEstacion, getActivity());
//        tvNombreEstacion.setTextColor(Color.parseColor(Utils.COLORS[MainLugaresInteresEntretencion.idLinea - 1]));

//        if (datosLugaresInteresEntretencions != null) {
//            datosLugaresInteresEntretencions.clear();
//        }

        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();

        if (isVerRubros && datosLugaresInteresEntretencions != null) {
            setData();
        } else {
            getEventos();


        }

    }

    public void getEventos() {

        datosLugaresInteresEntretencions.clear();
        apiService.getListaEventosDelMes(new Callback<ListLugaresInteresEntretencion>() {
            @Override
            public void success(ListLugaresInteresEntretencion listLugaresInteresEntretencion, Response response) {


                if (listLugaresInteresEntretencion.getLugares() != null) {

                    datosLugaresInteresEntretencions = listLugaresInteresEntretencion.getLugares();
                    setData();
                    progressBar.setVisibility(View.GONE);
                    mListView.setVisibility(View.VISIBLE);

                } else {
                    progressBar.setVisibility(View.GONE);
                    Utils.createAlertDialog(getActivity(), "No han encontrado Eventos del Mes ");
                }

            }

            @Override
            public void failure(RetrofitError error) {


            }
        });
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        datosLugaresInteresEntretencions.get(position);
        isVerRubros = true;

        Fragment fragment = new DetalleLugaresInteresEntretencion().newInstance(datosLugaresInteresEntretencions.get(position).getDescripcion(),
                datosLugaresInteresEntretencions.get(position).getDireccion(),
                datosLugaresInteresEntretencions.get(position).getHorario(),
                datosLugaresInteresEntretencions.get(position).getTarifa(),
                datosLugaresInteresEntretencions.get(position).getImagen());
        getActivity().getSupportFragmentManager().beginTransaction()
                .addToBackStack(null)
                .replace(R.id.container, fragment)
                .commit();

    }

    public void setData() {

        adapterListLugaresInteresEntretencion = new AdapterListLugaresInteresEntretencion(getActivity(),
                R.layout.list_item,
                R.id.text_item,
                datosLugaresInteresEntretencions);
        mListView.setAdapter(adapterListLugaresInteresEntretencion);

    }

}
