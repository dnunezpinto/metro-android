package cl.metro.mobile.Fragments.Notificaciones;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ToggleButton;

import cl.metro.mobile.MainActivity;
import cl.metro.mobile.R;
import cl.metro.mobile.Retrofit.ApiService;
import cl.metro.mobile.Utilities.EnunSharedPreference;
import cl.metro.mobile.Utilities.SharedPreferenceMetro;
import cl.metro.mobile.Utilities.Utils;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class NotificacionesFragmentLineas extends Fragment implements OnClickListener {

    private ToggleButton toggleButtonLinea1;
    private ToggleButton toggleButtonLinea2;
    private ToggleButton toggleButtonLinea4;
    private ToggleButton toggleButtonLinea4A;
    private ToggleButton toggleButtonLinea5;


    public NotificacionesFragmentLineas() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_notificaciones_fragment_lineas, container, false);

        ((Button) rootView.findViewById(R.id.button_guardar_preferencias))
                .setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        connection();
                    }
                });

        setPreferences(rootView);

        return rootView;
    }

    private void setPreferences(View rootView) {
        SharedPreferenceMetro preferences_metro = new SharedPreferenceMetro(
                getActivity());
        SharedPreferences preferences = preferences_metro.getPrefenrenceMetro();
        boolean is_checked;
        is_checked = preferences.getBoolean(
                EnunSharedPreference.linea1.getText(), true);
        toggleButtonLinea1 = ((ToggleButton) rootView.findViewById(R.id.toggle_linea1));
        toggleButtonLinea1.setChecked(is_checked);
        toggleButtonLinea1.setOnClickListener(this);
        // Toast.makeText(getApplicationContext(),"Setting "+is_checked,
        // Toast.LENGTH_SHORT).show();
        is_checked = preferences.getBoolean(
                EnunSharedPreference.linea2.getText(), true);
        toggleButtonLinea2 = ((ToggleButton) rootView.findViewById(R.id.toggle_linea2));
        toggleButtonLinea2.setChecked(is_checked);
        toggleButtonLinea2.setOnClickListener(this);

        is_checked = preferences.getBoolean(
                EnunSharedPreference.linea4.getText(), true);
        toggleButtonLinea4 = ((ToggleButton) rootView.findViewById(R.id.toggle_linea4));
        toggleButtonLinea4.setChecked(is_checked);
        toggleButtonLinea4.setOnClickListener(this);

        is_checked = preferences.getBoolean(
                EnunSharedPreference.linea4a.getText(), true);
        toggleButtonLinea4A = ((ToggleButton) rootView.findViewById(R.id.toggle_linea4a));
        toggleButtonLinea4A.setChecked(is_checked);
        toggleButtonLinea4A.setOnClickListener(this);

        is_checked = preferences.getBoolean(
                EnunSharedPreference.linea5.getText(), true);
        toggleButtonLinea5 = ((ToggleButton) rootView.findViewById(R.id.toggle_linea5));
        toggleButtonLinea5.setChecked(is_checked);
        toggleButtonLinea5.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        boolean is_checked = ((ToggleButton) v).isChecked();
        // Toast.makeText(getApplicationContext(),"Click toogle "+is_checked,
        // Toast.LENGTH_SHORT).show();
        SharedPreferenceMetro preferences = new SharedPreferenceMetro(getActivity());
        switch (v.getId()) {
            case R.id.toggle_linea1:
                preferences.setPreferenceLinea(EnunSharedPreference.linea1,
                        is_checked);
                break;
            case R.id.toggle_linea2:
                preferences.setPreferenceLinea(EnunSharedPreference.linea2,
                        is_checked);
                break;
            case R.id.toggle_linea4:
                preferences.setPreferenceLinea(EnunSharedPreference.linea4,
                        is_checked);
                break;
            case R.id.toggle_linea4a:
                preferences.setPreferenceLinea(EnunSharedPreference.linea4a,
                        is_checked);
                break;
            case R.id.toggle_linea5:
                preferences.setPreferenceLinea(EnunSharedPreference.linea5,
                        is_checked);
                break;
        }


    }

    public void onClickToggleLineas(View v) {
        boolean is_checked = ((ToggleButton) v).isChecked();
        // Toast.makeText(getApplicationContext(),"Click toogle "+is_checked,
        // Toast.LENGTH_SHORT).show();
        SharedPreferenceMetro preferences = new SharedPreferenceMetro(getActivity());
        switch (v.getId()) {
            case R.id.toggle_linea1:
                preferences.setPreferenceLinea(EnunSharedPreference.linea1,
                        is_checked);
                break;
            case R.id.toggle_linea2:
                preferences.setPreferenceLinea(EnunSharedPreference.linea2,
                        is_checked);
                break;
            case R.id.toggle_linea4:
                preferences.setPreferenceLinea(EnunSharedPreference.linea4,
                        is_checked);
                break;
            case R.id.toggle_linea4a:
                preferences.setPreferenceLinea(EnunSharedPreference.linea4a,
                        is_checked);
                break;
            case R.id.toggle_linea5:
                preferences.setPreferenceLinea(EnunSharedPreference.linea5,
                        is_checked);
                break;
        }
    }

    private void connection() {

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(Utils.URL_BASE_INTERMOVIL)
                .build();

        ApiService apiService = restAdapter.create(ApiService.class);

        apiService.setConfigNotificacionesLineas(Settings.Secure.getString(getActivity().getContentResolver(), Settings.Secure.ANDROID_ID),
                MainActivity.getTokenId(getActivity()),
                (toggleButtonLinea1.isChecked() ? "1" : "0"),
                (toggleButtonLinea2.isChecked() ? "1" : "0"),
                (toggleButtonLinea4.isChecked() ? "1" : "0"),
                (toggleButtonLinea4A.isChecked() ? "1" : "0"),
                (toggleButtonLinea5.isChecked() ? "1" : "0"),
                "android", new Callback<Response>() {
                    @Override
                    public void success(Response response, Response response2) {


                        if (response.equals("{}")) {
                            Utils.createAlertDialog(getActivity(), "No se ha podido guardar sus preferencias, intente nuevamente.");
                        } else {
                            Utils.createAlertDialog(getActivity(), "Preferencias Guardadas.");
                        }
                        Log.i("App", Utils.convertResponseBody(response));


                    }

                    @Override
                    public void failure(RetrofitError error) {

                        Utils.createAlertDialog(getActivity(), "No se ha podido guardar sus preferencias, intente nuevamente.");

                    }
                });

    }
}
