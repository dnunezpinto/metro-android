package cl.metro.mobile.Fragments.ClubMetro.Promociones;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import cl.metro.mobile.Fragments.ClubMetro.LoginActivity;
import cl.metro.mobile.Fragments.ClubMetro.Voucher.IngresarVoucher;
import cl.metro.mobile.MainActivity;
import cl.metro.mobile.Models.ListaClubMetro.ListClub;
import cl.metro.mobile.R;
import cl.metro.mobile.Retrofit.ApiService;
import cl.metro.mobile.Utilities.AdapterClubMetro.AdapterListClubMetro;
import cl.metro.mobile.Utilities.Utils;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.converter.GsonConverter;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Large screen devices (such as tablets) are supported by replacing the ListView
 * with a GridView.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnFragmentInteractionListener}
 * interface.
 */
public class PromocionesFragment extends Fragment implements AbsListView.OnItemClickListener {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    /**
     * The fragment's ListView/GridView.
     */
    private AbsListView mListView;
    private AdapterListClubMetro adapterListClubMetro;

    /**
     * The Adapter which will be used to populate the ListView/GridView with
     * Views.
     */
    private ListAdapter mAdapter;
    private MaterialDialog dialog;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public PromocionesFragment() {
    }

    // TODO: Rename and change types of parameters
    public static PromocionesFragment newInstance(String param1, String param2) {
        PromocionesFragment fragment = new PromocionesFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        // TODO: Change Adapter to display your content
//        mAdapter = new ArrayAdapter<DummyContent.DummyItem>(getActivity(),
//                android.R.layout.simple_list_item_1, android.R.id.text1, DummyContent.ITEMS);


        if (
//                MainActivity.initApp.listClubMetro.getPromociones() != null ||
                MainActivity.initApp.listClubMetro.getPromociones().size() != 0) {
            Log.i("LOG", ""+ MainActivity.initApp.listClubMetro.getPromociones().size());
            Log.i("LOG", "Datos");
            adapterListClubMetro = new AdapterListClubMetro(getActivity(),
                    R.layout.list_item_club_metro,
                    R.id.text_item,
                    MainActivity.initApp.listClubMetro.getPromociones(),
                    getActivity().getSupportFragmentManager(),
                    "Promociones");

        } else {


            Log.i("LOG", "Sin Datos");
//            setEmptyText("Sin Datos...");
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_promociones, container, false);

        // Set the adapter
        mListView = (AbsListView) view.findViewById(R.id.list);
//        ((AdapterView<ListAdapter>) mListView).setAdapter(mAdapter);

        // Set OnItemClickListener so we can be notified on item clicks
        mListView.setOnItemClickListener(this);

        mListView.setAdapter(adapterListClubMetro);
        mListView.setEmptyView(view.findViewById(R.id.empty));

        Button btnIngresarCupon = (Button) view.findViewById(R.id.btnScan);
        btnIngresarCupon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!Utils.isUserConnected(getActivity())) {

                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                    alertDialogBuilder.setMessage("Debe iniciar sesión en Club Metro para ingresar un cupón.")
                            .setCancelable(false)
                            .setPositiveButton("Iniciar Sesión",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {

                                            Intent intent = new Intent(getActivity(), LoginActivity.class);
                                            intent.putExtra("ViewCall", "IngresarCupon");
                                            startActivity(intent);

                                        }
                                    });
                    alertDialogBuilder.setNegativeButton("No",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alert = alertDialogBuilder.create();
                    alert.show();

                    return;
                }

                Intent intent = new Intent(getActivity(), IngresarVoucher.class);
                getActivity().startActivity(intent);


            }
        });

//        getPromocionesDesarrollo();

        return view;
    }

    public void getPromocionesDesarrollo() {

//        http://qamt.agenciacatedral.cl/includes/wsMobile/clubMetroLista.php

        dialog = new MaterialDialog.Builder(getActivity())
                .content("Cargando Datos Club Metro, favor espere ...")
                .progress(true, 0)
                .autoDismiss(false)
                .cancelable(false)
                .show();

        Gson gson = new GsonBuilder()
//                .excludeFieldsWithoutExposeAnnotation()
                .create();

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setConverter(new GsonConverter(gson))
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(Utils.URL_DESARROLLO)
                .build();

        ApiService apiService = restAdapter.create(ApiService.class);

        apiService.getListadoClubMetro(new Callback<ListClub>() {
            @Override
            public void success(ListClub listClub, Response response) {

                dialog.dismiss();

                MainActivity.initApp.listClubMetro = listClub;

                adapterListClubMetro = new AdapterListClubMetro(getActivity(),
                        R.layout.list_item_club_metro,
                        R.id.text_item,
                        MainActivity.initApp.listClubMetro.getPromociones(),
                        getActivity().getSupportFragmentManager(),
                        "Promociones");

                mListView.setAdapter(adapterListClubMetro);

                setEmptyText("Sin Datos...");


            }

            @Override
            public void failure(RetrofitError error) {

                dialog.dismiss();
                Toast.makeText(getActivity(), "Problemas al contactar el servidor, favor intentar nuevamente ...", Toast.LENGTH_LONG).show();

            }
        });
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onResume() {
        super.onResume();

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Promociones");


    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (null != mListener) {
            // Notify the active callbacks interface (the activity, if the
            // fragment is attached to one) that an item has been selected.
            Fragment fragment = new DetallePromocionFragment().newInstance(
                    MainActivity.initApp.listClubMetro.getPromociones().get(position).getId(),
                    MainActivity.initApp.listClubMetro.getPromociones().get(position).getTipo(),
                    MainActivity.initApp.listClubMetro.getPromociones().get(position).getCanjeable()
            );

            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.container, fragment)
                    .addToBackStack(null)
                    .commit();


            mListener.onFragmentInteraction(MainActivity.initApp.listClubMetro.getPromociones().get(position).getId());
        }
    }

    /**
     * The default content for this Fragment has a TextView that is shown when
     * the list is empty. If you would like to change the text, call this method
     * to supply the text it should use.
     */
    public void setEmptyText(CharSequence emptyText) {
        View emptyView = mListView.getEmptyView();

        if (emptyView instanceof TextView) {
            ((TextView) emptyView).setText(emptyText);
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(int id);
    }

}
