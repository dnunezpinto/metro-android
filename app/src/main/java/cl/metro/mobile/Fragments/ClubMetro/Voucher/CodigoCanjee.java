package cl.metro.mobile.Fragments.ClubMetro.Voucher;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.Button;

import cl.metro.mobile.R;

public class CodigoCanjee extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_codigo_canjee);

        Button codigoCanjee = (Button) findViewById(R.id.button9);
        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            String value = extras.getString("codigoCanjee");
            codigoCanjee.setText(value);
        }


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("¿ Ésta seguro que desea Salir ?")
                .setCancelable(false)
                .setPositiveButton("Salir",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                onBackPressed();

                            }
                        });
        alertDialogBuilder.setNegativeButton("Cancelar",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();

    }

}
