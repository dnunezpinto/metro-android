package cl.metro.mobile.Fragments.ClubMetro.Voucher;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.zxing.integration.android.IntentIntegrator;

import org.json.JSONException;
import org.json.JSONObject;

import cl.metro.mobile.Fragments.ClubMetro.EjemploBip;
import cl.metro.mobile.R;
import cl.metro.mobile.Retrofit.ApiService;
import cl.metro.mobile.Utilities.Utils;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class AgregarBip extends AppCompatActivity implements View.OnClickListener {

    private EditText eTxt_NumTarjetaBip, etxtNumChip;
    private Toolbar mToolbar;
    private MaterialDialog dialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agregar_bip);


        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setTitle("Agregar Bip");

        eTxt_NumTarjetaBip = (EditText) findViewById(R.id.eTxt_NumTarjetaBip);
        etxtNumChip = (EditText) findViewById(R.id.etxtNumChip);

        Button btnAgregarBip = (Button) findViewById(R.id.btnAgregarBip);

        btnAgregarBip.setOnClickListener(this);

        setSupportActionBar(mToolbar);
        assert getSupportActionBar() != null;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        TextView verEjemplo = (TextView) findViewById(R.id.textView53);
        verEjemplo.setOnClickListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.btnAgregarBip:

                if (validarCampos()) {
                    agregarBip();
                }

                break;
            case R.id.textView53:

                Intent intent = new Intent(AgregarBip.this, EjemploBip.class);
                startActivity(intent);

                break;

        }


    }

    public Boolean validarCampos() {

        boolean isValid = true;
        if (TextUtils.isEmpty(etxtNumChip.getText())) {

            etxtNumChip.setError("Campo Obligatorio");
            isValid = false;
        } else {
            isValid = true;
            etxtNumChip.setError(null);
        }

        if (TextUtils.isEmpty(eTxt_NumTarjetaBip.getText())) {

            eTxt_NumTarjetaBip.setError("Campo Obligatorio");

            isValid = false;
        } else {
            isValid = true;
            eTxt_NumTarjetaBip.setError(null);
        }

        return isValid;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case Utils.REQUEST_CODE_ASK_PERMISSIONS:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Permission Granted
                    Log.d("App Metro", "Permission Granted");
                    IntentIntegrator integrator = new IntentIntegrator(this);
                    integrator.initiateScan();


                } else {

                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    public void agregarBip() {

        dialog = new MaterialDialog.Builder(AgregarBip.this)
                .content("Ingresando la Bip en el sistema, favor espere ...")
                .progress(true, 0)
                .autoDismiss(false)
                .cancelable(false)
                .show();

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(Utils.URL_BASE_METRO)
                .build();

        ApiService apiService = restAdapter.create(ApiService.class);

        apiService.agregarBip(Utils.getFromPrefs(AgregarBip.this,
                Utils.PREFS_LOGIN_ID_USER, ""),
                etxtNumChip.getText().toString(),
                eTxt_NumTarjetaBip.getText().toString(),
                new Callback<Response>() {
                    @Override
                    public void success(Response response, Response response2) {
                        dialog.dismiss();

                        try {

                            JSONObject result = new JSONObject(Utils.convertResponseBody(response));

                            int estado = result.getInt("estado");
                            String mensaje = result.getString("mensaje");
                            if (estado != 0) {
                                Utils.createAlertDialog(AgregarBip.this, mensaje);
                            } else {


                                Utils.createAlertDialog(AgregarBip.this, mensaje);

                            }

//                  {"estado":0,"mensaje":"Voucher ya ingresado anteriormente"}


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }

                    @Override
                    public void failure(RetrofitError error) {

                        dialog.dismiss();
                        Utils.createAlertDialog(AgregarBip.this, "Ocurrio un error inesperado, favor intente nuevamente.");

                    }
                });

    }
}
