package cl.metro.mobile.Fragments;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.journeyapps.barcodescanner.Util;

import java.util.ArrayList;
import java.util.List;

import cl.metro.mobile.Models.Tweets.TweetResult;
import cl.metro.mobile.Models.Tweets.Tweets;
import cl.metro.mobile.R;
import cl.metro.mobile.Retrofit.ApiService;
import cl.metro.mobile.Retrofit.RestClient;
import cl.metro.mobile.Utilities.AdapterListTwitter;
import cl.metro.mobile.Utilities.Utils;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Daniel on 05-02-2015.
 */

public class DefaulFragment extends Fragment {

    private static final String ARG_SECTION_NUMBER = "section_number";
    private static final String ARG_SECTION_STRING = "section_string";
    private static boolean isFinish;
    List<TweetResult> results = new ArrayList<TweetResult>();
    private ListView mListView;
    private String urlTweet;
    private ArrayList asdf = new ArrayList();
    private ProgressBar mProgressBar;
    private Activity mActivity;
    private LinearLayout llRetry;
    private Button btnRetry;

    public DefaulFragment() {

    }

    public DefaulFragment newInstance(int sectionNumber, String url) {

        DefaulFragment fragment = new DefaulFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        args.putString(ARG_SECTION_STRING, url);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        urlTweet = getArguments().getString(ARG_SECTION_STRING);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_defaul, container, false);

        //Cambia de color rojo el actionbar/toolbar
//        Utils.toolbarRed(getActivity());

        llRetry = (LinearLayout) rootView.findViewById(R.id.llRetry);
        btnRetry = (Button) rootView.findViewById(R.id.button7);

        mProgressBar = (ProgressBar) rootView.findViewById(R.id.progressBar);
        mListView = (ListView) rootView.findViewById(R.id.listView);

        mListView.setVisibility(View.GONE);
        mProgressBar.setVisibility(View.VISIBLE);

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {


                Intent intent = null;
                try {
                    // Si el usuario tiene la app de twitter instalada, podrá elegir entre el navegador o la app,
                    // de lo contrario solo podrá abrirla con el navegador que tenga instalado

//                    PackageInfo info = getActivity().getPackageManager().getPackageInfo("com.twitter.android", 0);
//                    if(info.applicationInfo.enabled)
//                        intent = new Intent(Intent.ACTION_VIEW, Uri.parse("twitter://user?user_id=73459349"));
//                    else
//                        intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://twitter.com/metrodesantiago"));
                    intent = new Intent(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse("http://twitter.com/#!/metrodesantiago"));


                    getActivity().startActivity(intent);
//
//                    getActivity().getPackageManager().getPackageInfo("com.twitter.android", 0);
//                    intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://twitter.com/metrodesantiago"));
//                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                } catch (Exception e) {
                    e.printStackTrace();
                }

//                getActivity().startActivity(intent);


            }
        });


        btnRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mProgressBar.setVisibility(View.VISIBLE);
                llRetry.setVisibility(View.GONE);
                getService();

            }
        });

        getService();

        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();


    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        this.mActivity = activity;
    }

    //    @Override
//    public void setUserVisibleHint(boolean isVisibleToUser) {
//        super.setUserVisibleHint(isVisibleToUser);
//
//
//        if (isVisibleToUser) {
//
////            Log.d("LOG", "this fragment is now visible");
//        } else {
//            mListView.setVisibility(View.GONE);
////            Log.d("LOG", "this fragment is now invisible");
//        }
//    }


    public void getService() {

        if (urlTweet != null) {
            //Utils.URL_BASE_INTERMOVIL = "http:\\/\\/metro.rvalladares.com";

            RestAdapter restAdapter = new RestAdapter.Builder()
                    .setLogLevel(RestAdapter.LogLevel.FULL)
                    .setEndpoint("http://metro.rvalladares.com")
                    .build();

            ApiService apiService = restAdapter.create(ApiService.class);
            apiService.getTweets(urlTweet, new Callback<Tweets>() {
//            RestClient.get().getTweets(urlTweet, new Callback<Tweets>() {
                @Override
                public void success(Tweets tweets, Response response) {

                    //Log.i("App", tweets.getResults().get(1).getText());
                    results = tweets.getResults();
                    mListView.setAdapter(new AdapterListTwitter(mActivity,
                            R.layout.list_tweet,
                            R.id.title, results));

                    new CountDownTimer(3000, 1000) {

                        public void onTick(long millisUntilFinished) {
                        }

                        public void onFinish() {
                            mProgressBar.setVisibility(View.GONE);
                            mListView.setVisibility(View.VISIBLE);
                            llRetry.setVisibility(View.GONE);
                        }
                    }.start();


                }

                @Override
                public void failure(RetrofitError error) {

//                getService();

                    llRetry.setVisibility(View.VISIBLE);
                    mProgressBar.setVisibility(View.GONE);

                }


            });
        } else {

            llRetry.setVisibility(View.VISIBLE);
            mProgressBar.setVisibility(View.GONE);

        }


//                MainActivity.client.get(getActivity(), urlTweet, null, new JsonHttpResponseHandler() {
//
//
//                    @Override
//                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
//                        super.onSuccess(statusCode, headers, response);
//                        try {
//
//
//                            asdf.clear();
//
//                            JSONArray jResults = response.getJSONArray("results");
//
//                            String[] text = null;
//                            for (int i = 0; i < jResults.length(); i++) {
//
//                                JSONObject item = jResults.getJSONObject(i);
//                                asdf.add(item.getString("text"));
//
//                            }
//
//                            mListView.setAdapter(new AdapterListTwitter(getActivity(),
//                                    R.layout.list_tweet,
//                                    R.id.title, asdf));
//
//
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//
//
//                    }
//
//                    @Override
//                    public void onFinish() {
//                        super.onFinish();
//
//
//                        mProgressBar.setVisibility(View.GONE);
//                        mListView.setVisibility(View.VISIBLE);
//
//                    }
//                });

    }
}
