package cl.metro.mobile.Fragments.ClubMetro.Evento;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cl.metro.mobile.MainActivity;
import cl.metro.mobile.R;
import cl.metro.mobile.Retrofit.ApiService;
import cl.metro.mobile.Utilities.Utils;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class DetalleEventoFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    String codeGroup;
    // TODO: Rename and change types of parameters
    private int mId;
    private String mTipo;
    private TextView mTituloView, mSubitutloView, mDetalleView, mStockEntradasView;
    private ProgressBar progressBar, loadImage;
    private ScrollView mScrollView;
    private ImageView imgEvento;
    private TableLayout tableLayout;
    private RelativeLayout contentImg;
    private View viewGray;

//    private OnFragmentInteractionListener mListener;

    public DetalleEventoFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment DetalleEventoFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static DetalleEventoFragment newInstance(int param1, String param2) {
        DetalleEventoFragment fragment = new DetalleEventoFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mId = getArguments().getInt(ARG_PARAM1);
            mTipo = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_detalle_evento, container, false);

        mTituloView = (TextView) rootView.findViewById(R.id.txtTituloEvento);
        mSubitutloView = (TextView) rootView.findViewById(R.id.txtDescEvento);
        mDetalleView = (TextView) rootView.findViewById(R.id.txtDetalleEvento);
        mScrollView = (ScrollView) rootView.findViewById(R.id.scrollView4);
        mStockEntradasView = (TextView) rootView.findViewById(R.id.textView66);
        progressBar = (ProgressBar) rootView.findViewById(R.id.progressBar8);
        loadImage = (ProgressBar) rootView.findViewById(R.id.progressBar13);
        contentImg = (RelativeLayout) rootView.findViewById(R.id.contentImg);
        imgEvento = (ImageView) rootView.findViewById(R.id.imageView8);
        tableLayout = (TableLayout) rootView.findViewById(R.id.tabla_entradas);
        viewGray = (View) rootView.findViewById(R.id.viewGray);


        getDetalle();

        // Inflate the layout for this fragment
        return rootView;
    }

    // TODO: Rename method, update argument and hook method into UI event
//    public void onButtonPressed(Uri uri) {
//        if (mListener != null) {
//            mListener.onFragmentInteraction(uri);
//        }
//    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
//        try {
//            mListener = (OnFragmentInteractionListener) activity;
//        } catch (ClassCastException e) {
//            throw new ClassCastException(activity.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
//        mListener = null;
    }


    public void getDetalle() {

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(Utils.URL_BASE_METRO)
                .build();

        ApiService apiService = restAdapter.create(ApiService.class);

        apiService.getDetalleLista(mId, mTipo, new Callback<Response>() {
            @Override
            public void success(Response result, Response response) {


                try {
                    JSONObject jsonObject = new JSONObject(Utils.convertResponseBody(response));

                    String titulo = jsonObject.getString("titulo");

//                    ((ActionBarActivity) getActivity()).getSupportActionBar().setTitle(titulo);

                    String subtitulo = jsonObject.getString("subtitulo");
                    String detalle = jsonObject.getString("detalles");
                    int stockTotal = jsonObject.getInt("stock");
                    if (jsonObject.has("imagen")) {

                        String img = jsonObject.getString("imagen");

                        MainActivity.imageLoader.displayImage(img, imgEvento, MainActivity.options, new SimpleImageLoadingListener() {
                            @Override
                            public void onLoadingStarted(String imageUri, View view) {
                                loadImage.setVisibility(View.VISIBLE);
                            }

                            @Override
                            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                                @SuppressWarnings("unused")
                                String message = null;
                                switch (failReason.getType()) {
                                    case IO_ERROR:
                                        message = "Input/Output error";
                                        break;
                                    case DECODING_ERROR:
                                        message = "Image can't be decoded";
                                        break;
                                    case NETWORK_DENIED:
                                        message = "Downloads are denied";
                                        break;
                                    case OUT_OF_MEMORY:
                                        message = "Out Of Memory error";
                                        break;
                                    case UNKNOWN:
                                        message = "Unknown error";
                                        break;
                                }
                                imgEvento.setScaleType(ImageView.ScaleType.CENTER);
                                imgEvento.setImageResource(R.drawable.ic_error_red_48dp);
                                loadImage.setVisibility(View.GONE);
                            }

                            @Override
                            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                                loadImage.setVisibility(View.GONE);

                            }
                        });
                    }

                    mTituloView.setText(Html.fromHtml(titulo));
                    mSubitutloView.setText(Html.fromHtml(subtitulo));
                    mDetalleView.setText(Html.fromHtml(detalle));
                    mStockEntradasView.setText("" + stockTotal);

                    JSONArray arrayEntradas = jsonObject.getJSONArray("stock_estaciones");
//                    Log.i("App", "Array Entradas: " + arrayEntradas.length());
//                    Log.i("App", "Stock Entradas: " + arrayEntradas.length());

                    TableRow.LayoutParams paramsExample = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT, 1.0f);
                    paramsExample.setMargins(1, 1, 1, 1);

                    if (stockTotal == 0) {
                        viewGray.setVisibility(View.VISIBLE);
                    }


                    for (int e = 0; e < arrayEntradas.length(); e++) {

                        JSONObject item = arrayEntradas.getJSONObject(e);


                        TableRow tbrow = new TableRow(getActivity());

                        TextView estacionView = new TextView(getActivity());
                        TextView stockView = new TextView(getActivity());


                        String mEstacion = item.getString("estacion");
                        String mStock = item.getString("stock");

                        estacionView.setText(mEstacion);
                        estacionView.setBackgroundColor(Color.parseColor("#00aca7"));
                        estacionView.setLayoutParams(paramsExample);
                        estacionView.setTextColor(Color.WHITE);
                        estacionView.setPadding(6, 8, 4, 8);
                        tbrow.addView(estacionView);

                        stockView.setText(mStock);
                        stockView.setBackgroundColor(Color.parseColor("#004346"));
                        stockView.setLayoutParams(paramsExample);
                        stockView.setGravity(Gravity.CENTER);
                        stockView.setTextColor(Color.WHITE);
                        stockView.setPadding(6, 8, 4, 8);
                        tbrow.addView(stockView);


                        tableLayout.addView(tbrow);

                    }

//                    detalle = detalle.substring(0, detalle.lastIndexOf("<"));
//                    detalle = detalle.substring(detalle.lastIndexOf(">") + 1, detalle.length());
//                    String [] contents = detalle.split(":");
//                    for (String txt : contents){
//                        System.out.println(txt);
//                    }

                    // the pattern we want to search for
                    Pattern p = Pattern.compile("<p>(.+?)</p>");
                    Matcher m = p.matcher(detalle);

                    // if we find a match, get the group
//                    while (m.find()) {
//                        codeGroup = m.group(1);
////                        System.out.format("'%s'\n", codeGroup);
//                    }

//                    String[] contents = codeGroup.split(":");
//                    for (String txt : contents) {
//                        System.out.println(txt);
//                    }

                    mScrollView.setVisibility(View.VISIBLE);
                    progressBar.setVisibility(View.GONE);


                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void failure(RetrofitError error) {

                Toast.makeText(getActivity(), "Error al comunicarse con el Servidor, intente nuevamente", Toast.LENGTH_LONG).show();

            }
        });
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
//    public interface OnFragmentInteractionListener {
//        // TODO: Update argument type and name
//        public void onFragmentInteraction(Uri uri);
//    }

}
