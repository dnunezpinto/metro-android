package cl.metro.mobile.Fragments.ClubMetro;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cl.metro.mobile.R;
import cl.metro.mobile.Retrofit.ApiService;
import cl.metro.mobile.Utilities.Rut;
import cl.metro.mobile.Utilities.Utils;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class InscribeteFragment extends Fragment {

    private ViewGroup grupo;
    private ArrayList<View> campos = new ArrayList<>();
    private RelativeLayout contentFormulario;
    private String _Nombre, _Paterno, _Materno, _Rut, _Celular, _Email, _Sexo, _Passw;
//    _NumChip, _NumTarjeta,
    private MaterialDialog progressDialog;
    private Spinner mSpinnerSexo;

    public InscribeteFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ClubMetroFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static InscribeteFragment newInstance(String param1, String param2) {
        return new InscribeteFragment();
    }

    public static boolean isValidPhoneNumber(CharSequence target) {
        return !(target == null || TextUtils.isEmpty(target)) && android.util.Patterns.PHONE.matcher(target).matches();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_inscribete, container, false);

        mSpinnerSexo = (Spinner) rootView.findViewById(R.id.spinner4);

        //Color ToolBar #33b5e5
//        int color = Color.parseColor("#33b5e5");
//        Utils.changeColor(color, getActivity());

//
        final CheckBox cbCondicionesUso = (CheckBox) rootView.findViewById(R.id.checkBox);
        cbCondicionesUso.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    cbCondicionesUso.setError(null);
                } else {
                    cbCondicionesUso.setError("¡Debe Aceptar las condiciones de uso!");
                }
            }
        });

        TextView login = (TextView) rootView.findViewById(R.id.textView53);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getActivity(), LoginActivity.class);
                startActivity(intent);

            }
        });

        contentFormulario = (RelativeLayout) rootView.findViewById(R.id.contentFormulario);
        Button contentBasesLegales = (Button) rootView.findViewById(R.id.btnCondiciones);
        contentBasesLegales.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Utils.createAlertDialog(getActivity(), getActivity().getResources().getString(R.string.condiciones_uso));

            }
        });

        Button btnInscribirse = (Button) rootView.findViewById(R.id.btnInscribirse);
        btnInscribirse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                grupo = contentFormulario;


//                if (!cbCondicionesUso.isChecked()) {
//
//                }

                if (validar() && cbCondicionesUso.isChecked()) {
                    cbCondicionesUso.setError(null);
                    registrarse();
//                    Log.e("LOG", "Datos no validados");
//                    return;
                } else if (!cbCondicionesUso.isChecked()) {
                    cbCondicionesUso.setError("Debe Aceptar las condiciones de uso.");
                }


            }
        });


        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Registrarse");

        if (Utils.isUserConnected(getActivity())) {
            getFragmentManager().popBackStack();
        }

    }

    public void registrarse() {

        progressDialog = new MaterialDialog.Builder(getActivity())
                .title("Inscripción Club Metro")
                .content("Estamos registrando tus datos en el servidor, favor espera.")
                .progress(true, 0)
                .cancelable(false)
                .show();


        RestAdapter restAdapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(Utils.URL_BASE_METRO)
                .build();

        ApiService apiService = restAdapter.create(ApiService.class);
        String selectedItem = mSpinnerSexo.getSelectedItem().toString();

        if (selectedItem.equals("Masculino")) {
            _Sexo = "1";
        } else if (selectedItem.equals("Femenino")) {
            _Sexo = "2";
        }

        apiService.clubMetroRegistro(_Nombre, _Paterno, _Materno, _Sexo, _Celular, _Passw, _Email, _Rut, new Callback<Response>() {
            @Override
            public void success(Response response, Response response2) {

                try {
                    JSONObject result = new JSONObject(Utils.convertResponseBody(response));

                    int estado = result.getInt("estado");

                    if (estado != 1) {

                        progressDialog.dismiss();
                        Utils.createAlertDialog(getActivity(), result.getString("mensaje"));

                    } else {
                        progressDialog.dismiss();

                        Utils.createAlertDialog(getActivity(), "Cuenta Registrada, recuerda que debes poseer una Tarjeta bip asociada para acceder a los beneficios que Club Metro trae para ti.");

                        Intent intent = new Intent(getActivity(), LoginActivity.class);
                        startActivity(intent);

//                        http://qamt.agenciacatedral.cl/includes/wsMobile/clubMetroRegistro.php?nombre=Daniel&paterno=Nu%C3%B1ez&materno=Pinto&sexo=Masculino&cel=%2B56998168263&pass=coldown4150&email=dnunezpinto%40gmail.com&rut=17427522-k (3855ms)
//                        {"estado":1,"mensaje":"El usuario ha sido creado satisfactoriamente.","idUsuario":409347}

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void failure(RetrofitError error) {
                Utils.createAlertDialog(getActivity(), "Ocurrio un error inesperado, intenta nuevamente.");
            }
        });

//        void clubMetroRegistro(@Query("nombre") String _Nombre,
//                @Query("paterno") String _Paterno,
//                @Query("materno") String _Materno,
//                @Query("sexo") String _Sexo,
//                @Query("cel") String _Cel,
//                @Query("pass") String _Pass,
//                @Query("email") String _Email,
//                @Query("rut") String _Rut,
//                Callback<Response> callback);


    }

    public boolean validar() {

        boolean resultado = true;

        campos.clear();

        this.recorrer(grupo);

        for (int i = 0; i < campos.size(); i++) {

            View campo = campos.get(i);

            if (campo instanceof EditText) {

                EditText texto = (EditText) campo;

//                if (texto.length() == 0) {
//
//                    texto.setError("Campo Obligatorio");
//                    resultado = false;
//
//                } else {
//                    texto.setError(null);
////						texto.setTextColor( Color.DKGRAY );
//                }

                switch (texto.getId()) {
                    case R.id.eText_Nombre:

                        if (TextUtils.isEmpty(texto.getText())) {
                            texto.setError("Campo Obligatorio");
                            resultado = false;
                        } else {
                            texto.setError(null);
                            _Nombre = texto.getText().toString();
                        }

                        break;
                    case R.id.etxt_ApellidoPaterno:

                        if (TextUtils.isEmpty(texto.getText())) {
                            texto.setError("Campo Obligatorio");
                            resultado = false;
                        } else {
                            texto.setError(null);
                            _Paterno = texto.getText().toString();
                        }

                        break;
                    case R.id.eTxt_Materno:

                        if (TextUtils.isEmpty(texto.getText())) {
                            texto.setError("Campo Obligatorio");
                            resultado = false;
                        } else {
                            texto.setError(null);
                            _Materno = texto.getText().toString();
                        }

                        break;
                    case R.id.etxt_Rut:

                        if (TextUtils.isEmpty(texto.getText())) {
                            texto.setError("Campo Obligatorio");
                            resultado = false;
                        } else if (!Rut.validar(texto.getText().toString())) {
                            texto.setError("Rut inválido");
                            resultado = false;
                        } else {
                            texto.setError(null);
                            _Rut = texto.getText().toString();
                        }

                        break;
                    case R.id.etxt_Celular:

                        if (!isValidPhoneNumber(texto.getText().toString())) {
                            texto.setError("Celular inválido");
                            resultado = false;
                        } else {
                            texto.setError(null);
                            _Celular = texto.getText().toString();
                        }

                        break;
                    case R.id.etxt_Email:

                        if (!isEmailValid(texto.getText().toString())) {
                            texto.setError(getString(R.string.error_invalid_email));
                            resultado = false;
                        } else {
                            texto.setError(null);
                            _Email = texto.getText().toString();
                        }
                        break;
                    case R.id.eTxt_Passw:
                        if (TextUtils.isEmpty(texto.getText()) && !isPasswordValid(texto.getText().toString())) {
                            texto.setError(getString(R.string.error_invalid_password));
                            resultado = false;
                        } else {
                            texto.setError(null);
                            _Passw = texto.getText().toString();
                        }

                        break;
//                    case R.id.etxt_NumChip:
//
//                        if (!TextUtils.isEmpty(texto.getText().toString()) || texto.getText().length() < 8) {
//                            texto.setError("Número Chip NO válido");
//                            resultado = false;
//                        } else {
//                            texto.setError(null);
//                            _NumChip = texto.getText().toString();
//                        }
//
//                        break;
//                    case R.id.etxt_NumTarjeta:
//
//                        if (!TextUtils.isEmpty(texto.getText().toString()) || texto.getText().length() < 18) {
//                            texto.setError("Número Tarjeta NO válido");
//                            resultado = false;
//                        } else {
//                            texto.setError(null);
//                            _NumTarjeta = texto.getText().toString();
//                        }
//
//                        break;
                }

            }

            if (campo instanceof Spinner) {
                Spinner spinner = (Spinner) campo;

                String selectedItem = spinner.getSelectedItem().toString();

                if (selectedItem.equals("Masculino")) {
                    _Sexo = "1";
                } else if (selectedItem.equals("Femenino")) {
                    _Sexo = "2";
                }

            }


        }

        return resultado;
    }

    private void recorrer(ViewGroup vista) {

        for (int i = 0; i < vista.getChildCount(); i++) {

            if ((vista.getChildAt(i) instanceof LinearLayout || vista.getChildAt(i) instanceof RelativeLayout) && !(vista.getChildAt(i) instanceof RadioGroup)) {

                recorrer((ViewGroup) vista.getChildAt(i));

            } else {

                if (vista.getChildAt(i) instanceof EditText) {

                    campos.add(vista.getChildAt(i));
                }
            }
        }
    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 0;
    }

}
