package cl.metro.mobile.Fragments.FragmentServicios;


import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;

import cl.metro.mobile.MainActivity;
import cl.metro.mobile.R;
import cl.metro.mobile.Utilities.AdapterServiciosCercanos.RubrosSectionAdapter;


/**
 * A simple {@link Fragment} subclass.
 */
public class ServicioComercioRubrosFragment extends Fragment {

    public static String nombreCategoria;
    public static int idLinea;
    public static String nombreEstacion;
    private ListView listView;
    private Activity mActivity;
    private ArrayList<String> listRubros;
    private ProgressBar progressBar;


    public ServicioComercioRubrosFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        nombreEstacion = getArguments().getString("nombreEstacion");
        nombreCategoria = getArguments().getString("nombreCategoria");
        idLinea = getArguments().getInt("idLinea");

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_servicio_comercio_rubros, container, false);

//        sbMd = (SwitchButton) rootView.findViewById(R.id.sb_md);


        TextView tvNombreEstacion = (TextView) rootView.findViewById(R.id.nombreEstacion);
        tvNombreEstacion.setText(nombreEstacion + " / Categoria - " + nombreCategoria);
        MainActivity.initApp.setEstacionImage(idLinea, tvNombreEstacion, getActivity());
//        tvNombreEstacion.setTextColor(Color.parseColor(Utils.COLORS[idLinea - 1]));


        listView = (ListView) rootView.findViewById(R.id.listView3);
        progressBar = (ProgressBar) rootView.findViewById(R.id.progressBar7);

        Button volverMapa = (Button) rootView.findViewById(R.id.btnVolver);
        volverMapa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Fragment fragment = new ServicioMapaFragment();
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                Bundle args = new Bundle();

                args.putString("codigoEstacion", ServicioComercioCategoriasFragment.codigoEstacion);
                args.putString("nombreEstacion", ServicioComercioCategoriasFragment.nombreEstacion);
                args.putInt("idLinea", ServicioComercioCategoriasFragment.idLinea);
                fragment.setArguments(args);

                fragmentManager.beginTransaction()
                        .replace(R.id.container, fragment)
                        .commit();

//                getChildFragmentManager().popBackStack();
                getActivity().overridePendingTransition(R.anim.slide_in_top, R.anim.slide_out_top);
            }
        });

        getData();

        // Inflate the layout for this fragment
        return rootView;
    }



    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        mActivity = activity;
    }

    public void getData() {

        if (MainActivity.initApp.listRubros != null) {

            this.listRubros = new ArrayList<String>();

            for (int i = 0; i < MainActivity.initApp.listRubros.size(); i++) {


                String nombreRubro = MainActivity.initApp.listRubros.get(i);
                Log.i("App", nombreRubro);
                this.listRubros.add(nombreRubro);

            }

//            ArrayAdapter<String> adapter = new ArrayAdapter<String>(
//                    mActivity, (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) ?
//                    R.layout.custom_list_item :
//                    R.layout.custom_list_item_low, this.listRubros);
//
//            listView.setAdapter(adapter);
//            Utils.setListViewHeightBasedOnChildren(listView);

            RubrosSectionAdapter section = new RubrosSectionAdapter(this.listRubros, mActivity);
            listView.setAdapter(section);

            listView.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);

        }

    }


}
