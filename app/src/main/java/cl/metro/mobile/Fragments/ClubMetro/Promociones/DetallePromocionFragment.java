package cl.metro.mobile.Fragments.ClubMetro.Promociones;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import cl.metro.mobile.Fragments.ClubMetro.Voucher.CodigoCanjee;
import cl.metro.mobile.MainActivity;
import cl.metro.mobile.R;
import cl.metro.mobile.Retrofit.ApiService;
import cl.metro.mobile.Utilities.Utils;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class DetallePromocionFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_PARAM3 = "param3";
    String codeGroup;
    // TODO: Rename and change types of parameters
    private int mId;
    private String mTipo, mCanjeable;
    private TextView mTituloView, mSubitutloView, mLinkView;
    private ProgressBar progressBar, loadImage;
    private ScrollView mScrollView;
    private ImageView imgEvento;
    private TableLayout tableLayout;
    private RelativeLayout contentImg;
    private MaterialDialog dialog;

//    private OnFragmentInteractionListener mListener;

    public DetallePromocionFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment DetalleEventoFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static DetallePromocionFragment newInstance(int param1, String param2, String isCanjeable) {
        DetallePromocionFragment fragment = new DetallePromocionFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        args.putString(ARG_PARAM3, isCanjeable);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mId = getArguments().getInt(ARG_PARAM1);
            mTipo = getArguments().getString(ARG_PARAM2);
            mCanjeable = getArguments().getString(ARG_PARAM3);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_detalle_promocion, container, false);

        mTituloView = (TextView) rootView.findViewById(R.id.txtTituloEvento);
        mSubitutloView = (TextView) rootView.findViewById(R.id.txtDescEvento);
        mLinkView = (TextView) rootView.findViewById(R.id.linkBasesLegales);
        mScrollView = (ScrollView) rootView.findViewById(R.id.scrollView4);
        progressBar = (ProgressBar) rootView.findViewById(R.id.progressBar8);
        loadImage = (ProgressBar) rootView.findViewById(R.id.progressBar13);
        contentImg = (RelativeLayout) rootView.findViewById(R.id.contentImg);

        imgEvento = (ImageView) rootView.findViewById(R.id.imageView8);
        tableLayout = (TableLayout) rootView.findViewById(R.id.tabla_entradas);


        Button btnCanjearCupon = (Button) rootView.findViewById(R.id.btnCanjearCupon);
        btnCanjearCupon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {

                    ActivityCompat.requestPermissions(getActivity(),
                            new String[]{Manifest.permission.CAMERA},
                            Utils.REQUEST_CODE_ASK_PERMISSIONS);
                } else {
                    callScanIntent();

                }


            }
        });

//        if (mCanjeable.equals("0")) {
//
//            btnCanjearCupon.setVisibility(View.GONE);
//
//        }

        getDetalle();

        // Inflate the layout for this fragment
        return rootView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (requestCode) {
            case IntentIntegrator.REQUEST_CODE:
                if (resultCode == Activity.RESULT_OK) {
                    // Parsing bar code reader result
                    final IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
                    if (result != null) {
                        if (result.getContents() == null) {
//                            Log.d("MainActivity", "Cancelled scan");

                        } else {
    //                            Log.d("MainActivity", "Scanned");

                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                            alertDialogBuilder.setMessage("¿Ésta seguro que desea canjear la Promoción?")
                                    .setCancelable(false)
                                    .setPositiveButton("Si",
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {

                                                    canjearCupon(result.getContents());

                                                }
                                            });
                            alertDialogBuilder.setNegativeButton("No",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    });
                            AlertDialog alert = alertDialogBuilder.create();
                            alert.show();


                        }
                    }

                }
                break;
        }


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case Utils.REQUEST_CODE_ASK_PERMISSIONS:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Permission Granted

                    callScanIntent();


                } else {

                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    public void canjearCupon(String mCodigo) {

        dialog = new MaterialDialog.Builder(getActivity())
                .content("Canjeando Promoción, favor espere ...")
                .progress(true, 0)
                .autoDismiss(false)
                .cancelable(false)
                .show();

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(Utils.URL_BASE_METRO)
                .build();

        ApiService apiService = restAdapter.create(ApiService.class);

        apiService.canjearCupon(Utils.getFromPrefs(getActivity(), Utils.PREFS_LOGIN_ID_USER, ""),
                mId,
                mCodigo,
                new Callback<Response>() {
                    @Override
                    public void success(Response response, Response response2) {

                        dialog.dismiss();

                        try {
                            JSONObject result = new JSONObject(Utils.convertResponseBody(response));

                            int estado = result.getInt("estado");
                            if (estado != 0) {

                                String codigo = result.getString("codigo");

                                Intent intent = new Intent(getActivity(), CodigoCanjee.class);
                                intent.putExtra("codigoCanjee", codigo);
                                startActivity(intent);


                            } else {

                                String mensaje = result.getString("mensaje");
                                Utils.createAlertDialog(getActivity(), mensaje);

                            }

//                  {"estado":0,"mensaje":"Voucher ya ingresado anteriormente"}


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }

                    @Override
                    public void failure(RetrofitError error) {

                        dialog.dismiss();
                        Utils.createAlertDialog(getActivity(), "Ocurrio un error inesperado, favor intente nuevamente.");
                    }
                });

    }

    public void callScanIntent() {

        IntentIntegrator.forSupportFragment(this).setPrompt("Escanee el Código QR").initiateScan();

    }

    private void shareOnFacebook() {
        try {
            Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            shareIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Promocion");
            shareIntent.putExtra(android.content.Intent.EXTRA_TEXT, R.drawable.ic_launcher);
            shareIntent.putExtra(Intent.EXTRA_STREAM, "www.metro.cl");

            PackageManager pm = getContext().getPackageManager();
            List<ResolveInfo> activityList = pm.queryIntentActivities(shareIntent, 0);
            for (final ResolveInfo app : activityList) {
                if ((app.activityInfo.name).contains("facebook")) {
                    final ActivityInfo activity = app.activityInfo;
                    final ComponentName name = new ComponentName(activity.applicationInfo.packageName, activity.name);
                    shareIntent.addCategory(Intent.CATEGORY_LAUNCHER);
                    shareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
                    shareIntent.setComponent(name);
                    getContext().startActivity(shareIntent);
                    break;
                }
            }
            return;
        } catch (Exception e) {
            // User doesn't have Facebook app installed. Try sharing through browser.
        }

    }

    // TODO: Rename method, update argument and hook method into UI event
//    public void onButtonPressed(Uri uri) {
//        if (mListener != null) {
//            mListener.onFragmentInteraction(uri);
//        }
//    }


    @Override
    public void onDetach() {
        super.onDetach();
//        mListener = null;
    }

    public void getDetalle() {

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(Utils.URL_BASE_METRO)
                .build();

        ApiService apiService = restAdapter.create(ApiService.class);

        apiService.getDetalleLista(mId, mTipo, new Callback<Response>() {
            @Override
            public void success(Response result, Response response) {


                try {
                    JSONObject jsonObject = new JSONObject(Utils.convertResponseBody(response));

                    String titulo = jsonObject.getString("titulo");

                    String subtitulo = jsonObject.getString("subtitulo");
                    final String bases = jsonObject.getString("bases");
                    final String img = jsonObject.getString("imagen");

                    MainActivity.imageLoader.displayImage(img, imgEvento, MainActivity.options, new SimpleImageLoadingListener() {
                        @Override
                        public void onLoadingStarted(String imageUri, View view) {
                            loadImage.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                            @SuppressWarnings("unused")
                            String message = null;
                            switch (failReason.getType()) {
                                case IO_ERROR:
                                    message = "Input/Output error";
                                    break;
                                case DECODING_ERROR:
                                    message = "Image can't be decoded";
                                    break;
                                case NETWORK_DENIED:
                                    message = "Downloads are denied";
                                    break;
                                case OUT_OF_MEMORY:
                                    message = "Out Of Memory error";
                                    break;
                                case UNKNOWN:
                                    message = "Unknown error";
                                    break;
                            }
                            imgEvento.setScaleType(ImageView.ScaleType.CENTER);
                            imgEvento.setImageResource(R.drawable.ic_error_red_48dp);
//                            contentImg.setVisibility(View.GONE);
                            loadImage.setVisibility(View.GONE);
                        }

                        @Override
                        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                            loadImage.setVisibility(View.GONE);
                        }
                    });

                    mTituloView.setText(Html.fromHtml(titulo));
                    mSubitutloView.setText(Html.fromHtml(subtitulo));
                    mLinkView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            String googleDocsUrl = "http://docs.google.com/viewer?url=";

                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            intent.setDataAndType(Uri.parse(googleDocsUrl + bases), "text/html");
                            startActivity(intent);
                        }
                    });


                    mScrollView.setVisibility(View.VISIBLE);
                    progressBar.setVisibility(View.GONE);


                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void failure(RetrofitError error) {

                Toast.makeText(getActivity(), "Error al comunicarse con el Servidor, intente nuevamente", Toast.LENGTH_LONG).show();

            }
        });
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
//    public interface OnFragmentInteractionListener {
//        // TODO: Update argument type and name
//        public void onFragmentInteraction(Uri uri);
//    }

}
