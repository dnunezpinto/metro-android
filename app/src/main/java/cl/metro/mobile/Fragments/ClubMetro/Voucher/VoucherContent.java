package cl.metro.mobile.Fragments.ClubMetro.Voucher;

public class VoucherContent {

    private int idVoucher;
    private String voucher;
    private String fechaRegistro;
    private int idPromocion;
    private String fechaCobro;

    public VoucherContent(int idVoucher, String voucher, String fechaRegistro, int idPromocion, String fechaCobro) {
        this.idVoucher = idVoucher;
        this.voucher = voucher;
        this.fechaRegistro = fechaRegistro;
        this.idPromocion = idPromocion;
        this.fechaCobro = fechaCobro;
    }

    public int getIdVoucher() {
        return idVoucher;
    }

    public void setIdVoucher(int idVoucher) {
        this.idVoucher = idVoucher;
    }

    public String getVoucher() {
        return voucher;
    }

    public void setVoucher(String voucher) {
        this.voucher = voucher;
    }

    public String getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(String fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public int getIdPromocion() {
        return idPromocion;
    }

    public void setIdPromocion(int idPromocion) {
        this.idPromocion = idPromocion;
    }

    public String getFechaCobro() {
        return fechaCobro;
    }

    public void setFechaCobro(String fechaCobro) {
        this.fechaCobro = fechaCobro;
    }
}


