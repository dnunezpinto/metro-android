package cl.metro.mobile.Fragments.FragmentServicios;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.TextView;

import java.util.List;

import cl.metro.mobile.MainActivity;
import cl.metro.mobile.Models.LugaresInteresEntretencion.DatosLugaresInteresEntretencion;
import cl.metro.mobile.R;
import cl.metro.mobile.Utilities.AdapterListLugaresInteresEntretencion;

/**
 * A simple {@link Fragment} subclass.
 */
public class LugaresEventosCercanos extends Fragment implements AbsListView.OnItemClickListener {

    private static List<DatosLugaresInteresEntretencion> datosEventosCercanos;
    private AbsListView mListView;
    private AdapterListLugaresInteresEntretencion adapterListEventosCercanos;
    private boolean isVerRubros = false;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (MainActivity.initApp.currentListEventos != null) {
            datosEventosCercanos = MainActivity.initApp.currentListEventos.getEventos();
            adapterListEventosCercanos = new AdapterListLugaresInteresEntretencion(getActivity(),
                    R.layout.list_item,
                    R.id.text_item,
                    datosEventosCercanos);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_lugares_eventos_mes, container, false);

        mListView = (AbsListView) rootView.findViewById(R.id.list);
        mListView.setEmptyView(rootView.findViewById(R.id.empty));
        mListView.setVisibility(View.VISIBLE);
        mListView.setOnItemClickListener(this);
        mListView.setAdapter(adapterListEventosCercanos);

        TextView tvNombreEstacion = (TextView) rootView.findViewById(R.id.nombreEstacion);
        tvNombreEstacion.setText(MainLugaresInteresEntretencion.nombreEstacion);
        MainActivity.initApp.setEstacionImage(MainLugaresInteresEntretencion.idLinea, tvNombreEstacion, getActivity());
//        tvNombreEstacion.setTextColor(Color.parseColor(Utils.COLORS[MainLugaresInteresEntretencion.idLinea - 1]));


        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();

        if (isVerRubros && MainActivity.initApp.currentListEventos != null) {
            Log.i("App", "isVerRubros: " + isVerRubros + ", list con datos");
            setData();
        } else {

            if (MainActivity.initApp.currentListEventos == null && datosEventosCercanos != null) {
                datosEventosCercanos.clear();
                Log.i("App", "SIN DATOS... !");
            }

        }


    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        datosEventosCercanos.get(position);
        isVerRubros = true;

        Fragment fragment = new DetalleLugaresInteresEntretencion().newInstance(datosEventosCercanos.get(position).getDescripcion(),
                datosEventosCercanos.get(position).getDireccion(),
                datosEventosCercanos.get(position).getHorario(),
                datosEventosCercanos.get(position).getTarifa(),
                datosEventosCercanos.get(position).getImagenes());
        getActivity().getSupportFragmentManager().beginTransaction()
                .addToBackStack(null)
                .replace(R.id.container, fragment)
                .commit();

    }

    public void setData() {

        mListView.setAdapter(adapterListEventosCercanos);

    }

}
