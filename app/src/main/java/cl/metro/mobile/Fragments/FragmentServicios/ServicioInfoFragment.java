package cl.metro.mobile.Fragments.FragmentServicios;


import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import cl.metro.mobile.MainActivity;
import cl.metro.mobile.Models.EquipaminetoEstacion.Equipamiento;
import cl.metro.mobile.Models.EquipaminetoEstacion.EquipamientoResponse;
import cl.metro.mobile.Models.Estaciones;
import cl.metro.mobile.Retrofit.RestClient;
import cl.metro.mobile.Utilities.Utils;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class ServicioInfoFragment extends Fragment implements Button.OnClickListener {

    private static String codigoEstacion;
    private static String nombreEstacion;
    private static int idLinea;
    private List<EquipamientoResponse> equipamientoResponseArrayList;
    private ListView listView;
    private ArrayList<String> strEquipamiento;
    private ProgressBar progressBar;
    private Button btnRecargar;
    private Activity mActivity;
    private MenuItem agregarFavorito;

    public ServicioInfoFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);

        codigoEstacion = getArguments().getString("codigoEstacion");
        nombreEstacion = getArguments().getString("nombreEstacion");
        idLinea = getArguments().getInt("idLinea");

//        Log.i("App", "Codigo Estación: " + codigoEstacion);

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        inflater.inflate(cl.metro.mobile.R.menu.global_favoritos, menu);

        agregarFavorito = menu.findItem(cl.metro.mobile.R.id.action_favorite);

        MainActivity.initApp.validarFavorito(agregarFavorito);

        super.onCreateOptionsMenu(menu, inflater);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

//        if (id == R.id.action_settings) {
//            return true;
//        }

        if (id == cl.metro.mobile.R.id.action_favorite) {

            Estaciones estacion = MainActivity.initApp.getEstacionForNombreEstacionAndIdLinea("" + MainActivity.initApp.currentSelectedEstacion.getIdLinea(),
                    MainActivity.initApp.currentSelectedEstacion.getNombreEstacion());

            if (estacion.isFavorito()) {

//                Log.i("App", "Servicio isFavorito " + estacion.isFavorito() + ", true " );
                Toast.makeText(getActivity(), "Estación " + estacion.getNombreEstacion().toString() + " borrada de Favoritos", Toast.LENGTH_LONG).show();

                MainActivity.initApp.setFavorito("" + estacion.getIdLinea(), estacion.getNombreEstacion(), false, agregarFavorito);
            } else {

//                Log.i("App", "Servicio isFavorito " + estacion.isFavorito() + ", false " );

                Toast.makeText(getActivity(), "Estación " + estacion.getNombreEstacion().toString() + " agregada a Favoritos", Toast.LENGTH_LONG).show();
                MainActivity.initApp.setFavorito("" + estacion.getIdLinea(), estacion.getNombreEstacion(), true, agregarFavorito);
            }

        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(cl.metro.mobile.R.layout.fragment_servicio_info, container, false);

        listView = (ListView) rootView.findViewById(cl.metro.mobile.R.id.listView2);
        listView.setClickable(false);
        listView.setFocusable(false);
        progressBar = (ProgressBar) rootView.findViewById(cl.metro.mobile.R.id.progressBar5);
        btnRecargar = (Button) rootView.findViewById(cl.metro.mobile.R.id.button);

        TextView tvNombreEstacion = (TextView) rootView.findViewById(cl.metro.mobile.R.id.nombreEstacion);
        tvNombreEstacion.setText(String.format("%s / Información", "Los Leones"));
//        tvNombreEstacion.setText(String.format("%s / Información", nombreEstacion));
        MainActivity.initApp.setEstacionImage(idLinea, tvNombreEstacion, mActivity);
//        tvNombreEstacion.setTextColor(Color.parseColor(Utils.COLORS[idLinea - 1]));

        Button btnHorarioServicio = (Button) rootView.findViewById(cl.metro.mobile.R.id.btnHorarioServicio);
        btnHorarioServicio.setOnClickListener(this);
        Button btnTarifarioServicio = (Button) rootView.findViewById(cl.metro.mobile.R.id.btnTarifas);
        btnTarifarioServicio.setOnClickListener(this);

//        String[] mTestArray = getResources().getStringArray(R.array.test_equipamiento);


//        Toast.makeText(getActivity(), "Recuerda que puedes seleccionar cualquier estación y agregarla a tus favoritos.", Toast.LENGTH_LONG).show();

        getdata();


        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        mActivity = activity;
    }

    @Override
    public void onResume() {
        super.onResume();

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Servicios");
        MainActivity.mDrawerList.setItemChecked(4, true);
    }

    public void getdata() {

        RestClient.get().getEquipEstacion(codigoEstacion, new Callback<Equipamiento>() {
            @Override
            public void success(Equipamiento equipamiento, Response response) {

                equipamientoResponseArrayList = equipamiento.getGetEquipamientoEstacion().getEquipamientoResponses();
                strEquipamiento = new ArrayList<String>();
                for (EquipamientoResponse equipamientoResponse : equipamientoResponseArrayList) {

                    if (equipamientoResponse.getValor().equals("SI")) {
                        strEquipamiento.add(equipamientoResponse.getTipo());

                    }

                }

                if (strEquipamiento != null) {


                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                            mActivity, (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) ?
                            cl.metro.mobile.R.layout.custom_list_item :
                            cl.metro.mobile.R.layout.custom_list_item_low, strEquipamiento);

                    listView.setAdapter(adapter);
                    Utils.setListViewHeightBasedOnChildren3(listView);
                    listView.setVisibility(View.VISIBLE);
                    progressBar.setVisibility(View.GONE);

                }

            }

            @Override
            public void failure(RetrofitError error) {

                Log.e("App", error.toString());
                progressBar.setVisibility(View.GONE);
                btnRecargar.setVisibility(View.VISIBLE);


            }
        });


    }


    @Override
    public void onClick(View v) {

        Fragment fragment = null;
        Bundle bundle = new Bundle();

        switch (v.getId()) {
            case cl.metro.mobile.R.id.btnHorarioServicio:

                fragment = new ServicioHorarioFragment();
                bundle.putInt("idLinea", idLinea);
                bundle.putString("codigoEstacion", codigoEstacion);
                bundle.putString("nombreEstacion", nombreEstacion);
                fragment.setArguments(bundle);

                break;
            case cl.metro.mobile.R.id.btnTarifas:

                fragment = new ServicioTarifarioFragment();

                bundle.putInt("idLinea", idLinea);
                bundle.putString("codigoEstacion", codigoEstacion);
                bundle.putString("nombreEstacion", nombreEstacion);
                fragment.setArguments(bundle);
                break;
        }

        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(cl.metro.mobile.R.id.container, fragment)
                .addToBackStack(null)
                .commit();
    }
}
