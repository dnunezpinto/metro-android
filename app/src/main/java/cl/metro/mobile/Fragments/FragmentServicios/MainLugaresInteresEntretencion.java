package cl.metro.mobile.Fragments.FragmentServicios;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import net.i2p.android.ext.floatingactionbutton.FloatingActionButton;
import net.i2p.android.ext.floatingactionbutton.FloatingActionsMenu;

import java.util.HashMap;
import java.util.List;

import cl.metro.mobile.MainActivity;
import cl.metro.mobile.Models.LugaresInteresEntretencion.DatosLugaresInteresEntretencion;
import cl.metro.mobile.Models.LugaresInteresEntretencion.ListLugaresInteresEntretencion;
import cl.metro.mobile.R;
import cl.metro.mobile.Retrofit.ApiService;
import cl.metro.mobile.Utilities.Utils;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.converter.GsonConverter;

/**
 * Created by Daniel on 24-02-2015.
 */
public class MainLugaresInteresEntretencion extends Fragment implements OnMapReadyCallback {

    public static String nombreEstacion;
    public static int idLinea;
    public static String currentEstacion;
    public static HashMap<Marker, DatosLugaresInteresEntretencion> mHashMap = new HashMap<Marker, DatosLugaresInteresEntretencion>();
    private static View rootView;
    private static String codigoEstacion;
    MapView mapView;
    boolean estadoEventos;
    boolean estadoPatrimonios;
    private Activity mActivity;
    private FloatingActionsMenu floatingActionsMenu;
    private ApiService apiService;
    private MaterialDialog dialog;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        mActivity = activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);

        codigoEstacion = getArguments().getString("codigoEstacion");
        nombreEstacion = getArguments().getString("nombreEstacion");
        idLinea = getArguments().getInt("idLinea");

        Gson gson = new GsonBuilder()
//                .excludeFieldsWithoutExposeAnnotation()
                .create();

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setConverter(new GsonConverter(gson))
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(Utils.URL_BASE_METRO)
                .build();

        apiService = restAdapter.create(ApiService.class);

//        if(!estadoEventos && !estadoPatrimonios){
//            Utils.createAlertDialog(getActivity(), "No se han encontrado lugares para " + nombreEstacion);
//        }

    }


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        rootView = inflater.inflate(R.layout.fragment_patrimonios_eventos, container, false);

        dialog = new MaterialDialog.Builder(getActivity())
                .content("Cargando Patrimonios, favor espere ...")
                .progress(true, 0)
                .autoDismiss(false)
                .cancelable(false)
                .show();

        TextView tvNombreEstacion = (TextView) rootView.findViewById(R.id.nombreEstacion);
        tvNombreEstacion.setText(MainLugaresInteresEntretencion.nombreEstacion);
        MainActivity.initApp.setEstacionImage(MainLugaresInteresEntretencion.idLinea, tvNombreEstacion, getActivity());
//        tvNombreEstacion.setTextColor(Color.parseColor(Utils.COLORS[MainLugaresInteresEntretencion.idLinea - 1]));


        floatingActionsMenu = (FloatingActionsMenu) rootView.findViewById(R.id.multiple_actions);
        floatingActionsMenu.setOnFloatingActionsMenuUpdateListener(new FloatingActionsMenu.OnFloatingActionsMenuUpdateListener() {
            @Override
            public void onMenuExpanded() {
                floatingActionsMenu.setBackgroundColor(Color.parseColor("#66000000"));
            }

            @Override
            public void onMenuCollapsed() {
                floatingActionsMenu.setBackgroundColor(Color.TRANSPARENT);

            }

        });

        FloatingActionButton actionEventosMes = (FloatingActionButton) rootView.findViewById(R.id.action_EventosDelMes);
        actionEventosMes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                checkFloating();

                delayedStart(400, new LugaresEventosMes());


            }


        });

        FloatingActionButton actionEventosCercanos = (FloatingActionButton) rootView.findViewById(R.id.action_eventos);
        actionEventosCercanos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                checkFloating();
                Fragment fragment = new LugaresEventosCercanos();
                delayedStart(400, fragment);


            }


        });

        FloatingActionButton actionPatrimoniosHitosTuristicos = (FloatingActionButton) rootView.findViewById(R.id.action_PatrimoniosHitosTuristicos);
        actionPatrimoniosHitosTuristicos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                checkFloating();
                delayedStart(400, new LugaresPatrimoniosHitosTuristicos());
                if (LugaresPatrimoniosHitosTuristicos.datosPatrimoniosHitosTuristicos != null) {
                    LugaresPatrimoniosHitosTuristicos.datosPatrimoniosHitosTuristicos.clear();
                }


            }


        });

        mapView = (MapView) rootView.findViewById(R.id.map);
        mapView.onCreate(savedInstanceState);

        if (mapView != null) {
            mapView.getMapAsync(this);
        }

        MainActivity.mToolbar.setTitle("Servicios Entorno");


//        FloatingActionButton fab = (FloatingActionButton) rootView.findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//
//            }
//        });


        return rootView;
    }


    @Override
    public void onResume() {
        super.onResume();

        mapView.onResume();

    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();

        mapView.onDestroy();
    }

    @Override
    public void onPause() {
        super.onPause();

        checkFloating();

        mapView.onPause();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    public void checkFloating() {

        if (floatingActionsMenu.isExpanded()) {
            floatingActionsMenu.collapse();
        }

    }

    public Boolean getPatrimonios(final GoogleMap googleMap) {


        if (MainActivity.initApp.currentListPatrimonios != null) {
            MainActivity.initApp.currentListPatrimonios = null;
        }

        apiService.getPatrimoniosByEstacion(codigoEstacion, new Callback<ListLugaresInteresEntretencion>() {
            @Override
            public void success(ListLugaresInteresEntretencion listLugaresInteresEntretencion, Response response) {

                MainActivity.initApp.currentListPatrimonios = listLugaresInteresEntretencion;

                if (MainActivity.initApp.currentListPatrimonios.getLugares() != null) {
                    estadoPatrimonios = true;

                    addMarkerMap(googleMap, MainActivity.initApp.currentListPatrimonios.getLugares());

                } else {

                    MainActivity.initApp.currentListPatrimonios = null;
                    estadoPatrimonios = false;
                }

                dialog.dismiss();
            }

            @Override
            public void failure(RetrofitError error) {

                estadoPatrimonios = false;
                dialog.dismiss();

            }
        });

        return estadoPatrimonios;
    }

    public Boolean getEventos(final GoogleMap googleMap) {

        if (MainActivity.initApp.currentListEventos != null) {
            MainActivity.initApp.currentListEventos = null;
        }

        apiService.getEventosByEstacion(codigoEstacion, new Callback<ListLugaresInteresEntretencion>() {
            @Override
            public void success(ListLugaresInteresEntretencion listLugaresInteresEntretencion, Response response) {

                MainActivity.initApp.currentListEventos = listLugaresInteresEntretencion;

                if (listLugaresInteresEntretencion.getEventos() != null) {
                    estadoEventos = true;
                    MainActivity.initApp.currentListEventos = listLugaresInteresEntretencion;
                    addMarkerMap(googleMap, listLugaresInteresEntretencion.getEventos());

                } else {
                    MainActivity.initApp.currentListEventos = null;
                    estadoEventos = false;
                }

            }

            @Override
            public void failure(RetrofitError error) {

                estadoEventos = false;

            }
        });

        return estadoEventos;
    }


    public void delayedStart(final int delay, final Fragment mFragment) {
        final Handler handler = new Handler();

        Runnable rssTask = new Runnable() {

            @Override
            public void run() {

                try {
                    handler.postDelayed(new Runnable() {

                        @Override
                        public void run() {

                            try {


                                getActivity().getSupportFragmentManager().beginTransaction()
                                        .addToBackStack(null)
                                        .replace(R.id.container, mFragment)
                                        .commit();

                            } catch (Exception exc) {
                                Log.d("Test", "delayedStart(): " + exc.getMessage());
                            }
                        }
                    }, delay);
                } catch (Exception exc) {
                    Log.d("Test", "delayedStart(): " + exc.getMessage());
                }
            }
        };

        Thread rssThread = new Thread(rssTask);
        rssThread.start();
    }

    private void addMarkerMap(GoogleMap mMap, final List<DatosLugaresInteresEntretencion> datos) {
        BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.drawable.ic_lugares_marker_36dp);

        for (DatosLugaresInteresEntretencion item : datos) {


            Marker marker = mMap.addMarker(new MarkerOptions()
                    .position(new LatLng(item.getLatitud(), item.getLongitud()))
                    .title(item.getNombre())
                    .icon(icon)
                    .snippet(item.getDireccion()));


            mHashMap.put(marker, item);


        }

        mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {

            @Override
            public void onInfoWindowClick(Marker marker) {

                Log.i("App", "Clicked");


                DatosLugaresInteresEntretencion tempString = mHashMap.get(marker);

                Fragment fragment = new DetalleLugaresInteresEntretencion().newInstance(tempString.getDescripcion(),
                        tempString.getDireccion(), tempString.getHorario(), tempString.getTarifa(), tempString.getImagenes());
                getActivity().getSupportFragmentManager().beginTransaction()
                        .addToBackStack(null)
                        .replace(R.id.container, fragment)
                        .commit();


            }
        });

    }


    @Override
    public void onMapReady(GoogleMap googleMap) {

        MapsInitializer.initialize(mActivity);


        if (MainActivity.initApp.currentSelectedEstacion != null) {

            double lat = MainActivity.initApp.currentSelectedEstacion.getLatitud();
            double lng = MainActivity.initApp.currentSelectedEstacion.getLongitud();

            LatLng positionEstacion = new LatLng(lat, lng);
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(positionEstacion, 15));

//            googleMap.addMarker(new MarkerOptions()
//                    .position(positionEstacion)
//                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_metro_red24pd))
//                    .alpha(0.7f));


//            BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE))

        }

//        if(Utils.isNetworkAvailable(getActivity())){

        getEventos(googleMap);
        getPatrimonios(googleMap);

//        }else{
//
//            Utils.createAlertDialog(getActivity(), getActivity().getString(R.string.lost_connection));
//
//        }

    }

}
