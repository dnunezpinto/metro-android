package cl.metro.mobile.Fragments.FragmentServicios;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import cl.metro.mobile.MainActivity;
import cl.metro.mobile.Models.TarifasServicio.HorarioTarifa;
import cl.metro.mobile.Models.TarifasServicio.Tarifas;
import cl.metro.mobile.Retrofit.RestClient;
import cl.metro.mobile.Utilities.ListTagHandler;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Daniel on 04-03-2015.
 */
public class ServicioTarifarioFragment extends Fragment {

    private static int idLinea;
    private TextView cuadro1, cuadro2, cuadro3, cuadro4;
    private TextView textHorarioPunta, textHorarioValle, textHorarioBajo;
    private TextView textPuntaMetro, textValleMetro, textBajoMetro;
    private TextView textPuntaCombinacion, textValleCombinacion, textBajoCombinacion;
    private TextView textPuntaEstudiante, textValleEstudiante, textBajoEstudiante;
    private TextView textPuntaAdulto, textValleAdulto, textBajoAdulto;
    private ProgressBar progressBar;
    private ScrollView scrollView;
    private String nombreEstacion;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        nombreEstacion = getArguments().getString("nombreEstacion");
        idLinea = getArguments().getInt("idLinea");

    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(cl.metro.mobile.R.layout.fragment_tarifario, container, false);

        cuadro1 = (TextView) rootView.findViewById(cl.metro.mobile.R.id.text_descripcion_1);
        cuadro2 = (TextView) rootView.findViewById(cl.metro.mobile.R.id.text_descripcion_2);
        cuadro3 = (TextView) rootView.findViewById(cl.metro.mobile.R.id.text_descripcion_3);
        cuadro4 = (TextView) rootView.findViewById(cl.metro.mobile.R.id.text_descripcion_4);

        textHorarioPunta = (TextView) rootView.findViewById(cl.metro.mobile.R.id.horario_punta_lunes_viernes);
        textHorarioValle = (TextView) rootView.findViewById(cl.metro.mobile.R.id.horario_valle_lunes_viernes);
        textHorarioBajo = (TextView) rootView.findViewById(cl.metro.mobile.R.id.horario_bajo_lunes_viernes);

        textPuntaMetro = (TextView) rootView.findViewById(cl.metro.mobile.R.id.horario_punta_metro);
        textValleMetro = (TextView) rootView.findViewById(cl.metro.mobile.R.id.horario_valle_metro);
        textBajoMetro = (TextView) rootView.findViewById(cl.metro.mobile.R.id.horario_bajo_metro);


        textPuntaCombinacion = (TextView) rootView.findViewById(cl.metro.mobile.R.id.horario_punta_combinacion);
        textValleCombinacion = (TextView) rootView.findViewById(cl.metro.mobile.R.id.horario_valle_combinacion);
        textBajoCombinacion = (TextView) rootView.findViewById(cl.metro.mobile.R.id.horario_bajo_combinacion);

        textPuntaEstudiante = (TextView) rootView.findViewById(cl.metro.mobile.R.id.horario_punta_estudiante);
        textValleEstudiante = (TextView) rootView.findViewById(cl.metro.mobile.R.id.horario_valle_estudiante);
        textBajoEstudiante = (TextView) rootView.findViewById(cl.metro.mobile.R.id.horario_bajo_estudiante);


        textPuntaAdulto = (TextView) rootView.findViewById(cl.metro.mobile.R.id.horario_punta_adulto_mayor);
        textValleAdulto = (TextView) rootView.findViewById(cl.metro.mobile.R.id.horario_valle_adulto_mayor);
        textBajoAdulto = (TextView) rootView.findViewById(cl.metro.mobile.R.id.horario_bajo_adulto_mayor);

        progressBar = (ProgressBar) rootView.findViewById(cl.metro.mobile.R.id.progressBar3);
        scrollView = (ScrollView) rootView.findViewById(cl.metro.mobile.R.id.scroll_view_content);


        TextView tvNombreEstacion = (TextView) rootView.findViewById(cl.metro.mobile.R.id.nombreEstacion);
        tvNombreEstacion.setText(nombreEstacion + " / Tarifario");
        MainActivity.initApp.setEstacionImage(idLinea, tvNombreEstacion, getActivity());
//        tvNombreEstacion.setTextColor(Color.parseColor(Utils.COLORS[idLinea - 1]));


        getTarifas();


        return rootView;


    }

    @Override
    public void onResume() {
        super.onResume();

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Tarifas Servicios");


    }


    public void getTarifas() {

        RestClient.get().getTarifario(new Callback<Tarifas>() {
            @Override
            public void success(Tarifas tarifas, Response response) {

                progressBar.setVisibility(View.GONE);
                scrollView.setVisibility(View.VISIBLE);


                setHorarios((tarifas.getHorarios().getHorarios()));
                setTextHorarioMetro(tarifas);
                setTextHorarioTroncalMetro(tarifas);
                setTextHorarioEstudiante(tarifas);
                setTextHorarioAdultoMayor(tarifas);


                // Tarifas Tabla


                // Textos informativos

                cuadro1.setText(Html.fromHtml(tarifas.getCuadro1(), null, new ListTagHandler()));
                cuadro1.setMovementMethod(LinkMovementMethod.getInstance());

                cuadro2.setText(Html.fromHtml(tarifas.getCuadro2(), null, new ListTagHandler()));
                cuadro2.setMovementMethod(LinkMovementMethod.getInstance());

                cuadro3.setText(Html.fromHtml(tarifas.getCuadro3(), null, new ListTagHandler()));
                cuadro3.setMovementMethod(LinkMovementMethod.getInstance());

                cuadro4.setText(Html.fromHtml(tarifas.getCuadro4(), null, new ListTagHandler()));
                cuadro4.setMovementMethod(LinkMovementMethod.getInstance());


            }

            @Override
            public void failure(RetrofitError error) {

            }
        });

    }

    private void setHorarios(List<HorarioTarifa> listHorarioTarifas) {
        String horarioPunta = "", horarioValle = "", horarioBajo = "";

        for (HorarioTarifa horarioTarifaModel : listHorarioTarifas) {
            if (horarioTarifaModel.getHorario().equalsIgnoreCase("PUNTA")) {
                String saltoLinea = "\n";
                saltoLinea = horarioPunta.equals("") ? saltoLinea : "";
                horarioPunta += getHoraParse(horarioTarifaModel.getInicio()) + " - "
                        + getHoraParse(horarioTarifaModel.getFin()) + saltoLinea;
            } else if (horarioTarifaModel.getHorario().equalsIgnoreCase("VALLE")) {
                String saltoLinea = "\n";
                saltoLinea = horarioValle.equals("") ? "" : saltoLinea;
                horarioValle += saltoLinea + getHoraParse(horarioTarifaModel.getInicio()) + " - "
                        + getHoraParse(horarioTarifaModel.getFin());
            } else if (horarioTarifaModel.getHorario().equalsIgnoreCase("BAJO")) {
                String saltoLinea = "\n";
                saltoLinea = horarioBajo.equals("") ? saltoLinea : "";
                horarioBajo += getHoraParse(horarioTarifaModel.getInicio()) + " - "
                        + getHoraParse(horarioTarifaModel.getFin()) + saltoLinea;
            }
        }


        textHorarioPunta.setText(horarioPunta);
        textHorarioValle.setText(horarioValle);
        textHorarioBajo.setText(horarioBajo);
    }

    public String getHoraParse(String string) {
        String newString = "";

        try {
            Date date = new SimpleDateFormat("HH:mm").parse(string);
            newString = new SimpleDateFormat("HH:mm").format(date);

        } catch (Exception e) {

        }


        return newString;
    }

    private void setTextHorarioMetro(Tarifas tarifario) {

        textPuntaMetro.setText(tarifario.getTMetroPunta());
        textValleMetro.setText(tarifario.getTMetroValle());
        textBajoMetro.setText(tarifario.getTMetroBajo());
    }

    private void setTextHorarioTroncalMetro(Tarifas tarifario) {

        textPuntaCombinacion.setText(tarifario.getTCombinacionPunta());
        textValleCombinacion.setText(tarifario.getTCombinacionValle());
        textBajoCombinacion.setText(tarifario.getTCombinacionBajo());
    }

    private void setTextHorarioEstudiante(Tarifas tarifario) {

        textPuntaEstudiante.setText(tarifario.getTEstudiantePunta());
        textValleEstudiante.setText(tarifario.getTEstudianteValle());
        textBajoEstudiante.setText(tarifario.getTEstudianteBajo());
    }

    private void setTextHorarioAdultoMayor(Tarifas tarifario) {

        if (tarifario.getTAdultoPunta().equals("")) {
            textPuntaAdulto.setText("No aplica" + "\n" + "beneficio");
        } else {
            textPuntaAdulto.setText(tarifario.getTAdultoPunta());
        }

        textValleAdulto.setText(tarifario.getTAdultoValle());
        textBajoAdulto.setText(tarifario.getTAdultoBajo());
    }


}
