package cl.metro.mobile.Fragments;


import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;

import cl.metro.mobile.MainActivity;
import cl.metro.mobile.Models.Bip;
import cl.metro.mobile.R;
import cl.metro.mobile.Utilities.AdapterFavoritos.AdapterListFavoritosTarjetasBip;
import cl.metro.mobile.Utilities.GetSaldoAsyncTask;
import cl.metro.mobile.Utilities.Utils;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link BipFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class BipFragment extends Fragment {


    public static AdapterListFavoritosTarjetasBip adapterListFavoritosTarjetasBip;
    private static String idTarjeta;
    private static Activity mActivity;
    private static ListView contentTarjetasBip;
    private EditText txtIdTarjeta;
    private String mValueNumeroTarjetaBip;

    public static BipFragment newInstance(String param1, String param2) {
        BipFragment fragment = new BipFragment();
//        Bundle args = new Bundle();
//        args.putString(ARG_PARAM1, param1);
//        args.putString(ARG_PARAM2, param2);
//        fragment.setArguments(args);
        return fragment;
    }

    public static void getTarjetasBip() {

        List<Bip> bipList;
        bipList = MainActivity.initApp.getAllBip();

        adapterListFavoritosTarjetasBip = new AdapterListFavoritosTarjetasBip(mActivity,
                R.layout.list_bip_favorita, bipList, "BipFragment");

        contentTarjetasBip.setAdapter(adapterListFavoritosTarjetasBip);
        Utils.setListViewHeightBasedOnChildren(contentTarjetasBip);

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        if (getArguments() != null) {
//            mParam1 = getArguments().getString(ARG_PARAM1);
//            mParam2 = getArguments().getString(ARG_PARAM2);
//        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        mActivity = activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(cl.metro.mobile.R.layout.fragment_bip, container, false);

        Button btnConsultarSaldo = (Button) rootView.findViewById(cl.metro.mobile.R.id.btnConsultarSaldo);

        txtIdTarjeta = (EditText) rootView.findViewById(cl.metro.mobile.R.id.editText);

//        Drawable x = getResources().getDrawable(R.drawable.ic_clear_search_holo_light);
//        x.setBounds(0, 0, x.getIntrinsicWidth(), x.getIntrinsicHeight());
//        txtIdTarjeta.setCompoundDrawables(null, null, x, null);
//
//        txtIdTarjeta.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View view, MotionEvent motionEvent) {
//                if (motionEvent.getAction() == MotionEvent.ACTION_UP){
//                    if (motionEvent.getX()>(view.getWidth()-view.getPaddingRight())){
//                        ((EditText)view).setText("");
//                    }
//                }
//                return false;
//            }
//        });

        contentTarjetasBip = (ListView) rootView.findViewById(cl.metro.mobile.R.id.contentTarjetaBip);
        getTarjetasBip();

        btnConsultarSaldo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                idTarjeta = txtIdTarjeta.getText().toString();
                mValueNumeroTarjetaBip = txtIdTarjeta.getText().toString();
                if (mValueNumeroTarjetaBip.isEmpty() || mValueNumeroTarjetaBip.equals("") || mValueNumeroTarjetaBip == null) {
                    txtIdTarjeta.setError("Ingrese el número de la tarjeta Bip!");
                } else {
                    Integer integer = Integer.parseInt(idTarjeta);
                    if (Utils.isNetworkAvailable(mActivity)) {
                        new GetSaldoAsyncTask(getActivity()).execute(new Integer[]{integer});
                    } else {
                        Toast.makeText(mActivity, cl.metro.mobile.R.string.lost_connection, Toast.LENGTH_SHORT).show();

                    }

                }

            }
        });

        return rootView;


    }

    @Override
    public void onResume() {
        super.onResume();

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(getString(cl.metro.mobile.R.string.title_section4));
        MainActivity.mDrawerList.setItemChecked(3, true);

    }


}