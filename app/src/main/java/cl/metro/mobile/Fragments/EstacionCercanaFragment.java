package cl.metro.mobile.Fragments;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import cl.metro.mobile.Fragments.FragmentServicios.ServiciosFragment;
import cl.metro.mobile.MainActivity;
import cl.metro.mobile.R;
import cl.metro.mobile.Utilities.Utils;

/**
 * Created by Daniel on 06-02-2015.
 */
public class EstacionCercanaFragment extends Fragment {

    private TextView nombreEstacion;
    private TextView txtEstadoEstacion;
    private TextView distancia;
    private TextView ultimoTweet;
    private ProgressBar progressBar;
    private RelativeLayout content;
    private int estadoEstacion;
    private String tweetLinea;
    private Button btnServicios;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        estadoEstacion = getArguments().getInt("estadoEstacion");
        tweetLinea = getArguments().getString("tweetLinea");


    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_estado_estacion_cercana, container, false);

        initLayout(rootView);

        //Cambia de color rojo el actionbar/toolbar
        Utils.toolbarRed(getActivity());

        setInformacion(estadoEstacion, tweetLinea);

//        obtenerEstadoEstacion();


        return rootView;

    }

    @Override
    public void onResume() {
        super.onResume();

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Estación más cercana");
        MainActivity.mDrawerList.setItemChecked(3, true);

    }

    public void initLayout(View rootView) {

        nombreEstacion = (TextView) rootView.findViewById(R.id.textView14);
        txtEstadoEstacion = (TextView) rootView.findViewById(R.id.textView17);
        distancia = (TextView) rootView.findViewById(R.id.textView16);
        ultimoTweet = (TextView) rootView.findViewById(R.id.textView21);
        content = (RelativeLayout) rootView.findViewById(R.id.content);
        progressBar = (ProgressBar) rootView.findViewById(R.id.progressBar2);
        btnServicios = (Button) rootView.findViewById(R.id.button3);
        btnServicios.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ServiciosFragment.estacion = MainActivity.initApp.getEstacionMasCercana();
                ServiciosFragment.verServicio = true;
//                ServiciosFragment.idLinea = MainActivity.initApp.getEstacionMasCercana().getIdLinea();

                Fragment newFragment = new ServiciosFragment();
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.container, newFragment)
                        .addToBackStack(null)
                        .commit();
            }
        });

    }
//
//    public void obtenerEstadoEstacion(){
//
//
//        String url = Utils.URL_STATUS_ESTACION + MainActivity.initApp.estacionMasCercana.getCodigoEstacion() +
//                Utils.getLineaEstacion(MainActivity.initApp.estacionMasCercana.getIdLinea());
//
//        Log.d("LOG", url);
//
//        CallWebService.getJSONfromURL(url, new CallWebService.JSONCallback() {
//
//            @Override
//            public void onResult(JSONObject result, boolean isFinish) {
//
//                try {
//
//                    int estadoEstacion = result.getInt("estado_estacion");
//                    String tweet = result.getString("tweet_linea");
//                    setInformacion(estadoEstacion, tweet);
//
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//
//            }
//        });
//    }


    public void setInformacion(final int estadoEstacion, final String tweet) {


        //Se llama al Hilo de la interfaz para setear los textos. El Asynctask es llamado por un hilo distinto.
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {

                nombreEstacion.setText(MainActivity.initApp.estacionMasCercana.getNombreEstacion());

                Log.i("App", "Estado Estacion: " + estadoEstacion);

                txtEstadoEstacion.setCompoundDrawablesWithIntrinsicBounds(getEstado(estadoEstacion), null, null, null);
                if (estadoEstacion == 1) {
                    txtEstadoEstacion.setText(getString(R.string.estado_1));
                } else if (estadoEstacion == 2) {
                    txtEstadoEstacion.setText(getString(R.string.estado_2));
                } else if (estadoEstacion == 3) {
                    txtEstadoEstacion.setText(getString(R.string.estado_3));
                }

                distancia.setText(MainActivity.initApp.estacionMasCercana.getDistancia());
                ultimoTweet.setText(tweet);

                content.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
            }
        });


    }

    public Drawable getEstado(int estado) {

        Drawable img = null;

        if (estado == 1) {
            img = getActivity().getResources().getDrawable(R.drawable.ic_semaforo_verde);
        } else if (estado == 2) {
            img = getActivity().getResources().getDrawable(R.drawable.ic_semaforo_amarillo);
        } else if (estado == 3) {
            img = getActivity().getResources().getDrawable(R.drawable.ic_semaforo_rojo);
        }

        return img;

    }
}
