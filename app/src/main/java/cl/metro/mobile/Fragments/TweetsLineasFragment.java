package cl.metro.mobile.Fragments;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.TransitionDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import cl.metro.mobile.MainActivity;
import cl.metro.mobile.Models.EstadoRed.EstadoRed;
import cl.metro.mobile.R;
import cl.metro.mobile.Retrofit.RestClient;
import cl.metro.mobile.Splash;
import cl.metro.mobile.Utilities.SlidingTabLayout;
import cl.metro.mobile.Utilities.Utils;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class TweetsLineasFragment extends Fragment {

    private static final String ARG_SECTION_NUMBER = "section_number";
    public static TextView txtEstacionCercana;
    public static TextView txtEstacionDistancia;
    public static int estadoEstacion;
    public static String strTweet;
    public static SlidingTabLayout tabs;
    public static ViewPager pager;
    private static int pageMargin;
    private Drawable oldBackground = null;
    private boolean isPause = false;
    private ImageView imgFooter;


    public TweetsLineasFragment() {

    }

    public TweetsLineasFragment newInstance(int sectionNumber) {
        TweetsLineasFragment fragment = new TweetsLineasFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        View rootView = inflater.inflate(R.layout.fragment_tweets_lineas, container, false);

        pager = rootView.findViewById(R.id.pager);
        tabs = rootView.findViewById(R.id.tabs);
        TextView txt = rootView.findViewById(R.id.textView23);
        txt.setTypeface(Utils.getTypeface(getActivity(), "Roboto-Light.ttf"));
        RelativeLayout estacionCercana = rootView.findViewById(R.id.ll_EstacionCercana);
        txtEstacionCercana = rootView.findViewById(R.id.txtEstacionCercana);
        txtEstacionDistancia = rootView.findViewById(R.id.textView51);
        imgFooter = rootView.findViewById(R.id.footerBanner);

        pageMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 2, getResources()
                .getDisplayMetrics());


        if (Utils.isEstacionSelected) {

            MainActivity.initApp.searchEstacionMasCercanaChoosen(Utils.estacion, getActivity());

        } else {

            if (MainActivity.gpsTracker != null && MainActivity.initApp.estacionMasCercana != null) {

                MainActivity.initApp.searchEstacionMasCercana(MainActivity.gpsTracker.location, txtEstacionCercana, txtEstacionDistancia, getActivity());

            }
        }

        // Reemplaza el fragment actual por el detalle de la estacion mas cercana
        estacionCercana.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Fragment fragment = new EstacionCercanaFragment();

                Bundle args = new Bundle();
                args.putInt("estadoEstacion", estadoEstacion);
                args.putString("tweetLinea", strTweet);
                fragment.setArguments(args);


                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.container, fragment)
                        .addToBackStack(null)
                        .commit();

            }
        });


        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();

        loadFooterBanner();

        if (isPause) {
            init();
            return;
        }

//        if (Utils.isNetworkAvailable(getActivity())) {
//            getEstadoLineas();
//        } else {
//            Utils.createAlertDialog(getActivity(), getActivity().getString(R.string.lost_connection));
//        }

    }


    @Override
    public void onPause() {
        super.onPause();

        isPause = true;
//        Log.i("App", "onPause");

    }

    @Override
    public void onResume() {
        super.onResume();

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(getString(R.string.title_section1));
        MainActivity.mDrawerList.setItemChecked(0, true);

        Splash.sharedPreferencesDB = getActivity().getSharedPreferences(Splash.PREFS_URL, 0);
        Splash.sharedPreferencesDB.getString(Splash.FIRST_URL, Utils.URL_BASE_INTERMOVIL);

        if (Utils.isNetworkAvailable(getActivity())) {
            getEstadoLineas();
        } else {
            Utils.createAlertDialog(getActivity(), getActivity().getString(R.string.lost_connection));
        }

    }

    public void loadFooterBanner() {


        if (!Utils.footerBanner.equals("") || !Utils.footerBanner.isEmpty()) {
            MainActivity.imageLoader.displayImage(Utils.footerBanner, imgFooter, MainActivity.options, new SimpleImageLoadingListener() {
                @Override
                public void onLoadingStarted(String imageUri, View view) {
//                loadImage.setVisibility(View.VISIBLE);
                }

                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    imgFooter.setVisibility(View.VISIBLE);
//                loadImage.setVisibility(View.GONE);
//                    imgFooter.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, 50));
                }
            });
        } else {
//            imgFooter.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, 0));
            imgFooter.setVisibility(View.GONE);
        }

        imgFooter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.linkBannerFooter != null || !Utils.linkBannerFooter.isEmpty() || !Utils.linkBannerFooter.equals("")) {
                    Utils.openWebPage(Utils.linkBannerFooter, getActivity());
                }

            }
        });


    }

    public void changeColor(int newColor) {

        // change ActionBar color just if an ActionBar is available
        Drawable colorDrawable = new ColorDrawable(newColor);
        Drawable bottomDrawable = new ColorDrawable(getResources().getColor(android.R.color.transparent));
        LayerDrawable ld = new LayerDrawable(new Drawable[]{colorDrawable, bottomDrawable});
        if (oldBackground == null) {

            ((AppCompatActivity) getActivity()).getSupportActionBar().setBackgroundDrawable(ld);
        } else {

            TransitionDrawable td = new TransitionDrawable(new Drawable[]{oldBackground, ld});
            ((AppCompatActivity) getActivity()).getSupportActionBar().setBackgroundDrawable(td);
            td.startTransition(200);

        }

        oldBackground = ld;
    }

    public void getEstadoLineas() {

        RestClient.get().getStatusRed(Utils.codigoEstacion, new Callback<EstadoRed>() {
            @Override
            public void success(EstadoRed estadoRed, Response response) {

                estadoEstacion = estadoRed.getEstadoEstacion();
                strTweet = estadoRed.getTweetLinea();
                Utils.estadoLineas = estadoRed.getEstadoLineas();
                init();
            }

            @Override
            public void failure(RetrofitError error) {

                error.printStackTrace();
                init();

//                getEstadoLineas();

            }
        });

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public void init() {
        //Set an adapter ViewPager

        if (getActivity() == null) {
            return;
        }

        pager.setAdapter(new SectionsPagerAdapter(getChildFragmentManager()));
        pager.setOffscreenPageLimit(5);

        pager.setPageMargin(pageMargin);


        // Bind the tabs to the ViewPager
        tabs.setDistributeEvenly(true);
        tabs.setViewPager(pager);
        tabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {

            @Override
            public int getIndicatorColor(int position) {
                return ContextCompat.getColor(getActivity(), R.color.white);
            }

        });

        tabs.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                int color = Color.parseColor(Utils.COLORS[position]);
                Utils.changeStatusBarColor(Utils.darker(color, 0.8f), getActivity());
                changeColor(color);

            }

            @Override
            public void onPageScrollStateChanged(int state) {


            }
        });


    }

    //    implements PagerSlidingTabStrip.CustomTabProvider
    public class SectionsPagerAdapter extends FragmentStatePagerAdapter {
        private final String[] TITLES = {"L1", "L2", "L4", "L4a", "L5",  "L6"};

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }


        @Override
        public Fragment getItem(int position) {


            switch (position) {
                case 0:
//                    return new DefaulFragment().newInstance(position + 1, Utils.URL_TWEET_L1);
                    return new DefaulFragment().newInstance(position + 1, "tweets-l1.json");
                case 1:
                    return new DefaulFragment().newInstance(position + 1, "tweets-l2.json");
                case 3:
                    return new DefaulFragment().newInstance(position + 1, "tweets-l4.json");
                case 2:
                    return new DefaulFragment().newInstance(position + 1, "tweets-l4a.json");
                case 4:
                    return new DefaulFragment().newInstance(position + 1, "tweets-l5.json");
                case 5:
                    return new DefaulFragment().newInstance(position + 1, "tweets-l6.json");

            }

            return null;

        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {

            return super.instantiateItem(container, position);
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            try {
                super.destroyItem(container, position, object);
            } catch (IllegalStateException ex) {
                ex.printStackTrace();
            }
        }

        @Override
        public CharSequence getPageTitle(int position) {

            //Ejemplo cambiar font
//            SpannableString s = new SpannableString(TITLES[position]);
//            s.setSpan(new Utilities.TypefaceSpan(getApplicationContext(), "Roboto-Medium.ttf"), 0, s.length(),
//                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            return TITLES[position];
        }

        @Override
        public int getCount() {
            return TITLES.length;
        }
    }
}



