package cl.metro.mobile.Fragments.FragmentServicios;


import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import cl.metro.mobile.MainActivity;
import cl.metro.mobile.Models.ServiciosCercanos.CategoriaParse;
import cl.metro.mobile.Models.ServiciosCercanos.ServiciosCercanos;
import cl.metro.mobile.Models.ServiciosCercanos.ServiciosCercanosParse;
import cl.metro.mobile.R;
import cl.metro.mobile.Retrofit.RestClient;
import cl.metro.mobile.Utilities.Utils;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class ServicioComercioCategoriasFragment extends Fragment {

    public static String codigoEstacion;
    public static String nombreEstacion;
    public static int idLinea;
    private static ArrayList<String> listCategorias;
    private ListView listView;
    private Activity mActivity;
    private ProgressBar progressBar;
    private List<CategoriaParse> categoria;
    private boolean isVerRubros = false;

    public ServicioComercioCategoriasFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        codigoEstacion = getArguments().getString("codigoEstacion");
        nombreEstacion = getArguments().getString("nombreEstacion");
        idLinea = getArguments().getInt("idLinea");

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        mActivity = activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_servicio_comercio, container, false);

//        onRestoreInstanceStae(savedInstanceState);

        TextView tvNombreEstacion = (TextView) rootView.findViewById(R.id.nombreEstacion);
        tvNombreEstacion.setText(nombreEstacion + " / Servicios Cercanos");
        MainActivity.initApp.setEstacionImage(idLinea, tvNombreEstacion, getActivity());
//        tvNombreEstacion.setTextColor(Color.parseColor(Utils.COLORS[idLinea - 1]));


        listView = (ListView) rootView.findViewById(R.id.listView3);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                MainActivity.initApp.listRubros = MainActivity.initApp.categoriaList.get(position).getRubros();

                isVerRubros = true;
                Fragment fragment = new ServicioComercioRubrosFragment();
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();

                Bundle args = new Bundle();
                args.putString("nombreCategoria", MainActivity.initApp.categoriaList.get(position).getNombreCategoria());
                args.putString("nombreEstacion", nombreEstacion);
                args.putInt("idLinea", idLinea);
                fragment.setArguments(args);
                fragmentManager.beginTransaction()
                        .replace(R.id.container, fragment)
                        .addToBackStack(getTag())
                        .commit();


            }
        });

        progressBar = (ProgressBar) rootView.findViewById(R.id.progressBar6);

        // Inflate the layout for this fragment
        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();

//        Log.i("App", "ServicioComercioCategoriasFragment onStart " + isVerRubros);

        if (isVerRubros && listCategorias != null) {
//                Log.i("App", "listCategorias con Datos Size: " + listCategorias.size());
            setData();
            return;
        }

        getData();
    }

    @Override
    public void onResume() {
        super.onResume();

        Log.i("App", "ServicioComercioCategoriasFragment onResume " + isVerRubros);
    }

    public void getData() {

        RestClient.get().getServiciosCercanos(codigoEstacion, new Callback<ServiciosCercanos>() {
            @Override
            public void success(ServiciosCercanos serviciosCercanos, Response response) {

                listCategorias = new ArrayList<String>();
                listCategorias.clear();
                ServiciosCercanosParse serviCercano = serviciosCercanos.getServiciosCercanos();

                MainActivity.initApp.serviciosCercanosParse = serviCercano;

                MainActivity.initApp.categoriaList = serviciosCercanos.getServiciosCercanosLista().getCategoriaRubros();


                for (int i = 0; i < serviciosCercanos.getServiciosCercanos().getCategoria().size(); i++) {

                    String nombre = serviciosCercanos.getServiciosCercanosLista().getCategoriaRubros().get(i).getNombreCategoria();
//                    Log.i("App", "Nombre: " + nombre);

                    listCategorias.add(nombre);

                }

                setData();


//                CategoriasSectionAdapter section_planifique = new CategoriasSectionAdapter(
//                        getListServiciosTest(), this);
//                initButtonQuitarFiltros();
//                this.setListAdapter(section_planifique);


            }

            @Override
            public void failure(RetrofitError error) {

            }

        });


    }

    public void setData() {

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                mActivity, (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) ?
                R.layout.custom_list_item :
                R.layout.custom_list_item_low, listCategorias);

        listView.setAdapter(adapter);
        Utils.setListViewHeightBasedOnChildren(listView);
        listView.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);

    }


}
