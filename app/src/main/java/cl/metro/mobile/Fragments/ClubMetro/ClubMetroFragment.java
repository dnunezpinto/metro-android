package cl.metro.mobile.Fragments.ClubMetro;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.util.Random;

import cl.metro.mobile.Fragments.ClubMetro.Actividades.ActividadesFragment;
import cl.metro.mobile.Fragments.ClubMetro.Evento.EventosFragment;
import cl.metro.mobile.Fragments.ClubMetro.Promociones.PromocionesFragment;
import cl.metro.mobile.Fragments.ClubMetro.Voucher.ListCupones;
import cl.metro.mobile.MainActivity;
import cl.metro.mobile.Models.ListaClubMetro.ListClub;
import cl.metro.mobile.R;
import cl.metro.mobile.Retrofit.ApiService;
import cl.metro.mobile.Utilities.Utils;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.converter.GsonConverter;


public class ClubMetroFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    boolean isLoadImage;
    private Activity mActivity;
    private boolean verClubMetro = false;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private ImageView displayImage;
    private Button btnInscribete, btnCerrarSesion;
    private ProgressDialog mProgressDialog;
    private Button btnIniciarSesion;

    public ClubMetroFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ClubMetroFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ClubMetroFragment newInstance(String param1, String param2) {
        ClubMetroFragment fragment = new ClubMetroFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_club_metro, container, false);

        //Color ToolBar #33b5e5

//        int color = Color.parseColor("#33b5e5");
//        Utils.changeColor(color, getActivity());
        Utils.toolbarRed(getActivity());

//        int asdfColor = getActivity().getResources().getColor(R.color.club_metro_color_dark);
//        Utils.changeStatusBarColor(asdfColor, getActivity());

        displayImage = (ImageView) rootView.findViewById(R.id.imageView26);

        Button btnEventos = (Button) rootView.findViewById(R.id.btnEventos);
        btnEventos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                verClubMetro = true;

                Fragment fragment = new EventosFragment();
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.container, fragment)
                        .addToBackStack(null)
                        .commit();

            }
        });

        Button btnActividades = (Button) rootView.findViewById(R.id.btnActividades);
        btnActividades.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                verClubMetro = true;

                Fragment fragment = new ActividadesFragment();
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.container, fragment)
                        .addToBackStack(null)
                        .commit();

            }
        });
        Button btnPromociones = (Button) rootView.findViewById(R.id.btnPromociones);
        btnPromociones.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                verClubMetro = true;

                Fragment fragment = new PromocionesFragment();
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.container, fragment)
                        .addToBackStack(null)
                        .commit();

            }
        });


        btnInscribete = (Button) rootView.findViewById(R.id.btnInscribete);
        btnInscribete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                verClubMetro = true;

                Fragment fragment = new InscribeteFragment();
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction()
                        .addToBackStack(null)
                        .replace(R.id.container, fragment)
                        .commit();

            }
        });

        btnIniciarSesion = (Button) rootView.findViewById(R.id.btnIniciarSesion);
        btnIniciarSesion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getActivity(), LoginActivity.class);
                startActivity(intent);
            }
        });



        btnCerrarSesion = (Button) rootView.findViewById(R.id.btnCerrarSesion);
        btnCerrarSesion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Utils.saveToPrefs(getActivity(), Utils.PREFS_LOGIN_ID_USER, "-1");
                btnCerrarSesion.setVisibility(View.GONE);
                btnInscribete.setVisibility(View.VISIBLE);
                btnIniciarSesion.setVisibility(View.VISIBLE);
            }
        });

        if (Utils.isUserConnected(getActivity())) {
            btnInscribete.setVisibility(View.GONE);
            btnIniciarSesion.setVisibility(View.GONE);
            btnCerrarSesion.setVisibility(View.VISIBLE);
        } else {
            btnInscribete.setVisibility(View.VISIBLE);
            btnIniciarSesion.setVisibility(View.VISIBLE);
            btnCerrarSesion.setVisibility(View.GONE);
        }

        Button btnInfoEventos = (Button) rootView.findViewById(R.id.btnComoUsarAPP);
        btnInfoEventos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Utils.showAlertDialog(getActivity(), getResources().getString(R.string.informaciones));
//                Utils.showAlert(getActivity(), getResources().getString(R.string.informaciones));
//                Utils.createAlertDialog(getActivity(), getActivity().getResources().getString(R.string.informaciones));

            }
        });

        Button btnBilleteraVirtual = (Button) rootView.findViewById(R.id.btnBilleteraVirtual);
        btnBilleteraVirtual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!Utils.isUserConnected(getActivity())) {

                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                    alertDialogBuilder.setMessage("Debe iniciar sesión en Club Metro para revisar tú Billetera Virtual")
                            .setCancelable(false)
                            .setPositiveButton("Iniciar Sesión",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {

                                            Intent intent = new Intent(getActivity(), LoginActivity.class);
                                            intent.putExtra("ViewCall", "BilleteraVirtual");
                                            startActivity(intent);

                                        }
                                    });
                    alertDialogBuilder.setNegativeButton("No",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alert = alertDialogBuilder.create();
                    alert.show();

                    return;
                }

                verClubMetro = true;

                Fragment fragment = new ListCupones();
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction()
                        .addToBackStack(null)
                        .replace(R.id.container, fragment)
                        .commit();

            }
        });

        Button btnTarjetas = (Button) rootView.findViewById(R.id.btnTarjetas);
        btnTarjetas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!Utils.isUserConnected(getActivity())) {

                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                    alertDialogBuilder.setMessage("Debe iniciar sesión en Club Metro para revisar tus Tarjetas registradas")
                            .setCancelable(false)
                            .setPositiveButton("Iniciar Sesión",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {

                                            Intent intent = new Intent(getActivity(), LoginActivity.class);
                                            startActivity(intent);

                                        }
                                    });
                    alertDialogBuilder.setNegativeButton("No",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alert = alertDialogBuilder.create();
                    alert.show();

                    return;
                }

                verClubMetro = true;

                Fragment fragment = new ListBipClubMetro();
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction()
                        .addToBackStack(null)
                        .replace(R.id.container, fragment)
                        .commit();

            }
        });

        MainActivity.imageLoader.displayImage("assets://img_clubmetro.png", displayImage);

        // Inflate the layout for this fragment
        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        mActivity = activity;
    }

    @Override
    public void onStart() {
        super.onStart();

        if (!verClubMetro) {
            getListClubMetro();
        }

    }

    @Override
    public void onResume() {
        super.onResume();

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(getString(R.string.title_section7));
        MainActivity.mDrawerList.setItemChecked(6, true);

        if (Utils.isUserConnected(getActivity())) {
            btnInscribete.setVisibility(View.GONE);
            btnIniciarSesion.setVisibility(View.GONE);
            btnCerrarSesion.setVisibility(View.VISIBLE);
        } else {
            btnInscribete.setVisibility(View.VISIBLE);
            btnIniciarSesion.setVisibility(View.VISIBLE);
            btnCerrarSesion.setVisibility(View.GONE);
        }

    }

    public void getListClubMetro() {

        mProgressDialog = ProgressDialog.show(getActivity(), "",
                "Cargando Datos Club Metro, favor espere ...", true);

        Gson gson = new GsonBuilder()
//                .excludeFieldsWithoutExposeAnnotation()
                .create();

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setConverter(new GsonConverter(gson))
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(Utils.URL_BASE_METRO)
                .build();

        ApiService apiService = restAdapter.create(ApiService.class);

        apiService.getListadoClubMetro(new Callback<ListClub>() {
            @Override
            public void success(ListClub listClub, Response response) {

                mProgressDialog.dismiss();


//                for (int e = 0; e < listClub.getEventos().size(); e++) {
//                    String temp = listClub.getEventos().get(e).getTitulo();
//                    if (temp != null && !temp.equals("") && temp != "") {
//                        Log.i("app", "ID: " + listClub.getEventos().get(e).getId());
//                        Log.i("app", "lenght: " + listClub.getEventos().get(e).getTitulo().length());
//                        Log.i("app", "titulo: " + listClub.getEventos().get(e).getTitulo());
//
//                    } else {
//
////                        Log.i("app", "ID: " + listClub.getEventos().get(e).getId());
//                        listClub.getEventos().remove(e);
//                    }
//
//
//
//                }
//
//                for (int a = 0; a < listClub.getActividades().size(); a++) {
//                    if (listClub.getActividades().get(a).getTitulo() == null ||
//                            listClub.getActividades().get(a).getTitulo().isEmpty() ||
//                            listClub.getActividades().get(a).getTitulo().contains("null") ||
//                            listClub.getActividades().get(a).getTitulo().equals("") ||
//                            listClub.getActividades().get(a).getTitulo().equals(null) ||
//                            StringUtils.isNotBlank(listClub.getActividades().get(a).getTitulo()) ||
//                            StringUtils.isNotEmpty(listClub.getActividades().get(a).getTitulo())) {
//
//                        listClub.getActividades().remove(a);
//                    }
//                }

//                for (int p = 0; p < listClub.getPromociones().size(); p++) {
//                    if (listClub.getPromociones().get(p).getTitulo() == null ||
//                            listClub.getPromociones().get(p).getTitulo().isEmpty() ||
//                            listClub.getPromociones().get(p).getTitulo().contains("null") ||
//                            StringUtils.isNotBlank(listClub.getPromociones().get(p).getTitulo()) ||
//                            StringUtils.isNotEmpty(listClub.getPromociones().get(p).getTitulo())) {
//
//                        listClub.getPromociones().remove(p);
//                    }
//                }

                MainActivity.initApp.listClubMetro = listClub;
//                MainActivity.initApp.listClubMetro.getEventos().get(new Random().nextInt(MainActivity.initApp.listClubMetro.getEventos().size()));
//                loadImageDestacado(MainActivity.initApp.listClubMetro.getEventos().get(new Random().nextInt(MainActivity.initApp.listClubMetro.getEventos().size())).getImagen());

            }

            @Override
            public void failure(RetrofitError error) {

                mProgressDialog.dismiss();
                Toast.makeText(getActivity(), "Problemas al contactar el servidor, favor intentar nuevamente ...", Toast.LENGTH_LONG).show();

            }
        });
    }

    public void loadImageDestacado(String urlImage) {

//        displayImage.setScaleType(ImageView.ScaleType.CENTER_CROP);
        MainActivity.imageLoader.displayImage(urlImage, displayImage, MainActivity.options, new SimpleImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {
//                loadImage.setVisibility(View.VISIBLE);
            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                @SuppressWarnings("unused")
                String message = null;
                switch (failReason.getType()) {
                    case IO_ERROR:
                        message = "Input/Output error";
                        break;
                    case DECODING_ERROR:
                        message = "Image can't be decoded";
                        break;
                    case NETWORK_DENIED:
                        message = "Downloads are denied";
                        break;
                    case OUT_OF_MEMORY:
                        message = "Out Of Memory error";
                        break;
                    case UNKNOWN:
                        message = "Unknown error";
                        break;
                }


                loadImageDestacado(MainActivity.initApp.listClubMetro.getEventos().get(new Random().nextInt(MainActivity.initApp.listClubMetro.getEventos().size())).getImagen());
//                loadImage.setVisibility(View.GONE);
            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
//                loadImage.setVisibility(View.GONE);

            }
        });

    }


}
