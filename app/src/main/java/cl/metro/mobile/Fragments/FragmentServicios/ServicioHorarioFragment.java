package cl.metro.mobile.Fragments.FragmentServicios;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.ToggleButton;

import cl.metro.mobile.MainActivity;
import cl.metro.mobile.Models.Horario.Boleteria;
import cl.metro.mobile.Models.Horario.Estacion;
import cl.metro.mobile.Models.Horario.EstacionTren;
import cl.metro.mobile.Models.Horario.TrenParse;
import cl.metro.mobile.R;
import cl.metro.mobile.Retrofit.ApiService;
import cl.metro.mobile.Retrofit.RestClient;
import cl.metro.mobile.Utilities.Utils;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Daniel on 04-03-2015.
 */
public class ServicioHorarioFragment extends Fragment {


    private static String codigoEstacion;
    private static String nombreEstacion;
    private static int idLinea;
    private static TrenParse _trenParse;
    View rootView;
    private TextView textDiaLaboralPrimerTren, textDiaSabadoPrimerTren, textDiaDomingoPrimerTren, textDiaLaboralUltimoTren, textDiaSabadoUltimoTren, textDiaDomingoUltimoTren, tvNombreEstacion;
    private ToggleButton toggle_direccion;
    private ScrollView scrollHorario;
    private ProgressBar progressBar;
//    private TableLayout tlHorarioApertura, tlHorarioCierre;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        codigoEstacion = getArguments().getString("codigoEstacion");
        nombreEstacion = getArguments().getString("nombreEstacion");
        idLinea = getArguments().getInt("idLinea");

    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_servicio_horario, container, false);

        toggle_direccion = ((ToggleButton) rootView.findViewById(R.id.toggle_direccion_tren));
        textDiaLaboralPrimerTren = (TextView) rootView.findViewById(R.id.hora_primer_tren_dia_laboral);

        textDiaSabadoPrimerTren = (TextView) rootView.findViewById(R.id.hora_primer_tren_sabado);
        textDiaDomingoPrimerTren = (TextView) rootView.findViewById(R.id.hora_primer_tren_domingo_festivos);

        textDiaLaboralUltimoTren = (TextView) rootView.findViewById(R.id.hora_ultimo_tren_dia_laboral);
        textDiaSabadoUltimoTren = (TextView) rootView.findViewById(R.id.hora_ultimo_tren_sabado);

        textDiaDomingoUltimoTren = (TextView) rootView.findViewById(R.id.hora_ultimo_tren_domingo_festivos);

        scrollHorario = (ScrollView) rootView.findViewById(R.id.scrollHorario);
        progressBar = (ProgressBar) rootView.findViewById(R.id.progressBar11);

        tvNombreEstacion = (TextView) rootView.findViewById(R.id.nombreEstacion);
        tvNombreEstacion.setText(nombreEstacion + " / Horarios");
        MainActivity.initApp.setEstacionImage(idLinea, tvNombreEstacion, getActivity());

//        tvNombreEstacion.setTextColor(Color.parseColor(Utils.COLORS[idLinea - 1]));
//        tlHorarioApertura = (TableLayout) rootView.findViewById(R.id.tabla_horario_apertura);
//        tlHorarioCierre = (TableLayout) rootView.findViewById(R.id.tabla_horario_cierre);

        getHorarios(rootView);

        return rootView;


    }

    private void initToggleDireccion() {

        if (_trenParse != null) {

            String direccionA = _trenParse.getTren().getEstacionA().getNombre();
            String direccionB = _trenParse.getTren().getEstacionB().getNombre();

            if (direccionA.equalsIgnoreCase("Puente Alto")) {
                direccionA = "Plaza de Puente Alto";
            } else if (direccionB.equalsIgnoreCase("Puente Alto")) {
                direccionB = "Plaza de Puente Alto";
            }

            if (direccionA.equalsIgnoreCase("Plaza de Maipu")) {
                direccionA = "Plaza de Maipú";
            } else if (direccionB.equalsIgnoreCase("Plaza de Maipu")) {
                direccionB = "Plaza de Maipú";
            }

            setValuesToggleDireccion(direccionB, direccionA);
        }

        toggle_direccion.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {

                    setTextHorarioTren(_trenParse.getTren().getEstacionA());
                } else {

                    setTextHorarioTren(_trenParse.getTren().getEstacionB());
                }


            }
        });
        toggle_direccion.setChecked(true);
        toggle_direccion.refreshDrawableState();
    }

    private void setValuesToggleDireccion(String valor1, String valor2) {
        toggle_direccion.setTextOn(valor2);
        toggle_direccion.setTextOff(valor1);
        toggle_direccion.refreshDrawableState();
    }

    public void getHorarios(final View rootView) {


        RestAdapter restAdapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint("http://www.metro.cl/api/")
                .build();

        ApiService apiService = restAdapter.create(ApiService.class);
        apiService.getHorarios(codigoEstacion, new Callback<TrenParse>() {
            @Override
            public void success(TrenParse trenParse, Response response) {

                _trenParse = trenParse;

                setTextEstacionAperturaAndEstacion(rootView, trenParse.getEstacion());
                setTextBoleteria(rootView, trenParse.getBoleteria());

                scrollHorario.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);

                initToggleDireccion();

            }

            @Override
            public void failure(RetrofitError error) {

            }
        });

    }

    private void setTextEstacionAperturaAndEstacion(View rootView, Estacion estacion) {

        TextView textDiaLaboralApertura = (TextView) rootView.findViewById(R.id.hora_inicio_am);
        textDiaLaboralApertura.setText(estacion.getAbrir().getLunesViernes());

        TextView textDiaLaboralSabadoApertura = (TextView) rootView.findViewById(R.id.hora_inicio_pm);
        textDiaLaboralSabadoApertura.setText(estacion.getAbrir().getSabado());

        TextView textDiaLaboralDomingoApertura = (TextView) rootView.findViewById(R.id.hora_apertura_estacion_domingo_festivos);
        textDiaLaboralDomingoApertura.setText(estacion.getAbrir().getDomingo());

        TextView textDiaLaboralCierre = (TextView) rootView.findViewById(R.id.hora_cierre_estacion_dia_laboral);
        textDiaLaboralCierre.setText(estacion.getCerrar().getLunesViernes());

        TextView textDiaLaboralSabadoCierre = (TextView) rootView.findViewById(R.id.hora_cierre_estacion_sabado);
        textDiaLaboralSabadoCierre.setText(estacion.getCerrar().getSabado());

        TextView textDiaLaboralDomingoCierre = (TextView) rootView.findViewById(R.id.hora_cierre_estacion_domingo_festivos);
        textDiaLaboralDomingoCierre.setText(estacion.getCerrar().getDomingo());

    }

    private void setTextBoleteria(View rootView, Boleteria boleteria) {

        TextView textDiaLaboralApertura = (TextView) rootView.findViewById(R.id.hora_fin_am);
        textDiaLaboralApertura.setText(boleteria.getAbrir().getLunesViernes());

        TextView textDiaLaboralSabadoApertura = (TextView) rootView.findViewById(R.id.hora_fin_pm);
        textDiaLaboralSabadoApertura.setText(boleteria.getAbrir().getSabado());

        TextView textDiaLaboralDomingoApertura = (TextView) rootView.findViewById(R.id.hora_apertura_boleteria_domingo_festivos);
        textDiaLaboralDomingoApertura.setText(boleteria.getAbrir().getDomingo());

        TextView textDiaLaboralCierre = (TextView) rootView.findViewById(R.id.hora_cierre_boleteria_dia_laboral);
        textDiaLaboralCierre.setText(boleteria.getCerrar().getLunesViernes());

        TextView textDiaLaboralSabadoCierre = (TextView) rootView.findViewById(R.id.hora_cierre_boleteria_sabado);
        textDiaLaboralSabadoCierre.setText(boleteria.getCerrar().getSabado());

        TextView textDiaLaboralDomingoCierre = (TextView) rootView.findViewById(R.id.hora_cierre_boleteria_domingo_festivos);
        textDiaLaboralDomingoCierre.setText(boleteria.getCerrar().getDomingo());
    }

    private void setTextHorarioTren(EstacionTren estacionTren) {

//        TableRow.LayoutParams paramsExample = new TableRow.LayoutParams(80, 65, 1.0f);


        textDiaLaboralPrimerTren.setText(estacionTren.getPrimerTren()
                .getLunesViernes());
        textDiaLaboralPrimerTren.setGravity(Gravity.CENTER);
//        textDiaLaboralPrimerTren.setLayoutParams(paramsExample);


        textDiaSabadoPrimerTren.setText(estacionTren.getPrimerTren().getSabado());
        textDiaSabadoPrimerTren.setGravity(Gravity.CENTER);
//        textDiaSabadoPrimerTren.setLayoutParams(paramsExample);

        textDiaDomingoPrimerTren.setText(estacionTren.getPrimerTren()
                .getDomingo());
        textDiaDomingoPrimerTren.setGravity(Gravity.CENTER);
//        textDiaDomingoPrimerTren.setLayoutParams(paramsExample);

        textDiaLaboralUltimoTren.setText(estacionTren.getUltimoTren()
                .getLunesViernes());
        textDiaLaboralUltimoTren.setGravity(Gravity.CENTER);
//        textDiaLaboralUltimoTren.setLayoutParams(paramsExample);

        textDiaSabadoUltimoTren.setText(estacionTren.getUltimoTren().getSabado());
        textDiaSabadoUltimoTren.setGravity(Gravity.CENTER);
//        textDiaSabadoUltimoTren.setLayoutParams(paramsExample);

        textDiaDomingoUltimoTren.setText(estacionTren.getUltimoTren().getDomingo());
        textDiaDomingoUltimoTren.setGravity(Gravity.CENTER);
//        textDiaDomingoUltimoTren.setLayoutParams(paramsExample);

    }

}
