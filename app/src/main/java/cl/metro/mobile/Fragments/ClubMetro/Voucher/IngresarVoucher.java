package cl.metro.mobile.Fragments.ClubMetro.Voucher;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.json.JSONException;
import org.json.JSONObject;

import cl.metro.mobile.R;
import cl.metro.mobile.Retrofit.ApiService;
import cl.metro.mobile.Utilities.Utils;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class IngresarVoucher extends AppCompatActivity implements View.OnClickListener {

    final private int REQUEST_CODE_ASK_PERMISSIONS = 123;
    private Activity mActivity;
    private EditText eTxt_Voucher;
    private Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ingresar_voucher);

        mActivity = this;
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setTitle("Ingresa tu código");

        eTxt_Voucher = (EditText) findViewById(R.id.eTxt_NumTarjetaBip);

        Button btnScan = (Button) findViewById(R.id.btnScan);
        Button btnGenerarCupon = (Button) findViewById(R.id.btnGenerarCupon);

        btnScan.setOnClickListener(this);
        btnGenerarCupon.setOnClickListener(this);

        setSupportActionBar(mToolbar);
        assert getSupportActionBar() != null;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null) {
            if (result.getContents() == null) {
                Log.d("MainActivity", "Cancelled scan");
//                Toast.makeText(this, "Cancelled", Toast.LENGTH_LONG).show();
            } else {
                Log.d("MainActivity", "Scanned");
//                Toast.makeText(this, "Scanned: " + result.getContents(), Toast.LENGTH_LONG).show();
                eTxt_Voucher.setText(result.getContents());
            }
        } else {
            Log.d("MainActivity", "Weird");
            // This is important, otherwise the result will not be passed to the fragment
            super.onActivityResult(requestCode, resultCode, data);
        }

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.btnScan:
                if (ActivityCompat.checkSelfPermission(IngresarVoucher.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {

                    ActivityCompat.requestPermissions(IngresarVoucher.this,
                            new String[]{Manifest.permission.CAMERA},
                            REQUEST_CODE_ASK_PERMISSIONS);
                } else {

                    IntentIntegrator integrator = new IntentIntegrator(this);
                    integrator.setOrientationLocked(true);
                    integrator.initiateScan();
                }
                break;
            case R.id.btnGenerarCupon:

                if (!TextUtils.isEmpty(eTxt_Voucher.getText().toString())) {
                    eTxt_Voucher.setError(null);

                    verificarCupon();

                } else {
                    eTxt_Voucher.setError("Ingrese o Escanee un código");
                }

                break;

        }


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_PERMISSIONS:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Permission Granted
                    Log.d("App Metro", "Permission Granted");
                    IntentIntegrator integrator = new IntentIntegrator(this);
                    integrator.initiateScan();


                } else {

                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    public void verificarCupon() {

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(Utils.URL_BASE_METRO)
                .build();

        ApiService apiService = restAdapter.create(ApiService.class);
//        409338
//        Utils.getFromPrefs(getActivity(), Utils.PREFS_LOGIN_ID_USER, "")
        apiService.verificarCupon(Utils.getFromPrefs(IngresarVoucher.this,
                Utils.PREFS_LOGIN_ID_USER, ""),
                eTxt_Voucher.getText().toString(),
                new Callback<Response>() {
                    @Override
                    public void success(Response response, Response response2) {


                        try {
                            JSONObject result = new JSONObject(Utils.convertResponseBody(response));

                            int estado = result.getInt("estado");
                            if (estado != 0) {
                                String mensaje = result.getString("mensaje");
                                Utils.createAlertDialogfinish(IngresarVoucher.this, mensaje);

                            } else {

                                String mensaje = result.getString("mensaje");
                                Utils.createAlertDialog(IngresarVoucher.this, mensaje);

                            }

//                  {"estado":0,"mensaje":"Voucher ya ingresado anteriormente"}


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }

                    @Override
                    public void failure(RetrofitError error) {

                    }
                });

    }
}
