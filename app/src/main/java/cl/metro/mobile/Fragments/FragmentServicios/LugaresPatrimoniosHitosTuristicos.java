package cl.metro.mobile.Fragments.FragmentServicios;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.TextView;

import java.util.List;

import cl.metro.mobile.MainActivity;
import cl.metro.mobile.Models.LugaresInteresEntretencion.DatosLugaresInteresEntretencion;
import cl.metro.mobile.R;
import cl.metro.mobile.Utilities.AdapterListLugaresInteresEntretencion;

/**
 * A simple {@link Fragment} subclass.
 */
public class LugaresPatrimoniosHitosTuristicos extends Fragment implements AbsListView.OnItemClickListener {

    public static List<DatosLugaresInteresEntretencion> datosPatrimoniosHitosTuristicos;
    private AbsListView mListView;
    private AdapterListLugaresInteresEntretencion adapterListPatrimoniosHitosTuristicos;
    private boolean isVerRubros = false;
    private TextView emptyText;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_lugares_eventos_mes, container, false);

        mListView = (AbsListView) rootView.findViewById(R.id.list);
        emptyText = (TextView) rootView.findViewById(R.id.empty);
        emptyText.setText("No se registran Patrimonios e Hitos Turísticos cercanos a esta estación");
        mListView.setEmptyView(rootView.findViewById(R.id.empty));

        mListView.setOnItemClickListener(this);

        if (MainActivity.initApp.currentListPatrimonios != null) {

            Log.i("App", "Cargando Patrimonios");
            mListView.setVisibility(View.VISIBLE);

            datosPatrimoniosHitosTuristicos = MainActivity.initApp.currentListPatrimonios.getLugares();

            adapterListPatrimoniosHitosTuristicos = new AdapterListLugaresInteresEntretencion(getActivity(),
                    R.layout.list_item,
                    R.id.text_item,
                    datosPatrimoniosHitosTuristicos);
            mListView.setAdapter(adapterListPatrimoniosHitosTuristicos);

        } else {
            Log.i("App", "No se encontraron patrimonios");
            if (datosPatrimoniosHitosTuristicos != null)
                datosPatrimoniosHitosTuristicos.clear();
            mListView.setVisibility(View.GONE);
            emptyText.setVisibility(View.VISIBLE);
        }


        TextView tvNombreEstacion = (TextView) rootView.findViewById(R.id.nombreEstacion);
        tvNombreEstacion.setText(MainLugaresInteresEntretencion.nombreEstacion);
        MainActivity.initApp.setEstacionImage(MainLugaresInteresEntretencion.idLinea, tvNombreEstacion, getActivity());
//        tvNombreEstacion.setTextColor(Color.parseColor(Utils.COLORS[MainLugaresInteresEntretencion.idLinea - 1]));


        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();

        if (isVerRubros && MainActivity.initApp.currentListPatrimonios != null) {
            Log.i("App", "isVerRubros: " + isVerRubros + ", list con datos");
            setData();
        } else {
            if (MainActivity.initApp.currentListPatrimonios == null && datosPatrimoniosHitosTuristicos != null) {
                datosPatrimoniosHitosTuristicos.clear();
                mListView.setVisibility(View.GONE);
                emptyText.setVisibility(View.VISIBLE);
            }
            Log.i("App", "SIN DATOS... !");
        }

    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        datosPatrimoniosHitosTuristicos.get(position);
        isVerRubros = true;

        Fragment fragment = new DetalleLugaresInteresEntretencion().newInstance(datosPatrimoniosHitosTuristicos.get(position).getDescripcion(),
                datosPatrimoniosHitosTuristicos.get(position).getDireccion(),
                datosPatrimoniosHitosTuristicos.get(position).getHorario(),
                datosPatrimoniosHitosTuristicos.get(position).getTarifa(),
                datosPatrimoniosHitosTuristicos.get(position).getImagenes());

        Log.i("App", "Array Imagenes size: " + datosPatrimoniosHitosTuristicos.get(position).getImagenes().size());
        getActivity().getSupportFragmentManager().beginTransaction()
                .addToBackStack(null)
                .replace(R.id.container, fragment)
                .commit();

    }

    public void setData() {

        mListView.setAdapter(adapterListPatrimoniosHitosTuristicos);

    }

}
