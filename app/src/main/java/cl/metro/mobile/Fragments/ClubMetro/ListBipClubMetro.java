package cl.metro.mobile.Fragments.ClubMetro;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import cl.metro.mobile.Fragments.ClubMetro.Voucher.AgregarBip;
import cl.metro.mobile.R;
import cl.metro.mobile.Retrofit.ApiService;
import cl.metro.mobile.Utilities.AdapterClubMetro.AdapterListBipClubMetro;
import cl.metro.mobile.Utilities.Utils;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class ListBipClubMetro extends Fragment {

    private ProgressBar progressBar;
    private AbsListView mListView;
    /**
     * The Adapter which will be used to populate the ListView/GridView with
     * Views.
     */
    private ListAdapter mAdapter;

    private TextView emptyText;
    private MaterialDialog dialog;

    public ListBipClubMetro() {

        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_list_bip_club_metro, container, false);

        progressBar = (ProgressBar) rootView.findViewById(R.id.progressBar16);

        mListView = (ListView) rootView.findViewById(R.id.listView4);

        emptyText = (TextView) rootView.findViewById(R.id.textView94);


        Button btnAgregarBip = (Button) rootView.findViewById(R.id.btnAgregarBip);
        btnAgregarBip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getActivity(), AgregarBip.class);
                getActivity().startActivity(intent);

            }
        });


        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();

        getBipLista();

    }

    @Override
    public void onResume() {
        super.onResume();

            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Tarjetas Asociadas");
    }

    public void getBipLista() {

        dialog = new MaterialDialog.Builder(getActivity())
                .content("Obteniendo información, favor espere ...")
                .progress(true, 0)
                .autoDismiss(false)
                .cancelable(false)
                .show();


        RestAdapter restAdapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(Utils.URL_BASE_METRO)
                .build();

        ApiService apiService = restAdapter.create(ApiService.class);
        int aNumber = (int) (20 * Math.random()) + 1;
//        409338
//        Utils.getFromPrefs(getActivity(), Utils.PREFS_LOGIN_ID_USER, "")
        apiService.getBipLista(Utils.getFromPrefs(getActivity(), Utils.PREFS_LOGIN_ID_USER, ""), aNumber, new Callback<Response>() {
            @Override
            public void success(Response response, Response response2) {

                dialog.dismiss();

                try {
                    progressBar.setVisibility(View.GONE);
                    JSONObject result = new JSONObject(Utils.convertResponseBody(response));

                    int estado = result.getInt("estado");
                    if (result.has("bips") || estado != 0) {
                        JSONArray jArrayVoucher = result.getJSONArray("bips");

                        if (!BipContent.ITEMS_BIP.isEmpty()) {
                            BipContent.ITEMS_BIP.clear();
                        }

                        for (int i = 0; i < jArrayVoucher.length(); i++) {

                            JSONObject object = jArrayVoucher.getJSONObject(i);

                            String chip = object.getString("chip");
                            String bip = object.getString("bip");

                            BipContent.ITEMS_BIP.add(new BipContent.BipItem(chip, bip));


                        }

                        mAdapter = new AdapterListBipClubMetro(getActivity(),
                                R.layout.list_item_bip,
                                R.id.txtBip, BipContent.ITEMS_BIP);

                        mListView.setAdapter(mAdapter);
                        if (BipContent.ITEMS_BIP.size() == 0) {
                            mListView.setEmptyView(emptyText);
                        }


                        mListView.setVisibility(View.VISIBLE);

                    } else {
                        mListView.setVisibility(View.GONE);
                        mListView.setEmptyView(emptyText);
                    }
//                    else {
//
//                        setEmptyText("Aún no tiene cupones asociados.");
//                        contentList.setVisibility(View.GONE);
//                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void failure(RetrofitError error) {

                dialog.dismiss();
                Utils.createAlertDialog(getActivity(), "Ocurrio un error inesperado, favor intente nuevamente.");
            }
        });


    }

}
