package cl.metro.mobile.Fragments.ClubMetro.Voucher;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cl.metro.mobile.R;
import cl.metro.mobile.Retrofit.ApiService;
import cl.metro.mobile.Utilities.AdapterClubMetro.AdapterListVoucher;
import cl.metro.mobile.Utilities.Utils;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class ListCupones extends Fragment implements AbsListView.OnItemClickListener {

    public List<VoucherContent> ITEMS_DISPONIBLES = new ArrayList<>();
    public List<VoucherContent> ITEMS_UTILIZADOS = new ArrayList<>();
    /**
     * The fragment's ListView/GridView.
     */
    private AbsListView mListView_utilizados, mListView_disponibles;
    /**
     * The Adapter which will be used to populate the ListView/GridView with
     * Views.
     */
    private ListAdapter mAdapter_disponibles, mAdapter_utilizados;
    private ProgressBar progressBar;
    private LinearLayout contentList;
    private TextView emptyText_disponibles, emptyText_utilizados;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ListCupones() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        // TODO: Change Adapter to display your content


    }

    public void getListVoucher() {

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(Utils.URL_BASE_METRO)
                .build();

        ApiService apiService = restAdapter.create(ApiService.class);
//        409338
//        Utils.getFromPrefs(getActivity(), Utils.PREFS_LOGIN_ID_USER, "")
        int aNumber = (int) (20 * Math.random()) + 1;
        apiService.getListVoucher(Utils.getFromPrefs(getActivity(), Utils.PREFS_LOGIN_ID_USER, ""), aNumber, new Callback<Response>() {
            @Override
            public void success(Response response, Response response2) {

                try {

                    ITEMS_UTILIZADOS.clear();
                    ITEMS_DISPONIBLES.clear();

                    progressBar.setVisibility(View.GONE);
                    JSONObject result = new JSONObject(Utils.convertResponseBody(response));

                    int estado = result.getInt("estado");
                    if (result.has("vouchers") || estado != 0) {
                        JSONArray jArrayVoucher = result.getJSONArray("vouchers");

                        for (int i = 0; i < jArrayVoucher.length(); i++) {

                            JSONObject object = jArrayVoucher.getJSONObject(i);

                            int idVoucher = object.getInt("idVoucher");
                            String voucher = object.getString("voucher");
                            String fechaRegistro = object.getString("fechaRegistro");
                            int idPromocion = object.getInt("idPromocion");
                            String fechaCobro = object.getString("fechaCobro");

                            if (idPromocion != 0) {
                                ITEMS_UTILIZADOS.add(new VoucherContent(idVoucher, voucher, fechaRegistro, idPromocion, fechaCobro));
                            } else {
                                ITEMS_DISPONIBLES.add(new VoucherContent(idVoucher, voucher, fechaRegistro, idPromocion, fechaCobro));
                            }


                        }

//                        mAdapter_disponibles = new AdapterListVoucher(getActivity(),
//                                R.layout.list_item_club_metro,
//                                R.id.text_item, ITEMS_DISPONIBLES);

                        mAdapter_disponibles = new AdapterListVoucher(getActivity(),
                                R.layout.item_cupon,
                                R.id.txtVoucher, ITEMS_DISPONIBLES);

                        mAdapter_utilizados = new AdapterListVoucher(getActivity(),
                                R.layout.item_cupon,
                                R.id.txtVoucher, ITEMS_UTILIZADOS);


                        mListView_disponibles.setAdapter(mAdapter_disponibles);
                        if (ITEMS_DISPONIBLES.size() == 0) {
                            mListView_disponibles.setEmptyView(emptyText_disponibles);
                        }

                        mListView_utilizados.setAdapter(mAdapter_utilizados);
                        if (ITEMS_UTILIZADOS.size() == 0) {
                            mListView_utilizados.setEmptyView(emptyText_utilizados);
                        }


                        contentList.setVisibility(View.VISIBLE);

                    } else {
                        contentList.setVisibility(View.VISIBLE);
                        mListView_disponibles.setEmptyView(emptyText_disponibles);
                        mListView_utilizados.setEmptyView(emptyText_utilizados);
                    }
//                    else {
//
//                        setEmptyText("Aún no tiene cupones asociados.");
//                        contentList.setVisibility(View.GONE);
//                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    progressBar.setVisibility(View.GONE);
                    contentList.setVisibility(View.VISIBLE);
                    mListView_disponibles.setEmptyView(emptyText_disponibles);
                    mListView_utilizados.setEmptyView(emptyText_utilizados);
                }


            }

            @Override
            public void failure(RetrofitError error) {

            }
        });

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_cupon, container, false);

        progressBar = (ProgressBar) view.findViewById(R.id.progressBar15);
        contentList = (LinearLayout) view.findViewById(R.id.contentCupones);

        // Set the adapter
        mListView_utilizados = (AbsListView) view.findViewById(R.id.listView_utilizados);
        mListView_disponibles = (AbsListView) view.findViewById(R.id.listView_disponibles);

//        ((AdapterView<ListAdapter>) mListView).setAdapter(mAdapter);

        // Set OnItemClickListener so we can be notified on item clicks
//        mListView.setOnItemClickListener(this);

//        TextView emptyText = (TextView) view.findViewById(android.R.id.empty);
        emptyText_disponibles = (TextView) view.findViewById(R.id.textView92);
        emptyText_utilizados = (TextView) view.findViewById(R.id.textView91);

        Button btnAgregarBip = (Button) view.findViewById(R.id.btnAgregarBip);
        btnAgregarBip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getActivity(), AgregarBip.class);
                getActivity().startActivity(intent);

            }
        });

        Button btnIngresarCupon = (Button) view.findViewById(R.id.btnIngresarCupon);
        btnIngresarCupon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getActivity(), IngresarVoucher.class);
                getActivity().startActivity(intent);

            }
        });


        return view;
    }

    @Override
    public void onStart() {
        super.onStart();


        getListVoucher();
    }



    @Override
    public void onResume() {
        super.onResume();

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Billetera Virtual");


        Log.d("LOG", "onResume");

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//        if (null != mListener) {
        // Notify the active callbacks interface (the activity, if the
        // fragment is attached to one) that an item has been selected.
//            mListener.onFragmentInteraction(DummyContent.ITEMS.get(position).id);
//        }
    }

    /**
     * The default content for this Fragment has a TextView that is shown when
     * the list is empty. If you would like to change the text, call this method
     * to supply the text it should use.
     */
    public void setEmptyText(CharSequence emptyText) {
        View emptyView = mListView_disponibles.getEmptyView();

        if (emptyView instanceof TextView) {
            ((TextView) emptyView).setText(emptyText);
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
//    public interface OnFragmentInteractionListener {
//        // TODO: Update argument type and name
//        public void onFragmentInteraction(String id);
//    }

}
