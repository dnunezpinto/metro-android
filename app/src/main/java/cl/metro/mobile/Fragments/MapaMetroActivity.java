package cl.metro.mobile.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import cl.metro.mobile.Fragments.FragmentServicios.ServiciosFragment;
import cl.metro.mobile.MainActivity;
import cl.metro.mobile.Models.Estaciones;
import cl.metro.mobile.R;

public class MapaMetroActivity extends Fragment {

    String codigoEstacionSelected = "";
    WebViewClient mWebClient = new WebViewClient() {

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {

            codigoEstacionSelected = url.replace("file:///android_asset/www/", "");

            Estaciones estacion = MainActivity.initApp.getEstacionByCodigo(codigoEstacionSelected);

            Fragment newFragment = new ServiciosFragment();
            ServiciosFragment.estacion = estacion;
            ServiciosFragment.verServicio = true;
            ServiciosFragment.lastIdLinea = estacion.getIdLinea();

            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            transaction.replace(R.id.container, newFragment);
            transaction.addToBackStack(null);
            transaction.commit();


            return true;
        }
    };


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.activity_mapa_metro, container, false);

        WebView imgWV = (WebView) rootView.findViewById(R.id.WVsnoop);
        imgWV.setBackgroundColor(0xffffffff);
        imgWV.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        imgWV.getSettings().setLoadWithOverviewMode(true);
        imgWV.getSettings().setUseWideViewPort(true);
        imgWV.getSettings().setBuiltInZoomControls(true);
        imgWV.getSettings().setSupportZoom(true);
        imgWV.setWebViewClient(mWebClient);
        imgWV.loadUrl("file:///android_asset/www/plano.html");

        return rootView;
    }

}