package cl.metro.mobile.Fragments.FragmentServicios;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import cl.metro.mobile.MainActivity;
import cl.metro.mobile.Models.Estaciones;
import cl.metro.mobile.Models.ServiciosCercanos.CategoriaParse;
import cl.metro.mobile.Models.ServiciosCercanos.Elementos;
import cl.metro.mobile.Models.ServiciosCercanos.ServiciosCercanosParse;
import cl.metro.mobile.R;

/**
 * Created by Daniel on 24-02-2015.
 */
public class ServicioMapaFragment extends Fragment implements OnMapReadyCallback {

    public static String currentEstacion;
    private static String codigoEstacion;
    private static String nombreEstacion;
    private static int idLinea;
    private static View rootView;
    MapView mapView;
    SupportMapFragment mMapFragment;
    private Activity mActivity;
    private MenuItem agregarFavorito;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        mActivity = activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);

        codigoEstacion = getArguments().getString("codigoEstacion");
        nombreEstacion = getArguments().getString("nombreEstacion");
        idLinea = getArguments().getInt("idLinea");

    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

//        ((ActionBarActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        rootView = inflater.inflate(R.layout.fragment_servicio_detalle, container, false);

        TextView tvNombreEstacion = (TextView) rootView.findViewById(R.id.nombreEstacion);
        tvNombreEstacion.setText(nombreEstacion);
        MainActivity.initApp.setEstacionImage(idLinea, tvNombreEstacion, getActivity());

        mapView = rootView.findViewById(R.id.map);
        mapView.onCreate(savedInstanceState);

        if (mapView != null) {
            mapView.getMapAsync(this);
        }

        MainActivity.mToolbar.setTitle("Servicios Entorno");

        if (currentEstacion != codigoEstacion) {
            currentEstacion = codigoEstacion;
            MainActivity.initApp.arrayFiltroMapa.clear();
        }

        Button btnSeleccionarRubros = (Button) rootView.findViewById(R.id.btnSeleccionarRubros);
        btnSeleccionarRubros.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Bundle args = new Bundle();
                Fragment fragment = new ServicioComercioCategoriasFragment();
                args.putString("codigoEstacion", codigoEstacion);
                args.putString("nombreEstacion", nombreEstacion);
                args.putInt("idLinea", idLinea);
                fragment.setArguments(args);


                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.beginTransaction()
                        .addToBackStack(null)
                        .replace(R.id.container, fragment)
                        .commit();


            }
        });


        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Servicios");

        mapView.onResume();

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        mapView.onDestroy();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        inflater.inflate(R.menu.global_favoritos, menu);

        agregarFavorito = menu.findItem(R.id.action_favorite);

        MainActivity.initApp.validarFavorito(agregarFavorito);

        super.onCreateOptionsMenu(menu, inflater);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

//        if (id == R.id.action_settings) {
//            return true;
//        }

        if (id == R.id.action_favorite) {

            Estaciones estacion = MainActivity.initApp.getEstacionForNombreEstacionAndIdLinea("" + MainActivity.initApp.currentSelectedEstacion.getIdLinea(),
                    MainActivity.initApp.currentSelectedEstacion.getNombreEstacion());
            if (estacion.isFavorito()) {
                Toast.makeText(getActivity(), "Estación " + estacion.getNombreEstacion().toString() + " borrada de Favoritos", Toast.LENGTH_LONG).show();
                MainActivity.initApp.setFavorito("" + estacion.getIdLinea(), estacion.getNombreEstacion(), false, agregarFavorito);
            } else {
                Toast.makeText(getActivity(), "Estación " + estacion.getNombreEstacion().toString() + " agregada a Favoritos", Toast.LENGTH_LONG).show();
                MainActivity.initApp.setFavorito("" + estacion.getIdLinea(), estacion.getNombreEstacion(), true, agregarFavorito);
            }

        }


        return super.onOptionsItemSelected(item);
    }

    private void addMarkerMap(GoogleMap mMap) {
        ServiciosCercanosParse scp = MainActivity.initApp.serviciosCercanosParse;

        mMap.clear();
        if (scp != null && scp.getCategoria() != null) {
            for (CategoriaParse categorio : scp.getCategoria()) {
                for (Elementos element : categorio.getElementos()) {
                    for (String elementFiltro : MainActivity.initApp.arrayFiltroMapa) {

//                        Log.i("App", "ElementoFiltro: " + element.getRubro());

                        if (elementFiltro.equalsIgnoreCase(element.getRubro())) {

                            String[] array = element.getCoordenadas().split("[,]");
                            element.setLatitud(Double.parseDouble(array[0]));
                            element.setLongitud(Double.parseDouble(array[1]));
                            MarkerOptions marker = new MarkerOptions();
                            marker.position(new LatLng(element.getLatitud(), element.getLongitud()));
                            marker.title(element.getNombreFantasia());
                            marker.snippet(element.getDireccion() + " " + element.getNumero());
                            mMap.addMarker(marker);


//                            Log.i("App", "nombre: " + element.getNombreFantasia());
//                            Log.i("App", "latlng: " + element.getLatitud() + ", " + element.getLongitud());
//                            Log.i("App", "latlng: " + element.getCoordenadas());

                        }
                    }
                }
            }
        }

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        MapsInitializer.initialize(mActivity);

        if (MainActivity.initApp.currentSelectedEstacion != null) {

            double lat = MainActivity.initApp.currentSelectedEstacion.getLatitud();
            double lng = MainActivity.initApp.currentSelectedEstacion.getLongitud();

            LatLng positionEstacion = new LatLng(lat, lng);
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(positionEstacion, 15));

        }

        if (MainActivity.initApp.arrayFiltroMapa != null && MainActivity.initApp.arrayFiltroMapa.size() > 0) {
            addMarkerMap(googleMap);
        }


    }


}
