package cl.metro.mobile.Fragments.Notificaciones;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ToggleButton;

import cl.metro.mobile.MainActivity;
import cl.metro.mobile.R;
import cl.metro.mobile.Retrofit.ApiService;
import cl.metro.mobile.Utilities.EnunSharedPreference;
import cl.metro.mobile.Utilities.SharedPreferenceMetro;
import cl.metro.mobile.Utilities.Utils;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class NotificacionesDiasFragment extends Fragment implements OnClickListener {

    private ToggleButton toggleButtonLunes;
    private ToggleButton toggleButtonMartes;
    private ToggleButton toggleButtonMiercoles;
    private ToggleButton toggleButtonJueves;
    private ToggleButton toggleButtonViernes;
    private ToggleButton toggleButtonSabado;
    private ToggleButton toggleButtonDomingo;


    public NotificacionesDiasFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_notificaciones_dias, container, false);

        ((Button) rootView.findViewById(R.id.button_guardar_preferencias))
                .setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        connection();

                    }
                });

        setPreferences(rootView);


        return rootView;
    }

    void setPreferences(View rootView) {
        SharedPreferenceMetro preferences_metro = new SharedPreferenceMetro(
                getActivity());
        SharedPreferences preferences = preferences_metro.getPrefenrenceMetro();
        boolean is_checked;
        is_checked = preferences.getBoolean(
                EnunSharedPreference.diaLunes.getText(), true);
        toggleButtonLunes = ((ToggleButton) rootView.findViewById(R.id.toggle_lunes));
        toggleButtonLunes.setChecked(is_checked);
        toggleButtonLunes.setOnClickListener(this);

        is_checked = preferences.getBoolean(
                EnunSharedPreference.diaMartes.getText(), true);
        toggleButtonMartes = ((ToggleButton) rootView.findViewById(R.id.toggle_martes));
        toggleButtonMartes.setChecked(is_checked);
        toggleButtonMartes.setOnClickListener(this);
        is_checked = preferences.getBoolean(
                EnunSharedPreference.diaMiercoles.getText(), true);
        toggleButtonMiercoles = ((ToggleButton) rootView.findViewById(R.id.toggle_miercoles));
        toggleButtonMiercoles.setChecked(is_checked);
        toggleButtonMiercoles.setOnClickListener(this);
        is_checked = preferences.getBoolean(
                EnunSharedPreference.diaJueves.getText(), true);
        toggleButtonJueves = ((ToggleButton) rootView.findViewById(R.id.toggle_jueves));
        toggleButtonJueves.setChecked(is_checked);
        toggleButtonJueves.setOnClickListener(this);

        is_checked = preferences.getBoolean(
                EnunSharedPreference.diaViernes.getText(), true);
        toggleButtonViernes = ((ToggleButton) rootView.findViewById(R.id.toggle_viernes));
        toggleButtonViernes.setChecked(is_checked);
        toggleButtonViernes.setOnClickListener(this);

        is_checked = preferences.getBoolean(
                EnunSharedPreference.diaSabado.getText(), true);
        toggleButtonSabado = ((ToggleButton) rootView.findViewById(R.id.toggle_sabado));
        toggleButtonSabado.setChecked(is_checked);
        toggleButtonSabado.setOnClickListener(this);

        is_checked = preferences.getBoolean(
                EnunSharedPreference.diaDomingo.getText(), true);
        toggleButtonDomingo = ((ToggleButton) rootView.findViewById(R.id.toggle_domingo));
        toggleButtonDomingo.setChecked(is_checked);
        toggleButtonDomingo.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {

        boolean is_checked = ((ToggleButton) v).isChecked();
        SharedPreferenceMetro preferences = new SharedPreferenceMetro(getActivity());
        switch (v.getId()) {
            case R.id.toggle_lunes:
                preferences.setPreferenceDias(EnunSharedPreference.diaLunes,
                        is_checked);
                // Toast.makeText(getApplicationContext(),"Set lunes "+is_checked,
                // Toast.LENGTH_SHORT).show();
                break;
            case R.id.toggle_martes:
                preferences.setPreferenceDias(EnunSharedPreference.diaMartes,
                        is_checked);
                break;
            case R.id.toggle_miercoles:
                preferences.setPreferenceDias(EnunSharedPreference.diaMiercoles,
                        is_checked);
                break;
            case R.id.toggle_jueves:
                preferences.setPreferenceDias(EnunSharedPreference.diaJueves,
                        is_checked);
                break;
            case R.id.toggle_viernes:
                preferences.setPreferenceDias(EnunSharedPreference.diaViernes,
                        is_checked);
                break;
            case R.id.toggle_sabado:
                preferences.setPreferenceDias(EnunSharedPreference.diaSabado,
                        is_checked);
                break;
            case R.id.toggle_domingo:
                preferences.setPreferenceDias(EnunSharedPreference.diaDomingo,
                        is_checked);
                break;
        }

    }

    private void connection() {

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(Utils.URL_BASE_INTERMOVIL)
                .build();

        ApiService apiService = restAdapter.create(ApiService.class);

        apiService.setConfigNotificacionesDiasSemana(Settings.Secure.getString(getActivity().getContentResolver(), Settings.Secure.ANDROID_ID),
                MainActivity.getTokenId(getActivity()),
                (toggleButtonLunes.isChecked() ? "1" : "0"),
                (toggleButtonMartes.isChecked() ? "1" : "0"),
                (toggleButtonMiercoles.isChecked() ? "1" : "0"),
                (toggleButtonJueves.isChecked() ? "1" : "0"),
                (toggleButtonViernes.isChecked() ? "1" : "0"),
                (toggleButtonSabado.isChecked() ? "1" : "0"),
                (toggleButtonDomingo.isChecked() ? "1" : "0"),
                "android", new Callback<Response>() {
                    @Override
                    public void success(Response response, Response response2) {


                        Log.i("App", Utils.convertResponseBody(response));

                        if (response.equals("{}")) {
                            Utils.createAlertDialog(getActivity(), "No se ha podido guardar sus preferencias, intente nuevamente.");
                        } else {
                            Utils.createAlertDialog(getActivity(), "Preferencias Guardadas.");
                        }

                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Utils.createAlertDialog(getActivity(), "No se ha podido guardar sus preferencias, intente nuevamente.");

                    }
                });


    }
}
