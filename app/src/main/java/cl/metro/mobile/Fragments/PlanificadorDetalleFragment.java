package cl.metro.mobile.Fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.journeyapps.barcodescanner.Util;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import cl.metro.mobile.MainActivity;
import cl.metro.mobile.Models.Planificador;
import cl.metro.mobile.Models.PlanificadorMetro;
import cl.metro.mobile.Models.ResponseParse;
import cl.metro.mobile.Models.Ruta;
import cl.metro.mobile.R;
import cl.metro.mobile.Retrofit.ApiService;
import cl.metro.mobile.Retrofit.RestClient;
import cl.metro.mobile.Utilities.AdaptersPlanificador.AdapterPlanifiqueSection;
import cl.metro.mobile.Utilities.Utils;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Daniel on 05-02-2015.
 */

public class PlanificadorDetalleFragment extends Fragment {


    private static final String ARG_SECTION_NUMBER = "section_number";
    private static ListView listView;
    private String codigoEstacionOrigen;
    private String codigoEstacionDestino;
    private String tipoDia;
    private String horaEstimada;
    private TextView tiempoEstimado;
    private LinearLayout header;
    private ProgressBar progressBar;
    private boolean verPlanificador = false;
    private PlanificadorMetro planificadorMetro;
    private MenuItem agregarFavorito;
    private OnFragmentInteractionListener mListener;

    public PlanificadorDetalleFragment() {
    }

    public PlanificadorDetalleFragment newInstance(int sectionNumber) {

        PlanificadorDetalleFragment fragment = new PlanificadorDetalleFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            this.mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

//        ((MainActivity)getActivity()).resetActionBar(true,
//                DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

//        ((ActionBarActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        codigoEstacionOrigen = getArguments().getString("codigoEstacionOrigen");
        codigoEstacionDestino = getArguments().getString("codigoEstacionDestino");
        tipoDia = getArguments().getString("tipoDia");
        horaEstimada = getArguments().getString("horaEstimada");

        planificadorMetro = new PlanificadorMetro();
        planificadorMetro.setEstacionOrigen(codigoEstacionOrigen);
        planificadorMetro.setEstacionDestino(codigoEstacionDestino);
        planificadorMetro.setTipoDia(tipoDia);
        planificadorMetro.setHoraDia(horaEstimada);

//        if(!verPlanificador){
//            Log.i("App", "Estado ver: " + verPlanificador);
//        }


//        String stringUrl = Utils.URL_PLANIFICA;
//        stringUrl += codigoEstacionOrigen + "/"
//                + codigoEstacionDestino + "/" + tipoDia + "/"
//                + horaEstimada;
//        new HttpAsyncTask().execute(stringUrl);
//        Log.d("LOG", stringUrl);

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        inflater.inflate(R.menu.global_favoritos, menu);

        agregarFavorito = menu.findItem(R.id.action_favorite);

        validateFavorito();

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:

                getFragmentManager().popBackStack();
                getActivity().getActionBar().setDisplayHomeAsUpEnabled(false);

//                Intent parentActivityIntent = new Intent(getActivity(), MainActivity.class);
//
//                parentActivityIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
//                startActivity(parentActivityIntent);
//                getActivity().finish();

                return true;

            case R.id.action_favorite:
                if (planificadorMetro.isFavorito()) {

                    Toast.makeText(getActivity(), "Borrando de Favoritos Recorrido: " + planificadorMetro.getNombre(), Toast.LENGTH_LONG).show();

                    MainActivity.initApp.borrarPlanificador(planificadorMetro);
                    agregarFavorito.setIcon(R.drawable.ic_star_outline_white_36dp);

                } else {

                    createDialog();
                }
                return true;
            // Other case statements...

            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

//        FragmentActivity activity = getActivity();
//        ((MainActivity) activity).setOnBackPressedListener(new BaseBackPressedListener(activity));

        View rootView = inflater.inflate(R.layout.fragment_planificador_detalle, container, false);

        //Cambia de color rojo el actionbar/toolbar
        Utils.toolbarRed(getActivity());

        header = (LinearLayout) rootView.findViewById(R.id.headerLayout);
        listView = (ListView) rootView.findViewById(R.id.listPlanificador);
        tiempoEstimado = (TextView) rootView.findViewById(R.id.tiempoEstimado);
        progressBar = (ProgressBar) rootView.findViewById(R.id.progressBar4);


//        MainActivity.initApp.getPlanificadorParse().getArrayResponseRuta();
//Log.d("LOG", MainActivity.initApp.getPlanificadorParse().getDistanciaTotal());

//        init();

        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();

//        if(!verPlanificador && MainActivity.initApp.planificadorParse != null){
//
//                tiempoEstimado.setText("Tiempo estimado: " + MainActivity.initApp.getPlanificadorParse().getTiempoTotal() + " min.");
//                AdapterPlanifiqueSection section_planifique = new AdapterPlanifiqueSection(MainActivity.initApp.getPlanificadorParse().getArrayResponseRuta(), getActivity());
//                listView.setAdapter(section_planifique);
//
//                header.setVisibility(View.VISIBLE);
//                listView.setVisibility(View.VISIBLE);
//                progressBar.setVisibility(View.GONE);
//        }else{

        init();

//        }

    }

    @Override
    public void onPause() {
        super.onPause();

        verPlanificador = true;

    }

    @Override
    public void onResume() {
        super.onResume();
        mListener.showDrawerToggle(false);

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(getString(R.string.title_section2));

    }

    private void validateFavorito() {

        List<PlanificadorMetro> listitems = MainActivity.initApp.getAllPlanificadoresMetro();
//        toggleFavorito.setChecked(false);
        if (listitems != null && listitems.size() > 0) {
            for (int i = 0; i < listitems.size(); i++) {


//                Log.i("App", "Planificador destino: " + planificadorMetro.getEstacionDestino());
//                Log.i("App", "List comparador destino: " + listitems.get(i).getEstacionDestino());
//
//                Log.i("App", "Planificador Hora: " + planificadorMetro.getHoraDia());
//                Log.i("App", "List comparador Hora: " + listitems.get(i).getHoraDia());


                if (planificadorMetro.getEstacionDestino().equalsIgnoreCase(listitems.get(i).getEstacionDestino()) &&
                        planificadorMetro.getEstacionOrigen().equalsIgnoreCase(listitems.get(i).getEstacionOrigen()) &&
                        planificadorMetro.getHoraDia().equalsIgnoreCase(listitems.get(i).getHoraDia()) &&
                        planificadorMetro.getTipoDia().equalsIgnoreCase(listitems.get(i).getTipoDia())) {

//                    planificadorMetro.getTiempo().compareTo(listitems.get(i).getTiempo())== 0 &&
                    planificadorMetro.setId(listitems.get(i).getId());
                    planificadorMetro.setFavorito(listitems.get(i).isFavorito());
                    agregarFavorito.setIcon(R.drawable.ic_star_white_36dp);
                    break;

                } else {
                    Log.i("App", "False");
                }

            }
        }
    }

    public void init() {

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint("http://www.metro.cl/api")
                .build();

        ApiService service = restAdapter.create(ApiService.class);

        service.getPlanificador(codigoEstacionOrigen, codigoEstacionDestino, new Callback<Response>() {
            @Override
            public void success(Response result, Response response) {

                verPlanificador = true;

                if(response.getBody() != null){
                    //Try to get response body
                    BufferedReader reader;
                    StringBuilder sb = new StringBuilder();
                    try {

                        reader = new BufferedReader(new InputStreamReader(response.getBody().in()));

                        String line;

                        while ((line = reader.readLine()) != null) {
                            sb.append(line);
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                    String output = sb.toString();
                    planificar(output);
                }else{
                    Utils.showAlert(getActivity(), "No se ha encontrado la planificación del viaje, porfavor intenta mas tarde!");
                    getActivity().onBackPressed();
                }


            }

            @Override
            public void failure(RetrofitError error) {

            }
        });


//"/includes/ws/planificador/{estacionOrigen}/{estacionDestino}/{dia}/{hora}
//        String stringUrl = Utils.URL_BASE_INTERMOVIL+"/includes/ws/planificador/";
//            stringUrl += codigoEstacionOrigen + "/"
//                    + codigoEstacionDestino + "/" + tipoDia + "/"
//                    + horaEstimada;
////            new HttpAsyncTask().execute(stringUrl);
//            Log.d("LOG", stringUrl);


    }

    public void planificar(String response) {

        try {
            Planificador planificador = new Planificador();

            Log.d("LOG", "Planificador json: " + response);

            JSONObject result = new JSONObject(response);
            JSONArray rutas = result.getJSONArray("rutas");

            List<ResponseParse> arrayResponseRuta = new ArrayList<>();

            for (int i = 0; i < rutas.length(); i++) {

                ResponseParse responseParse = new ResponseParse();

                Ruta ruta = new Ruta();

                JSONObject jsonRuta = rutas.getJSONObject(i);
                JSONObject item = jsonRuta.getJSONObject("ruta");


                ruta.setTipo(item.getString("tipo"));
                ruta.setEstacionLineaNombre(item.getString("estacion-linea-nombre"));
                ruta.setEstacionLinea(item.getString("estacion-linea"));

                if (item.has("estacion-nombre")) {

                    ruta.setEstacionNombre(item.getString("estacion-nombre"));

                }
                if (item.has("movilidad-reducida")) {

                    ruta.setMovilidadReducida(item.getBoolean("movilidad-reducida"));

                }
                if (item.has("movilidad-reducida-aternativas")) {


                    JSONArray movilidadAlternativas = item.getJSONArray("movilidad-reducida-aternativas");
                    String[] alternativas = new String[movilidadAlternativas.length()];
                    for (int j = 0; j < movilidadAlternativas.length(); j++) {
                        alternativas[j] = movilidadAlternativas.getString(j);
                    }

                    ruta.setMovilidadReducidaAternativas(alternativas);

                }
                if (item.has("direccion")) {

                    ruta.setDireccion(item.getString("direccion"));
                    Log.d("LOG", item.getString("direccion"));

                }
                if (item.has("estacion-bajada")) {

                    ruta.setEstacionBajada(item.getString("estacion-bajada"));

                }
                if (item.has("tiempo")) {

                    ruta.setTiempo(item.getString("tiempo"));

                }
                if (item.has("distancia")) {

                    ruta.setDistancia(item.getString("distancia"));

                }
                if (item.has("is-combinacion")) {

                    ruta.setCombinacion(item.getBoolean("is-combinacion"));

                }

                responseParse.setRuta(ruta);
                arrayResponseRuta.add(responseParse);

            }

            planificador.setDistanciaTotal(result.getString("distancia-total"));
            planificador.setTiempoTotal(result.getString("tiempo-total"));
            planificador.setArrayResponseRuta(arrayResponseRuta);
            MainActivity.initApp.setPlanificadorParse(planificador);
            MainActivity.initApp.planificadorParse = planificador;

        } catch (Exception e) {
            e.printStackTrace();
        }


        if (MainActivity.initApp.planificadorParse != null) {

            tiempoEstimado.setText("Tiempo estimado: " + MainActivity.initApp.getPlanificadorParse().getTiempoTotal() + " min.");
            AdapterPlanifiqueSection section_planifique = new AdapterPlanifiqueSection(MainActivity.initApp.getPlanificadorParse().getArrayResponseRuta(), getActivity());
            listView.setAdapter(section_planifique);

            header.setVisibility(View.VISIBLE);
            listView.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);
        }


    }


    private void createDialog() {

        final AlertDialog builder = new AlertDialog.Builder(getActivity()).create();
        builder.setCancelable(true);
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_nombre, null);

        final EditText text = (EditText) v.findViewById(R.id.editText3);
        Button btnAceptar = (Button) v.findViewById(R.id.button4);

        btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                String nombre = text.getText().toString();
                if (!nombre.equals("") && !nombre.isEmpty()) {
                    text.setError(null);
                    planificadorMetro.setNombre(nombre);
                    planificadorMetro.setFavorito(true);
                    int insert = (int) MainActivity.initApp.insertRecorridoFavorito(planificadorMetro);
                    if (insert != -1) {

                        planificadorMetro.setId(insert);

                        agregarFavorito.setIcon(R.drawable.ic_star_white_36dp);

                    } else {
                        // Error al hacer el insert
                        Toast.makeText(
                                getActivity(),
                                "Error al guardar el recorrido",
                                Toast.LENGTH_SHORT).show();
                    }

                } else {

                    text.setError("Ingrese Nombre del Recorrido");

//                    Toast.makeText(getActivity(),
//                            "Debe ingresar un nombre para el recorrido",
//                            Toast.LENGTH_LONG).show();

//            toggleFavorito.setChecked(false);
                }


                builder.dismiss();


            }
        });


        builder.setView(v);
        builder.show();
//        return builder.show();
    }

//    public void setFavoritoRecorrido() {
//
//        planificadorMetro.setFavorito(true);
//
//    }

    public interface OnFragmentInteractionListener {
        public void showDrawerToggle(boolean showDrawerToggle);
    }

//    public class HttpAsyncTask extends AsyncTask<String, Void, String> {
//
//        @Override
//        protected String doInBackground(String... urls) {
//
//            return Utils.GET(urls[0]);
//
//
//        }
//
//        @Override
//        protected void onPostExecute(String result) {
//            try {
//                planificar(new JSONObject(result));
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//
//        }
//
//    }


}
