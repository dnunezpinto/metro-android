package cl.metro.mobile.Fragments.ClubMetro;

import java.util.ArrayList;
import java.util.List;

/**
 * Helper class for providing sample content for user interfaces created by
 * Android template wizards.
 * <p/>
 * TODO: Replace all uses of this class before publishing your app.
 */
public class BipContent {

    /**
     * An array of sample (dummy) items.
     */
    public static List<BipItem> ITEMS_BIP = new ArrayList<>();


    private static void addItem(BipItem item) {
        ITEMS_BIP.add(item);
    }

    /**
     * A dummy item representing a piece of content.
     */
    public static class BipItem {
        public String id;
        public String _chip;
        public String _bip;

        public BipItem(String _chip, String _bip) {
            this._chip = _chip;
            this._bip = _bip;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String get_chip() {
            return _chip;
        }

        public void set_chip(String _chip) {
            this._chip = _chip;
        }

        public String get_bip() {
            return _bip;
        }

        public void set_bip(String _bip) {
            this._bip = _bip;
        }


    }
}
