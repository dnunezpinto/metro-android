package cl.metro.mobile.Fragments.FragmentServicios;


import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.util.List;

import cl.metro.mobile.MainActivity;
import cl.metro.mobile.Models.LugaresInteresEntretencion.DatosImagenesLugares;
import cl.metro.mobile.R;
import cl.metro.mobile.Utilities.ImagePageAdapter;

/**
 * A simple {@link Fragment} subclass.
 */
public class DetalleLugaresInteresEntretencion extends Fragment {

    public static List<DatosImagenesLugares> mImagenesValue;
    public static String imageSRC;
    public static ViewPager pagerDestacados;
    public String imagenSRC;
    private String mDescripcionValue;
    private String mDireccionValue;
    private String mHorarioValue;
    private String mTarifaValue;
    private ImagePageAdapter imagePageAdapter;
    private Activity mActivity;
    private ImageView displayImage;

    public DetalleLugaresInteresEntretencion() {
        // Required empty public constructor
    }

    public static DetalleLugaresInteresEntretencion newInstance(String mDescripcion,
                                                                String mDireccion,
                                                                String mHorario,
                                                                String mTarifa,
                                                                List<DatosImagenesLugares> imagenes) {

        DetalleLugaresInteresEntretencion fragment = new DetalleLugaresInteresEntretencion();

        DetalleLugaresInteresEntretencion.imageSRC = null;
        DetalleLugaresInteresEntretencion.mImagenesValue = imagenes;

        Bundle args = new Bundle();
        args.putString("Descripcion", mDescripcion);
        args.putString("Direccion", mDireccion);
        args.putString("Horario", mHorario);
        args.putString("Tarifa", mTarifa);
        fragment.setArguments(args);
        return fragment;
    }

    public static DetalleLugaresInteresEntretencion newInstance(String mDescripcion,
                                                                String mDireccion,
                                                                String mHorario,
                                                                String mTarifa,
                                                                String imagen) {

        DetalleLugaresInteresEntretencion fragment = new DetalleLugaresInteresEntretencion();

        DetalleLugaresInteresEntretencion.imageSRC = imagen;

        Bundle args = new Bundle();
        args.putString("Descripcion", mDescripcion);
        args.putString("Direccion", mDireccion);
        args.putString("Horario", mHorario);
        args.putString("Tarifa", mTarifa);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mDescripcionValue = getArguments().getString("Descripcion");
            mDireccionValue = getArguments().getString("Direccion");
            mHorarioValue = getArguments().getString("Horario");
            mTarifaValue = getArguments().getString("Tarifa");
        }

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        mActivity = activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_detalle_lugares_interes_entretencion, container, false);

        TextView mDescripcionView = (TextView) rootView.findViewById(R.id.mTextDescripcion);
        TextView mDireccionView = (TextView) rootView.findViewById(R.id.mTextUbicacion);
        TextView mHorarioView = (TextView) rootView.findViewById(R.id.mTextHorario);
        TextView mTarifaView = (TextView) rootView.findViewById(R.id.mTextTarifa);
        displayImage = (ImageView) rootView.findViewById(R.id.imageView7);
        pagerDestacados = (ViewPager) rootView.findViewById(R.id.pagerImagenes);

        mDescripcionView.setText(Html.fromHtml(mDescripcionValue));
        mDireccionView.setText(mDireccionValue);
        mHorarioView.setText(mHorarioValue);
        mTarifaView.setText(mTarifaValue);

        if (imageSRC != null) {
            loadImage();
        } else {
            imagePageAdapter = new ImagePageAdapter(mImagenesValue, mActivity);
            pagerDestacados.setAdapter(imagePageAdapter);
            pagerDestacados.setCurrentItem(0);
        }


        TextView tvNombreEstacion = (TextView) rootView.findViewById(R.id.nombreEstacion);
        tvNombreEstacion.setText(MainLugaresInteresEntretencion.nombreEstacion);
        MainActivity.initApp.setEstacionImage(MainLugaresInteresEntretencion.idLinea, tvNombreEstacion, getActivity());
//        tvNombreEstacion.setTextColor(Color.parseColor(Utils.COLORS[MainLugaresInteresEntretencion.idLinea - 1]));

        return rootView;
    }


    public void loadImage() {
        MainActivity.imageLoader.displayImage(imageSRC, displayImage, MainActivity.options, new SimpleImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {
//                progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                @SuppressWarnings("unused")
                String message = null;
                switch (failReason.getType()) {
                    case IO_ERROR:
                        message = "Input/Output error";
                        break;
                    case DECODING_ERROR:
                        message = "Image can't be decoded";
                        break;
                    case NETWORK_DENIED:
                        message = "Downloads are denied";
                        break;
                    case OUT_OF_MEMORY:
                        message = "Out Of Memory error";
                        break;
                    case UNKNOWN:
                        message = "Unknown error";
                        break;
                }

//                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
//                progressBar.setVisibility(View.GONE);
//                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(loadedImage.getWidth(), loadedImage.getHeight());
//                layoutParams.addRule(RelativeLayout.BELOW, R.id.linearLayout3);
//                DetalleLugaresInteresEntretencion.pagerDestacados.setLayoutParams(layoutParams);

            }
        });
    }


}
