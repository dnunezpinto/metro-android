package cl.metro.mobile.Fragments.FragmentServicios;


import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import cl.metro.mobile.MainActivity;
import cl.metro.mobile.Models.ExpresoData;
import cl.metro.mobile.R;
import cl.metro.mobile.Retrofit.ApiService;
import cl.metro.mobile.Utilities.Utils;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class ServicioOperacionExpreso extends Fragment {

    public static String nombreEstacion;
    public static int idLinea;
    private static String codigoEstacion;
    ExpresoData expresoDataA = new ExpresoData();
    ExpresoData expresoDataB = new ExpresoData();
    //    private ApiService apiService;
    //    private TextView mHoraInicioAMView, mHoraInicioPMView, mHoraFinAMView, mHoraFinPMView, mTipoEstacion, mValle_InicioView, mValle_FinView;
    private TextView mTipoEstacion;
    private TextView mSinExpreso;
    private LinearLayout mContentExpreso;
    private ProgressBar progressBar;
    private ToggleButton toggle_direccion;
    private TableLayout tablaExpresoHorario;
    private List<View> listViews;


    public ServicioOperacionExpreso() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        codigoEstacion = getArguments().getString("codigoEstacion");
        nombreEstacion = getArguments().getString("nombreEstacion");
        idLinea = getArguments().getInt("idLinea");

//        Gson gson = new GsonBuilder()
//                .excludeFieldsWithoutExposeAnnotation()
//                .create();
//        RestAdapter restAdapter = new RestAdapter.Builder()
//                .setConverter(new GsonConverter(gson))
//                .setLogLevel(RestAdapter.LogLevel.FULL)
//                .setEndpoint(Utils.URL_CLUB_METRO)
//                .build();
//        apiService = restAdapter.create(ApiService.class);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(cl.metro.mobile.R.layout.fragment_operacion_expreso, container, false);

        TextView tvNombreEstacion = (TextView) rootView.findViewById(cl.metro.mobile.R.id.nombreEstacion);
        tvNombreEstacion.setText(nombreEstacion);
        MainActivity.initApp.setEstacionImage(idLinea, tvNombreEstacion, getActivity());

        toggle_direccion = ((ToggleButton) rootView.findViewById(R.id.toggle_direccion_tren));

//        mHoraInicioAMView = (TextView) rootView.findViewById(cl.metro.mobile.R.id.hora_inicio_am);
//        mHoraInicioPMView = (TextView) rootView.findViewById(cl.metro.mobile.R.id.hora_inicio_pm);
//        mValle_InicioView = (TextView) rootView.findViewById(R.id.hora_inicio_valle);
//        mValle_FinView = (TextView) rootView.findViewById(R.id.hora_fin_valle);
//        mHoraFinAMView = (TextView) rootView.findViewById(cl.metro.mobile.R.id.hora_fin_am);
//        mHoraFinPMView = (TextView) rootView.findViewById(cl.metro.mobile.R.id.hora_fin_pm);
        mTipoEstacion = (TextView) rootView.findViewById(cl.metro.mobile.R.id.textView70);
        mSinExpreso = (TextView) rootView.findViewById(cl.metro.mobile.R.id.textView78);

        mContentExpreso = (LinearLayout) rootView.findViewById(cl.metro.mobile.R.id.llContentExpreso);
        progressBar = (ProgressBar) rootView.findViewById(cl.metro.mobile.R.id.progressBar10);

        tablaExpresoHorario = (TableLayout) rootView.findViewById(R.id.tableLayout);

        TextView txtTitleRuta = (TextView) rootView.findViewById(R.id.textView72);


        ImageView imageView = (ImageView) rootView.findViewById(cl.metro.mobile.R.id.imageView13);
        String imageUri = null;
        if (nombreEstacion.equalsIgnoreCase("Vespucio Norte")) {
            imageUri = "drawable://" + cl.metro.mobile.R.drawable.linea2expres_vespnorte;
        } else if (nombreEstacion.equalsIgnoreCase("La Cisterna")) {
            imageUri = "drawable://" + cl.metro.mobile.R.drawable.linea2expres_lacisterna;
        } else if (nombreEstacion.equalsIgnoreCase("Plaza de Puente Alto")) {
            imageUri = "drawable://" + cl.metro.mobile.R.drawable.linea4express_ptealto;
        } else if (nombreEstacion.equalsIgnoreCase("Tobalaba")) {
            imageUri = "drawable://" + cl.metro.mobile.R.drawable.linea4express_tobalaba;
        } else if (nombreEstacion.equalsIgnoreCase("Vicente Valdés")) {
            imageUri = "drawable://" + cl.metro.mobile.R.drawable.linea5express_vicentevaldes;
        } else if (nombreEstacion.equalsIgnoreCase("Plaza de Maipú")) {
            imageUri = "drawable://" + cl.metro.mobile.R.drawable.linea5express_plazamaipu;
        }

        if (idLinea == 2) {
            // Linea 2
            imageUri = "drawable://" + cl.metro.mobile.R.drawable.linea2expres_vespnorte;
            txtTitleRuta.setText("Ruta Expreso Línea 2");
        } else if (idLinea == 3) {
            // Linea 4
            imageUri = "drawable://" + cl.metro.mobile.R.drawable.linea4express_tobalaba;
            txtTitleRuta.setText("Ruta Expreso Línea 4");
        } else if (idLinea == 5) {
            //Linea 5
            imageUri = "drawable://" + cl.metro.mobile.R.drawable.linea5express_plazamaipu;
            txtTitleRuta.setText("Ruta Expreso Línea 5");
        }


        MainActivity.imageLoader.displayImage(imageUri, imageView);


        if (Utils.isNetworkAvailable(getActivity())) {

            getExpresoDesarrollo();

        } else {

            Utils.createAlertDialog(getActivity(), getActivity().getString(cl.metro.mobile.R.string.lost_connection));

        }


        return rootView;
    }

    private void initToggleDireccion() {

        if (expresoDataA != null || expresoDataB != null) {

            String direccionA = expresoDataA.getDireccion();
            String direccionB = expresoDataB.getDireccion();

            if (expresoDataA.getDireccion().equalsIgnoreCase("Puente Alto")) {
                direccionA = "Plaza de Puente Alto";
            } else if (expresoDataB.getDireccion().equalsIgnoreCase("Puente Alto")) {
                direccionB = "Plaza de Puente Alto";
            }

            if (direccionA.equalsIgnoreCase("Maipu")) {
                direccionA = "Plaza de Maipú";
            } else if (direccionB.equalsIgnoreCase("Maipu")) {
                direccionB = "Plaza de Maipú";
            }

            if (direccionA.equalsIgnoreCase("Vicente Valdes")) {
                direccionA = "Vicente Valdés";
            } else if (direccionB.equalsIgnoreCase("Vicente Valdes")) {
                direccionB = "Vicente Valdés";
            }

            setValuesToggleDireccion(direccionB, direccionA);
        }

        toggle_direccion.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                GlobalDataSource global = (GlobalDataSource)HorarioEstacionActivity.this.getApplication();
                if (isChecked) {
//                    setTextHorarioTren(global.horarioTrenEstacion.getTren().getEstacionA());
                    setDataText(expresoDataA);
                } else {
//                    setTextHorarioTren(global.horarioTrenEstacion.getTren().getEstacionB());
                    setDataText(expresoDataB);
                }


            }
        });
        toggle_direccion.setChecked(true);
        toggle_direccion.refreshDrawableState();
    }

    private void setValuesToggleDireccion(String valor1, String valor2) {
        toggle_direccion.setTextOn(valor2);
        toggle_direccion.setTextOff(valor1);
        toggle_direccion.refreshDrawableState();
    }

    @Override
    public void onResume() {
        super.onResume();

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Expreso");
        MainActivity.mDrawerList.setItemChecked(4, true);

    }


    public void getExpresoDesarrollo() {

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(Utils.URL_BASE_METRO)
                .build();

        ApiService apiService = restAdapter.create(ApiService.class);

        apiService.getEstacionExpreso(codigoEstacion, new Callback<Response>() {
            @Override
            public void success(Response responseJSON, Response response) {

                try {

                    listViews = new ArrayList<>();

                    JSONObject jsonObject = new JSONObject(Utils.convertResponseBody(responseJSON));
                    JSONArray jsonArrayExpreso = jsonObject.getJSONArray("expreso");

                    LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                    View lineaHeader = inflater.inflate(R.layout.fila_header, null);
                    View lineaRutas = inflater.inflate(R.layout.linea_header_rutas, null);

                    tablaExpresoHorario.addView(lineaHeader);

                    if (jsonArrayExpreso.length() > 0) {

                        mContentExpreso.setVisibility(View.VISIBLE);
                        progressBar.setVisibility(View.GONE);

                        if (!jsonArrayExpreso.getJSONObject(0).isNull("horarios")) {

                            tablaExpresoHorario.addView(lineaRutas);

                            for (int i = 0; i < jsonArrayExpreso.length(); i++) {

                                JSONObject items = jsonArrayExpreso.getJSONObject(i);

                                JSONObject horarios = items.getJSONObject("horarios");
                                List<ExpresoData> expresoDataList = new ArrayList<ExpresoData>();

                                Iterator<?> keys = horarios.keys();
                                while (keys.hasNext()) {

                                    ExpresoData data = new ExpresoData();

                                    String key = (String) keys.next();

                                    JSONObject jItem = horarios.getJSONObject(key);

                                    View lineaHorario = inflater.inflate(R.layout.fila_horario, null);
                                    TextView titleRow = (TextView) lineaHorario.findViewById(R.id.titleRow);
                                    titleRow.setText(key.toUpperCase());

                                    TextView txtRoja = (TextView) lineaHorario.findViewById(R.id.horario_inicio_rojo);
                                    TextView txtVerde = (TextView) lineaHorario.findViewById(R.id.horario_inicio_verde);
                                    TextView txtRojaFin = (TextView) lineaHorario.findViewById(R.id.horario_fin_rojo);
                                    TextView txtVerdeFin = (TextView) lineaHorario.findViewById(R.id.horario_fin_verde);

                                    txtRoja.setTextColor(ContextCompat.getColor(getActivity(), R.color.red));
                                    txtVerde.setTextColor(ContextCompat.getColor(getActivity(), R.color.verde));
                                    txtRojaFin.setTextColor(ContextCompat.getColor(getActivity(), R.color.red));
                                    txtVerdeFin.setTextColor(ContextCompat.getColor(getActivity(), R.color.verde));


                                    if (i == 0) {
                                        tablaExpresoHorario.addView(lineaHorario);
                                        listViews.add(lineaHorario);
                                    }

                                    lineaHorario.setTag(key.toUpperCase());
                                    data.setTituloHorario(key.toUpperCase());

                                    data.setInicioHorarioRoja(replaceStringEmtpy(jItem.getString("inicio_roja")));
                                    data.setInicioHorarioVerde(replaceStringEmtpy(jItem.getString("inicio_verde")));
                                    data.setFinHorarioRoja(replaceStringEmtpy(jItem.getString("fin_roja")));
                                    data.setFinHorarioVerde(replaceStringEmtpy(jItem.getString("fin_verde")));

                                    expresoDataList.add(data);

                                }


                                if (i == 0) {

                                    expresoDataA.setTipo(items.getString("tipo"));
                                    expresoDataA.setDireccion(items.getString("direccion"));
                                    expresoDataA.setExpreList(expresoDataList);
                                } else if (i == 1) {
                                    expresoDataB.setTipo(items.getString("tipo"));
                                    expresoDataB.setDireccion(items.getString("direccion"));
                                    expresoDataB.setExpreList(expresoDataList);
                                }


                            }

                        } else {

                            // Direccion B

                            JSONObject direccionB = jsonArrayExpreso.getJSONObject(1);
                            List<ExpresoData> expresoDataList_B = new ArrayList<>();
                            ExpresoData data_AM_B = new ExpresoData();


                            data_AM_B.setTituloHorario("AM");


                            expresoDataB.setTipo(direccionB.getString("tipo"));
                            expresoDataB.setDireccion(direccionB.getString("direccion"));

                            String[] mInicioAM_B = direccionB.getString("inicio_am").replace(" ", "").split("-");
                            data_AM_B.setInicioHorarioRoja(mInicioAM_B[0]);
                            data_AM_B.setInicioHorarioVerde(mInicioAM_B[1]);

                            String[] mFinAM_B = direccionB.getString("fin_am").replace(" ", "").split("-");
                            data_AM_B.setFinHorarioRoja(mFinAM_B[0]);
                            data_AM_B.setFinHorarioVerde(mFinAM_B[1]);

                            ExpresoData data_PM_B = new ExpresoData();
                            data_PM_B.setTituloHorario("PM");

                            String[] mInicioPM_B = direccionB.getString("inicio_pm").replace(" ", "").split("-");
                            data_PM_B.setInicioHorarioRoja(mInicioPM_B[0]);
                            data_PM_B.setInicioHorarioVerde(mInicioPM_B[1]);

                            String[] mFinPM_B = direccionB.getString("fin_pm").replace(" ", "").split("-");
                            data_PM_B.setFinHorarioRoja(mFinPM_B[0]);
                            data_PM_B.setFinHorarioVerde(mFinPM_B[1]);


                            expresoDataList_B.add(data_AM_B);
                            expresoDataList_B.add(data_PM_B);
                            expresoDataB.setExpreList(expresoDataList_B);

                            // Direccion A

                            JSONObject items = jsonArrayExpreso.getJSONObject(0);
                            List<ExpresoData> expresoDataList = new ArrayList<>();
                            ExpresoData data_AM = new ExpresoData();

                            View lineaHorario_AM = inflater.inflate(R.layout.fila_horario, null);
                            lineaHorario_AM.setTag("AM");

                            TextView titleRow_AM = (TextView) lineaHorario_AM.findViewById(R.id.titleRow);
                            titleRow_AM.setText("AM");

                            data_AM.setTituloHorario("AM");
                            expresoDataA.setTipo(items.getString("tipo"));
                            expresoDataA.setDireccion(items.getString("direccion"));

                            String[] mInicioAM = items.getString("inicio_am").replace(" ", "").split("-");
                            data_AM.setInicioHorarioRoja(mInicioAM[0]);
                            data_AM.setInicioHorarioVerde(mInicioAM[1]);

                            String[] mFinAM = items.getString("fin_am").replace(" ", "").split("-");
                            data_AM.setFinHorarioRoja(mFinAM[0]);
                            data_AM.setFinHorarioVerde(mFinAM[1]);

                            ExpresoData data_PM = new ExpresoData();

                            View lineaHorario_PM = inflater.inflate(R.layout.fila_horario, null);
                            lineaHorario_PM.setTag("PM");

                            TextView titleRow_PM = (TextView) lineaHorario_PM.findViewById(R.id.titleRow);
                            titleRow_PM.setText("PM");

                            data_PM.setTituloHorario("PM");

                            String[] mInicioPM = items.getString("inicio_pm").replace(" ", "").split("-");
                            data_PM.setInicioHorarioRoja(mInicioPM[0]);
                            data_PM.setInicioHorarioVerde(mInicioPM[1]);

                            String[] mFinPM = items.getString("fin_pm").replace(" ", "").split("-");
                            data_PM.setFinHorarioRoja(mFinPM[0]);
                            data_PM.setFinHorarioVerde(mFinPM[1]);

                            expresoDataList.add(data_AM);
                            expresoDataList.add(data_PM);
                            expresoDataA.setExpreList(expresoDataList);

                            tablaExpresoHorario.addView(lineaHorario_AM);
                            tablaExpresoHorario.addView(lineaHorario_PM);

                            listViews.add(lineaHorario_AM);
                            listViews.add(lineaHorario_PM);


                        }


//                        JSONObject items = jsonArrayExpreso.getJSONObject(0);

//                        String mTipo = items.getString("tipo");
//                        expresoDataA.setTipo(mTipo);
//                        String mInicioAM = items.getString("inicio_am");
//                        expresoDataA.setInicio_am(mInicioAM);
//                        String mFinAM = items.getString("fin_am");
//                        expresoDataA.setFin_am(mFinAM);
//                        String mInicioPM = items.getString("inicio_pm");
//                        expresoDataA.setInicio_pm(mInicioPM);
//                        String mFinPM = items.getString("fin_pm");
//                        expresoDataA.setFin_pm(mFinPM);
//                        String mDireccion = items.getString("direccion");
//                        expresoDataA.setDireccion(mDireccion);

//                        JSONObject horarios = jsonArrayExpreso.getJSONObject(0).getJSONObject("horarios");

//                        JSONObject jAM = horarios.getJSONObject("am");
//                        String _AM_Inicio = jAM.getString("inicio");
//                        expresoDataA.set_AM_Inicio(_AM_Inicio);
//
//                        String _AM_Fin = jAM.getString("fin");
//                        expresoDataA.set_AM_Fin(_AM_Fin);
//
//                        JSONObject jValle = horarios.getJSONObject("valle");
//                        String _Valle_Inicio = jValle.getString("inicio");
//                        expresoDataA.set_Valle_Inicio(_Valle_Inicio);
//
//                        String _Valle_Fin = jValle.getString("fin");
//                        expresoDataA.set_Valle_Fin(_Valle_Fin);
//
//                        JSONObject jPM = horarios.getJSONObject("pm");
//                        String _PM_Inicio = jPM.getString("inicio");
//                        expresoDataA.set_PM_Inicio(_PM_Inicio);
//                        String _PM_Fin = jPM.getString("fin");
//                        expresoDataA.set_PM_Fin(_PM_Fin);


//                        JSONObject direccionB = jsonArrayExpreso.getJSONObject(1);
//
//                        expresoDataB.setTipo(direccionB.getString("tipo"));
//                        expresoDataB.setInicio_am(direccionB.getString("inicio_am"));
//                        expresoDataB.setFin_am(direccionB.getString("fin_am"));
//                        expresoDataB.setInicio_pm(direccionB.getString("inicio_pm"));
//                        expresoDataB.setFin_pm(direccionB.getString("fin_pm"));
//                        expresoDataB.setDireccion(direccionB.getString("direccion"));
//
//                        JSONObject horariosB = jsonArrayExpreso.getJSONObject(1).getJSONObject("horarios");
//
//                        JSONObject jAM_B = horariosB.getJSONObject("am");
//                        expresoDataB.set_AM_Inicio(jAM_B.getString("inicio"));
//                        expresoDataB.set_AM_Fin(jAM_B.getString("fin"));
//
//                        JSONObject jValle_B = direccionB.getJSONObject("valle");
//                        expresoDataB.set_Valle_Inicio(jValle_B.getString("inicio"));
//                        expresoDataB.set_Valle_Fin(jValle_B.getString("fin"));
//
//                        JSONObject jPM_B = direccionB.getJSONObject("pm");
//                        expresoDataB.set_PM_Inicio(jPM_B.getString("inicio"));
//                        expresoDataB.set_PM_Fin(jPM_B.getString("fin"));


                        initToggleDireccion();

                    } else {

                        mSinExpreso.setText(String.format("Estación %s no cuenta con operación expresa", nombreEstacion));
                        mSinExpreso.setVisibility(View.VISIBLE);
                        progressBar.setVisibility(View.GONE);

                    }

                } catch (Exception e) {
                    e.printStackTrace();

                }

            }

            @Override
            public void failure(RetrofitError error) {

                progressBar.setVisibility(View.GONE);
                mSinExpreso.setText("Ocurrio un error con la comunicación con el servidor, intente nuevamente ...");
                mSinExpreso.setVisibility(View.VISIBLE);

            }
        });

    }

    public String replaceStringEmtpy(String string) {
        String mReplace;
        if (TextUtils.isEmpty(string)) {
            mReplace = string.replace("", "-");
        } else {
            mReplace = string;
        }

        return mReplace;
    }

//    public void getExpreso() {
//
//        apiService.getEstacionExpreso(codigoEstacion, new Callback<Response>() {
//            @Override
//            public void success(Response responseJSON, Response response) {
//
//                try {
//                    JSONObject jsonObject = new JSONObject(Utils.convertResponseBody(responseJSON));
//                    JSONArray jsonArrayExpreso = jsonObject.getJSONArray("expreso");
//                    if (jsonArrayExpreso.length() > 0) {
//
//                        mContentExpreso.setVisibility(View.VISIBLE);
//                        progressBar.setVisibility(View.GONE);
//
////                        for(int i = 0; i < jsonArrayExpreso.length(); i++){
//
//
//                        JSONObject items = jsonArrayExpreso.getJSONObject(0);
//
//                        String mTipo = items.getString("tipo");
//                        expresoDataA.setTipo(mTipo);
//                        String mInicioAM = items.getString("inicio_am");
//                        expresoDataA.setInicio_am(mInicioAM);
//                        String mFinAM = items.getString("fin_am");
//                        expresoDataA.setFin_am(mFinAM);
//                        String mInicioPM = items.getString("inicio_pm");
//                        expresoDataA.setInicio_pm(mInicioPM);
//                        String mFinPM = items.getString("fin_pm");
//                        expresoDataA.setFin_pm(mFinPM);
//                        String mDireccion = items.getString("direccion");
//                        expresoDataA.setDireccion(mDireccion);
//
//
//                        JSONObject direccionB = jsonArrayExpreso.getJSONObject(1);
//
//                        expresoDataB.setTipo(direccionB.getString("tipo"));
//                        expresoDataB.setInicio_am(direccionB.getString("inicio_am"));
//                        expresoDataB.setFin_am(direccionB.getString("fin_am"));
//                        expresoDataB.setInicio_pm(direccionB.getString("inicio_pm"));
//                        expresoDataB.setFin_pm(direccionB.getString("fin_pm"));
//                        expresoDataB.setDireccion(direccionB.getString("direccion"));
//
////                        }
//
//                        initToggleDireccion();
//
//                    } else {
//
//                        mSinExpreso.setText(String.format("Estación %s no cuenta con operación expresa", nombreEstacion));
//                        mSinExpreso.setVisibility(View.VISIBLE);
//                        progressBar.setVisibility(View.GONE);
//
//                    }
//
//                } catch (Exception e) {
//                    e.printStackTrace();
//
//                }
//
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//
//                progressBar.setVisibility(View.GONE);
//                mSinExpreso.setText("Ocurrio un error con la comunicación con el servidor, intente nuevamente ...");
//                mSinExpreso.setVisibility(View.VISIBLE);
//
//            }
//        });
//    }

    public void setDataText(ExpresoData expresoData) {
//        mHoraInicioAMView.setText(expresoData.getInicio_am());
//        mHoraInicioPMView.setText(expresoData.getInicio_pm());
//        mHoraFinAMView.setText(expresoData.getFin_am());
//        mHoraFinPMView.setText(expresoData.getFin_pm());
//
//        mValle_InicioView.setText(expresoData.get_Valle_Inicio());
//        mValle_FinView.setText(expresoData.get_Valle_Fin());

        mTipoEstacion.setText(expresoData.getTipo());
        if (expresoData.getTipo().equalsIgnoreCase("comun")) {
//            mTipoEstacion.setText(expresoData."C");
        }else if (expresoData.getTipo().equalsIgnoreCase("Roja")) {
//            mTipoEstacion.setText(expresoData.getTipo());
            mTipoEstacion.setBackgroundColor(Color.parseColor("#e22730"));
        } else if (expresoData.getTipo().equalsIgnoreCase("Verde")) {
//            mTipoEstacion.setText(expresoData.getTipo());
            mTipoEstacion.setBackgroundColor(Color.parseColor("#008962"));

        }

        if (listViews != null) {

            for (int i = 0; i < listViews.size(); i++) {

//                Log.d("LOG", listViews.get(i).getTag().toString());
                TextView titleRow = (TextView) listViews.get(i).findViewById(R.id.titleRow);
                TextView txtRoja = (TextView) listViews.get(i).findViewById(R.id.horario_inicio_rojo);
                TextView txtVerde = (TextView) listViews.get(i).findViewById(R.id.horario_inicio_verde);
                TextView txtRojaFin = (TextView) listViews.get(i).findViewById(R.id.horario_fin_rojo);
                TextView txtVerdeFin = (TextView) listViews.get(i).findViewById(R.id.horario_fin_verde);


                List<ExpresoData> list = expresoData.getExpreList();
                for (int j = 0; j < list.size(); j++) {
                    if (listViews.get(i).getTag().toString().equals(list.get(j).getTituloHorario())) {

//                        String horarioInicio = list.get(j).getInicioHorario();

//                        n % 2 == 0

                        if (i % 2 == 0) {
                            titleRow.setBackgroundColor(Color.parseColor("#ffababab"));
                        }

                        txtRoja.setText(list.get(j).getInicioHorarioRoja());
                        txtVerde.setText(list.get(j).getInicioHorarioVerde());
                        txtRojaFin.setText(list.get(j).getFinHorarioRoja());
                        txtVerdeFin.setText(list.get(j).getFinHorarioVerde());

//                        if (!TextUtils.isEmpty(horarioInicio) || !horarioInicio.isEmpty() || !horarioInicio.equals("")) {
//                            String[] mInicio = horarioInicio.split(" - ");
//                            txtRoja.setText(mInicio[0]);
//                            txtVerde.setText(mInicio[1]);
//                        } else {
//                            txtRoja.setText("-");
//                            txtVerde.setText("-");
//                        }

//                        String horarioFin = list.get(j).getFinHorario();
//                        if (!TextUtils.isEmpty(horarioFin) || !horarioFin.isEmpty() || !horarioFin.equals("")) {
//                            String[] mFin = horarioFin.split(" - ");
//                            txtRojaFin.setText(mFin[0]);
//                            txtVerdeFin.setText(mFin[1]);
//
//                        } else {
//                            txtRojaFin.setText("-");
//                            txtVerdeFin.setText("-");
//                        }

                    }
                }

            }


        }

    }


}
