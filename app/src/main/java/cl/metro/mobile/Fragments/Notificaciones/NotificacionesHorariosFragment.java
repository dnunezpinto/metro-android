package cl.metro.mobile.Fragments.Notificaciones;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ToggleButton;

import cl.metro.mobile.MainActivity;
import cl.metro.mobile.R;
import cl.metro.mobile.Retrofit.ApiService;
import cl.metro.mobile.Utilities.EnunSharedPreference;
import cl.metro.mobile.Utilities.SharedPreferenceMetro;
import cl.metro.mobile.Utilities.Utils;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class NotificacionesHorariosFragment extends Fragment implements View.OnClickListener {


    public SharedPreferenceMetro preferences;
    private ToggleButton horarios_toggle_1;
    private ToggleButton horarios_toggle_2;
    private ToggleButton horarios_toggle_3;
    private ToggleButton horarios_toggle_4;
    private ToggleButton horarios_toggle_5;
    private ToggleButton horarios_toggle_6;

    public NotificacionesHorariosFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_notificaciones_horarios, container, false);

        ((Button) rootView.findViewById(R.id.button_guardar_preferencias))
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        connection();
                    }
                });
        setPreferences(rootView);

        return rootView;
    }

    public void setPreferences(View rootView) {
        horarios_toggle_1 = ((ToggleButton) rootView.findViewById(R.id.horarios_toggle_1));
        horarios_toggle_2 = ((ToggleButton) rootView.findViewById(R.id.horarios_toggle_2));
        horarios_toggle_3 = ((ToggleButton) rootView.findViewById(R.id.horarios_toggle_3));
        horarios_toggle_4 = ((ToggleButton) rootView.findViewById(R.id.horarios_toggle_4));
        horarios_toggle_5 = ((ToggleButton) rootView.findViewById(R.id.horarios_toggle_5));
        horarios_toggle_6 = ((ToggleButton) rootView.findViewById(R.id.horarios_toggle_6));

        SharedPreferenceMetro preferences_metro = new SharedPreferenceMetro(getActivity());
        SharedPreferences preferences = preferences_metro.getPrefenrenceMetro();
        boolean is_checked;
        is_checked = preferences.getBoolean(EnunSharedPreference.horarioTramo1.getText(), true);
        horarios_toggle_1.setChecked(is_checked);
        horarios_toggle_1.setOnClickListener(this);
        is_checked = preferences.getBoolean(EnunSharedPreference.horarioTramo2.getText(), true);
        horarios_toggle_2.setChecked(is_checked);
        horarios_toggle_2.setOnClickListener(this);
        is_checked = preferences.getBoolean(EnunSharedPreference.horarioTramo3.getText(), true);
        horarios_toggle_3.setChecked(is_checked);
        horarios_toggle_3.setOnClickListener(this);
        is_checked = preferences.getBoolean(EnunSharedPreference.horarioTramo4.getText(), true);
        horarios_toggle_4.setChecked(is_checked);
        horarios_toggle_4.setOnClickListener(this);
        is_checked = preferences.getBoolean(EnunSharedPreference.horarioTramo5.getText(), true);
        horarios_toggle_5.setChecked(is_checked);
        horarios_toggle_5.setOnClickListener(this);
        is_checked = preferences.getBoolean(EnunSharedPreference.horarioTramo6.getText(), true);
        horarios_toggle_6.setChecked(is_checked);
        horarios_toggle_6.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        boolean is_checked = ((ToggleButton) v).isChecked();
        SharedPreferenceMetro preferences = new SharedPreferenceMetro(getActivity());
        switch (v.getId()) {
            case R.id.horarios_toggle_1:
                preferences.setPreferenceHorarioTramo(EnunSharedPreference.horarioTramo1, is_checked);
                break;
            case R.id.horarios_toggle_2:
                preferences.setPreferenceHorarioTramo(EnunSharedPreference.horarioTramo2, is_checked);
                break;
            case R.id.horarios_toggle_3:
                preferences.setPreferenceHorarioTramo(EnunSharedPreference.horarioTramo3, is_checked);
                break;
            case R.id.horarios_toggle_4:
                preferences.setPreferenceHorarioTramo(EnunSharedPreference.horarioTramo4, is_checked);
                break;
            case R.id.horarios_toggle_5:
                preferences.setPreferenceHorarioTramo(EnunSharedPreference.horarioTramo5, is_checked);
                break;
            case R.id.horarios_toggle_6:
                preferences.setPreferenceHorarioTramo(EnunSharedPreference.horarioTramo6, is_checked);
                break;
        }

    }

    private void connection() {

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(Utils.URL_BASE_INTERMOVIL)
                .build();

        ApiService apiService = restAdapter.create(ApiService.class);

        apiService.setConfigNotificacionesHorarios(Settings.Secure.getString(getActivity().getContentResolver(), Settings.Secure.ANDROID_ID),
                MainActivity.getTokenId(getActivity()),
                (horarios_toggle_1.isChecked() ? "1" : "0"),
                (horarios_toggle_2.isChecked() ? "1" : "0"),
                (horarios_toggle_3.isChecked() ? "1" : "0"),
                (horarios_toggle_4.isChecked() ? "1" : "0"),
                (horarios_toggle_5.isChecked() ? "1" : "0"),
                (horarios_toggle_6.isChecked() ? "1" : "0"),
                "android", new Callback<Response>() {
                    @Override
                    public void success(Response response, Response response2) {


                        Log.i("App", Utils.convertResponseBody(response));

                        if (response.equals("{}")) {
                            Utils.createAlertDialog(getActivity(), "No se ha podido guardar sus preferencias, intente nuevamente.");
                        } else {
                            Utils.createAlertDialog(getActivity(), "Preferencias Guardadas.");
                        }

                    }

                    @Override
                    public void failure(RetrofitError error) {

                        Utils.createAlertDialog(getActivity(), "No se ha podido guardar sus preferencias, intente nuevamente.");

                    }
                });

    }


}
