package cl.metro.mobile.Fragments.FragmentServicios;


import android.app.Dialog;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;

import cl.metro.mobile.MainActivity;
import cl.metro.mobile.Models.Paraderos.DatosParadero;
import cl.metro.mobile.Models.Paraderos.ListParaderos;
import cl.metro.mobile.R;
import cl.metro.mobile.Retrofit.ApiService;
import cl.metro.mobile.Utilities.Utils;
import cl.metro.mobile.Utilities.ViewRecorridos;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.converter.GsonConverter;

/**
 * A simple {@link Fragment} subclass.
 */
public class ServicioParaderosRecorridosFragment extends Fragment {

    public static String nombreEstacion;
    public static int idLinea;
    public static ApiService apiService;
    private static String codigoEstacion;
    private GoogleMap mMap; // Might be null if Google Play services APK is not available.
    private MapView mapView;
    private HashMap<Marker, DatosParadero> mHashMap = new HashMap<Marker, DatosParadero>();
    private MaterialDialog materialDialog;
    private ScrollView scrollView;
    private MaterialDialog dialog;
    private Typeface _tsInfGruesa;

    public ServicioParaderosRecorridosFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        codigoEstacion = getArguments().getString("codigoEstacion");
        nombreEstacion = getArguments().getString("nombreEstacion");
        idLinea = getArguments().getInt("idLinea");

        Gson gson = new GsonBuilder()
//                .excludeFieldsWithoutExposeAnnotation()
                .create();

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setConverter(new GsonConverter(gson))
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(Utils.URL_BASE_METRO)
                .build();

        apiService = restAdapter.create(ApiService.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_servicio_paraderos_recorridos, container, false);

        scrollView = (ScrollView) rootView.findViewById(R.id.scrollView5);

        TextView tvNombreEstacion = (TextView) rootView.findViewById(R.id.nombreEstacion);
        tvNombreEstacion.setText(nombreEstacion);
        MainActivity.initApp.setEstacionImage(idLinea, tvNombreEstacion, getActivity());
//        tvNombreEstacion.setTextColor(Color.parseColor(Utils.COLORS[idLinea - 1]));

        mapView = (MapView) rootView.findViewById(R.id.map);
        mapView.onCreate(savedInstanceState);

        MapsInitializer.initialize(getActivity());
        mMap = mapView.getMap();
        if (MainActivity.initApp.currentSelectedEstacion != null) {

            double lat = MainActivity.initApp.currentSelectedEstacion.getLatitud();
            double lng = MainActivity.initApp.currentSelectedEstacion.getLongitud();

            LatLng positionEstacion = new LatLng(lat, lng);
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(positionEstacion, 15));
            mMap.setInfoWindowAdapter(new MyInfoWindowAdapter());
//            mMap.addMarker(new MarkerOptions()
//                    .position(positionEstacion)
//                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_metro_red24pd))
//                    .alpha(0.7f));


//            BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE))

        }

        _tsInfGruesa = Utils.getTypeface(getActivity(), "TSInfGruesa.otf");
        getParaderos();

//        setUpMapIfNeeded();

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();

        mapView.onResume();

    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();

        mapView.onDestroy();
    }

    @Override
    public void onPause() {
        super.onPause();

        mapView.onPause();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }


    public void getParaderos() {

        dialog = new MaterialDialog.Builder(getActivity())
                .content("Cargando Paraderos, favor espere ...")
                .widgetColor(R.color.club_metro_color)
                .progress(true, 0)
                .autoDismiss(false)
                .cancelable(false)
                .show();

        apiService.getParaderos(codigoEstacion, new Callback<ListParaderos>() {
            @Override
            public void success(ListParaderos listParaderos, Response response) {
                dialog.dismiss();
//                Log.i("App", "Size array: " + listParaderos.getDatosParaderos().size());

                if (listParaderos.getDatosParaderos() != null) {

                    for (DatosParadero item : listParaderos.getDatosParaderos()) {

                        Marker marker = mMap.addMarker(new MarkerOptions()
                                .position(new LatLng(item.getLatitud(), item.getLongitud()))
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_paradero_24dp))
                                .title(item.getNombre()));


                        mHashMap.put(marker, item);

                    }

                    mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                        @Override
                        public boolean onMarkerClick(Marker marker) {


                            DatosParadero paradero = mHashMap.get(marker);
                            if (paradero != null) {
                                getRecorridos(paradero);
                            }


                            return false;
                        }
                    });
                }
            }

            @Override
            public void failure(RetrofitError error) {
                dialog.dismiss();

            }
        });
    }

    public void getRecorridos(final DatosParadero paradero) {

        apiService.getParaderosDetalle(paradero.getNombre(), new Callback<Response>() {
            @Override
            public void success(Response response, Response response2) {

                LayoutInflater li = LayoutInflater.from(getActivity());
                View myView = li.inflate(R.layout.view_paradero, null);

                final Dialog dialog = new Dialog(getActivity());
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(myView);
                dialog.setCanceledOnTouchOutside(true);
                dialog.setCancelable(true);


                Utils.overrideFonts(getActivity(), myView, "TSMapGruesa.otf");

                TextView mNombreEstacionView = (TextView) dialog.findViewById(R.id.mTextNombreEstacion);
                mNombreEstacionView.setText(paradero.getUbicacion());

                TextView tvNumParadero = (TextView) dialog.findViewById(R.id.tvNumParadero);

                if (!paradero.getParada().equals("0")) {
                    tvNumParadero.setText(paradero.getParada());
                }


                TextView tvCodigoParada = (TextView) dialog.findViewById(R.id.tvCodigoParada);
                tvCodigoParada.setText(paradero.getNombre());

                LinearLayout content1 = (LinearLayout) dialog.findViewById(R.id.content1);
                content1.setGravity(Gravity.CENTER);


                try {


                    JSONObject jResponse = new JSONObject(Utils.convertResponseBody(response));

                    if (jResponse.has("mensaje")) {

                        Utils.createAlertDialog(getActivity(), jResponse.getString("mensaje"));

                    } else {

                        JSONArray jArrayRecorrido = jResponse.getJSONArray("recorridos");
                        for (int i = 0; i < jArrayRecorrido.length(); i++) {

                            JSONObject jItem = jArrayRecorrido.getJSONObject(i);
                            String numRecorrido = jItem.getString("recorrido");
                            String direccion = jItem.getString("direccion");


                            ViewRecorridos recorridos = new ViewRecorridos(getActivity(), paradero.getNombre(), numRecorrido);
                            recorridos.txtNumRecorrido.setText(numRecorrido);
                            recorridos.txtDireccion.setText("a " + direccion);
                            recorridos.setPadding(15, 20, 15, 15);
                            recorridos.setGravity(Gravity.CENTER);

                            content1.addView(recorridos);

                        }

                        dialog.show();

                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void failure(RetrofitError error) {

            }
        });


    }

//    public void getDetalle(String codigo, String numero) {
//
//        apiService.getParaderosRecorridos(codigo, numero, new Callback<Response>() {
//            @Override
//            public void success(Response response, Response response2) {
//
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//
//            }
//        });
//
//    }

    class MyInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {


        MyInfoWindowAdapter() {
//            ContextThemeWrapper cw = new ContextThemeWrapper(
//                    getActivity(),R.style.);
//            myContentsView = getActivity().getLayoutInflater().inflate(R.layout.custominfowindow, null);
        }

        @Override
        public View getInfoContents(Marker marker) {


            return null;
        }

        @Override
        public View getInfoWindow(Marker marker) {

            View v = getActivity().getLayoutInflater().inflate(R.layout.custominfowindow, null);

            DatosParadero paradero = mHashMap.get(marker);
            Utils.overrideFonts(getActivity(), v, "TSMapGruesa.otf");

            String parada = paradero.getParada();
            TextView tvParada = ((TextView) v.findViewById(R.id.txtParada));
            parada.replace("PARADA ", "");

            if (!paradero.getParada().equals("0")) {
                tvParada.setText(parada);
            }
            TextView tvCodigo = ((TextView) v.findViewById(R.id.txtCodigo));
            tvCodigo.setText(paradero.getNombre());
            TextView tvDireccion = ((TextView) v.findViewById(R.id.txtDireccion));
            tvDireccion.setText(paradero.getUbicacion());

            return v;
        }

    }

}
