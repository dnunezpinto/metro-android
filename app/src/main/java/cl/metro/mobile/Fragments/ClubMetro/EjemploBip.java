package cl.metro.mobile.Fragments.ClubMetro;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import cl.metro.mobile.R;
import cl.metro.mobile.Utilities.common.view.ZoomImageView;

public class EjemploBip extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ejemplo_bip);


        ZoomImageView touch = (ZoomImageView) findViewById(R.id.imgEjemplo);


        Bitmap icon = BitmapFactory.decodeResource(getResources(),
                R.drawable.ejemplobip);

        touch.setImageBitmap(icon);

    }

}
