package cl.metro.mobile.Fragments;

import android.app.Activity;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import com.doomonafireball.betterpickers.radialtimepicker.RadialTimePickerDialog;

import java.util.ArrayList;
import java.util.Calendar;

import cl.metro.mobile.MainActivity;
import cl.metro.mobile.Models.Estaciones;
import cl.metro.mobile.R;
import cl.metro.mobile.Utilities.Utils;

/**
 * Created by Daniel on 05-02-2015.
 */
public class PlanificadorFragment extends DialogFragment implements RadialTimePickerDialog.OnTimeSetListener, AdapterView.OnItemSelectedListener {


    public static final String PREFS_NAME_FRAGMENT = "MyPrefsFileFragment";
    public static final String FIRST_RUN_FRAGMENT = "FirstRunFragment";
    private static final String ARG_SECTION_NUMBER = "section_number";
    private static final String FRAG_TAG_TIME_PICKER = "timePickerDialogFragment";
    public static Spinner spinner_estacion_inicio;
    public static Spinner spinner_estacion_termino;
    public static ArrayList<Estaciones> arrayListOrigen;
    public static ArrayList<Estaciones> arrayListDestino;
    public static int lastIdLineaInicio;
    public static int lastIdLineaTermino;
    private static int mCurrentPositionEstacionInicio = 0;
    private static int mCurrentPositionEstacionTermino = 0;
    private TextView horaTxt;
    private int hour;
    private int minute;
    private Estaciones estacionOrigen;
    private Estaciones estacionDestino;
    private String tipoDia;
    private String horaEstimada;
    private Activity mActivity;

    public PlanificadorFragment() {
    }

    public PlanificadorFragment newInstance(int sectionNumber) {

        PlanificadorFragment fragment = new PlanificadorFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);

        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        this.mActivity = activity;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_planificador, container, false);

        initLayout(rootView, savedInstanceState);

        //Cambia de color rojo el actionbar/toolbar
        Utils.toolbarRed(mActivity);

        //Obtenemos la hora del dispositivo para mostrarla como deafult en el timepickerdialog
        Calendar mcurrentTime = Calendar.getInstance();
        hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        minute = mcurrentTime.get(Calendar.MINUTE);


        horaTxt.setText("" + this.hour + ":" + Utils.getMinute(this.minute) + Utils.getAM_PM(this.hour));
        this.horaEstimada = this.hour + ":" + Utils.getMinute(this.minute);

        horaTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // TimePicker para version android menor a 4.0 (Nativo)
                if (android.os.Build.VERSION.SDK_INT < 14) {

                    TimePickerDialog tpd = new TimePickerDialog(mActivity,
                            new TimePickerDialog.OnTimeSetListener() {

                                @Override
                                public void onTimeSet(TimePicker view, int hourOfDay,
                                                      int minute) {
                                    setTime(hourOfDay, minute);
                                }
                            }, hour, minute, false);
                    tpd.show();


                } else {

                    //TimePicker para version android mayor a 4.0

                    RadialTimePickerDialog radialTimePickerDialog = RadialTimePickerDialog
                            .newInstance(PlanificadorFragment.this, hour, minute,
                                    DateFormat.is24HourFormat(mActivity.getApplicationContext()));

                    radialTimePickerDialog.show(getActivity().getSupportFragmentManager(), FRAG_TAG_TIME_PICKER);
                }


            }
        });


        checkDialogFavorito();

        return rootView;
    }

    public void checkDialogFavorito() {
        MainActivity.sharedPreferences = getActivity().getSharedPreferences(PREFS_NAME_FRAGMENT, 0);

        if (MainActivity.sharedPreferences.getBoolean(FIRST_RUN_FRAGMENT, true)) {

            Utils.createAlertDialog(getActivity(), "Recuerda que puedes seleccionar y guardar el resultado del planificador de viaje y agregarlo a tus favoritos.");

            SharedPreferences.Editor editere = MainActivity.sharedPreferences.edit();
            editere.putBoolean(FIRST_RUN_FRAGMENT, false);
            editere.commit();

        }
    }

    public void initLayout(View rootView, final Bundle savedInstanceState) {

        Spinner spinner_linea_inicio = rootView.findViewById(R.id.sp_linea_desde);
        spinner_estacion_inicio = rootView.findViewById(R.id.sp_estaciones_desde);
        Spinner spinner_linea_termino = rootView.findViewById(R.id.sp_lineas_hasta);
        spinner_estacion_termino = rootView.findViewById(R.id.sp_estaciones_hasta);
        Spinner spinner_dia = rootView.findViewById(R.id.sp_dias);
        horaTxt = rootView.findViewById(R.id.txtHora);

        Button btnPlanificar = rootView.findViewById(R.id.btnPlanificar);
        btnPlanificar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                onPlanificar();
//                String stringUrl = Utils.URL_PLANIFICA;
//                stringUrl += estacionOrigen.getCodigoEstacion() + "/"
//                        + estacionDestino.getCodigoEstacion() + "/" + tipoDia + "/"
//                        + horaEstimada;
//                new HttpAsyncTask().execute(stringUrl);
//                Log.d("LOG", stringUrl);

//                if (savedInstanceState != null) {
//
//                }

                if (Utils.isNetworkAvailable(mActivity)) {


                    lastIdLineaInicio = estacionOrigen.getIdLinea();
                    lastIdLineaTermino = estacionDestino.getIdLinea();

                    Fragment fragment = new PlanificadorDetalleFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("codigoEstacionOrigen", estacionOrigen.getCodigoEstacion());
                    bundle.putString("codigoEstacionDestino", estacionDestino.getCodigoEstacion());
                    bundle.putString("tipoDia", tipoDia);
                    bundle.putString("horaEstimada", horaEstimada);
                    fragment.setArguments(bundle);
                    getFragmentManager().beginTransaction()
                            .replace(R.id.container, fragment)
                            .addToBackStack(null)
                            .commit();
                } else {

                    Utils.createAlertDialog(mActivity, mActivity.getString(R.string.lost_connection));

                }


            }
        });


        spinner_linea_inicio.setOnItemSelectedListener(this);
        spinner_estacion_inicio.setOnItemSelectedListener(this);
        spinner_linea_termino.setOnItemSelectedListener(this);
        spinner_estacion_termino.setOnItemSelectedListener(this);
        spinner_dia.setOnItemSelectedListener(this);

        Button btnPlanificaTransantiago = (Button) rootView.findViewById(R.id.btnPlanificaTransantiago);
        btnPlanificaTransantiago.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = "http://www.transantiago.cl/";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        switch (parent.getId()) {

            case R.id.sp_linea_desde:
                //Cambiamos las estaciones de acuerdo a la linea seleccionada
                if (lastIdLineaInicio - 1 != position) {
                    Utils.setActionListenerForSpinner(spinner_estacion_inicio, parent, 0, getActivity());
                } else {
                    if (estacionOrigen != null) {
                        Utils.setActionListenerForSpinner(spinner_estacion_inicio, parent, estacionOrigen.getOrden() - 1, getActivity());
                    } else {
                        Utils.setActionListenerForSpinner(spinner_estacion_inicio, parent, 0, getActivity());
                    }
                }
                break;
            case R.id.sp_lineas_hasta:
                if (lastIdLineaTermino - 1 != position) {
                    Utils.setActionListenerForSpinner(spinner_estacion_termino, parent, 0, getActivity());
                } else {
                    if (estacionDestino != null) {
                        Utils.setActionListenerForSpinner(spinner_estacion_termino, parent, estacionDestino.getOrden() - 1, getActivity());
                    } else {
                        Utils.setActionListenerForSpinner(spinner_estacion_termino, parent, 0, getActivity());
                    }

                }


                break;
            case R.id.sp_estaciones_desde:
                mCurrentPositionEstacionInicio = position;
                if (arrayListOrigen != null) {
                    estacionOrigen = arrayListOrigen.get(position);
                }
                break;
            case R.id.sp_estaciones_hasta:
                mCurrentPositionEstacionTermino = position;
                if (arrayListDestino != null) {
                    estacionDestino = arrayListDestino
                            .get(position);
                }
                break;
            case R.id.sp_dias:
                if (position == 0) {
                    //Dia laboral
                    tipoDia = "DL";
                } else if (position == 1) {
                    //Sabado
                    tipoDia = "DS";
                } else if (position == 2) {
                    //Domingo y festivos
                    tipoDia = "DF";
                }
                break;

        }

    }

    // TimeSet para Android mayor a 4.0
    @Override
    public void onTimeSet(RadialTimePickerDialog radialTimePickerDialog, int hourOfDay, int _minute) {

        setTime(hourOfDay, _minute);

    }

    // TimeSet para Android menor a 4.0
    public void setTime(int hourOfDay, int _minute) {

        this.horaTxt.setText("" + hourOfDay + ":" + Utils.getMinute(_minute) + Utils.getAM_PM(hourOfDay));
        this.horaEstimada = hourOfDay + ":" + Utils.getMinute(_minute);
    }


    @Override
    public void onResume() {

        // Example of reattaching to the fragment
        super.onResume();

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(getString(R.string.title_section2));
        MainActivity.mDrawerList.setItemChecked(1, true);

//        ((ActionBarActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        ((ActionBarActivity)getActivity()).getSupportActionBar().setHomeButtonEnabled(true);

        //Retoma la instancia del timepicker para android 4.0+
        RadialTimePickerDialog rtpd = (RadialTimePickerDialog) getActivity().getSupportFragmentManager().findFragmentByTag(
                FRAG_TAG_TIME_PICKER);
        if (rtpd != null) {
            rtpd.setOnTimeSetListener(this);
        }

    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            // Restore last state for checked position.
            mCurrentPositionEstacionInicio = savedInstanceState.getInt("mCurrentPositionEstacionInicio", 0);
            mCurrentPositionEstacionTermino = savedInstanceState.getInt("mCurrentPositionEstacionTermino", 1);
//            spinnerEstaciones.setSelection(currentPositionEstacion);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("mCurrentPositionEstacionInicio", mCurrentPositionEstacionInicio);
        outState.putInt("mCurrentPositionEstacionTermino", mCurrentPositionEstacionTermino);
    }


}
