package cl.metro.mobile.Fragments;


import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import java.util.ArrayList;

import cl.metro.mobile.MainActivity;
import cl.metro.mobile.Models.Estaciones;
import cl.metro.mobile.R;
import cl.metro.mobile.Utilities.Utils;

/**
 * A simple {@link Fragment} subclass.
 */
public class Emergencias1411_Fragment extends Fragment {

    public static Spinner spinner_linea;
    public static Spinner spinner_estacion;
    public static ArrayList<Estaciones> mArrayList;
    private static int lugarSelected;
    private static Spinner spLugar, spDireccion, spLugarEstacion;
    private static int mPosicionLinea = 1;
    final private int REQUEST_CODE_ASK_PERMISSIONS = 123;
    public ArrayList<String> direccionLinea;
    private View viewDialog;
    private EditText txtCarro;
    //    private EditText txtProxEstacion;
    private String mValueNumeroCarro;
    private boolean isValidNumeroCarro = false;
    private String mValueProxEstacion;
    private boolean isValidProxEstacion = false;
    private String mValueSituacion;
    private boolean isValidSituacion = false;
    private String bodySMS;
    private EditText txtSituacion;
    private Estaciones mEstacion;
    private TextView txtEstacion;
    private MaterialDialog materialDialog;


    public Emergencias1411_Fragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        MenuItem menuConfigPush = menu.findItem(R.id.action_call);
        super.onCreateOptionsMenu(menu, inflater);
        menuConfigPush.setVisible(false);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        return super.onOptionsItemSelected(item);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_emergencias1411, container, false);

        //Cambia de color rojo el actionbar/toolbar
        Utils.toolbarRed(getActivity());
        setHasOptionsMenu(true);


        ImageButton btnLlamar = (ImageButton) rootView.findViewById(R.id.btnLlamar);
        btnLlamar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                call();

            }
        });

        ImageButton btnSMS = (ImageButton) rootView.findViewById(R.id.btnSMS);
        btnSMS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                boolean wrapInScrollView = true;


                materialDialog = new MaterialDialog.Builder(getActivity())
                        .contentColor(Color.WHITE)
                        .backgroundColorRes(R.color.white)
                        .cancelable(false)
                        .autoDismiss(false)
                        .customView(R.layout.activity_dialog__sms__form, true)
                        .negativeText("Cancelar")
                        .positiveText("Enviar sms")
                        .onNegative(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog materialDialog, @NonNull DialogAction dialogAction) {

                                materialDialog.dismiss();

                            }
                        })
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog materialDialog, @NonNull DialogAction dialogAction) {

                                mValueSituacion = txtSituacion.getText().toString();
                                mValueNumeroCarro = txtCarro.getText().toString();
//                                mValueProxEstacion = txtProxEstacion.getText().toString();

                                if (mValueSituacion == null || mValueSituacion.isEmpty() || mValueSituacion.equals("")) {
                                    txtSituacion.setError("Ingrese la Situación");
                                    isValidSituacion = false;
                                } else {
                                    txtSituacion.setError(null);
                                    isValidSituacion = true;
                                }


                                if (lugarSelected == 0) {
//                                    txtCarro.requestFocus();
                                    if (mValueNumeroCarro == null || mValueNumeroCarro.isEmpty() || mValueNumeroCarro.equals("")) {
                                        txtCarro.setError("Ingrese N° Carro");
                                        isValidNumeroCarro = false;
                                    } else {
                                        txtCarro.setError(null);
                                        bodySMS = "Lugar: " + spLugar.getSelectedItem().toString() + "\n" +
                                                "N° Carro: " + mValueNumeroCarro + "\n" +
                                                "Próxima Estación: " + spinner_estacion.getSelectedItem().toString() + "\n" +
                                                "Dirección:" + spDireccion.getSelectedItem() + "\n" +
                                                "Situación: " + mValueSituacion;

                                        isValidNumeroCarro = true;
                                    }

                                } else if (lugarSelected == 1) {

//                                    txtProxEstacion.requestFocus();

//                                    if(mValueProxEstacion == null || mValueProxEstacion.isEmpty() || mValueProxEstacion.equals("")){
//                                        txtProxEstacion.setError("Ingrese Próxima Estación");
//                                        isValidProxEstacion = false;
//                                    }else{
//                                        txtProxEstacion.setError(null);
//                                    bodySMS = "Lugar: " + spLugar.getSelectedItemPosition() + "\n" +
//                                            "Prox. Estación: " + mEstacion.getNombreEstacion() + "\n" +
//                                            "Lugar:" + spLugarEstacion.getSelectedItem() + "\n" +
//                                            "Situación: " + mValueSituacion;

                                    if (spLugarEstacion.getSelectedItem().toString().equalsIgnoreCase("Andén")) {
                                        bodySMS = "Lugar: " + spLugar.getSelectedItem().toString() + "\n" +
                                                "Estación: " + spinner_estacion.getSelectedItem().toString() + "\n" +
                                                "Lugar: " + spLugarEstacion.getSelectedItem() + "\n" +
                                                "Dirección a: " + spDireccion.getSelectedItem() + "\n" +
                                                "Situación: " + mValueSituacion;
                                    } else {
                                        bodySMS = "Lugar: " + spLugar.getSelectedItem().toString() + "\n" +
                                                "Estación: " + spinner_estacion.getSelectedItem().toString() + "\n" +
                                                "Lugar: " + spLugarEstacion.getSelectedItem() + "\n" +
                                                "Situación: " + mValueSituacion;
                                    }

                                    isValidProxEstacion = true;
//                                    }
                                }

                                if (isValidSituacion && isValidProxEstacion || isValidSituacion && isValidNumeroCarro) {


                                    Toast.makeText(getActivity(),
                                            "Enviando SMS",
                                            Toast.LENGTH_LONG).show();
                                    materialDialog.dismiss();

                                    Intent sendIntent = new Intent(Intent.ACTION_VIEW);
                                    sendIntent.addCategory(Intent.CATEGORY_DEFAULT);
                                    sendIntent.setType("vnd.android-dir/mms-sms");
                                    sendIntent.setData(Uri.parse("sms:" + 1411));
                                    sendIntent.putExtra("sms_body", bodySMS);
                                    startActivity(sendIntent);


                                }


                            }

                        })
                        .build();

                viewDialog = materialDialog.getCustomView();


                final LinearLayout contentCarro = (LinearLayout) viewDialog.findViewById(R.id.linearLayout6);
                final RelativeLayout contentLugar = (RelativeLayout) viewDialog.findViewById(R.id.linearLayout7);
                final RelativeLayout contentSentidoLinea = (RelativeLayout) viewDialog.findViewById(R.id.linearLayout11);

                txtSituacion = (EditText) viewDialog.findViewById(R.id.txtSituacion);
                txtCarro = (EditText) viewDialog.findViewById(R.id.txtCarro);
                spLugar = (Spinner) viewDialog.findViewById(R.id.spinner2);
                spLugarEstacion = (Spinner) viewDialog.findViewById(R.id.spinner3);

                spDireccion = (Spinner) viewDialog.findViewById(R.id.spDireccion);
                spinner_linea = (Spinner) viewDialog.findViewById(R.id.spLinea);
                spinner_estacion = (Spinner) viewDialog.findViewById(R.id.spEstacion);
                txtEstacion = (TextView) viewDialog.findViewById(R.id.textView30);

                spinner_linea.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                        Utils.setActionListenerForSpinner(spinner_estacion, parent, Utils.pos_estacion_inicio, getActivity());
                        direccionLinea = MainActivity.initApp.obtenerDireccionesLinea(position + 1);
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                                getActivity(), android.R.layout.simple_spinner_item, direccionLinea);
                        adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
                        spDireccion.setAdapter(adapter);

                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
                spinner_estacion.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        if (mArrayList != null) {
                            mEstacion = mArrayList.get(position);

                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });


                lugarSelected = spLugar.getSelectedItemPosition();

                spLugar.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                        lugarSelected = position;

                        if (lugarSelected == 0) {
                            txtCarro.requestFocus();

                            contentCarro.setVisibility(View.VISIBLE);
//                            contentSentidoLinea.setVisibility(View.VISIBLE);
                            contentLugar.setVisibility(View.GONE);
                            txtEstacion.setText("Próxima Estación");

                        } else if (lugarSelected == 1) {
//                                    txtProxEstacion.requestFocus();
                            contentCarro.setVisibility(View.GONE);
//                            contentSentidoLinea.setVisibility(View.GONE);
                            contentLugar.setVisibility(View.VISIBLE);
                            txtEstacion.setText("Estación");
                        }

                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

                spLugarEstacion.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                        if (position == 0) {
                            contentSentidoLinea.setVisibility(View.VISIBLE);

                        } else if (lugarSelected == 1) {
                            contentSentidoLinea.setVisibility(View.GONE);
                        }

                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

                materialDialog.show();


//                startActivity(new Intent(getActivity(), Dialog_SMS_Form.class));

//

            }
        });


        // Inflate the layout for this fragment
        return rootView;
    }

    public void call() {

        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CALL_PHONE},
                    REQUEST_CODE_ASK_PERMISSIONS);

        } else {

            Intent intent = new Intent(Intent.ACTION_CALL);
            intent.setData(Uri.parse("tel:" + 1411));
            getActivity().startActivity(intent);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case REQUEST_CODE_ASK_PERMISSIONS:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    call();

                } else {
                    // Permission Denied

                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }

    }

    @Override
    public void onResume() {
        super.onResume();

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Emergencia");

    }


}
//