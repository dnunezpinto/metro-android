package cl.metro.mobile.Database;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;


public class DBHelperDataBase extends SQLiteOpenHelper {

    // Ruta por defecto de las bases de datos en el sistema Android
    public static final String DB_PATH = "data/data/cl.metro.mobile/databases/";
    public static final String DB_NAME = "MetroDataBase.sqlite";
    private static final int versionDB = 8;

    public static String TABLE_LINEA = "lineas";
    public static String TABLE_ESTACIONES = "estaciones";
    public static String TABLE_PLANIFICADOR = "planificador_metro";

    // Table Names
    private final Context myContext;
    private SQLiteDatabase myDataBase;


    /**
     * Contructor de la aplicacion Crea un objeto DBHelper que nos permitirá
     * controlar la apertura de la base de datos.
     *
     * @param context toma el contexto de la aplicación que lo invoca para poder
     *                acceder a los 'assets' y 'resources' de la aplicacion
     */
    public DBHelperDataBase(Context context) {
        super(context, DB_NAME, null, versionDB);
        this.myContext = context;
    }

    /**
     * Inicia el proceso de copia del fichero de base de datos, o crea una base
     * de datos vacía en su lugar
     */
    public SQLiteDatabase getDataBase() {
        // Abre la base de datos
        try {
            createDataBase();
        } catch (IOException e) {
            throw new Error("Ha sido imposible crear la Base de Datos");
        }

        String myPath = DB_PATH + DB_NAME;
        return SQLiteDatabase.openDatabase(myPath, null,
                SQLiteDatabase.OPEN_READWRITE);
    }

    public void createDataBase() throws IOException {
        boolean dbExist = checkDataBase();

        if (dbExist) {
            System.out.println("La base de datos ya existe");
            // la base de datos existe y no hacemos nada.
        } else {
            // Llamando a este método se crea la base de datos vacía en la ruta
            // por defecto del sistema
            // de nuestra aplicación por lo que podremos sobreescribirla con
            // nuestra base de datos.
            System.out.println("La base de datos NO existe");
            this.getReadableDatabase();

            try {
// /data/data/com.nubenueve.metro/databases/metroEstaciones.db (No such file or directory)
                copyDataBase();

            } catch (IOException e) {
                //Log.e("ERROR COPIANDO DB", e.getMessage());
                throw new Error("Error copiando Base de Datos");
            }
        }

    }

    /**
     * Comprueba si la base de datos existe para evitar copiar siempre el
     * fichero cada vez que se abra la aplicación.
     *
     * @return true si existe, false si no existe
     */
    private boolean checkDataBase() {

        boolean checkdb = false;
        try {
            String myPath = myContext.getFilesDir().getAbsolutePath().replace("files", "databases") + File.separator + DB_NAME;
            File dbfile = new File(myPath);
            checkdb = dbfile.exists();
        } catch (SQLiteException e) {
            System.out.println("Database doesn't exist");
        }

        return checkdb;

//        SQLiteDatabase checkDB = null;
//
//        try {
//
//            String myPath = DB_PATH + DB_NAME;
//            checkDB = SQLiteDatabase.openDatabase(myPath, null,
//                    SQLiteDatabase.OPEN_READWRITE);
//
//        } catch (SQLiteException e) {
//
//            // si llegamos aqui es porque la base de datos no existe todavía.
//        }
//        if (checkDB != null) {
//
//            checkDB.close();
//
//        }
//        return checkDB != null ? true : false;
    }

    /**
     * Copia nuestra base de datos desde la carpeta assets a la recién creada
     * base de datos en la carpeta de sistema, desde dónde podremos acceder a
     * ella. Esto se hace con bytestream.
     */
    private void copyDataBase() throws IOException {

        System.out.println("Copiando BD");

        // Abrimos el fichero de base de datos como entrada
        InputStream myInput = myContext.getAssets().open(DB_NAME);

        // Ruta a la base de datos vacía recién creada
        String outFileName = DB_PATH + DB_NAME;

        // Abrimos la base de datos vacía como salida
        OutputStream myOutput = new FileOutputStream(outFileName);

        // Transferimos los bytes desde el fichero de entrada al de salida
        byte[] buffer = new byte[1024];
        int length;
        while ((length = myInput.read(buffer)) > 0) {
            myOutput.write(buffer, 0, length);
        }

        // Liberamos los streams
        myOutput.flush();
        myOutput.close();
        myInput.close();

    }

    public void open() throws SQLException {

        // Abre la base de datos
        try {
            createDataBase();
        } catch (IOException e) {
            throw new Error("Ha sido imposible crear la Base de Datos");
        }

        String myPath = DB_PATH + DB_NAME;
        myDataBase = SQLiteDatabase.openDatabase(myPath, null,
                SQLiteDatabase.OPEN_READONLY);
    }

    public SQLiteDatabase readDB() {

        String myPath = DB_PATH + DB_NAME;

        return SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);

    }


    @Override
    public synchronized void close() {
        if (myDataBase != null)
            myDataBase.close();
        super.close();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ESTACIONES);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_LINEA);

        // Create tables again
        try {

            copyDataBase();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
