package cl.metro.mobile.Utilities.AdapterFavoritos;


import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import cl.metro.mobile.Fragments.FragmentServicios.ServiciosFragment;
import cl.metro.mobile.MainActivity;
import cl.metro.mobile.Models.Estaciones;
import cl.metro.mobile.R;

public class AdapterListFavoritosEstacion extends BaseAdapter {

    int layoutResourceId;
    Context mContext;
    List<Estaciones> listEstaciones;
    private LayoutInflater inflater;
    private FragmentManager manager;


    public AdapterListFavoritosEstacion(Context context, int resource, int textViewResourceId, List<Estaciones> objects, FragmentManager fragmentManager) {

        this.listEstaciones = objects;
        inflater = LayoutInflater.from(context);
        layoutResourceId = resource;
        mContext = context;
        this.manager = fragmentManager;

    }

    @Override
    public int getCount() {
        return listEstaciones.size();
    }

    @Override
    public Object getItem(int position) {
        return listEstaciones.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        View listItem = convertView;

        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        listItem = inflater.inflate(layoutResourceId, parent, false);

        RelativeLayout contentFavoritos = (RelativeLayout) listItem.findViewById(R.id.contentFavoritos);

        TextView textViewName = (TextView) listItem.findViewById(R.id.txtNombreFavorito);
        textViewName.setText("Los Leones L6");

//        textViewName.setText(listEstaciones.get(position).getNombreEstacion());

        View viewLine = (View) listItem.findViewById(R.id.line);

        MainActivity.initApp.setEstacionImage(listEstaciones.get(position).getIdLinea(), textViewName, mContext);

//        if(position == 0 || position == getCount() -1){
//
//            viewLine.setVisibility(View.VISIBLE);
//
//        }

        contentFavoritos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                ServiciosFragment.estacion = listEstaciones.get(position);
                ServiciosFragment.verServicio = true;
//                ServiciosFragment.idLinea = listEstaciones.get(position).getIdLinea();


                Fragment newFragment = new ServiciosFragment();

                FragmentTransaction transaction = manager.beginTransaction();
                transaction.replace(R.id.container, newFragment);
                transaction.addToBackStack(null);
                transaction.commit();

//                listEstaciones.get(position).getNombreEstacion();
//                Log.i("App", listEstaciones.get(position).getNombreEstacion());
            }
        });

//        listEstaciones.get(position).getEstado();
//
//        String lineTweet = asdf.get(position).getText();
//        textViewName.setText(asdf.get(position).getText());
//
//        TextView txtFecha = (TextView) listItem.findViewById(R.id.txtFecha);
//        txtFecha.setText(getTimeZone(asdf.get(position).getCreatedAt()));

        return listItem;

    }

}
