package cl.metro.mobile.Utilities;

import android.app.Application;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import cl.metro.mobile.Database.DBHelperDataBase;
import cl.metro.mobile.Fragments.TweetsLineasFragment;
import cl.metro.mobile.MainActivity;
import cl.metro.mobile.Models.Bip;
import cl.metro.mobile.Models.Estaciones;
import cl.metro.mobile.Models.Lineas;
import cl.metro.mobile.Models.ListaClubMetro.ListClub;
import cl.metro.mobile.Models.LugaresInteresEntretencion.ListLugaresInteresEntretencion;
import cl.metro.mobile.Models.Planificador;
import cl.metro.mobile.Models.PlanificadorMetro;
import cl.metro.mobile.Models.ServiciosCercanos.CategoriasRubros;
import cl.metro.mobile.Models.ServiciosCercanos.ServiciosCercanosParse;
import cl.metro.mobile.R;

/**
 * Created by Daniel on 11-02-2015.
 */
public class InitApp extends Application {

    public static Estaciones estacionMasCercana = null;

    public static Estaciones currentSelectedEstacion;
    public static Planificador planificadorParse;
    public static List<CategoriasRubros> categoriaList;
    public static ArrayList<String> arrayFiltroMapa = new ArrayList<String>();
    public static List<String> listRubros;
    public static ServiciosCercanosParse serviciosCercanosParse;
    public static ListClub listClubMetro;
    public static ListLugaresInteresEntretencion currentListPatrimonios;
    public static ListLugaresInteresEntretencion currentListEventos;
    public static HashMap<Lineas, ArrayList<Estaciones>> lineasArrayListHashMap;
    public static String gcm_registration_id;
    public static String android_id;
    public ArrayList<Lineas> lineasArrayList;
    public ArrayList<Estaciones> estacionesArrayList;
    DBHelperDataBase dbHelperDataBase;

    public static Planificador getPlanificadorParse() {
        return planificadorParse;
    }

    public static void setPlanificadorParse(Planificador planificadorParse) {
        InitApp.planificadorParse = planificadorParse;
    }

    public static HashMap<Lineas, ArrayList<Estaciones>> getArrayLineasEstacion() {
        return lineasArrayListHashMap;
    }

    public static void validarFavorito(MenuItem favorito) {

        if (MainActivity.initApp.currentSelectedEstacion != null) {
            if (MainActivity.initApp.currentSelectedEstacion.isFavorito()) {
                favorito.setIcon(cl.metro.mobile.R.drawable.ic_star_white_36dp);
                Log.i("App Favoritos", "Favoritos True = " + MainActivity.initApp.currentSelectedEstacion.isFavorito());
            } else {
                favorito.setIcon(cl.metro.mobile.R.drawable.ic_star_outline_white_36dp);
                Log.i("App Favoritos", "Favoritos False = " + MainActivity.initApp.currentSelectedEstacion.isFavorito());
            }
        }

    }

    public static void setEstacionImage(int linea, TextView imageView, Context context) {
        Drawable img = null;
        if (linea == 1) {
            //L1
//            holder.layout.setBackgroundResource(R.color.rojol1);
            img = context.getResources().getDrawable(cl.metro.mobile.R.drawable.ic_l1_24dp);
            imageView.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);

        } else if (linea == 2) {
            //L2
//            holder.layout.setBackgroundResource(R.color.naranjo);
            img = context.getResources().getDrawable(R.drawable.ic_l2_24dp);
            imageView.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);

        } else if (linea == 3) {
            //L4
//            holder.layout.setBackgroundResource(R.color.azull4);
            img = context.getResources().getDrawable(R.drawable.ic_l3_24dp);
            imageView.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);

        }else if (linea == 4) {
            //L4
//            holder.layout.setBackgroundResource(R.color.azull4);
            img = context.getResources().getDrawable(cl.metro.mobile.R.drawable.ic_l4_24dp);
            imageView.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);

        } else if (linea == 5) {
            //L4a
//            holder.layout.setBackgroundResource(R.color.azull4a);

            img = context.getResources().getDrawable(cl.metro.mobile.R.drawable.ic_l4a_24dp);
            imageView.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);

        } else if (linea == 6) {
            //L5
//            holder.layout.setBackgroundResource(R.color.verde_test);
            img = context.getResources().getDrawable(cl.metro.mobile.R.drawable.ic_l5_24dp);
            imageView.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
        } else if (linea == 7) {
            //L5
//            holder.layout.setBackgroundResource(R.color.verde_test);
            img = context.getResources().getDrawable(R.drawable.ic_l6_24dp);
            imageView.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
        }
//        holder.layout.setPadding(20, 20, 0, 40);
    }

    public void obtenerLineasEstaciones() {

        lineasArrayList = new ArrayList<>();
        estacionesArrayList = new ArrayList<>();
        lineasArrayListHashMap = new HashMap<>();
        lineasArrayList = this.obtenerLineas();

        for (Lineas itemLineas : lineasArrayList) {
            ArrayList<Estaciones> estacionesLinea = obtenerEstacionesByLinea(itemLineas.getCodigoLinea());
            lineasArrayListHashMap.put(itemLineas, estacionesLinea);

        }

    }

    public ArrayList<Lineas> obtenerLineas() {

        ArrayList<Lineas> models = new ArrayList<>();
        dbHelperDataBase = new DBHelperDataBase(this);
        SQLiteDatabase sqldb = dbHelperDataBase.readDB();

        Cursor cursor = sqldb.query(DBHelperDataBase.TABLE_LINEA, null, null,
                null, null, null, "id");
        while (cursor.moveToNext()) {
            Lineas linea = new Lineas();
            linea.setIdLinea(cursor.getInt(cursor.getColumnIndex("id")));
            linea.setNombreLinea(cursor.getString(cursor.getColumnIndex("nombreLinea")));
            linea.setCodigoLinea(cursor.getInt(cursor.getColumnIndex("codigoLinea")));
            linea.setNotificacionLinea(cursor.getInt(cursor.getColumnIndex("is_notificacion")) > 0);

            models.add(linea);

            System.out.print(models.toArray().toString());
        }
        cursor.close();
        sqldb.close();

        return models;
    }

    public String[] getLineasNombre() {
        String[] arrayLinea = null;
        int i = 0;

//        if (arrayListModel != null && arrayListModel.size() > 0) {
        arrayLinea = new String[getArrayLineasEstacion().size()];
        ArrayList<Lineas> list = new ArrayList<Lineas>(getArrayLineasEstacion().keySet());
        for (Lineas lineas : list) {
            arrayLinea[i] = lineas.getNombreLinea();
            System.out.println(lineas.getNombreLinea());
            i++;
        }
//        }
        return arrayLinea;
    }

    public String[] getArrayListNombreEstaciones(ArrayList<Estaciones> arrayListEstaciones) {
        String[] arrayEstaciones = null;
        int i = 0;
        if (arrayListEstaciones != null) {

            arrayEstaciones = new String[arrayListEstaciones.size()];

            for (Estaciones estaciones : arrayListEstaciones) {

                arrayEstaciones[i] = estaciones.getNombreEstacion();
                i++;
            }

        }
        return arrayEstaciones;
    }

    public ArrayList<Estaciones> obtenerEstacionesByLinea(int idLinea) {

        ArrayList<Estaciones> estaciones = new ArrayList<Estaciones>();

        dbHelperDataBase = new DBHelperDataBase(this);
        SQLiteDatabase sqldb = dbHelperDataBase.readDB();

        Cursor cursor = sqldb.query(DBHelperDataBase.TABLE_ESTACIONES, null,
                "id_linea" + "= ?", new String[]{""
                        + idLinea}, null, null, "id");
        while (cursor.moveToNext()) {
            Estaciones estacion = getEstacionByCursor(cursor);
            estaciones.add(estacion);
        }
        cursor.close();
        sqldb.close();
        dbHelperDataBase.close();

        return estaciones;
    }

    public ArrayList<String> obtenerDireccionesLinea(int idLinea) {

        ArrayList<Estaciones> estaciones = new ArrayList<Estaciones>();
        ArrayList<String> estacionesDirecciones = new ArrayList<String>();

        dbHelperDataBase = new DBHelperDataBase(this);
        SQLiteDatabase sqldb = dbHelperDataBase.readDB();

        Cursor cursor = sqldb.query(DBHelperDataBase.TABLE_ESTACIONES, null,
                "id_linea" + "= ?", new String[]{""
                        + idLinea}, null, null, "id");
        while (cursor.moveToNext()) {
            Estaciones estacion = getEstacionByCursor(cursor);
            estaciones.add(estacion);
        }
        cursor.close();
        sqldb.close();
        dbHelperDataBase.close();

        estacionesDirecciones.add(estaciones.get(0).getNombreEstacion());
        estacionesDirecciones.add(estaciones.get(estaciones.size() - 1).getNombreEstacion());

        return estacionesDirecciones;
    }

    private Estaciones getEstacionByCursor(Cursor res) {

        Estaciones estacion = new Estaciones();
        estacion.setIdEstacion(res.getInt(res.getColumnIndex("id")));
        estacion.setCodigoEstacion(res.getString(res.getColumnIndex("codigoEstacion")));
        estacion.setNombreEstacion(res.getString(res.getColumnIndex("nombreEstacion")));
        estacion.setLatitud(res.getFloat(res.getColumnIndex("latitud")));
        estacion.setLongitud(res.getFloat(res.getColumnIndex("longitud")));
        estacion.setOrden(res.getInt(res.getColumnIndex("orden")));
        estacion.setFavorito(res.getInt(res.getColumnIndex("is_favorito")) > 0);
        estacion.setIdLinea(res.getInt(res.getColumnIndex("id_linea")));
        estacion.setCombinacion(res.getInt(res.getColumnIndex("is_combinacion")) > 0);

        return estacion;
    }


    public Estaciones getEstacionByCodigo(String codigoEstacion) {
        Estaciones estacion = null;

        dbHelperDataBase = new DBHelperDataBase(this);
        SQLiteDatabase sqldb = dbHelperDataBase.readDB();

        String select = "codigoEstacion = ?";
        Cursor res = sqldb.query(DBHelperDataBase.TABLE_ESTACIONES, null,
                select, new String[]{codigoEstacion}, null, null,
                "id");
        while (res.moveToNext()) {
            estacion = getEstacionByCursor(res);
        }
        res.close();

        return estacion;
    }

    public Estaciones getEstacionForNombreEstacionAndIdLinea(String idLinea, String nombreEstacion) {
        Estaciones estacion = null;

        dbHelperDataBase = new DBHelperDataBase(this);
        SQLiteDatabase sqldb = dbHelperDataBase.readDB();

        String select = "nombreEstacion = ?" + " AND " + "id_linea = ?";
        Cursor res = sqldb.query(DBHelperDataBase.TABLE_ESTACIONES, null,
                select, new String[]{nombreEstacion, idLinea}, null, null,
                "id");
        while (res.moveToNext()) {
            estacion = getEstacionByCursor(res);
        }
        res.close();

        return estacion;
    }

    public ArrayList<Estaciones> getFavoritosEstaciones(boolean isFavorito) {
        ArrayList<Estaciones> models = new ArrayList<Estaciones>();

        dbHelperDataBase = new DBHelperDataBase(this);
        SQLiteDatabase sqldb = dbHelperDataBase.readDB();

        Cursor res = sqldb.query(DBHelperDataBase.  TABLE_ESTACIONES, null,
                "is_favorito = ?", new String[]{isFavorito ? "1" : "0"}, null, null,
            "id_linea");

        while (res.moveToNext()) {
            Estaciones model = getEstacionByCursor(res);
            models.add(model);
        }
        res.close();

        return models;
    }

    public boolean setFavorito(String idLinea, String nombreEstacion, boolean isFavorito, MenuItem favorito) {
        boolean isUpdate = false;

        dbHelperDataBase = new DBHelperDataBase(this);
        SQLiteDatabase sqldb = dbHelperDataBase.readDB();

        ContentValues cv = new ContentValues();
        cv.put("is_favorito", isFavorito ? 1 : 0);
        String stringWhere = "id_linea " + " = ?" + " AND " +
                "nombreEstacion" + " = ?";
        String[] stringValues = {idLinea, nombreEstacion};


        if (sqldb.update(DBHelperDataBase.TABLE_ESTACIONES, cv, stringWhere,
                stringValues) > 0) {
            Log.i("Update", "Actualización OK");

            if (isFavorito) {

                Log.i("App", "isFavorito " + isFavorito + ", true ");

                favorito.setIcon(cl.metro.mobile.R.drawable.ic_star_white_36dp);
            } else {

                Log.i("App", "isFavorito " + isFavorito + ", false ");

                favorito.setIcon(cl.metro.mobile.R.drawable.ic_star_outline_white_36dp);
            }


            isUpdate = true;
        }
        return isUpdate;
    }

    public long insertRecorridoFavorito(PlanificadorMetro planificadorMetro) {
        long idRow = -1;

        PlanificadorMetro model = planificadorMetro;

        ContentValues cv = new ContentValues();

        cv.put("estacion_origen", model.getEstacionOrigen());
        cv.put("estacion_destino", model.getEstacionDestino());
        cv.put("tipo_dia", model.getTipoDia());
        cv.put("hora_dia", model.getHoraDia());
        cv.put("nombre", model.getNombre() != null ? model.getNombre() : "");
        cv.put("is_favorito", model.isFavorito());
        cv.put("tiempo", model.getTiempo());

        dbHelperDataBase = new DBHelperDataBase(this);
        SQLiteDatabase sqldb = dbHelperDataBase.readDB();

        idRow = sqldb.insert(DBHelperDataBase.TABLE_PLANIFICADOR, null, cv);

        sqldb.close();


        return idRow;
    }

    public long insertBip(Bip bip) {
        long idRow = -1;

        dbHelperDataBase = new DBHelperDataBase(this);
        SQLiteDatabase sqldb = dbHelperDataBase.readDB();

        String queryf = "select * from bip where numero_bip =" + "'" + bip.getNumeroBip() + "'";

        Cursor c = sqldb.rawQuery(queryf, null);
        if (c.getCount() == 0) {

            ContentValues cv = new ContentValues();
            cv.put("nombre", bip.getNombre());
            cv.put("numero_bip", bip.getNumeroBip());
            cv.put("is_favorito", bip.isFavorito());
            cv.put("saldo", bip.getSaldo());

            try {

                idRow = sqldb.insertWithOnConflict("bip", null, cv, SQLiteDatabase.CONFLICT_REPLACE);
                sqldb.close();
                c.close();

            } catch (Exception e) {

            }

        }

        if (idRow != -1) {
            Log.i("Sqlite", "Insert Bip Succesful");
        } else {
            Log.i("Sqlite", "No se ha insertado nada.");
        }


        return idRow;
    }

    public boolean updateBip(Bip bip) {
        boolean isUpdate = false;

        dbHelperDataBase = new DBHelperDataBase(this);
        SQLiteDatabase sqldb = dbHelperDataBase.readDB();

        ContentValues cv = new ContentValues();
        cv.put("nombre", bip.getNombre());
        cv.put("numero_bip", bip.getNumeroBip());
        cv.put("is_favorito", bip.isFavorito());
        cv.put("saldo", bip.getSaldo());

        Log.i("Update", "nombre " + bip.getNombre());
        Log.i("Update", "numero_bip " + bip.getNumeroBip());
        Log.i("Update", "is_favorito " + bip.isFavorito());


        if (sqldb.update("bip", cv, "numero_bip =?",
                new String[]{"" + bip.getNumeroBip()}) > 0) {
            Log.i("Update", "Actualización OK");
            isUpdate = true;
        }


        return isUpdate;
    }

    public ArrayList<Bip> getAllBip() {
        ArrayList<Bip> models = new ArrayList<>();

        dbHelperDataBase = new DBHelperDataBase(this);
        SQLiteDatabase sqldb = dbHelperDataBase.readDB();

        Cursor res = sqldb.query("bip", null,
                "is_favorito = ?", new String[]{"1"}, null, null,
                "id");

        while (res.moveToNext()) {
            Bip model = new Bip();
            model.setId(res.getInt(res.getColumnIndex("id")));
            model.setNombre(res.getString(res
                    .getColumnIndex("nombre")));
            model.setNumeroBip(res.getString(res
                    .getColumnIndex("numero_bip")));
            model.setFavorito((res.getInt(res
                    .getColumnIndex("is_favorito"))) > 0);
            model.setSaldo(res.getString(res.getColumnIndex("saldo")));

            models.add(model);
        }
        res.close();
        return models;
    }

    public ArrayList<PlanificadorMetro> getAllPlanificadoresMetro() {
        ArrayList<PlanificadorMetro> models = new ArrayList<PlanificadorMetro>();

        dbHelperDataBase = new DBHelperDataBase(this);
        SQLiteDatabase sqldb = dbHelperDataBase.readDB();

        Cursor res = sqldb.query(DBHelperDataBase.TABLE_PLANIFICADOR,
                null, null, null, null, null,
                "id");
        while (res.moveToNext()) {
            PlanificadorMetro model = getPlanificadorMetroByCursor(res);
            models.add(model);
        }

        res.close();

        return (ArrayList<PlanificadorMetro>) models;
    }

    public PlanificadorMetro getPlanificadorMetroByCursor(Cursor res) {
        PlanificadorMetro model = new PlanificadorMetro();
        if (res != null) {
            model.setId(res.getInt(res
                    .getColumnIndex("id")));
            model.setEstacionOrigen(res.getString(res
                    .getColumnIndex("estacion_origen")));
            model.setEstacionDestino(res.getString(res
                    .getColumnIndex("estacion_destino")));
            model.setTipoDia(res.getString(res
                    .getColumnIndex("tipo_dia")));
            model.setHoraDia(res.getString(res
                    .getColumnIndex("hora_dia")));
            model.setNombre(res.getString(res
                    .getColumnIndex("nombre")));
            model.setFavorito((res.getInt(res
                    .getColumnIndex("is_favorito")) > 0));
            model.setTiempo(res.getString(res
                    .getColumnIndex("tiempo")));
        }
        return model;
    }

    public boolean borrarPlanificador(PlanificadorMetro planificadorMetro) {
        boolean isDelete = false;

        dbHelperDataBase = new DBHelperDataBase(this);
        SQLiteDatabase sqldb = dbHelperDataBase.readDB();

        int temp = sqldb.delete(DBHelperDataBase.TABLE_PLANIFICADOR, "id = " + planificadorMetro.getId(), null);
        if (temp != 0) {
            isDelete = true;
        }

        return isDelete;
    }

    public Estaciones getEstacionMasCercana() {
        return estacionMasCercana;
    }

    public void setEstacionMasCercana(Estaciones estacionMasCercana, Location _Location) {

        if (this.estacionMasCercana == null) {
            this.estacionMasCercana = estacionMasCercana;
        } else if ((this.estacionMasCercana.getDistance(_Location) >= estacionMasCercana.getDistance(_Location))
                && estacionMasCercana != null) {
            this.estacionMasCercana = estacionMasCercana;
        }
    }

    public void searchEstacionMasCercana(Location location, TextView txtEstacionCercana, TextView txtEstacionDistancia, Context context) {
        for (ArrayList<Estaciones> arrayList : lineasArrayListHashMap.values()) {
            for (Estaciones estaciones : arrayList) {
                setEstacionMasCercana(estaciones, location);
            }
        }

        getCodigoEstacion(MainActivity.initApp.estacionMasCercana);

        // Log.d("Estacion mas cercana", this.estacionMasCercana.getNombreEstacion());
        // Log.d("codigoEstacion", "Codigo Estacion: " + Utils.codigoEstacion);

        if (txtEstacionCercana != null) {

            txtEstacionCercana.setText(MainActivity.initApp.getEstacionMasCercana().getNombreEstacion());
            setEstacionImage(MainActivity.initApp.getEstacionMasCercana().getIdLinea(), txtEstacionCercana, context);
            txtEstacionDistancia.setText(MainActivity.initApp.getEstacionMasCercana().getDistancia());

            //Subraya la estacion mas cercana.
            txtEstacionCercana.setPaintFlags(TweetsLineasFragment.txtEstacionCercana.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        }

    }

    public void searchEstacionMasCercanaChoosen(Estaciones estacion, Context context) {
        Location loc = new Location("dummy");
        loc.setLatitude(estacion.getLatitud());
        loc.setLongitude(estacion.getLongitud());
        setEstacionMasCercana(estacion, loc);
        getCodigoEstacion(estacion);

       // Log.d("codigoEstacion", "Codigo Estacion: " + Utils.codigoEstacion);
       // Log.d("Estacion mas cercana", this.estacionMasCercana.getNombreEstacion());

        if (TweetsLineasFragment.txtEstacionCercana != null) {

            TweetsLineasFragment.txtEstacionCercana.setText(MainActivity.initApp.getEstacionMasCercana().getNombreEstacion());
            TweetsLineasFragment.txtEstacionDistancia.setText(MainActivity.initApp.getEstacionMasCercana().getDistancia());
            setEstacionImage(MainActivity.initApp.getEstacionMasCercana().getIdLinea(), TweetsLineasFragment.txtEstacionCercana, context);

            //Subraya la estacion mas cercana.
            TweetsLineasFragment.txtEstacionCercana.setPaintFlags(TweetsLineasFragment.txtEstacionCercana.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        }

    }

    public void getCodigoEstacion(Estaciones estacion) {

//        if (!estacion.isCombinacion()) {
//            Utils.codigoEstacion = Utils.estacionFormat(MainActivity.initApp.estacionMasCercana.getCodigoEstacion(), MainActivity.initApp.estacionMasCercana.getIdLinea());
//        } else {
            Utils.codigoEstacion = MainActivity.initApp.estacionMasCercana.getCodigoEstacion();
//        }
    }

}
