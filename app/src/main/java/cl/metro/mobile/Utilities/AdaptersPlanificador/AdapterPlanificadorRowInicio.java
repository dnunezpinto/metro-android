/**
 *
 */
package cl.metro.mobile.Utilities.AdaptersPlanificador;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import cl.metro.mobile.Models.Ruta;
import cl.metro.mobile.R;

/**
 * @author jvalladares
 */
@SuppressLint("DefaultLocale")
public class AdapterPlanificadorRowInicio {

    private static final String DIRIGITE_A = "Dirígete a";
    private static final String ESTACION = "Estación ";

    private View mFullView;
    private ViewHolderInicio holder;

    public AdapterPlanificadorRowInicio(LayoutInflater inflater,
                                        Ruta model, View convertView) {

//		if(convertView == null){
//			convertView = (View)inflater.inflate(R.layout.item_list_planifique_main_or_finish, null);
//			holder = new ViewHolderInicio();
//			holder.image = (ImageView) convertView.findViewById(R.id.image_recorrido);
//			holder.textNombre = (TextView)convertView.findViewById(R.id.text_nombre);
//			holder.textDescripcion = (TextView)convertView.findViewById(R.id.text_descripcion);
//
//			convertView.setTag(holder);
//		}else{
//			holder = (ViewHolderInicio)convertView.getTag();
//		}


        convertView = (View) inflater.inflate(R.layout.item_list_planifique_main_or_finish, null);
        holder = new ViewHolderInicio();
        holder.imageWalk = (ImageView) convertView.findViewById(R.id.image_recorrido);
//        holder.imageLine = (ImageView) convertView.findViewById(R.id.imageView21);
        holder.textNombre = (TextView) convertView.findViewById(R.id.text_nombre);
        holder.textDescripcion = (TextView) convertView.findViewById(R.id.text_descripcion);
//
//
        holder.textNombre.setText(DIRIGITE_A);
        holder.textDescripcion.setText(ESTACION + model.getEstacionNombre()); //+ " " + model.getEstacionLinea().toUpperCase()


        this.mFullView = convertView;
    }

    public View getView() {
        return this.mFullView;
    }

    public static class ViewHolderInicio {
        ImageView imageWalk;
        ImageView imageLine;
        TextView textNombre;
        TextView textDescripcion;
    }


}
