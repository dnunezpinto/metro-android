package cl.metro.mobile.Utilities;


import android.Manifest;
import android.app.AlertDialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

public class GPSTracker extends Service implements LocationListener {

    //The minimum distance to change updates in metters
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 0; //10 metters
    //The minimum time beetwen updates in milliseconds
    private static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 1; // 1 minute
    public static String lat;
    public static String lng;
    final private int REQUEST_CODE_ASK_PERMISSIONS = 123;
    public boolean canGetLocation = false;
    public Location location;
    public double latitude;
    public double longitude;
    //Declaring a Location Manager
    protected LocationManager locationManager;
    //flag for GPS Status
    boolean isGPSEnabled = false;
    //flag for network status
    boolean isNetworkEnabled = false;
    private Context mContext;

    public GPSTracker(Context context) {
        this.mContext = context;
        getLocation();
    }

    private void showGPSDisabledAlertToUser(Context context) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setMessage("GPS está desactivado en su dispositivo. ¿Le gustaría activarlo?")
                .setCancelable(false)
                .setPositiveButton("Ir a Ajustes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent callGPSSettingIntent = new Intent(
                                        android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                startActivity(callGPSSettingIntent);
                            }
                        });
        alertDialogBuilder.setNegativeButton("No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

    public Location getLocation() {
        try {
            locationManager = (LocationManager) mContext.getSystemService(LOCATION_SERVICE);

            //obtiene el estado del gps
            isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

            //Obtiene si hay conexion por wifi o internet mobile
            isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

//            Log.i("Network Enabled", "isNetworkEnabled: " + isNetworkEnabled);
//            Log.i("GPS Enabled", "isGPSEnabled: " + isGPSEnabled);

            if (!isGPSEnabled && !isNetworkEnabled) {
                this.canGetLocation = false;
//                showGPSDisabledAlertToUser(mContext);
            } else {
                this.canGetLocation = true;

                if (isNetworkEnabled) {

                    Log.d("Network", "Network");

                    if (ContextCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                            ContextCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {


                        locationManager.requestLocationUpdates(
                                LocationManager.NETWORK_PROVIDER,
                                MIN_TIME_BW_UPDATES,
                                MIN_DISTANCE_CHANGE_FOR_UPDATES, this);


                        if (locationManager != null) {

                            location = locationManager
                                    .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

                            if (location != null) {
                                this.canGetLocation = true;
                                lat = String.valueOf(location.getLatitude());
                                lng = String.valueOf(location.getLongitude());
//                                MainActivity.initApp.searchEstacionMasCercana(location, TweetsLineasFragment.txtEstacionCercana, TweetsLineasFragment.txtEstacionDistancia, mContext);

                            } else {
                                this.canGetLocation = false;
                            }
                        } else
                            // if GPS Enabled get lat/long using GPS Services
                            if (isGPSEnabled) {
                                if (location == null) {
                                    locationManager.requestLocationUpdates(
                                            LocationManager.GPS_PROVIDER,
                                            MIN_TIME_BW_UPDATES,
                                            MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                                    Log.d("GPS Enabled", "GPS Enabled");
                                    if (locationManager != null) {
                                        location = locationManager
                                                .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                                        if (location != null) {
                                            this.canGetLocation = true;

                                            lat = String.valueOf(location.getLatitude());
                                            lng = String.valueOf(location.getLongitude());

                                        } else {
                                            this.canGetLocation = false;
                                        }
                                    }
                                } else {
//                                    MainActivity.initApp.searchEstacionMasCercana(location, TweetsLineasFragment.txtEstacionCercana, TweetsLineasFragment.txtEstacionDistancia, mContext);
                                }

                            }


                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("Error : Location", "Impossible to connect to LocationManager", e);
        }

        return location;
    }


    public void updateGPSCoordinates() {
        if (location != null) {
            latitude = location.getLatitude();
            longitude = location.getLongitude();


        }
    }


    /**
     * Stop using GPS listener
     * Calling this function will stop using GPS in your app
     */

    public void stopUsingGPS() {
        if (locationManager != null) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            locationManager.removeUpdates(GPSTracker.this);
        }
    }

    /**
     * Function to get latitude
     */
    public double getLatitude() {
        if (location != null) {
            latitude = location.getLatitude();
        }

        return latitude;
    }

    /**
     * Function to get longitude
     */
    public double getLongitude() {
        if (location != null) {
            longitude = location.getLongitude();
        }

        return longitude;
    }

    /**
     * Function to check GPS/wifi enabled
     */
    public boolean canGetLocation() {
        return this.canGetLocation;
    }

    /**
     * Get provider name.
     *
     * @return Name of best suiting provider.
     */
    String getProviderName() {
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        Criteria criteria = new Criteria();
        criteria.setPowerRequirement(Criteria.POWER_LOW); // Chose your desired power consumption level.
        criteria.setAccuracy(Criteria.ACCURACY_FINE); // Choose your accuracy requirement.
        criteria.setSpeedRequired(true); // Chose if speed for first location fix is required.
        criteria.setAltitudeRequired(false); // Choose if you use altitude.
        criteria.setBearingRequired(false); // Choose if you use bearing.
        criteria.setCostAllowed(false); // Choose if this provider can waste money :-)

        // Provide your criteria and flag enabledOnly that tells
        // LocationManager only to return active providers.
        return locationManager.getBestProvider(criteria, true);
    }

    /**
     * Function to show settings alert dialog
     */
//    public void showSettingsAlert()
//    {
//        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);
//        
//        alertDialog.setMessage("Su GPS parece estar deshabilitado, �Quieres activarlo?");
//        alertDialog.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() 
//        {   
//            @Override
//            public void onClick(DialogInterface dialog, int which) 
//            {
//                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
//                mContext.startActivity(intent);
//            }
//        });
//        alertDialog.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() 
//        {   
//            @Override
//            public void onClick(DialogInterface dialog, int which) 
//            {
//                dialog.cancel();
//            }
//        });
//
//        alertDialog.show();
//    }
    @Override
    public void onLocationChanged(Location location) {
    }

    @Override
    public void onProviderDisabled(String provider) {
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


}