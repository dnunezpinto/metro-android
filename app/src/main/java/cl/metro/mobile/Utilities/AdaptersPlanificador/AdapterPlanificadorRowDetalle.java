package cl.metro.mobile.Utilities.AdaptersPlanificador;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import cl.metro.mobile.Models.Ruta;
import cl.metro.mobile.R;


public class AdapterPlanificadorRowDetalle {

    private View mFullView;
    private ViewHolderDetalle holder;

    public AdapterPlanificadorRowDetalle(LayoutInflater inflater, Ruta model,
                                         View convertView, ViewGroup parent) {

        mFullView = null;
        holder = null;

            mFullView = inflater.inflate(
                    R.layout.item_list_planifique_detalle, parent, false);
            holder = new ViewHolderDetalle();
            holder.layout = mFullView
                    .findViewById(R.id.layout_container);
            holder.imageVinieta1 =  mFullView
                    .findViewById(R.id.image_vinieta1);
            holder.imageVinieta2 =  mFullView
                    .findViewById(R.id.image_vinieta2);

            holder.lineaColor =  mFullView
                    .findViewById(R.id.imageView21);
            holder.txtTiempo =  mFullView.findViewById(R.id.txtTiempo);
            holder.txtDistancia =  mFullView.findViewById(R.id.txtDistancia);
            holder.textDescripcion =  mFullView
                    .findViewById(R.id.text_descripcion);
            holder.textVinieta1 =  mFullView
                    .findViewById(R.id.text_vinieta1);
            holder.textVinieta2 =  mFullView
                    .findViewById(R.id.text_vinieta2);
            mFullView.setTag(holder);

        //holder.textNombre.setText(model.getEstacionLineaNombre());
//		holder.textNombre.setText("");
//		holder.textNombre.setVisibility(View.GONE);

        String descripcion = "Sube al Metro en Estación "+ model.getEstacionNombre();


        holder.textDescripcion.setText(descripcion);
        holder.txtTiempo.setText(model.getTiempo());
        holder.txtDistancia.setText(model.getDistancia());

        setBkgColor(model.getEstacionLinea());


//		if (holder.imageVinieta1 != null && holder.imageVinieta2 != null) {
//            setArrow(model.getEstacionLinea());
//		}

        holder.textVinieta1.setText("Dirección " + model.getDireccion());
        holder.textVinieta2.setText(model.getEstacionBajada());

        //holder.textVinieta2.setText(model.getEstacionBajada());
      //  holder.textVinieta2.setText("Los Leones L6");


    }


//	private void setArrow(String linea) {
//
//		if (linea.equalsIgnoreCase("l1")) {
//			setImageView(holder.imageVinieta1, R.drawable.ic__arrow_right_white36dp);
//			setImageView(holder.imageVinieta2, R.drawable.ic_arrow_down_l1);
//		} else if (linea.equalsIgnoreCase("l2")) {
//            setImageView(holder.imageVinieta1, R.drawable.ic__arrow_right_white36dp);
//            setImageView(holder.imageVinieta2, R.drawable.ic_arrow_down_l1);
//		} else if (linea.equalsIgnoreCase("l4")) {
//            setImageView(holder.imageVinieta1, R.drawable.ic__arrow_right_white36dp);
//            setImageView(holder.imageVinieta2, R.drawable.ic_arrow_down_l1);
//		} else if (linea.equalsIgnoreCase("l4a")) {
//            setImageView(holder.imageVinieta1, R.drawable.ic__arrow_right_white36dp);
//            setImageView(holder.imageVinieta2, R.drawable.ic_arrow_down_l1);
//		} else if (linea.equalsIgnoreCase("l5")) {
//            setImageView(holder.imageVinieta1, R.drawable.ic__arrow_right_white36dp);
//            setImageView(holder.imageVinieta2, R.drawable.ic_arrow_down_l1);
//		} else{ //Default
//            setImageView(holder.imageVinieta1, R.drawable.ic__arrow_right_white36dp);
//            setImageView(holder.imageVinieta2, R.drawable.ic_arrow_down_l1);
//		}
//	}

    private void setBkgColor(String linea) {
        if (linea.equalsIgnoreCase("l1")) {
            //L1
            holder.lineaColor.setBackgroundResource(R.color.rojo_l1);

        } else if (linea.equalsIgnoreCase("l2")) {
            //L2
            holder.lineaColor.setBackgroundResource(R.color.naranjo_l2);

        } else if (linea.equalsIgnoreCase("l3")) {
            //L3
            holder.lineaColor.setBackgroundResource(R.color.cafe_l3);

        }else if (linea.equalsIgnoreCase("l4")) {
            //L4
            holder.lineaColor.setBackgroundResource(R.color.azul_l4);

        } else if (linea.equalsIgnoreCase("l4A")) {
            //L4a
            holder.lineaColor.setBackgroundResource(R.color.celeste_l4a);

        } else if (linea.equalsIgnoreCase("l5")) {
            //L5
            holder.lineaColor.setBackgroundResource(R.color.verde_l5);

        } else if (linea.equalsIgnoreCase("l6")) {
            //L5
            holder.lineaColor.setBackgroundResource(R.color.morado_l6);
        }
//        holder.layout.setPadding(20, 20, 0, 40);
    }

    private void setImageView(ImageView image, int resId) {
        image.setImageResource(resId);
    }


    public View getView() {
        return this.mFullView;
    }

    public static class ViewHolderDetalle {
        RelativeLayout layout;
        TextView textNombre;
        TextView textDescripcion;
        TextView txtTiempo;
        TextView txtDistancia;
        ImageView lineaColor;
        ImageView imageVinieta1;
        ImageView imageVinieta2;
        TextView textVinieta1;
        TextView textVinieta2;
    }
}
