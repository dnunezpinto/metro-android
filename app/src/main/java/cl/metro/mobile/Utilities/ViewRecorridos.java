package cl.metro.mobile.Utilities;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import cl.metro.mobile.Fragments.FragmentServicios.ServicioParaderosRecorridosFragment;
import cl.metro.mobile.R;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class ViewRecorridos extends RelativeLayout {


    public ImageView imgEstado;
    public TextView txtNumRecorrido, txtDireccion;
    public String colorParse;
    private Typeface robotoBold, tsMapGruesa, tsInfGruesa;
    private LinearLayout contentRecorrido;
    private Context mContext;
    private ImageView etiquetaView;


    public ViewRecorridos(Context context, String codigoParadero, String mRecorrido) {
        super(context);

        LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.view_paradero_test2, this, true);

        mContext = context;


        etiquetaView = (ImageView) findViewById(R.id.imageView19);

        robotoBold = Typeface.createFromAsset(context.getAssets(), "Roboto-Bold.ttf");
        tsInfGruesa = Typeface.createFromAsset(context.getAssets(), "TSInfGruesa.otf");
        tsMapGruesa = Typeface.createFromAsset(context.getAssets(), "TSMapGruesa.otf");

        txtNumRecorrido = (TextView) findViewById(R.id.textView82);
        txtNumRecorrido.setTypeface(tsMapGruesa);

        txtDireccion = (TextView) findViewById(R.id.textView67);
        txtDireccion.setTypeface(tsInfGruesa);

        contentRecorrido = (LinearLayout) findViewById(R.id.contentRecorrido);

        getDataRecorrido(codigoParadero, mRecorrido);

//        LayerDrawable layerDrawable = (LayerDrawable) getResources()
//                .getDrawable(R.drawable.triangle_test);
//        ShapeDrawable gradientDrawable = (ShapeDrawable) layerDrawable
//                .findDrawableByLayerId(R.id.shape);
//        gradientDrawable.set

//        LayerDrawable backgroundGradient = (LayerDrawable )imageView.getBackground();
//        backgroundGradient.setColor(getResources().getColor(R.color.naranjo));

//        ShapeDrawable shapeDrawable = (ShapeDrawable)imageView.getBackground();
//        shapeDrawable.getPaint().setColor(getResources().getColor(R.color.naranjo));
//        ((LayerDrawable)imageView.getBackground()).getPaint().setColor(getResources().getColor(R.color.naranjo));


    }

    public void getDataRecorrido(String codigoParadero, String mRecorrido) {

        ServicioParaderosRecorridosFragment.apiService.getParaderosRecorridos(codigoParadero, mRecorrido, new Callback<Response>() {
            @Override
            public void success(Response response, Response response2) {


                try {
                    JSONObject jResponse = new JSONObject(Utils.convertResponseBody(response));
                    String color = jResponse.getString("color");


                    if (color.equalsIgnoreCase("Amarillo")) {
                        etiquetaView.setImageResource(R.drawable.ic_amarillo_32dp);
                    } else if (color.equalsIgnoreCase("Azul")) {
                        etiquetaView.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_azul_32dp));
                    } else if (color.equalsIgnoreCase("Celeste")) {
                        etiquetaView.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_celeste_32dp));
                    } else if (color.equalsIgnoreCase("Naranja")) {
                        etiquetaView.setImageResource(R.drawable.ic_naranja_32dp);
                    } else if (color.equalsIgnoreCase("Rojo")) {
                        etiquetaView.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_rojo_32dp));
                    } else if (color.equalsIgnoreCase("Turquesa")) {
                        etiquetaView.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_turquesa_32dp));
                    } else if (color.equalsIgnoreCase("Verde")) {
                        etiquetaView.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_verde_32dp));
                    }

                    JSONArray jArray = jResponse.getJSONArray("texto");
                    for (int i = 0; i < jArray.length(); i++) {
                        String items = jArray.getString(i);
                        TextView textView = new TextView(mContext);
                        textView.setText(items);
                        textView.setTextSize(14);
                        textView.setTypeface(tsInfGruesa);
                        textView.setTextColor(mContext.getResources().getColor(android.R.color.black));
                        textView.setPadding(8, 0, 0, 0);
                        contentRecorrido.addView(textView);

                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void failure(RetrofitError error) {

            }
        });

    }

}
