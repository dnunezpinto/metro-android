package cl.metro.mobile.Utilities.AdapterFavoritos;


import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import cl.metro.mobile.Fragments.PlanificadorDetalleFragment;
import cl.metro.mobile.Models.PlanificadorMetro;
import cl.metro.mobile.R;

public class AdapterListFavoritosRecorridos extends BaseAdapter {

    int layoutResourceId;
    Context mContext;
    List<PlanificadorMetro> metroList;
    private LayoutInflater inflater;
    private FragmentManager manager;

    public AdapterListFavoritosRecorridos(Context context, int resource, int textViewResourceId, List<PlanificadorMetro> objects, FragmentManager fragmentManager) {

        this.metroList = objects;
        inflater = LayoutInflater.from(context);
        layoutResourceId = resource;
        mContext = context;
        this.manager = fragmentManager;

    }

    @Override
    public int getCount() {
        return metroList.size();
    }

    @Override
    public Object getItem(int position) {
        return metroList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        View listItem = convertView;

        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        listItem = inflater.inflate(layoutResourceId, parent, false);

        RelativeLayout contentFavoritos = (RelativeLayout) listItem.findViewById(R.id.contentFavoritos);

        TextView textViewName = (TextView) listItem.findViewById(R.id.txtNombreFavorito);
        textViewName.setText(metroList.get(position).getNombre());

        View viewLine = (View) listItem.findViewById(R.id.line);

        Drawable img = mContext.getResources().getDrawable(R.drawable.ic_recorrido);
        textViewName.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);

//
//        if(position == 0 || position == getCount() -1){
//
//            viewLine.setVisibility(View.VISIBLE);
//
//        }

        contentFavoritos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Fragment fragment = new PlanificadorDetalleFragment();
                Bundle bundle = new Bundle();
                bundle.putString("codigoEstacionOrigen", metroList.get(position).getEstacionOrigen());
                bundle.putString("codigoEstacionDestino", metroList.get(position).getEstacionDestino());
                bundle.putString("tipoDia", metroList.get(position).getTipoDia());
                bundle.putString("horaEstimada", metroList.get(position).getHoraDia());

                fragment.setArguments(bundle);
                manager.beginTransaction()
                        .replace(R.id.container, fragment)
                        .addToBackStack(null)
                        .commit();

//                listEstaciones.get(position).getNombreEstacion();
//                Log.i("App", listEstaciones.get(position).getNombreEstacion());
            }
        });

        return listItem;

    }

}
