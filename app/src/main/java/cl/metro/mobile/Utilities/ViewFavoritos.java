package cl.metro.mobile.Utilities;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;


public class ViewFavoritos extends RelativeLayout {


    public ImageView imgEstado;
    public TextView txtNombreFavorito;
    private Typeface robotoLight;


    public ViewFavoritos(Context context) {
        super(context);

        LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        inflater.inflate(cl.metro.mobile.R.layout.list_item_favoritos, this, true);

        robotoLight = Typeface.createFromAsset(context.getAssets(), "Roboto-Light.ttf");


        txtNombreFavorito = (TextView) findViewById(cl.metro.mobile.R.id.txtNombreFavorito);
        txtNombreFavorito.setTypeface(robotoLight);

//        imgEstado = (ImageView) findViewById(R.id.imgEstado);

    }

}
