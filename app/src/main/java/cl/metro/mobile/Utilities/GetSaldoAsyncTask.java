package cl.metro.mobile.Utilities;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.net.ssl.HttpsURLConnection;

import cl.metro.mobile.Fragments.BipFragment;
import cl.metro.mobile.MainActivity;
import cl.metro.mobile.Models.Bip;
import cl.metro.mobile.R;

public class GetSaldoAsyncTask extends AsyncTask {

    public static final int ERROR_CONEXION = 1;
    public static final int ERROR_DESCONOCIDO = 4;
    public static final int ERROR_HOST_EXCEPTION = 3;
    public static final int ERROR_TARJETA = 2;
    public static final int NO_REGISTRADA = -1;
    public Context mContext;
    //    public Activity mActivity;
    Matcher matcher;
    String numeroTarjeta;
    String saldo;
    String fecha;
    private MaterialDialog _materialDialog;
    private Bip newBip;

    private MaterialDialog dialogRequest;


    public GetSaldoAsyncTask(Context context) {
//        mActivity = activity;
        mContext = context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        dialogRequest = new MaterialDialog.Builder(mContext)
                .content("Consultando Saldo, favor espere ...")
                .progress(true, 0)
                .autoDismiss(false)
                .cancelable(false)
                .show();

    }

    protected Object doInBackground(Object aobj[]) {


        return doInBackground((Integer[]) aobj);
    }

    protected String doInBackground(Integer ainteger[]) {
//        Log.("consultando saldo...");


        Integer integer = ainteger[0];
        numeroTarjeta = String.valueOf(integer);
        String response = "";
        try {

            URL url = new URL("http://pocae.tstgo.cl/PortalCAE-WAR-MODULE/SesionPortalServlet");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setDoInput(true);
            connection.setDoOutput(true);

            HashMap<String, String> params = new HashMap<>();
            params.put("accion", "6");
            params.put("NumDistribuidor", "99");
            params.put("NomUsuario", "usuInternet");
            params.put("NumTarjeta", String.valueOf(integer));
            params.put("RutUsuario", "0");
            params.put("NomHost", "AFT");
            params.put("NomDominio", "aft.cl");
            params.put("bloqueable", "0");
            params.put("Trx", "");

            OutputStream os = connection.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(getPostDataString(params));

            writer.flush();
            writer.close();
            os.close();

            int responseCode = connection.getResponseCode();

            if (responseCode == HttpsURLConnection.HTTP_OK) {
                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                while ((line = br.readLine()) != null) {
                    response += line;
                }
            } else {
                response = "";

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return response;

    }

    protected void onPostExecute(Object obj) {
        onPostExecute((String) obj);
    }

    protected void onPostExecute(String respuesta) {

//        System.out.println("Resultado: " + respuesta);
        dialogRequest.dismiss();

        if (respuesta.equals(String.valueOf(3))) {
//            act.resultNull(3);
            Toast.makeText(mContext,
                    "Error",
                    Toast.LENGTH_LONG).show();
            return;
        }
        if (respuesta.equals(String.valueOf(1))) {
//            act.resultNull(1);
            Toast.makeText(mContext,
                    "Error",
                    Toast.LENGTH_LONG).show();
            return;
        }
        if (respuesta.equals(String.valueOf(4))) {
//            act.resultNull(4);
            Toast.makeText(mContext,
                    "Error",
                    Toast.LENGTH_LONG).show();
            return;
        }
        if (respuesta.contains("cod 100")) {
//            act.resultNull(0);
            Toast.makeText(mContext,
                    "Error",
                    Toast.LENGTH_LONG).show();
            return;
        }

        String as[];

        saldo = "";
        fecha = "";

        as = respuesta.split(Pattern.quote("$"));
//                    Log.d("a", (new StringBuilder("count:")).append(as.length).toString());

        if (as.length <= 1) {
//                        Log.d("saldo", saldo);
//                        Log.d("fecha", fecha);

//                        showDialog();

        } else {

            saldo = as[1].split(Pattern.quote("<"))[0];
            Log.d("saldo", saldo);
            matcher = Pattern.compile("[0-9][0-9]/[0-9][0-9]/[0-9][0-9][0-9][0-9] [0-9][0-9]:[0-9][0-9]").matcher(as[1]);
        }

        if (matcher != null && matcher.find()) {
            fecha = matcher.group();
//                       Log.d("fecha", fecha);

        }
//        else {

//            showDialog();

//        }

        showDialog();


    }

    private String getPostDataString(HashMap<String, String> params) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean first = true;
        for (Map.Entry<String, String> entry : params.entrySet()) {
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
        }

        return result.toString();
    }

    public void showDialog() {

        newBip = new Bip();
        newBip.setNombre("");
        newBip.setNumeroBip(numeroTarjeta);
        newBip.setFavorito(false);
        newBip.setSaldo(saldo);

        MainActivity.initApp.insertBip(newBip);

        _materialDialog = new MaterialDialog.Builder(mContext)
                .customView(R.layout.layout_saldo_bip, true)
                .autoDismiss(false)
                .cancelable(false)
                .negativeText("No, gracias")
                .positiveText("Guardar tarjeta Bip!")
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        super.onPositive(dialog);

                        guardarBip();


                    }

                    @Override
                    public void onNegative(MaterialDialog dialog) {
                        super.onNegative(dialog);

                        dialog.dismiss();

                    }
                })
                .build();


        View view = _materialDialog.getCustomView();


        TextView txtSaldo = (TextView) view.findViewById(R.id.textView61);
        TextView txtFecha = (TextView) view.findViewById(R.id.textView36);
        TextView txtNumeroTarjeta = (TextView) view.findViewById(R.id.textView59);


        txtSaldo.setText(saldo);
        txtFecha.setText(fecha);
        txtNumeroTarjeta.setText(numeroTarjeta);

        _materialDialog.show();
    }

    public void guardarBip() {

        MaterialDialog guardarBipDialog = new MaterialDialog.Builder(mContext)
                .autoDismiss(false)
                .cancelable(false)
                .negativeText("Cancelar")
                .positiveText("Guardar")
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        super.onPositive(dialog);

                        dialog.dismiss();
                        _materialDialog.dismiss();
                        Utils.hideSoftKeyboard(mContext);

                        newBip.setNombre(dialog.getInputEditText().getText().toString());
                        newBip.setFavorito(true);
                        MainActivity.initApp.updateBip(newBip);
                        BipFragment.getTarjetasBip();

                    }

                    @Override
                    public void onNegative(MaterialDialog dialog) {
                        super.onNegative(dialog);
                        dialog.dismiss();
                        Utils.hideSoftKeyboard(mContext);
                    }
                })
                .input("Nombre Tarjeta", null, new MaterialDialog.InputCallback() {
                    @Override
                    public void onInput(MaterialDialog materialDialog, CharSequence charSequence) {

                        if (charSequence.toString().isEmpty() || charSequence == "" || charSequence == null) {
                            materialDialog.getInputEditText().setError("Ingrese Nombre tarjeta");
                        } else {
                            materialDialog.getInputEditText().setError(null);
                        }


                    }
                })
                .show();

    }


}
