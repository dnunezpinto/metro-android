package cl.metro.mobile.Utilities;


import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import cl.metro.mobile.Models.Tweets.TweetResult;
import cl.metro.mobile.R;

public class AdapterListTwitter extends BaseAdapter {

    private static final String FORMAT_DATETIME_IN = "EEE MMM d HH:mm:ss Z yyyy";
    private static final String FORMAT_DATETIME_OUT = "EEEE d 'de' MMMM yyyy 'a las' HH:mm";
    int layoutResourceId;
    Context mContext;
    List<TweetResult> asdf;
    private LayoutInflater inflater;


    public AdapterListTwitter(Context context, int resource, int textViewResourceId, List<TweetResult> objects) {

        asdf = objects;
        inflater = LayoutInflater.from(context);
        layoutResourceId = resource;
        mContext = context;

    }

    @Override
    public int getCount() {
        return asdf.size();
    }

    @Override
    public Object getItem(int position) {
        return asdf.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View listItem = convertView;

        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        listItem = inflater.inflate(layoutResourceId, parent, false);

        TextView textViewName = (TextView) listItem.findViewById(R.id.textView5);

//        String lineTweet = asdf.get(position).getText();
        textViewName.setText(Html.fromHtml(asdf.get(position).getText()));

        TextView txtFecha = (TextView) listItem.findViewById(R.id.txtFecha);

        if (android.os.Build.VERSION.SDK_INT > 11) {
            txtFecha.setText(getTimeZone(asdf.get(position).getCreatedAt()));

        } else {
            txtFecha.setText(asdf.get(position).getCreatedAt());
        }


        return listItem;

    }

    public String getTimeZone(String strDate) {
        SimpleDateFormat sf_in = new SimpleDateFormat(FORMAT_DATETIME_IN, Locale.US);
        Date date;
        try {
            date = sf_in.parse(strDate);
            SimpleDateFormat sf_out = new SimpleDateFormat(FORMAT_DATETIME_OUT, Locale.getDefault());
            return sf_out.format(date);
        } catch (ParseException e) {
//            e.printStackTrace();
            return strDate;
        }
    }

}
