package cl.metro.mobile.Utilities;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import cl.metro.mobile.Models.LugaresInteresEntretencion.DatosLugaresInteresEntretencion;
import cl.metro.mobile.R;

public class AdapterListLugaresInteresEntretencion<T> extends BaseAdapter {


    private int layoutResourceId;
    private int mFieldId = 0;
    private Context mContext;
    private List<DatosLugaresInteresEntretencion> list;
    private LayoutInflater mInflater;


    public AdapterListLugaresInteresEntretencion(Context context,
                                                 int resource,
                                                 int textViewResourceId,
                                                 List<DatosLugaresInteresEntretencion> objects) {

        list = objects;
        mFieldId = textViewResourceId;
        layoutResourceId = resource;
        mContext = context;

    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {

        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        View listItem = convertView;


        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        listItem = mInflater.inflate(layoutResourceId, parent, false);


        RelativeLayout contentFavoritos = (RelativeLayout) listItem.findViewById(R.id.contentFavoritos);
        TextView textViewName = (TextView) listItem.findViewById(mFieldId);
//        textViewName.setCompoundDrawablesWithIntrinsicBounds(mContext.getResources().getDrawable(R.drawable.ic_place_mark),
//                null,
//                mContext.getResources().getDrawable(R.drawable.change_arrow),
//                null);
//            textViewName.setCompoundDrawablePadding(12);
        textViewName.setText(list.get(position).getNombre());

        return listItem;


    }

    private static class ViewHolder {
        TextView name;
    }


}
