package cl.metro.mobile.Utilities;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.util.List;

import cl.metro.mobile.Fragments.FragmentServicios.DetalleLugaresInteresEntretencion;
import cl.metro.mobile.MainActivity;
import cl.metro.mobile.Models.LugaresInteresEntretencion.DatosImagenesLugares;
import cl.metro.mobile.R;

public class ImagePageAdapter extends PagerAdapter {

    public static List<DatosImagenesLugares> mImagenesValue;
    private LayoutInflater inflater = null;
    private Activity mActivity;

    public ImagePageAdapter(List<DatosImagenesLugares> values, Activity activity) {

        mImagenesValue = values;
        inflater = activity.getLayoutInflater();
        mActivity = activity;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return mImagenesValue.size();
    }


    @Override
    public Object instantiateItem(final ViewGroup view, final int position) {
        RelativeLayout layout = (RelativeLayout) inflater.inflate(R.layout.item_pager_image, view, false);
        final ImageView imageView = (ImageView) layout.findViewById(R.id.image);
        imageView.setAdjustViewBounds(true);
        imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
//        imageView.setScaleType(ScaleType.FIT_CENTER);
        final ProgressBar progressBar = (ProgressBar) layout.findViewById(R.id.loading);

        MainActivity.imageLoader.displayImage(mImagenesValue.get(position).getSrc(), imageView, MainActivity.options, new SimpleImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {
                progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                @SuppressWarnings("unused")
                String message = null;
                switch (failReason.getType()) {
                    case IO_ERROR:
                        message = "Input/Output error";
                        break;
                    case DECODING_ERROR:
                        message = "Image can't be decoded";
                        break;
                    case NETWORK_DENIED:
                        message = "Downloads are denied";
                        break;
                    case OUT_OF_MEMORY:
                        message = "Out Of Memory error";
                        break;
                    case UNKNOWN:
                        message = "Unknown error";
                        break;
                }

                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                progressBar.setVisibility(View.GONE);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(loadedImage.getWidth(), loadedImage.getHeight());
                layoutParams.addRule(RelativeLayout.BELOW, R.id.linearLayout3);
                DetalleLugaresInteresEntretencion.pagerDestacados.setLayoutParams(layoutParams);

            }
        });

        view.addView(layout, 0);
        return layout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }


    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

}
