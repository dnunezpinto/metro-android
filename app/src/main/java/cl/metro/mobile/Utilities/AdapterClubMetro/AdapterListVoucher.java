package cl.metro.mobile.Utilities.AdapterClubMetro;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import cl.metro.mobile.Fragments.ClubMetro.Voucher.VoucherContent;
import cl.metro.mobile.R;

public class AdapterListVoucher extends BaseAdapter {


    private int layoutResourceId;
    private int mFieldId = 0;
    private Context mContext;
    private List<VoucherContent> list;
    private LayoutInflater mInflater;


    public AdapterListVoucher(Context context,
                              int resource,
                              int textViewResourceId,
                              List<VoucherContent> objects) {

        list = objects;
        mFieldId = textViewResourceId;
        mInflater = LayoutInflater.from(context);
        layoutResourceId = resource;
        mContext = context;

    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {

        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        View listItem = convertView;

        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        listItem = mInflater.inflate(layoutResourceId, parent, false);

        String voucher = list.get(position).getVoucher();

        TextView textViewName = (TextView) listItem.findViewById(R.id.txtVoucher);
        TextView txtRegistrado = (TextView) listItem.findViewById(R.id.txtRegistrado);
        TextView txtCobrado = (TextView) listItem.findViewById(R.id.txtCobrado);


//        Drawable image = ResourcesCompat.getDrawable(mContext.getResources(), R.drawable.ic_cupon, null);
//        textViewName.setCompoundDrawablesWithIntrinsicBounds(image, null, null , null);
        textViewName.setText("Cupón Descuento N° " + voucher);
        txtRegistrado.setText("Registrado el " + list.get(position).getFechaRegistro());
        txtCobrado.setText("Cobrado el " + list.get(position).getFechaCobro());


        return listItem;


    }


}
