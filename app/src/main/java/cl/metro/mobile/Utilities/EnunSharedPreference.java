package cl.metro.mobile.Utilities;


public enum EnunSharedPreference {

    linea1("linea1"),
    linea2("linea2"),
    linea4("linea4"),
    linea4a("linea4A"),
    linea5("linea5"),
    horarioDesde("horario_desde"),
    horarioHasta("horario_hasta"),
    horarioTramo1("horario_tramo1"),
    horarioTramo2("horario_tramo2"),
    horarioTramo3("horario_tramo3"),
    horarioTramo4("horario_tramo4"),
    horarioTramo5("horario_tramo5"),
    horarioTramo6("horario_tramo6"),
    diaLunes("dia_lunes"),
    diaMartes("dia_martes"),
    diaMiercoles("dia_miercoles"),
    diaJueves("dia_jueves"),
    diaViernes("dia_viernes"),
    diaSabado("dia_sabado"),
    diaDomingo("dia_domingo"),;

    private String text;

    EnunSharedPreference(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

}
