package cl.metro.mobile.Utilities.AdaptersPlanificador;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;

import java.util.List;

import cl.metro.mobile.Models.ResponseParse;
import cl.metro.mobile.Models.Ruta;

public class AdapterPlanifiqueSection extends BaseAdapter implements
        ListAdapter {

    private List<ResponseParse> dataRow;
    private LayoutInflater mInflater;

    public AdapterPlanifiqueSection(List<ResponseParse> listitems,
                                    Activity activity) {
        dataRow = listitems;
        this.mInflater = (LayoutInflater) activity.getBaseContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
//        Log.d("LOG Size", String.valueOf(this.dataRow.size()));
        return this.dataRow.size();
    }

    @Override
    public ResponseParse getItem(int position) {
        return this.dataRow.get(position);
    }

    @Override
    public long getItemId(int position) {
        Integer integer = position;
        return integer.longValue();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final Ruta ruta = this.dataRow.get(position).getRuta();

        if (ruta.getTipo().equalsIgnoreCase("inicio")) {
            AdapterPlanificadorRowInicio row = new AdapterPlanificadorRowInicio(this.mInflater, ruta, convertView);
            return row.getView();
        } else if (ruta.getTipo().equalsIgnoreCase("transbordo")) {
            AdapterPlanificadorRowTransbordo row = new AdapterPlanificadorRowTransbordo(this.mInflater, ruta, convertView, parent);
            return row.getView();
        } else if (ruta.getTipo().equalsIgnoreCase("estacion")) {
            AdapterPlanificadorRowEstacion row = new AdapterPlanificadorRowEstacion(this.mInflater, ruta, convertView, position, getCount());
            return row.getView();
        } else if (ruta.getTipo().equalsIgnoreCase("detalle")) {
            AdapterPlanificadorRowDetalle row = new AdapterPlanificadorRowDetalle(this.mInflater, ruta, convertView, parent);
            return row.getView();
        } else if (ruta.getTipo().equalsIgnoreCase("fin")) {
//            AdapterPlanificadorRowFin row = new AdapterPlanificadorRowFin(this.mInflater, ruta, convertView);
            AdapterPlanificadorRowFinUpdated row = new AdapterPlanificadorRowFinUpdated(this.mInflater, ruta, convertView, dataRow);
            return row.getView();
        } else {
            Log.e("Error de getView", "Error no hay tag definido");
            return null;
        }


    }

    /**
     * Sets a new Data set for the StoreModel section.
     *
     * @param stores
     */
    public void setData(List<ResponseParse> stores) {
        this.dataRow = stores;
        this.notifyDataSetChanged();
    }

}
