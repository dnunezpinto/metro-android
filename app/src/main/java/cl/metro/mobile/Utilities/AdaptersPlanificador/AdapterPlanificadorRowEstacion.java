/**
 *
 */
package cl.metro.mobile.Utilities.AdaptersPlanificador;

import android.annotation.SuppressLint;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import cl.metro.mobile.Models.Ruta;
import cl.metro.mobile.R;

/**
 * @author jvalladares
 */
@SuppressLint("DefaultLocale")
public class AdapterPlanificadorRowEstacion {

    private static final String ESTACION_METRO = "Estación Metro";

    private View mFullView;
    private ViewHolderEstacion holder;

    public AdapterPlanificadorRowEstacion(LayoutInflater inflater, Ruta model,
                                          View convertView, int position, int count) {

//		if (convertView == null) {
//			convertView = (View) inflater.inflate(
//					R.layout.item_list_planifique_estacion, null);
//			holder = new ViewHolderEstacion();
//			holder.layout = (RelativeLayout) convertView
//					.findViewById(R.id.layout_container);
//			holder.textNombre = (TextView) convertView
//					.findViewById(R.id.text_nombre);
//			holder.textDescripcion = (TextView) convertView
//					.findViewById(R.id.text_descripcion);
//
//			holder.imageMovilidadReducida = (ImageView)convertView.findViewById(R.id.image_movilidad_reducida);
//			holder.textMovilidadReducida = (TextView)convertView.findViewById(R.id.text_movilidad_reducida);
//
//			convertView.setTag(holder);
//		} else {
//			holder = (ViewHolderEstacion) convertView.getTag();
//		}


        convertView = inflater.inflate(
                R.layout.item_list_planifique_estacion, null);
        holder = new ViewHolderEstacion();

        holder.layout = convertView
                .findViewById(R.id.layout_container);
        holder.textNombre = convertView
                .findViewById(R.id.text_nombre);
        holder.textDescripcion = convertView
                .findViewById(R.id.text_descripcion);
        holder.layoutMovilidad = convertView
                .findViewById(R.id.linearLayoutMovilidadReducida);

        holder.layoutMovilidad = convertView
                .findViewById(R.id.linearLayoutMovilidadReducida);

        holder.imgCirculoBot = convertView.findViewById(R.id.imageView6);

        holder.imageEstacionLinea = convertView.findViewById(R.id.imageView3);
        holder.imageMovilidadReducida = convertView.findViewById(R.id.image_movilidad_reducida);
        holder.textMovilidadReducida = convertView.findViewById(R.id.text_movilidad_reducida);


//        Log.i("App", "Position: " + position);
//        Log.i("App", "Count: " + count);

        if (position + 1 == count) {
//            holder.layout.setPadding(0,0,0,8);
            holder.imgCirculoBot.setVisibility(View.GONE);
        }


        if (holder.layout != null) {
            setBkgColor(model.getEstacionLinea());
        }


        if (model.getEstacionLineaNombre() != null) {
            //holder.textNombre.setText(ESTACION_METRO + model.getEstacionLineaNombre());
            holder.textNombre.setText(ESTACION_METRO);
        } else {
            holder.textNombre.setText("");
        }

        if (model.getEstacionNombre() != null) {
           holder.textDescripcion.setText(model.getEstacionNombre());
//            holder.textDescripcion.setText(model.getEstacionNombre());
        } else {
            holder.textDescripcion.setText("");
        }

//		setImageView(model.getEstacionLinea());


        if (!model.isMovilidadReducida()) {
            String movilidadReducidaAternativas = TextUtils.join(", ", model.getMovilidadReducidaAternativas());
            holder.textMovilidadReducida.setText("Puntos de la Red cercanos con infraestructura para personas con movilidad reducida: " + movilidadReducidaAternativas + ". Informaciones al 800 540 800");

        } else {
            holder.imageMovilidadReducida.setVisibility(View.GONE);
            holder.textMovilidadReducida.setVisibility(View.GONE);
        }

        this.mFullView = convertView;

    }


    private void setBkgColor(String linea) {
        if (linea.equalsIgnoreCase("l1")) {
            //L1
            holder.layout.setBackgroundResource(R.color.rojo_l1);
            holder.imageEstacionLinea.setImageResource(R.drawable.ic_l1_number);

        } else if (linea.equalsIgnoreCase("l2")) {
            //L2
            holder.layout.setBackgroundResource(R.color.naranjo_l2);
            holder.imageEstacionLinea.setImageResource(R.drawable.ic_l2_number);
        } else if (linea.equalsIgnoreCase("l3")) {
            //L3
            holder.layout.setBackgroundResource(R.color.cafe_l3);
            holder.imageEstacionLinea.setImageResource(R.drawable.ic_l3_number);

        } else if (linea.equalsIgnoreCase("l4")) {
            //L4
            holder.layout.setBackgroundResource(R.color.azul_l4);
            holder.imageEstacionLinea.setImageResource(R.drawable.ic_l4_number);

        } else if (linea.equalsIgnoreCase("l4a")) {
            //L4a
            holder.layout.setBackgroundResource(R.color.celeste_l4a);
            holder.imageEstacionLinea.setImageResource(R.drawable.ic_l4a_number);

        } else if (linea.equalsIgnoreCase("l5")) {
            //L5
            holder.layout.setBackgroundResource(R.color.verde_l5);
            holder.imageEstacionLinea.setImageResource(R.drawable.ic_l5_number);
        } else if (linea.equalsIgnoreCase("l6")) {
            //L6
            holder.layout.setBackgroundResource(R.color.morado_l6);
            holder.imageEstacionLinea.setImageResource(R.drawable.ic_l6_number);
        }
//        holder.layout.setPadding(20, 20, 0, 40);
    }

    public View getView() {
        return this.mFullView;
    }

    public static class ViewHolderEstacion {

        RelativeLayout layout;
        LinearLayout layoutMovilidad;
        TextView textNombre;
        TextView textDescripcion;
        TextView textMovilidadReducida;
        ImageView imageMovilidadReducida;
        ImageView imageEstacionLinea;
        ImageView imgCirculoBot;

    }

}
