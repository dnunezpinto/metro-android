/**
 *
 */
package cl.metro.mobile.Utilities.AdaptersPlanificador;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import cl.metro.mobile.Models.ResponseParse;
import cl.metro.mobile.Models.Ruta;
import cl.metro.mobile.R;


/**
 * @author jvalladares
 */
public class AdapterPlanificadorRowFinUpdated {

    private View rowView;
    public AdapterPlanificadorRowFinUpdated(LayoutInflater inflater, Ruta model,
                                            View convertView, List<ResponseParse> dataRow) {

        ViewHolderFin holder = new ViewHolderFin();

        convertView = inflater.inflate(
                R.layout.layout_finish_planificador, null);

        holder.layout = (RelativeLayout) convertView
                .findViewById(R.id.layout_estacion);
        holder.txtNombreEstacion = (TextView) convertView
                .findViewById(R.id.txtNombreEstacion);
        holder.txtEstacionSalida = (TextView) convertView
                .findViewById(R.id.txtEstacionSalida);
        holder.textDescripcion = (TextView) convertView
                .findViewById(R.id.text_descripcion);
        holder.layoutMovilidad = (LinearLayout) convertView
                .findViewById(R.id.linearLayoutMovilidadReducida);

        holder.imgCirculoBot = (ImageView) convertView.findViewById(R.id.imageView6);

        holder.imageEstacionLinea = (ImageView) convertView.findViewById(R.id.imageView3);
        holder.imageMovilidadReducida = (ImageView) convertView.findViewById(R.id.image_movilidad_reducida);
        holder.text_movilidad_reducida = (TextView) convertView.findViewById(R.id.text_movilidad_reducida);



        convertView.setTag(holder);

        if(dataRow.get(dataRow.size()-2).getRuta().getTipo().equalsIgnoreCase("estacion")){
            holder.layout.setVisibility(View.GONE);
        }

        if(!model.isMovilidadReducida() && model.getMovilidadReducidaAternativas().length > 0){
//            layoutMovilidad.setVisibility(View.VISIBLE);

            String movilidadReducidaAternativas = TextUtils.join(", ", model.getMovilidadReducidaAternativas());
            holder.text_movilidad_reducida.setText("Puntos de la Red cercanos con infraestructura para personas con movilidad reducida: " + movilidadReducidaAternativas + ". Informaciones al 800 540 800");
        }else {
//            holder.layoutMovilidad.setVisibility(View.GONE);
        }

        if (holder.txtEstacionSalida != null) {
            holder.txtEstacionSalida.setText("Dirígete hacia la salida");

            if (holder.txtEstacionSalida != null) {
                holder.txtEstacionSalida.setText("Estación: " + model.getEstacionNombre());
                holder.txtEstacionSalida.setPadding(0, 3, 0, 10);
            }
        }

        if (holder.layout != null) {
            if (model.getEstacionLinea().equalsIgnoreCase("l1")) {
                //L1
                holder.layout.setBackgroundResource(R.color.rojo_l1);
                holder.imageEstacionLinea.setImageResource(R.drawable.ic_l1_number);

            } else if (model.getEstacionLinea().equalsIgnoreCase("l2")) {
                //L2
                holder.layout.setBackgroundResource(R.color.naranjo_l2);
                holder.imageEstacionLinea.setImageResource(R.drawable.ic_l2_number);

            } else if (model.getEstacionLinea().equalsIgnoreCase("l3")) {
                //L3
                holder.layout.setBackgroundResource(R.color.cafe_l3);
                holder.imageEstacionLinea.setImageResource(R.drawable.ic_l3_number);

            } else if (model.getEstacionLinea().equalsIgnoreCase("l4")) {
                //L4
                holder.layout.setBackgroundResource(R.color.azul_l4);
                holder.imageEstacionLinea.setImageResource(R.drawable.ic_l4_number);

            } else if (model.getEstacionLinea().equalsIgnoreCase("l4a")) {
                //L4a
                holder.layout.setBackgroundResource(R.color.celeste_l4a);
                holder.imageEstacionLinea.setImageResource(R.drawable.ic_l4a_number);

            } else if (model.getEstacionLinea().equalsIgnoreCase("l5")) {
                //L5
                holder.layout.setBackgroundResource(R.color.verde_l5);
                holder.imageEstacionLinea.setImageResource(R.drawable.ic_l5_number);

            } else if (model.getEstacionLinea().equalsIgnoreCase("l6")) {
                //L6
                holder.layout.setBackgroundResource(R.color.morado_l6);
                holder.imageEstacionLinea.setImageResource(R.drawable.ic_l6_number);
            }
        }


        if (model.getEstacionNombre() != null) {
            holder.txtNombreEstacion.setText(model.getEstacionNombre());
        } else {
            holder.txtNombreEstacion.setText("");
        }


        rowView = convertView;

    }


    public View getView() {

        return rowView;
    }

    public static class ViewHolderFin {
        private ImageView imageWalk;
        private ImageView imageLine;
        private TextView txtNombreEstacion;
        private TextView txtEstacionSalida;
        private TextView textDescripcion;
        private TextView text_movilidad_reducida;
        RelativeLayout layout;
        LinearLayout layoutMovilidad;
        ImageView imageMovilidadReducida;
        ImageView imageEstacionLinea;
        ImageView imgCirculoBot;
    }


}
