/**
 *
 */
package cl.metro.mobile.Utilities.AdaptersPlanificador;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import cl.metro.mobile.Models.Ruta;
import cl.metro.mobile.R;


/**
 * @author jvalladares
 */
public class AdapterPlanificadorRowFin {

    private View rowView;


    public AdapterPlanificadorRowFin(LayoutInflater inflater, Ruta model,
                                     View convertView) {

        ViewHolderFin holder = new ViewHolderFin();

        convertView = inflater.inflate(
                R.layout.item_list_planifique_main_or_finish, null);

        holder.imageWalk = (ImageView) convertView.findViewById(R.id.image_recorrido);
        holder.imageLine = (ImageView) convertView.findViewById(R.id.imageView21);
        holder.textNombre = (TextView) convertView.findViewById(R.id.text_nombre);
        holder.textDescripcion = (TextView) convertView.findViewById(R.id.text_descripcion);

        convertView.setTag(holder);

        if (holder.textNombre != null) {
            holder.textNombre.setText("Dirígete hacia la salida");

            if (holder.textDescripcion != null) {
                holder.textDescripcion.setText("Estación: " + model.getEstacionNombre());
                holder.textDescripcion.setPadding(0, 3, 0, 10);
            }

            holder.imageLine.setVisibility(View.GONE);

        }

        rowView = convertView;

    }

    public View getView() {

        return rowView;
    }

    public static class ViewHolderFin {
        private ImageView imageWalk;
        private ImageView imageLine;
        private TextView textNombre;
        private TextView textDescripcion;
    }


}
