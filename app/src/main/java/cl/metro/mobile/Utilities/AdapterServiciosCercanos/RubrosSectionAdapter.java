package cl.metro.mobile.Utilities.AdapterServiciosCercanos;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;

import java.lang.ref.WeakReference;
import java.util.List;


public class RubrosSectionAdapter extends BaseAdapter implements ListAdapter {

    private List<String> mRubros;
    private LayoutInflater mInflater;
    private WeakReference<Activity> weakReference;


    public RubrosSectionAdapter(List<String> listitems, Activity activity) {
        mRubros = listitems;
        weakReference = new WeakReference<Activity>(activity);
        this.mInflater = (LayoutInflater) activity.getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return this.mRubros.size();
    }

    @Override
    public String getItem(int position) {
        return this.mRubros.get(position);
    }

    @Override
    public long getItemId(int position) {
        Integer integer = position;
        return integer.longValue();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        RubrosRowAdapter row = new RubrosRowAdapter(this.mInflater, this.mRubros.get(position), convertView, weakReference.get());
        return row.getView();
    }

    /**
     * Sets a new Data set for the StoreModel section.
     *
     * @param stores
     */
    public void setData(List<String> stores) {
        this.mRubros = stores;
        this.notifyDataSetChanged();
    }

}
