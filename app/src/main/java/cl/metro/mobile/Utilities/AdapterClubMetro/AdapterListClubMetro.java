package cl.metro.mobile.Utilities.AdapterClubMetro;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import cl.metro.mobile.Models.ListaClubMetro.Datos;
import cl.metro.mobile.R;

public class AdapterListClubMetro<T> extends BaseAdapter {


    private int layoutResourceId;
    private int mFieldId = 0;
    private Context mContext;
    private List<Datos> list;
    private LayoutInflater mInflater;
    private FragmentManager manager;
    private String tipoLista;


    public AdapterListClubMetro(Context context,
                                int resource,
                                int textViewResourceId,
                                List<Datos> objects,
                                FragmentManager fragmentManager,
                                String tipo) {

        list = objects;
        mFieldId = textViewResourceId;
        mInflater = LayoutInflater.from(context);
        layoutResourceId = resource;
        mContext = context;
        this.manager = fragmentManager;
        tipoLista = tipo;

    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {

        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        View listItem = convertView;

        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        listItem = mInflater.inflate(layoutResourceId, parent, false);

        if (list.get(position).getTitulo() != null) {
            String input = list.get(position).getTitulo();
            StringBuilder rackingSystemSb = new StringBuilder(input.toLowerCase());
            rackingSystemSb.setCharAt(0, Character.toUpperCase(rackingSystemSb.charAt(0)));
            input = rackingSystemSb.toString();

//            Log.i("App", "ID: " + list.get(position).getId());
            RelativeLayout contentFavoritos = (RelativeLayout) listItem.findViewById(R.id.contentFavoritos);
            TextView textViewName = (TextView) listItem.findViewById(mFieldId);
            textViewName.setText(input);
        } else {
            list.remove(position);
            notifyDataSetChanged();
        }

//
//        if (tipoLista.equals("Eventos")) {
//
//        } else if (tipoLista.equals("Actividades")) {
//
//        } else if (tipoLista.equals("Promociones")) {
//
//        }

        return listItem;


    }

    private static class ViewHolder {
        TextView name;
    }


}
