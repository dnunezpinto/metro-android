package cl.metro.mobile.Utilities.AdapterFavoritos;


import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import cl.metro.mobile.Fragments.BipFragment;
import cl.metro.mobile.Fragments.FavoritosFragment;
import cl.metro.mobile.MainActivity;
import cl.metro.mobile.Models.Bip;
import cl.metro.mobile.Utilities.GetSaldoAsyncTask;
import cl.metro.mobile.Utilities.Utils;

public class AdapterListFavoritosTarjetasBip extends BaseAdapter {

    int layoutResourceId;
    Context mContext;
    List<Bip> bipList;
    private LayoutInflater inflater;
    //    private FragmentManager manager;
    private String mNombreVista;


    public AdapterListFavoritosTarjetasBip(Context context, int resource, List<Bip> objects, String nombreVista) {

        this.bipList = objects;
        inflater = LayoutInflater.from(context);
        layoutResourceId = resource;
        mContext = context;
        mNombreVista = nombreVista;
//        this.manager = fragmentManager;

    }

    @Override
    public int getCount() {
        return bipList.size();
    }

    @Override
    public Object getItem(int position) {
        return bipList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, final View convertView, final ViewGroup parent) {

        View listItem = convertView;

        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        listItem = inflater.inflate(layoutResourceId, parent, false);

        RelativeLayout contentFavoritos = (RelativeLayout) listItem.findViewById(cl.metro.mobile.R.id.contentFavoritos);

        TextView textViewName = (TextView) listItem.findViewById(cl.metro.mobile.R.id.txtNombreFavorito);
        textViewName.setText(bipList.get(position).getNombre());

        contentFavoritos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Integer integer = Integer.parseInt(bipList.get(position).getNumeroBip());
                if (Utils.isNetworkAvailable(((Activity) mContext))) {

                    new GetSaldoAsyncTask(mContext).execute(new Integer[]{integer});

                } else {

                    Toast.makeText(mContext, cl.metro.mobile.R.string.lost_connection, Toast.LENGTH_SHORT).show();

                }
            }
        });

        ImageButton btnDelete = (ImageButton) listItem.findViewById(cl.metro.mobile.R.id.btnDelete);
        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                bipList.get(position).setFavorito(false);
                MainActivity.initApp.updateBip(bipList.get(position));


                if (mNombreVista.equalsIgnoreCase("BipFragment")) {

                    BipFragment.getTarjetasBip();

                } else {

                    FavoritosFragment.getTarjetasBip();

                }

            }
        });

        return listItem;

    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }
}
