package cl.metro.mobile.Utilities.AdapterClubMetro;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import cl.metro.mobile.Fragments.ClubMetro.BipContent;
import cl.metro.mobile.R;

public class AdapterListBipClubMetro extends BaseAdapter {


    private int layoutResourceId;
    private int mFieldId = 0;
    private Context mContext;
    private List<BipContent.BipItem> list;
    private LayoutInflater mInflater;


    public AdapterListBipClubMetro(Context context,
                                   int resource,
                                   int textViewResourceId,
                                   List<BipContent.BipItem> objects) {

        list = objects;
        mFieldId = textViewResourceId;
        mInflater = LayoutInflater.from(context);
        layoutResourceId = resource;
        mContext = context;

    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {

        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        View listItem = convertView;

        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        listItem = mInflater.inflate(layoutResourceId, parent, false);


        TextView txtChip = (TextView) listItem.findViewById(R.id.txtChip);
        TextView txtBip = (TextView) listItem.findViewById(R.id.txtBip);

        txtChip.setText("Chip N° " + list.get(position).get_chip());
        txtBip.setText("Bip N° " + list.get(position).get_bip());


        return listItem;


    }


}
