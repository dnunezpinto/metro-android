package cl.metro.mobile.Utilities.AdaptersPlanificador;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import cl.metro.mobile.Models.Ruta;
import cl.metro.mobile.R;


public class AdapterPlanificadorRowTransbordo {

    private static final String REALIZAR_COMBINACION = "Realizar Combinación";
    private static final String COMBINA_CON_METRO = "Combina con el Metro ";
    private static final String EN_DIRECCION = " en dirección ";

    private View mFullView;
    private ViewHolderTransbordo holder;

    public AdapterPlanificadorRowTransbordo(LayoutInflater inflater, Ruta model, View convertView, ViewGroup parent) {

        mFullView = null;
        holder = null;

        if (mFullView == null) {

            mFullView = inflater.inflate(R.layout.item_list_planifique_combinacion, parent, false);
            holder = new ViewHolderTransbordo();

            holder.layout = mFullView.findViewById(R.id.layout_container);
            holder.textNombre = (TextView) mFullView.findViewById(R.id.text_nombre);
            holder.textDescripcion = (TextView) mFullView.findViewById(R.id.text_descripcion);
            holder.txtTiempo = (TextView) mFullView.findViewById(R.id.txtTiempo);
            mFullView.setTag(holder);

        } else {
            holder = (ViewHolderTransbordo) mFullView.getTag();
        }

//		convertView = (View)inflater.inflate(R.layout.item_list_planifique_combinacion, null);
//		holder = new ViewHolderTransbordo();
//		holder.image = (ImageView) convertView.findViewById(R.id.image_recorrido);
//		holder.textNombre = (TextView)convertView.findViewById(R.id.text_nombre);
//		holder.textDescripcion = (TextView)convertView.findViewById(R.id.text_descripcion);

        holder.textNombre.setText(REALIZAR_COMBINACION);
        holder.textDescripcion.setText(COMBINA_CON_METRO
                + model.getEstacionLinea().toUpperCase() + EN_DIRECCION
                + model.getDireccion());
        holder.txtTiempo.setText(model.getTiempo());
        if (holder.layout != null) {
            setBkgColor(model.getEstacionLinea());
        }


    }

    private void setBkgColor(String linea) {
        if (linea.equalsIgnoreCase("l1")) {
            //L1
            holder.layout.setBackgroundResource(R.color.rojo_l1);

        } else if (linea.equalsIgnoreCase("l2")) {
            //L2
            holder.layout.setBackgroundResource(R.color.naranjo_l2);

        }else if (linea.equalsIgnoreCase("l3")) {
            //L3
            holder.layout.setBackgroundResource(R.color.cafe_l3);

        } else if (linea.equalsIgnoreCase("l4")) {
            //L4
            holder.layout.setBackgroundResource(R.color.azul_l4);

        } else if (linea.equalsIgnoreCase("l4a")) {
            //L4a
            holder.layout.setBackgroundResource(R.color.celeste_l4a);

        } else if (linea.equalsIgnoreCase("l5")) {
            //L5
            holder.layout.setBackgroundResource(R.color.verde_l5);
        } else if (linea.equalsIgnoreCase("l6")) {
            //L5
            holder.layout.setBackgroundResource(R.color.morado_l6);
        }
//        holder.layout.setPadding(20, 20, 0, 40);
    }

    public View getView() {
        return this.mFullView;
    }

    public static class ViewHolderTransbordo {
        ImageView image;
        RelativeLayout layout;
        TextView textNombre;
        TextView textDescripcion;
        TextView txtTiempo;
    }
}
