package cl.metro.mobile.Utilities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import java.lang.ref.WeakReference;


@SuppressLint({"CommitPrefEdits", "DefaultLocale"})
public class SharedPreferenceMetro {

    private WeakReference<Activity> weakReference;

    public SharedPreferenceMetro(Activity activity) {
        weakReference = new WeakReference<Activity>(activity);
    }

    public SharedPreferences getPrefenrenceMetro() {
        weakReference.get();
        return weakReference.get().getSharedPreferences("metro_preference",
                Context.MODE_PRIVATE);
    }

    public void setPreferenceLinea(EnunSharedPreference enunSharedPreference,
                                   boolean isActivate) {
        if (enunSharedPreference.getText().equals(
                EnunSharedPreference.linea1.getText())
                || enunSharedPreference.getText().equals(
                EnunSharedPreference.linea2.getText())
                || enunSharedPreference.getText().equals(
                EnunSharedPreference.linea4.getText())
                || enunSharedPreference.getText().equals(
                EnunSharedPreference.linea4a.getText())
                || enunSharedPreference.getText().equals(
                EnunSharedPreference.linea5.getText())) {

            SharedPreferences settings = getPrefenrenceMetro();
            if (settings != null) {
                SharedPreferences.Editor editor = settings.edit();
                editor.putBoolean(enunSharedPreference.getText(), isActivate);
                editor.commit();

            }
        }
    }

    public void setPreferenceHorario(EnunSharedPreference enumHorario,
                                     String valueHorario) {
        if (enumHorario.getText().equals(
                EnunSharedPreference.horarioDesde.getText())
                || enumHorario.getText().equals(
                EnunSharedPreference.horarioHasta.getText())) {
            SharedPreferences settings = getPrefenrenceMetro();
            if (settings != null) {
                SharedPreferences.Editor editor = settings.edit();
                editor.putString(enumHorario.getText(), valueHorario);
                editor.commit();
            }
        }
    }

    public void setPreferenceHorarioTramo(EnunSharedPreference enumHorarioTramo, boolean isActive) {
        if (enumHorarioTramo.getText().equals(EnunSharedPreference.horarioTramo1.getText()) ||
                enumHorarioTramo.getText().equals(EnunSharedPreference.horarioTramo2.getText()) ||
                enumHorarioTramo.getText().equals(EnunSharedPreference.horarioTramo3.getText()) ||
                enumHorarioTramo.getText().equals(EnunSharedPreference.horarioTramo4.getText()) ||
                enumHorarioTramo.getText().equals(EnunSharedPreference.horarioTramo5.getText()) ||
                enumHorarioTramo.getText().equals(EnunSharedPreference.horarioTramo6.getText())) {
            SharedPreferences settings = getPrefenrenceMetro();
            if (settings != null) {
                SharedPreferences.Editor editor = settings.edit();
                editor.putBoolean(enumHorarioTramo.getText(), isActive);
                editor.commit();
            }
        }

    }

    public void setPreferenceDias(EnunSharedPreference enumDia,
                                  boolean isActiveDia) {

        if (enumDia.getText().equals(
                EnunSharedPreference.diaLunes.getText())
                || enumDia.getText().equals(
                EnunSharedPreference.diaMartes.getText())
                || enumDia.getText().equals(
                EnunSharedPreference.diaMiercoles.getText())
                || enumDia.getText().equals(
                EnunSharedPreference.diaJueves.getText())
                || enumDia.getText().equals(
                EnunSharedPreference.diaViernes.getText())
                || enumDia.getText().equals(
                EnunSharedPreference.diaSabado.getText())
                || enumDia.getText().equals(
                EnunSharedPreference.diaDomingo.getText())) {
            SharedPreferences settings = getPrefenrenceMetro();

            if (settings != null) {
                SharedPreferences.Editor editor = settings.edit();
                editor.putBoolean(enumDia.getText(), isActiveDia);
                editor.commit();

            }
        }
    }
}
