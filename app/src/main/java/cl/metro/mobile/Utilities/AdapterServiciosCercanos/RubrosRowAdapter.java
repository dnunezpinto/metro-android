package cl.metro.mobile.Utilities.AdapterServiciosCercanos;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;

import com.kyleduo.switchbutton.SwitchButton;

import java.lang.ref.WeakReference;
import java.util.HashSet;

import cl.metro.mobile.MainActivity;
import cl.metro.mobile.R;

public class RubrosRowAdapter {

    public final ViewHolderPlanifique holder;
    public WeakReference<Activity> weakReference;
    private View mFullView;
    private String model;

    public RubrosRowAdapter(LayoutInflater inflater,
                            String rubro, View convertView, Activity activiy) {
        this.model = rubro;
        weakReference = new WeakReference<Activity>(activiy);
        if (convertView == null) {
            convertView = (View) inflater.inflate(R.layout.item_list_servicio_rubro, null);
            holder = new ViewHolderPlanifique();
            holder.nombre = (TextView) convertView.findViewById(R.id.text_nombre);
            holder.isSelect = (SwitchButton) convertView.findViewById(R.id.switch_rubro);
            holder.isSelect.setOnCheckedChangeListener(checkChange());
            convertView.setTag(holder);
        } else {
            holder = (ViewHolderPlanifique) convertView.getTag();
        }

        holder.nombre = (TextView) convertView.findViewById(R.id.text_nombre);
        holder.isSelect = (SwitchButton) convertView.findViewById(R.id.switch_rubro);
        holder.isSelect.setOnCheckedChangeListener(checkChange());
        convertView.setTag(holder);
        this.mFullView = convertView;
        holder.nombre.setText(rubro);

        removeDuplicates();
        if (MainActivity.initApp.arrayFiltroMapa.contains(rubro)) {
            holder.isSelect.setChecked(true);
        } else {
            holder.isSelect.setChecked(false);
        }


    }

    private OnCheckedChangeListener checkChange() {
        return new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

//				Log.d("TESTING", "ischeked:"+isChecked);
                if (isChecked) {
                    MainActivity.initApp.arrayFiltroMapa.add(model);
                    removeDuplicates();
                } else {
                    if (MainActivity.initApp.arrayFiltroMapa.contains(model)) {
                        MainActivity.initApp.arrayFiltroMapa.remove(model);

                        removeDuplicates();
                    }
                }

//				Log.d("TESTING", "clicked:"+model+" ListState:"+((GlobalDataSource)weakReference.get().getApplication()).arrayFiltroMapa);
            }
        };
    }

    public View getView() {
        return this.mFullView;
    }

    public void removeDuplicates() {
        HashSet<String> hs = new HashSet();
        hs.addAll(MainActivity.initApp.arrayFiltroMapa);
        MainActivity.initApp.arrayFiltroMapa.clear();
        MainActivity.initApp.arrayFiltroMapa.addAll(hs);
    }

    static class ViewHolderPlanifique {
        TextView nombre;
        SwitchButton isSelect;
    }

}
