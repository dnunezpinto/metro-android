package cl.metro.mobile.Utilities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.TransitionDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;

import cl.metro.mobile.Database.DBHelperDataBase;
import cl.metro.mobile.Fragments.Emergencias1411_Fragment;
import cl.metro.mobile.Fragments.FragmentServicios.ServiciosFragment;
import cl.metro.mobile.Fragments.PlanificadorFragment;
import cl.metro.mobile.MainActivity;
import cl.metro.mobile.Models.Estaciones;
import cl.metro.mobile.Models.EstadoRed.EstadoLineas;
import cl.metro.mobile.Models.Lineas;
import cl.metro.mobile.R;
import retrofit.client.Response;

/**
 * Created by Daniel on 04-02-2015.
 */
public class Utils {

    public static final String SENDER_ID = "894006265243";
    //    public static String URL_BASE_INTERMOVIL = "http://metro.rvalladares.com/";
    public static final String BASE_URL_BIP = "http://pocae.tstgo.cl/";
    public static final String PREFS_LOGIN_ID_USER = "id_user";
    final public static int REQUEST_CODE_ASK_PERMISSIONS = 123;
    public static boolean isEstacionSelected = false;
    public static Estaciones estacion;
    public static EstadoLineas estadoLineas;
    public static int estadoEstacion;
    public static String strTweet;
    public static String codigoEstacion;
    public static boolean hasLocationPermission;
    //Banner URL
    public static String BannerITTURL = "";
    public static String linkBanner = "";
    public static String footerBanner = "";
    // URL BASE
    //Colores
    public static String[] COLORS = {"#e32731", "#df5d00", "#20259a", "#115ed2", "#298a5d", "#a35db5"};
    public static String URL_BASE_METRO = "http://www.metro.cl/api";
    public static String URL_BASE_INTERMOVIL = "";
    public static String URL_DESARROLLO = "http://qamt.agenciacatedral.cl/api/";
//    public static String URL_SERVICES = URL_BASE_METRO + "/includes/wsMobile";
    //    public static String URL_LOGIN = URL_BASE_DESARROLLO + "clubmetro/includes/wsMobile";
    // URL para el servicio de Login con Club Metro.
    public static String URL_LOGIN = URL_BASE_INTERMOVIL + "clubmetro/includes/wsMobile";
    public static String BASES_CLUB_METRO_PDF_URL = "http://www.metro.cl/clubmetro/contents/bases-eventos/pdf_bases/bases_de_la_promocion_clubmetro_2014.pdf";
    /**
     * ***************************************************************************************************
     */


    /* Servicio Club Metro */

    public static int pos_estacion_inicio = 0;
    public static Drawable oldBackground = null;
    public static int currentColor;
    public static String linkBannerFooter = "";
    private static Hashtable<String, Typeface> fontCache = new Hashtable<String, Typeface>();

    /**
     * *************************************************************************************************
     */

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }

    public static String convertResponseBody(Response response) {
        BufferedReader reader = null;
        StringBuilder sb = new StringBuilder();

        try {

            reader = new BufferedReader(new InputStreamReader(response.getBody().in()));

            String line;

            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return sb.toString();
    }

    public static Typeface getTypeface(Context context, String font) {
        Typeface typeface = fontCache.get(font);
        if (typeface == null) {
            typeface = Typeface.createFromAsset(context.getAssets(), font);
            fontCache.put(font, typeface);
        }
        return typeface;
    }

    public static void overrideFonts(final Context context, final View v, String font) {
        try {
            if (v instanceof ViewGroup) {
                ViewGroup vg = (ViewGroup) v;
                for (int i = 0; i < vg.getChildCount(); i++) {
                    View child = vg.getChildAt(i);
                    overrideFonts(context, child, font);
                }
            } else if (v instanceof TextView) {
                ((TextView) v).setTypeface(Typeface.createFromAsset(context.getAssets(), font));
            }
        } catch (Exception e) {
        }
    }

    public static void hideSoftKeyboard(Context context) {// EditText
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
    }

    public static String getLineaEstacion(int idLinea) {

        String lineaEstacion = null;

        if (idLinea == 1) {
            lineaEstacion = "-l1";
        } else if (idLinea == 2) {
            lineaEstacion = "-l2";
        } else if (idLinea == 3) {
            lineaEstacion = "-l4";
        } else if (idLinea == 4) {
            lineaEstacion = "-l4a";
        } else if (idLinea == 5) {
            lineaEstacion = "-l5";
        }

        return lineaEstacion;
    }


    public static String estacionFormat(String estacion, int idLinea) {
        if (idLinea == 1) {
            estacion += "-l1";
        } else if (idLinea == 2) {
            estacion += "-l2";
        } else if (idLinea == 3) {
            estacion += "-l4";
        } else if (idLinea == 4) {
            estacion += "-l4a";
        } else if (idLinea == 5) {
            estacion += "-l5";
        }

        return estacion;
    }

    public static String getAM_PM(int _hour) {
        String AM_PM = null;

        AM_PM = (_hour < 12) ? " AM" : " PM";

        return AM_PM;
    }

    //Obtiene el minito con un 0 antepuesto si es menor a 10
    public static String getMinute(int minute) {
        String _minute = null;
        if (minute < 10) {
            _minute = "0" + minute;
        } else {
            _minute = String.valueOf(minute);
        }

        return _minute;

    }

    public static void setActionListenerForSpinner(Spinner spinner, AdapterView<?> parent, int selection, Activity activity) {

        ArrayList<Estaciones> arrayList = null;
        Iterator it = MainActivity.initApp.getArrayLineasEstacion().entrySet().iterator();

        while (it.hasNext()) {

            Map.Entry e = (Map.Entry) it.next();

            Lineas lineas = (Lineas) e.getKey();

            if (lineas.getNombreLinea().equalsIgnoreCase(parent.getSelectedItem().toString())) {
                arrayList = ((ArrayList<Estaciones>) e.getValue());
            }

            if (arrayList != null) {
                if (spinner == PlanificadorFragment.spinner_estacion_inicio) {
                    PlanificadorFragment.arrayListOrigen = arrayList;

                } else if (spinner == PlanificadorFragment.spinner_estacion_termino) {
                    PlanificadorFragment.arrayListDestino = arrayList;
                } else if (spinner == ServiciosFragment.spinnerEstaciones) {
                    ServiciosFragment.arrayList = arrayList;
                    ServiciosFragment.spinnerEstaciones.setSelection(selection - 1);
                } else if (spinner == MainActivity.spinnerEstacionesDialog) {

                    MainActivity.arrayEstaciones = arrayList;

                } else if (spinner == Emergencias1411_Fragment.spinner_linea) {
                    Emergencias1411_Fragment.mArrayList = arrayList;

                }
                setAdapter(spinner, arrayList, activity, selection);
            }
        }

        if (spinner == PlanificadorFragment.spinner_estacion_inicio) {
            PlanificadorFragment.spinner_estacion_inicio.setSelection(selection);
        }

//        pos_estacion_inicio = 0;

    }

    public static void setAdapter(Spinner spinner, ArrayList<Estaciones> arrayList, Activity activity, int mCurrentPosition) {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                activity, android.R.layout.simple_spinner_item, MainActivity.initApp.getArrayListNombreEstaciones(arrayList));
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        if (spinner == ServiciosFragment.spinnerEstaciones) {
            spinner.setSelection(mCurrentPosition - 1);
        } else if (spinner == PlanificadorFragment.spinner_estacion_inicio) {
            spinner.setSelection(mCurrentPosition - 1);

        } else if (spinner == PlanificadorFragment.spinner_estacion_termino) {
            spinner.setSelection(mCurrentPosition);
        }

    }

    public static int darker(int color, float factor) {
        int a = Color.alpha(color);
        int r = Color.red(color);
        int g = Color.green(color);
        int b = Color.blue(color);

        return Color.argb(a,
                Math.max((int) (r * factor), 0),
                Math.max((int) (g * factor), 0),
                Math.max((int) (b * factor), 0));
    }

    public static void changeColor(int newColor, Activity activity) {

        Drawable colorDrawable = new ColorDrawable(newColor);
        Drawable bottomDrawable = new ColorDrawable(activity.getResources().getColor(android.R.color.transparent));
        LayerDrawable ld = new LayerDrawable(new Drawable[]{colorDrawable, bottomDrawable});
        if (oldBackground == null) {

            ((AppCompatActivity) activity).getSupportActionBar().setBackgroundDrawable(ld);
        } else {

            TransitionDrawable td = new TransitionDrawable(new Drawable[]{oldBackground, ld});
            ((AppCompatActivity) activity).getSupportActionBar().setBackgroundDrawable(td);
            td.startTransition(200);

        }

        oldBackground = ld;
        currentColor = newColor;
    }

    public static void changeStatusBarColor(int color, Activity activity) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = activity.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(color);
        }

    }

    public static void toolbarRed(Activity activity) {

        int color = activity.getResources().getColor(R.color.my_awesome_color);
        Utils.changeColor(color, activity);

    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) return;
        if (listAdapter.getCount() <= 1) return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.AT_MOST);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }

    public static void setListViewHeightBasedOnChildren3(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }

    public static boolean isNetworkAvailable(Activity activity) {
        ConnectivityManager connMgr = (ConnectivityManager) activity.getSystemService(Activity.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected())
            return true;
        else
            return false;
    }

    public static Lineas getLineaForByID(String id, Context context) {
        Lineas linea = null;

        DBHelperDataBase dbHelperDataBase = new DBHelperDataBase(context);
        SQLiteDatabase sqldb = dbHelperDataBase.readDB();

        Cursor cursor = sqldb.query(DBHelperDataBase.TABLE_LINEA, null, "id=?",
                new String[]{id}, null, null, "id");
        while (cursor.moveToNext()) {
            linea = new Lineas();
            linea.setIdLinea(cursor.getInt(cursor.getColumnIndex("id")));
            linea.setNombreLinea(cursor.getString(cursor.getColumnIndex("nombreLinea")));
            linea.setCodigoLinea(cursor.getInt(cursor.getColumnIndex("codigoLinea")));
            linea.setNotificacionLinea(cursor.getInt(cursor.getColumnIndex("is_notificacion")) > 0);
        }
        cursor.close();
        sqldb.close();
        dbHelperDataBase.close();

        return linea;
    }


    public static Drawable getDrawableStatus(int i, Context context) {
        int estado = -1;
        if (i == 0) {

            estado = estadoLineas.getL1();
        }
        if (i == 1) {
            estado = estadoLineas.getL2();
        }
        if (i == 2) {
            estado = estadoLineas.getL4();
        }
        if (i == 3) {
            estado = estadoLineas.getL4a();
        }
        if (i == 4) {
            estado = estadoLineas.getL5();
        }


        Drawable img = null;

        if (estado == 1) {
            img = context.getResources().getDrawable(R.drawable.ic_semaforo_verde);
        } else if (estado == 2) {
            img = context.getResources().getDrawable(R.drawable.ic_semaforo_amarillo_low);
        } else if (estado == 3) {
            img = context.getResources().getDrawable(R.drawable.ic_semaforo_rojo_low);
        } else {
            img = context.getResources().getDrawable(R.drawable.ic_semaforo_verde);
        }

        return img;
    }

    public static void createAlertDialogfinish(final Activity activity, String string) {

        new MaterialDialog.Builder(activity)
                .content(string)
                .positiveText(android.R.string.ok)
                .cancelable(true)
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        super.onPositive(dialog);
                            activity.finish();

                    }
                })
                .show();

    }

    public static void createAlertDialog(Activity activity, String string) {

        new MaterialDialog.Builder(activity)
                .content(string)
                .positiveText(android.R.string.ok)
                .cancelable(true)
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        super.onPositive(dialog);


                    }
                })
                .show();

    }

    public static void showAlertDialog(Context context, String mensaje) {
        final AlertDialog d = new AlertDialog.Builder(context)
                .setPositiveButton(android.R.string.ok, null)
                .setCancelable(true)
                .setMessage(Html.fromHtml(mensaje))
                .create();
        d.show();

        ((TextView) d.findViewById(android.R.id.message)).setMovementMethod(LinkMovementMethod.getInstance());

    }

    public static void showAlert(final Context context, String mensaje) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setMessage(Html.fromHtml(mensaje))
                .setCancelable(false)
                .setPositiveButton("Ok",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        });
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

    public static void showGPSDisabledAlertToUser(final Context context) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setMessage("GPS está desactivado en su dispositivo. ¿Le gustaría activarlo?")
                .setCancelable(false)
                .setPositiveButton("Ir a Ajustes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent callGPSSettingIntent = new Intent(
                                        android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                context.startActivity(callGPSSettingIntent);
                            }
                        });
        alertDialogBuilder.setNegativeButton("No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

    public static void openWebPage(String url, Context context) {
        Uri webpage = Uri.parse(url);
        Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
        if (intent.resolveActivity(context.getPackageManager()) != null) {
            context.startActivity(intent);
        }
    }


    public static void saveToPrefs(Context context, String key, String value) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static String getFromPrefs(Context context, String key, String defaultValue) {
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        try {
            return sharedPrefs.getString(key, defaultValue);
        } catch (Exception e) {
            e.printStackTrace();
            return defaultValue;
        }
    }

    public static Boolean isUserConnected(Context context) {
        String loggedInUserName = getFromPrefs(context, PREFS_LOGIN_ID_USER, "");

        if (TextUtils.isEmpty(loggedInUserName) || loggedInUserName.equals("-1")) {
            return false;
        } else {
            return true;
        }
    }


}

