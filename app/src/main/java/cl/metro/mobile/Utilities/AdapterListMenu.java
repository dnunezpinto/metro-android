package cl.metro.mobile.Utilities;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import cl.metro.mobile.R;

public class AdapterListMenu extends ArrayAdapter<Object> {

    int layoutResourceId;
    Context mContext;
    private LayoutInflater inflater;
    private String[] strings;
    private int[] navMenuIcons;

    public AdapterListMenu(Context context, int resource,
                           int textViewResourceId, String[] objects, int[] navMenuIcons) {
        super(context, resource, textViewResourceId, objects);

        this.strings = objects;
        this.navMenuIcons = navMenuIcons;
        this.inflater = LayoutInflater.from(context);
        this.layoutResourceId = resource;
        this.mContext = context;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View listItem = convertView;

        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        listItem = inflater.inflate(layoutResourceId, parent, false);


        TextView textViewName = (TextView) listItem.findViewById(R.id.title);
        textViewName.setText(strings[position]);

        ImageView icon = (ImageView) listItem.findViewById(R.id.icon);
        icon.setImageResource(navMenuIcons[position]);

//	        Typeface typeface = Utils.getTypeface(mContext, "Roboto-Regular.ttf");
//        Typeface typeface = Typeface.createFromAsset(mContext.getAssets(), "Roboto-Regular.ttf");
//        textViewName.setTypeface(typeface);

//	        for(int i = 0; i < Utils.categoriasList.size(); i++){
//				if(Utilities.categoriasList.get(i).getNombreCategoria().equals(strings[position])){
//
//					textViewName.setTextColor(Utilities.categoriasList.get(i).getColorCat());
////					textViewName.setTextColor(Color.parseColor("#000000"));
//				}else if(strings[position].equalsIgnoreCase("Portada")){
//
//					textViewName.setTextColor(Color.parseColor("#000000"));
//
//				}
//
//			}


        return listItem;
    }

}
