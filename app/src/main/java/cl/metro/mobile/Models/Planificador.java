/**
 *
 */
package cl.metro.mobile.Models;

import java.util.ArrayList;
import java.util.List;

public class Planificador {

    private List<ResponseParse> arrayResponseRuta = new ArrayList<ResponseParse>();

    private String tiempoTotal;

    private String distanciaTotal;

    public List<ResponseParse> getArrayResponseRuta() {
        return arrayResponseRuta;
    }

    public void setArrayResponseRuta(List<ResponseParse> arrayResponseRuta) {
        this.arrayResponseRuta = arrayResponseRuta;
    }

    public String getTiempoTotal() {
        return tiempoTotal;
    }

    public void setTiempoTotal(String tiempoTotal) {
        this.tiempoTotal = tiempoTotal;
    }

    public String getDistanciaTotal() {
        return distanciaTotal;
    }

    public void setDistanciaTotal(String distanciaTotal) {
        this.distanciaTotal = distanciaTotal;
    }


}
