package cl.metro.mobile.Models.Paraderos;

import com.google.gson.annotations.Expose;

public class DatosParadero {

    @Expose
    private Integer id;
    @Expose
    private String nombre;
    @Expose
    private String ubicacion;
    @Expose
    private String recorridos;
    @Expose
    private String horario;
    @Expose
    private Double latitud;
    @Expose
    private Double longitud;
    @Expose
    private String parada;

    public String getParada() {
        return parada;
    }

    public void setParada(String parada) {
        this.parada = parada;
    }

    /**
     * @return The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre The nombre
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return The ubicacion
     */
    public String getUbicacion() {
        return ubicacion;
    }

    /**
     * @param ubicacion The ubicacion
     */
    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }

    /**
     * @return The recorridos
     */
    public String getRecorridos() {
        return recorridos;
    }

    /**
     * @param recorridos The recorridos
     */
    public void setRecorridos(String recorridos) {
        this.recorridos = recorridos;
    }

    /**
     * @return The horario
     */
    public String getHorario() {
        return horario;
    }

    /**
     * @param horario The horario
     */
    public void setHorario(String horario) {
        this.horario = horario;
    }

    /**
     * @return The latitud
     */
    public Double getLatitud() {
        return latitud;
    }

    /**
     * @param latitud The latitud
     */
    public void setLatitud(Double latitud) {
        this.latitud = latitud;
    }

    /**
     * @return The longitud
     */
    public Double getLongitud() {
        return longitud;
    }

    /**
     * @param longitud The longitud
     */
    public void setLongitud(Double longitud) {
        this.longitud = longitud;
    }

}
