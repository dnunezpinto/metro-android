/**
 *
 */
package cl.metro.mobile.Models.EquipaminetoEstacion;


import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;


/**
 * @author jvalladares
 */

public class GetEquipamientoEstacion {

    @SerializedName("equipamiento")
    private List<EquipamientoResponse> equipamientoResponses = new ArrayList<EquipamientoResponse>();

    public List<EquipamientoResponse> getEquipamientoResponses() {
        return equipamientoResponses;
    }

    public void setEquipamientoResponses(List<EquipamientoResponse> equipamientoResponses) {
        this.equipamientoResponses = equipamientoResponses;
    }
}
