/**
 *
 */
package cl.metro.mobile.Models.ServiciosCercanos;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;


public class CategoriaParse {

    @SerializedName("nombre")
    private String nombre;

    @SerializedName("elementos")
    private List<Elementos> elementos = new ArrayList<Elementos>();

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {


        this.nombre = nombre;
    }

    public List<Elementos> getElementos() {
        return elementos;
    }

    public void setElementos(List<Elementos> elementos) {
        this.elementos = elementos;
    }

}
