/**
 *
 */
package cl.metro.mobile.Models.ServiciosCercanos;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;


public class ServiciosCercanosListaParse {

    @SerializedName("categorias-rubros")
    private List<CategoriasRubros> categoriaRubros = new ArrayList<CategoriasRubros>();

    public List<CategoriasRubros> getCategoriaRubros() {
        return categoriaRubros;
    }

    public void setCategoriaRubros(List<CategoriasRubros> categoriaRubros) {
        this.categoriaRubros = categoriaRubros;
    }

}
