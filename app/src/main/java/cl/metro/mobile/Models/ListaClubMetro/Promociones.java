package cl.metro.mobile.Models.ListaClubMetro;

import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.Map;


public class Promociones {

    @SerializedName("id")
    private String id;
    @SerializedName("titulo")
    private String titulo;
    @SerializedName("tipo")
    private String tipo;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * @return The id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return The titulo
     */
    public String getTitulo() {
        return titulo;
    }

    /**
     * @param titulo The titulo
     */
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    /**
     * @return The tipo
     */
    public String getTipo() {
        return tipo;
    }

    /**
     * @param tipo The tipo
     */
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
