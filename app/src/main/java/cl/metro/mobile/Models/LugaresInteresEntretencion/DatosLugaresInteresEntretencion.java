package cl.metro.mobile.Models.LugaresInteresEntretencion;

import com.google.gson.annotations.Expose;

import java.util.ArrayList;
import java.util.List;

public class DatosLugaresInteresEntretencion {

    @Expose
    private Integer id;
    @Expose
    private String nombre;
    @Expose
    private Integer tipo;
    @Expose
    private String direccion;
    @Expose
    private String horario;
    @Expose
    private String tarifa;
    @Expose
    private String descripcion;
    @Expose
    private Double latitud;
    @Expose
    private Double longitud;
    @Expose
    private List<DatosImagenesLugares> imagenes = new ArrayList<DatosImagenesLugares>();
    @Expose
    private String imagen;

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    /**
     * @return The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre The nombre
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return The tipo
     */
    public Integer getTipo() {
        return tipo;
    }

    /**
     * @param tipo The tipo
     */
    public void setTipo(Integer tipo) {
        this.tipo = tipo;
    }

    /**
     * @return The direccion
     */
    public String getDireccion() {
        return direccion;
    }

    /**
     * @param direccion The direccion
     */
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    /**
     * @return The horario
     */
    public String getHorario() {
        return horario;
    }

    /**
     * @param horario The horario
     */
    public void setHorario(String horario) {
        this.horario = horario;
    }

    /**
     * @return The tarifa
     */
    public String getTarifa() {
        return tarifa;
    }

    /**
     * @param tarifa The tarifa
     */
    public void setTarifa(String tarifa) {
        this.tarifa = tarifa;
    }

    /**
     * @return The descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * @param descripcion The descripcion
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * @return The latitud
     */
    public Double getLatitud() {
        return latitud;
    }

    /**
     * @param latitud The latitud
     */
    public void setLatitud(Double latitud) {
        this.latitud = latitud;
    }

    /**
     * @return The longitud
     */
    public Double getLongitud() {
        return longitud;
    }

    /**
     * @param longitud The longitud
     */
    public void setLongitud(Double longitud) {
        this.longitud = longitud;
    }

    /**
     * @return The imagenes
     */
    public List<DatosImagenesLugares> getImagenes() {
        return imagenes;
    }

    /**
     * @param imagenes The imagenes
     */
    public void setImagenes(List<DatosImagenesLugares> imagenes) {
        this.imagenes = imagenes;
    }


}
