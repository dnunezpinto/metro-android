package cl.metro.mobile.Models.TarifasServicio;

/**
 * Created by Daniel on 04-03-2015.
 */

import com.google.gson.annotations.Expose;

public class HorarioTarifa {

    @Expose
    private String horario;
    @Expose
    private String inicio;
    @Expose
    private String fin;

    /**
     * @return The horario
     */
    public String getHorario() {
        return horario;
    }

    /**
     * @param horario The horario
     */
    public void setHorario(String horario) {
        this.horario = horario;
    }

    /**
     * @return The inicio
     */
    public String getInicio() {
        return inicio;
    }

    /**
     * @param inicio The inicio
     */
    public void setInicio(String inicio) {
        this.inicio = inicio;
    }

    /**
     * @return The fin
     */
    public String getFin() {
        return fin;
    }

    /**
     * @param fin The fin
     */
    public void setFin(String fin) {
        this.fin = fin;
    }

}