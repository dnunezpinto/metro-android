package cl.metro.mobile.Models.TarifasServicio;

/**
 * Created by Daniel on 04-03-2015.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Tarifas {

    @SerializedName("t_metro_punta")
    @Expose
    private String tMetroPunta;
    @SerializedName("t_metro_valle")
    @Expose
    private String tMetroValle;
    @SerializedName("t_metro_bajo")
    @Expose
    private String tMetroBajo;
    @SerializedName("t_combinacion_punta")
    @Expose
    private String tCombinacionPunta;
    @SerializedName("t_combinacion_valle")
    @Expose
    private String tCombinacionValle;
    @SerializedName("t_combinacion_bajo")
    @Expose
    private String tCombinacionBajo;
    @SerializedName("t_estudiante_punta")
    @Expose
    private String tEstudiantePunta;
    @SerializedName("t_estudiante_valle")
    @Expose
    private String tEstudianteValle;
    @SerializedName("t_estudiante_bajo")
    @Expose
    private String tEstudianteBajo;
    @SerializedName("t_adulto_punta")
    @Expose
    private String tAdultoPunta;
    @SerializedName("t_adulto_valle")
    @Expose
    private String tAdultoValle;
    @SerializedName("t_adulto_bajo")

    @Expose
    private String tAdultoBajo;

    @SerializedName("cuadro1")
    private String cuadro1;

    @Expose
    private String cuadro2;
    @Expose
    private String cuadro3;
    @Expose
    private String cuadro4;
    @Expose
    private Horarios horarios;

    /**
     * @return The tMetroPunta
     */
    public String getTMetroPunta() {
        return tMetroPunta;
    }

    /**
     * @param tMetroPunta The t_metro_punta
     */
    public void setTMetroPunta(String tMetroPunta) {
        this.tMetroPunta = tMetroPunta;
    }

    /**
     * @return The tMetroValle
     */
    public String getTMetroValle() {
        return tMetroValle;
    }

    /**
     * @param tMetroValle The t_metro_valle
     */
    public void setTMetroValle(String tMetroValle) {
        this.tMetroValle = tMetroValle;
    }

    /**
     * @return The tMetroBajo
     */
    public String getTMetroBajo() {
        return tMetroBajo;
    }

    /**
     * @param tMetroBajo The t_metro_bajo
     */
    public void setTMetroBajo(String tMetroBajo) {
        this.tMetroBajo = tMetroBajo;
    }

    /**
     * @return The tCombinacionPunta
     */
    public String getTCombinacionPunta() {
        return tCombinacionPunta;
    }

    /**
     * @param tCombinacionPunta The t_combinacion_punta
     */
    public void setTCombinacionPunta(String tCombinacionPunta) {
        this.tCombinacionPunta = tCombinacionPunta;
    }

    /**
     * @return The tCombinacionValle
     */
    public String getTCombinacionValle() {
        return tCombinacionValle;
    }

    /**
     * @param tCombinacionValle The t_combinacion_valle
     */
    public void setTCombinacionValle(String tCombinacionValle) {
        this.tCombinacionValle = tCombinacionValle;
    }

    /**
     * @return The tCombinacionBajo
     */
    public String getTCombinacionBajo() {
        return tCombinacionBajo;
    }

    /**
     * @param tCombinacionBajo The t_combinacion_bajo
     */
    public void setTCombinacionBajo(String tCombinacionBajo) {
        this.tCombinacionBajo = tCombinacionBajo;
    }

    /**
     * @return The tEstudiantePunta
     */
    public String getTEstudiantePunta() {
        return tEstudiantePunta;
    }

    /**
     * @param tEstudiantePunta The t_estudiante_punta
     */
    public void setTEstudiantePunta(String tEstudiantePunta) {
        this.tEstudiantePunta = tEstudiantePunta;
    }

    /**
     * @return The tEstudianteValle
     */
    public String getTEstudianteValle() {
        return tEstudianteValle;
    }

    /**
     * @param tEstudianteValle The t_estudiante_valle
     */
    public void setTEstudianteValle(String tEstudianteValle) {
        this.tEstudianteValle = tEstudianteValle;
    }

    /**
     * @return The tEstudianteBajo
     */
    public String getTEstudianteBajo() {
        return tEstudianteBajo;
    }

    /**
     * @param tEstudianteBajo The t_estudiante_bajo
     */
    public void setTEstudianteBajo(String tEstudianteBajo) {
        this.tEstudianteBajo = tEstudianteBajo;
    }

    /**
     * @return The tAdultoPunta
     */
    public String getTAdultoPunta() {
        return tAdultoPunta;
    }

    /**
     * @param tAdultoPunta The t_adulto_punta
     */
    public void setTAdultoPunta(String tAdultoPunta) {
        this.tAdultoPunta = tAdultoPunta;
    }

    /**
     * @return The tAdultoValle
     */
    public String getTAdultoValle() {
        return tAdultoValle;
    }

    /**
     * @param tAdultoValle The t_adulto_valle
     */
    public void setTAdultoValle(String tAdultoValle) {
        this.tAdultoValle = tAdultoValle;
    }

    /**
     * @return The tAdultoBajo
     */
    public String getTAdultoBajo() {
        return tAdultoBajo;
    }

    /**
     * @param tAdultoBajo The t_adulto_bajo
     */
    public void setTAdultoBajo(String tAdultoBajo) {
        this.tAdultoBajo = tAdultoBajo;
    }

    /**
     * @return The cuadro1
     */
    public String getCuadro1() {
        return cuadro1;
    }

    /**
     * @param cuadro1 The cuadro1
     */
    public void setCuadro1(String cuadro1) {
        this.cuadro1 = cuadro1;
    }

    /**
     * @return The cuadro2
     */
    public String getCuadro2() {
        return cuadro2;
    }

    /**
     * @param cuadro2 The cuadro2
     */
    public void setCuadro2(String cuadro2) {
        this.cuadro2 = cuadro2;
    }

    /**
     * @return The cuadro3
     */
    public String getCuadro3() {
        return cuadro3;
    }

    /**
     * @param cuadro3 The cuadro3
     */
    public void setCuadro3(String cuadro3) {
        this.cuadro3 = cuadro3;
    }

    /**
     * @return The cuadro4
     */
    public String getCuadro4() {
        return cuadro4;
    }

    /**
     * @param cuadro4 The cuadro4
     */
    public void setCuadro4(String cuadro4) {
        this.cuadro4 = cuadro4;
    }

    /**
     * @return The horarios
     */
    public Horarios getHorarios() {
        return horarios;
    }

    /**
     * @param horarios The horarios
     */
    public void setHorarios(Horarios horarios) {
        this.horarios = horarios;
    }

}