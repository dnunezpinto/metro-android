package cl.metro.mobile.Models;

public class Ruta {

    private String tipo;

    private String estacionNombre;

    private String estacionLinea;

    private String estacionLineaNombre;

    private boolean movilidadReducida;

    private String direccion;

    private String estacionBajada;

    private String tiempo;

    private String distancia;

    private boolean isCombinacion;

    private String[] movilidadReducidaAternativas;

    private boolean isInitNormal = false;


    /**
     * @return the tipo
     */
    public String getTipo() {
        return tipo;
    }

    /**
     * @param tipo the tipo to set
     */
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    /**
     * @return the estacionNombre
     */
    public String getEstacionNombre() {
        return estacionNombre;
    }

    /**
     * @param estacionNombre the estacionNombre to set
     */
    public void setEstacionNombre(String estacionNombre) {
        this.estacionNombre = estacionNombre;
    }

    /**
     * @return the estacionLinea
     */
    public String getEstacionLinea() {
        return estacionLinea;
    }

    /**
     * @param estacionLinea the estacionLinea to set
     */
    public void setEstacionLinea(String estacionLinea) {
        this.estacionLinea = estacionLinea;
    }

    /**
     * @return the estacionLineaNombre
     */
    public String getEstacionLineaNombre() {
        return estacionLineaNombre;
    }

    /**
     * @param estacionLineaNombre the estacionLineaNombre to set
     */
    public void setEstacionLineaNombre(String estacionLineaNombre) {
        this.estacionLineaNombre = estacionLineaNombre;
    }

    /**
     * @return the movilidadReducida
     */
    public boolean isMovilidadReducida() {
        return movilidadReducida;
    }

    /**
     * @param movilidadReducida the movilidadReducida to set
     */
    public void setMovilidadReducida(boolean movilidadReducida) {
        this.movilidadReducida = movilidadReducida;
    }

    /**
     * @return the direccion
     */
    public String getDireccion() {
        return direccion;
    }

    /**
     * @param direccion the direccion to set
     */
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    /**
     * @return the tiempo
     */
    public String getTiempo() {
        return tiempo;
    }

    /**
     * @param tiempo the tiempo to set
     */
    public void setTiempo(String tiempo) {
        this.tiempo = tiempo;
    }

    /**
     * @return the distancia
     */
    public String getDistancia() {
        return distancia;
    }

    /**
     * @param distancia the distancia to set
     */
    public void setDistancia(String distancia) {
        this.distancia = distancia;
    }

    /**
     * @return the movilidadReducidaAternativas
     */
    public String[] getMovilidadReducidaAternativas() {
        return movilidadReducidaAternativas;
    }

    /**
     * @param movilidadReducidaAternativas the movilidadReducidaAternativas to set
     */
    public void setMovilidadReducidaAternativas(
            String[] movilidadReducidaAternativas) {
        this.movilidadReducidaAternativas = movilidadReducidaAternativas;
    }

    /**
     * @return the isInitNormal
     */
    public boolean isInitNormal() {
        return isInitNormal;
    }

    /**
     * @param isInitNormal the isInitNormal to set
     */
    public void setInitNormal(boolean isInitNormal) {
        this.isInitNormal = isInitNormal;
    }

    public String getEstacionBajada() {
        return estacionBajada;
    }

    public void setEstacionBajada(String estacionBajada) {
        this.estacionBajada = estacionBajada;
    }

    public boolean isCombinacion() {
        return isCombinacion;
    }

    public void setCombinacion(boolean isCombinacion) {
        this.isCombinacion = isCombinacion;
    }
}
