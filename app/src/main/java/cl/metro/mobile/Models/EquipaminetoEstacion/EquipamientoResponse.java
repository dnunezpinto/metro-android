/**
 *
 */
package cl.metro.mobile.Models.EquipaminetoEstacion;


import com.google.gson.annotations.Expose;

public class EquipamientoResponse {

    @Expose
    private String tipo;
    @Expose
    private String valor;

    /**
     * @return The tipo
     */
    public String getTipo() {
        return tipo;
    }

    /**
     * @param tipo The tipo
     */
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    /**
     * @return The valor
     */
    public String getValor() {
        return valor;
    }

    /**
     * @param valor The valor
     */
    public void setValor(String valor) {
        this.valor = valor;
    }

}