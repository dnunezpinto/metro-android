/**
 *
 */
package cl.metro.mobile.Models.Horario;


import com.google.gson.annotations.SerializedName;

public class Estacion {

    @SerializedName("abrir")
    private Horario abrir;

    @SerializedName("cerrar")
    private Horario cerrar;

    /**
     * @return the abrir
     */
    public Horario getAbrir() {
        return abrir;
    }

    /**
     * @param abrir the abrir to set
     */
    public void setAbrir(Horario abrir) {
        this.abrir = abrir;
    }

    /**
     * @return the cerrar
     */
    public Horario getCerrar() {
        return cerrar;
    }

    /**
     * @param cerrar the cerrar to set
     */
    public void setCerrar(Horario cerrar) {
        this.cerrar = cerrar;
    }

}
