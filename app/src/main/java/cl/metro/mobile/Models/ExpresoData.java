package cl.metro.mobile.Models;

import java.util.List;

public class ExpresoData {

    public String tipo;
    public String inicioHorarioRoja;
    public String inicioHorarioVerde;
    public String finHorarioRoja;
    public String finHorarioVerde;
    public String inicioHorario;
    public String finHorario;
    public String direccion;
    public String tituloHorario;

    public String getInicioHorarioRoja() {
        return inicioHorarioRoja;
    }

    public void setInicioHorarioRoja(String inicioHorarioRoja) {
        this.inicioHorarioRoja = inicioHorarioRoja;
    }

    public String getInicioHorarioVerde() {
        return inicioHorarioVerde;
    }

    public void setInicioHorarioVerde(String inicioHorarioVerde) {
        this.inicioHorarioVerde = inicioHorarioVerde;
    }

    public String getFinHorarioRoja() {
        return finHorarioRoja;
    }

    public void setFinHorarioRoja(String finHorarioRoja) {
        this.finHorarioRoja = finHorarioRoja;
    }

    public String getFinHorarioVerde() {
        return finHorarioVerde;
    }

    public void setFinHorarioVerde(String finHorarioVerde) {
        this.finHorarioVerde = finHorarioVerde;
    }

    public List<ExpresoData> expreList;

    public List<ExpresoData> getExpreList() {
        return expreList;
    }

    public void setExpreList(List<ExpresoData> expreList) {
        this.expreList = expreList;
    }

    public String getTituloHorario() {
        return tituloHorario;
    }

    public void setTituloHorario(String tituloHorario) {
        this.tituloHorario = tituloHorario;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getInicioHorario() {
        return inicioHorario;
    }

    public void setInicioHorario(String inicioHorario) {
        this.inicioHorario = inicioHorario;
    }

    public String getFinHorario() {
        return finHorario;
    }

    public void setFinHorario(String finHorario) {
        this.finHorario = finHorario;
    }

}
