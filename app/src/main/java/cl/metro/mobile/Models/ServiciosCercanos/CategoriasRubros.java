/**
 *
 */
package cl.metro.mobile.Models.ServiciosCercanos;

import android.util.Log;

import com.google.gson.annotations.SerializedName;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;


public class CategoriasRubros {

    @SerializedName("categoria")
    private String nombreCategoria;

    @SerializedName("rubros")
    private List<String> rubros = new ArrayList<String>();

    public String getNombreCategoria() {
        return nombreCategoria;
    }

    public void setCategoria(String categoria) {


        Log.i("App", "Nombre: " + categoria);

        String s = "";

        try {
            s = URLDecoder.decode(categoria, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        this.nombreCategoria = s;
    }

    public List<String> getRubros() {
        return rubros;
    }

    public void setRubros(List<String> rubros) {
        this.rubros = rubros;
    }
}
