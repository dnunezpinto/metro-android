package cl.metro.mobile.Models.EstadoRed;

/**
 * Created by Daniel on 05-03-2015.
 */

import com.google.gson.annotations.Expose;


public class EstadoLineas {

    @Expose
    private int l1;
    @Expose
    private int l2;
    @Expose
    private int l4;
    @Expose
    private int l4a;
    @Expose
    private int l5;

    /**
     * @return The l1
     */
    public int getL1() {
        return l1;
    }

    /**
     * @param l1 The l1
     */
    public void setL1(int l1) {
        this.l1 = l1;
    }

    /**
     * @return The l2
     */
    public int getL2() {
        return l2;
    }

    /**
     * @param l2 The l2
     */
    public void setL2(int l2) {
        this.l2 = l2;
    }

    /**
     * @return The l4
     */
    public int getL4() {
        return l4;
    }

    /**
     * @param l4 The l4
     */
    public void setL4(int l4) {
        this.l4 = l4;
    }

    /**
     * @return The l4a
     */
    public int getL4a() {
        return l4a;
    }

    /**
     * @param l4a The l4a
     */
    public void setL4a(int l4a) {
        this.l4a = l4a;
    }

    /**
     * @return The l5
     */
    public int getL5() {
        return l5;
    }

    /**
     * @param l5 The l5
     */
    public void setL5(int l5) {
        this.l5 = l5;
    }

}