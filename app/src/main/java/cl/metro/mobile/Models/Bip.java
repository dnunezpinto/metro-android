/**
 *
 */
package cl.metro.mobile.Models;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author jvalladares
 */
public class Bip {

    private int id;
    private String nombre;
    private String numeroBip;
    private String saldo;
    private boolean isFavorito;
    private Date ultimoUpdate;
    private String estadoTarjeta;

    public Bip() {
    }

    public Bip(int id, String nombre, String numeroBip,
               boolean isFavorito, String saldo,
               String estadoTarjeta) {

        this.id = id;
        this.nombre = nombre;
        this.isFavorito = isFavorito;
        this.numeroBip = numeroBip;
        this.saldo = saldo;
        this.estadoTarjeta = estadoTarjeta;
    }

//	public Bip(String nombre, String numeroBip, boolean isFavorito,
//			String ultimoUpdate, String saldo, String estadoTarjeta) {
//
//		this((new BipEntityAdapter<Bip>()).getCount() + 1, nombre,
//				numeroBip, isFavorito, ultimoUpdate, saldo, estadoTarjeta);
//
//	}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNumeroBip() {
        return numeroBip;
    }

    public void setNumeroBip(String numeroBip) {
        this.numeroBip = numeroBip;
    }

    public boolean isFavorito() {
        return isFavorito;
    }

    public void setFavorito(boolean isFavorito) {
        this.isFavorito = isFavorito;
    }

    public Date getUltimoUpdate() {
        return ultimoUpdate;
    }

    public void setUltimoUpdate(String ultimoUpdate) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        try {
            Date date = sdf.parse(ultimoUpdate);
            if (date != null) {
                setUltimoUpdate(date);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public void setUltimoUpdate(Date ultimoUpdate) {
        this.ultimoUpdate = ultimoUpdate;
    }

    public String getSaldo() {
        return saldo;
    }

    public void setSaldo(String saldo) {
        this.saldo = saldo;
    }

    public String getEstadoTarjeta() {
        return estadoTarjeta;
    }

    public void setEstadoTarjeta(String estadoTarjeta) {
        this.estadoTarjeta = estadoTarjeta;
    }

    public String getStrignUltimoUpdateFormat() {
        if (ultimoUpdate != null) {
            DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm");
            return df.format(ultimoUpdate);
        } else
            return "";
    }

    public Date getDateFormatUltimoUpdate(String ultimoUpdate)
            throws ParseException {
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        return df.parse(ultimoUpdate);
    }
}
