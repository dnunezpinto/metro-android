package cl.metro.mobile.Models.Paraderos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class ListParaderos {

    @SerializedName("paraderos")
    private List<DatosParadero> datosParaderos = new ArrayList<DatosParadero>();
    @Expose
    private Integer estado;

    /**
     * @return The datosParaderos
     */
    public List<DatosParadero> getDatosParaderos() {
        return datosParaderos;
    }

    /**
     * @param datosParaderos The datosParaderos
     */
    public void setDatosParaderos(List<DatosParadero> datosParaderos) {
        this.datosParaderos = datosParaderos;
    }

    /**
     * @return The estado
     */
    public Integer getEstado() {
        return estado;
    }

    /**
     * @param estado The estado
     */
    public void setEstado(Integer estado) {
        this.estado = estado;
    }

}
