package cl.metro.mobile.Models.EstadoRed;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Daniel on 05-03-2015.
 */
public class EstadoRed {


    @SerializedName("estado_estacion")
    @Expose
    private int estadoEstacion;
    @SerializedName("estado_lineas")
    @Expose
    private EstadoLineas estadoLineas;
    @SerializedName("tweet_linea")
    @Expose
    private String tweetLinea;
    @SerializedName("hash_estaciones")
    @Expose
    private String hashEstaciones;
    @SerializedName("hash_publicidad")
    @Expose
    private String hashPublicidad;

    /**
     * @return The estadoEstacion
     */
    public int getEstadoEstacion() {
        return estadoEstacion;
    }

    /**
     * @param estadoEstacion The estado_estacion
     */
    public void setEstadoEstacion(int estadoEstacion) {
        this.estadoEstacion = estadoEstacion;
    }

    /**
     * @return The estadoLineas
     */
    public EstadoLineas getEstadoLineas() {
        return estadoLineas;
    }

    /**
     * @param estadoLineas The estado_lineas
     */
    public void setEstadoLineas(EstadoLineas estadoLineas) {
        this.estadoLineas = estadoLineas;
    }

    /**
     * @return The tweetLinea
     */
    public String getTweetLinea() {
        return tweetLinea;
    }

    /**
     * @param tweetLinea The tweet_linea
     */
    public void setTweetLinea(String tweetLinea) {
        this.tweetLinea = tweetLinea;
    }

    /**
     * @return The hashEstaciones
     */
    public String getHashEstaciones() {
        return hashEstaciones;
    }

    /**
     * @param hashEstaciones The hash_estaciones
     */
    public void setHashEstaciones(String hashEstaciones) {
        this.hashEstaciones = hashEstaciones;
    }

    /**
     * @return The hashPublicidad
     */
    public String getHashPublicidad() {
        return hashPublicidad;
    }

    /**
     * @param hashPublicidad The hash_publicidad
     */
    public void setHashPublicidad(String hashPublicidad) {
        this.hashPublicidad = hashPublicidad;
    }

}
