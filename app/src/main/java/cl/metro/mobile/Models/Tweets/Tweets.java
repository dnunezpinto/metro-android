package cl.metro.mobile.Models.Tweets;

import com.google.gson.annotations.Expose;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Daniel on 05-03-2015.
 */
public class Tweets {

    @Expose
    private List<TweetResult> results = new ArrayList<TweetResult>();

    /**
     * @return The results
     */
    public List<TweetResult> getResults() {
        return results;
    }

    /**
     * @param results The results
     */
    public void setResults(List<TweetResult> results) {
        this.results = results;
    }

}
