package cl.metro.mobile.Models.EquipaminetoEstacion;


import com.google.gson.annotations.SerializedName;

/**
 * Created by Daniel on 03-03-2015.
 */
public class Equipamiento {

    @SerializedName("getEquipamientoEstacion")
    private GetEquipamientoEstacion getEquipamientoEstacion;

    /**
     * @return The getEquipamientoEstacion
     */
    public GetEquipamientoEstacion getGetEquipamientoEstacion() {
        return getEquipamientoEstacion;
    }

    /**
     * @param getEquipamientoEstacion The getEquipamientoEstacion
     */
    public void setGetEquipamientoEstacion(GetEquipamientoEstacion getEquipamientoEstacion) {
        this.getEquipamientoEstacion = getEquipamientoEstacion;
    }

}