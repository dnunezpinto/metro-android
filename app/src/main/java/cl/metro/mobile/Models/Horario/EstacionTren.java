/**
 *
 */
package cl.metro.mobile.Models.Horario;

import com.google.gson.annotations.SerializedName;

public class EstacionTren {

    @SerializedName("nombre")
    private String nombre;

    @SerializedName("primer_tren")
    private Horario primerTren;

    @SerializedName("ultimo_tren")
    private Horario ultimoTren;

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the primerTren
     */
    public Horario getPrimerTren() {
        return primerTren;
    }

    /**
     * @param primerTren the primerTren to set
     */
    public void setPrimerTren(Horario primerTren) {
        this.primerTren = primerTren;
    }

    /**
     * @return the ultimoTren
     */
    public Horario getUltimoTren() {
        return ultimoTren;
    }

    /**
     * @param ultimoTren the ultimoTren to set
     */
    public void setUltimoTren(Horario ultimoTren) {
        this.ultimoTren = ultimoTren;
    }

}
