package cl.metro.mobile.Models;

import android.location.Location;

/**
 * Created by Daniel on 11-02-2015.
 */
public class Estaciones {

    private int idEstacion;
    private int idLinea;
    private String codigoEstacion;
    private String nombreEstacion;
    private float latitud;
    private float longitud;
    private int orden;
    private boolean isFavorito;
    private String estado;
    private String distancia;
    private boolean isCombinacion;

    public int getIdEstacion() {
        return idEstacion;
    }

    public void setIdEstacion(int idEstacion) {
        this.idEstacion = idEstacion;
    }

    public int getIdLinea() {
        return idLinea;
    }

    public void setIdLinea(int idLinea) {
        this.idLinea = idLinea;
    }

    public String getCodigoEstacion() {
        return codigoEstacion;
    }

    public void setCodigoEstacion(String codigoEstacion) {
        this.codigoEstacion = codigoEstacion;
    }

    public String getNombreEstacion() {
        return nombreEstacion;
    }

    public void setNombreEstacion(String nombreEstacion) {
        this.nombreEstacion = nombreEstacion;
    }

    public float getLatitud() {
        return latitud;
    }

    public void setLatitud(float latitud) {
        this.latitud = latitud;
    }

    public float getLongitud() {
        return longitud;
    }

    public void setLongitud(float longitud) {
        this.longitud = longitud;
    }

    public int getOrden() {
        return orden;
    }

    public void setOrden(int orden) {
        this.orden = orden;
    }

    public boolean isFavorito() {
        return isFavorito;
    }

    public void setFavorito(boolean isFavorito) {
        this.isFavorito = isFavorito;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getDistancia() {
        return distancia;
    }

    public void setDistancia(String distancia) {
        this.distancia = distancia;
    }

    public boolean isCombinacion() {
        return isCombinacion;
    }

    public void setCombinacion(boolean isCombinacion) {
        this.isCombinacion = isCombinacion;
    }

    public float getDistance(Location l) {
        Location loc = new Location(l);
        loc.setLatitude(getLatitud());
        loc.setLongitude(getLongitud());
        float distance = l.distanceTo(loc);
        int distancia_enteros = (int) distance;
        String tipoDistancia = "Mts.";

        if (distance > 1000) {
            distancia_enteros = (int) (distance / 1000);
            tipoDistancia = "Kms.";
        }
        setDistancia("Aprox. " + distancia_enteros + " " + tipoDistancia);
        return distance;
    }

}
