package cl.metro.mobile.Models.LugaresInteresEntretencion;

import com.google.gson.annotations.Expose;

/**
 * Created by Daniel on 07-05-2015.
 */
public class DatosImagenesLugares {

    @Expose
    private String src;

    /**
     * @return The src
     */
    public String getSrc() {
        return src;
    }

    /**
     * @param src The src
     */
    public void setSrc(String src) {
        this.src = src;
    }
}
