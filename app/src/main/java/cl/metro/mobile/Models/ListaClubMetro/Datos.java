package cl.metro.mobile.Models.ListaClubMetro;

import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.Map;

public class Datos {

    @SerializedName("id")
    private int id;
    @SerializedName("titulo")
    private String titulo;
    @SerializedName("tipo")
    private String tipo;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    @SerializedName("imagen")
    private String imagen;
    @SerializedName("canjeable")
    private String canjeable;

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    /**
     * @return The id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return The titulo
     */
    public String getTitulo() {
        return titulo;
    }

    /**
     * @param titulo The titulo
     */
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    /**
     * @return The tipo
     */
    public String getTipo() {
        return tipo;
    }

    /**
     * @param tipo The tipo
     */
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    /**
     * @return The canjeable
     */
    public String getCanjeable() {
        return canjeable;
    }

    /**
     * @param canjeable The canjeable
     */
    public void setCanjeable(String canjeable) {
        this.canjeable = canjeable;
    }

}
