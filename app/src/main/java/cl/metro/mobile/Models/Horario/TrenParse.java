/**
 *
 */
package cl.metro.mobile.Models.Horario;

import com.google.gson.annotations.SerializedName;

public class TrenParse {

    @SerializedName("estacion")
    private Estacion estacion;

    @SerializedName("boleteria")
    private Boleteria boleteria;

    @SerializedName("tren")
    private Tren tren;

    /**
     * @return the estacion
     */
    public Estacion getEstacion() {
        return estacion;
    }

    /**
     * @param estacion the estacion to set
     */
    public void setEstacion(Estacion estacion) {
        this.estacion = estacion;
    }

    /**
     * @return the boleteria
     */
    public Boleteria getBoleteria() {
        return boleteria;
    }

    /**
     * @param boleteria the boleteria to set
     */
    public void setBoleteria(Boleteria boleteria) {
        this.boleteria = boleteria;
    }

    /**
     * @return the tren
     */
    public Tren getTren() {
        return tren;
    }

    /**
     * @param tren the tren to set
     */
    public void setTren(Tren tren) {
        this.tren = tren;
    }

}
