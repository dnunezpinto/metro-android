package cl.metro.mobile.Models;

/**
 * Created by Daniel on 11-02-2015.
 */
public class Lineas {

    int idLinea;
    String nombreLinea;
    int codigoLinea;
    boolean isNotificacionLinea;

    public int getIdLinea() {
        return idLinea;
    }

    public void setIdLinea(int idLinea) {
        this.idLinea = idLinea;
    }

    public String getNombreLinea() {
        return nombreLinea;
    }

    public void setNombreLinea(String nombreLinea) {
        this.nombreLinea = nombreLinea;
    }

    public int getCodigoLinea() {
        return codigoLinea;
    }

    public void setCodigoLinea(int codigoLinea) {
        this.codigoLinea = codigoLinea;
    }

    public boolean isNotificacionLinea() {
        return isNotificacionLinea;
    }

    public void setNotificacionLinea(boolean isNotificacionLinea) {
        this.isNotificacionLinea = isNotificacionLinea;
    }
}
