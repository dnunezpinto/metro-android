package cl.metro.mobile.Models.ListaClubMetro;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ListClub {

    @SerializedName("eventos")
    private List<Datos> eventos = new ArrayList<Datos>();
    @SerializedName("promociones")
    private List<Datos> promociones = new ArrayList<Datos>();
    @SerializedName("actividades")
    private List<Datos> actividades = new ArrayList<Datos>();

    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * @return The eventos
     */
    public List<Datos> getEventos() {
        return eventos;
    }

    /**
     * @param eventos The eventos
     */
    public void setEventos(List<Datos> eventos) {
        this.eventos = eventos;
    }

    /**
     * @return The promociones
     */
    public List<Datos> getPromociones() {
        return promociones;
    }

    /**
     * @param promociones The promociones
     */
    public void setPromociones(List<Datos> promociones) {
        this.promociones = promociones;
    }

    /**
     * @return The actividades
     */
    public List<Datos> getActividades() {
        return actividades;
    }

    /**
     * @param actividades The actividades
     */
    public void setActividades(List<Datos> actividades) {
        this.actividades = actividades;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
