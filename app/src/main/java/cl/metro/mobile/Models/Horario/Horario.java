/**
 *
 */
package cl.metro.mobile.Models.Horario;

import com.google.gson.annotations.SerializedName;

public class Horario {

    @SerializedName("lunes_viernes")
    private String lunesViernes;

    @SerializedName("sabado")
    private String sabado;

    @SerializedName("domingo")
    private String domingo;

    /**
     * @return the lunesViernes
     */
    public String getLunesViernes() {
        return lunesViernes;
    }

    /**
     * @param lunesViernes the lunesViernes to set
     */
    public void setLunesViernes(String lunesViernes) {
        this.lunesViernes = lunesViernes;
    }

    /**
     * @return the sabado
     */
    public String getSabado() {
        return sabado;
    }

    /**
     * @param sabado the sabado to set
     */
    public void setSabado(String sabado) {
        this.sabado = sabado;
    }

    /**
     * @return the domingo
     */
    public String getDomingo() {
        return domingo;
    }

    /**
     * @param domingo the domingo to set
     */
    public void setDomingo(String domingo) {
        this.domingo = domingo;
    }

}
