/**
 *
 */
package cl.metro.mobile.Models.ServiciosCercanos;

import com.google.gson.annotations.SerializedName;

import java.util.StringTokenizer;


public class Elementos {

    @SerializedName("nombre_fantasia")
    private String nombreFantasia;

    @SerializedName("rubro")
    private String rubro;

    @SerializedName("direccion")
    private String direccion;

    @SerializedName("numero")
    private String numero;

    @SerializedName("cuadras")
    private String cuadras;

    @SerializedName("coordenadas")
    private String coordenadas;

    private double latitud;
    private double longitud;

    public String getNombreFantasia() {
        return nombreFantasia;
    }

    public void setNombreFantasia(String nombreFantasia) {
        this.nombreFantasia = nombreFantasia;
    }

    public String getRubro() {
        return rubro;
    }

    public void setRubro(String rubro) {
        this.rubro = rubro;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getCuadras() {
        return cuadras;
    }

    public void setCuadras(String cuadras) {
        this.cuadras = cuadras;
    }

    public String getCoordenadas() {
        return coordenadas;
    }

    public void setCoordenadas(String coordenadas) {
        setLatitudAndLongitud(coordenadas);
        this.coordenadas = coordenadas;
    }

    public double getLatitud() {
        return latitud;
    }

    public void setLatitud(double latitud) {

        this.latitud = latitud;
    }

    public double getLongitud() {
        return longitud;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }

    private void setLatitudAndLongitud(String coordenadas) {

        StringTokenizer tokens = new StringTokenizer(coordenadas, ",");
        String[] items = coordenadas.split(",");
        setLatitud(Double.parseDouble(tokens.nextToken()));
        setLongitud(Double.parseDouble(tokens.nextToken()));

    }


}
