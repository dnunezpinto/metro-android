/**
 *
 */
package cl.metro.mobile.Models.Horario;

import com.google.gson.annotations.SerializedName;

public class Tren {

    @SerializedName("estacion_a")
    private EstacionTren estacionA;

    @SerializedName("estacion_b")
    private EstacionTren estacionB;

    /**
     * @return the estacionA
     */
    public EstacionTren getEstacionA() {
        return estacionA;
    }

    /**
     * @param estacionA the estacionA to set
     */
    public void setEstacionA(EstacionTren estacionA) {
        this.estacionA = estacionA;
    }

    /**
     * @return the estacionB
     */
    public EstacionTren getEstacionB() {
        return estacionB;
    }

    /**
     * @param estacionB the estacionB to set
     */
    public void setEstacionB(EstacionTren estacionB) {
        this.estacionB = estacionB;
    }


}
