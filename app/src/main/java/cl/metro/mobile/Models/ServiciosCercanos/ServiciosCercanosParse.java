/**
 *
 */
package cl.metro.mobile.Models.ServiciosCercanos;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class ServiciosCercanosParse {

    @SerializedName("categoria")
    private List<CategoriaParse> categoria = new ArrayList<CategoriaParse>();

    /**
     * @return the categoria
     */
    public List<CategoriaParse> getCategoria() {
        return categoria;
    }

    /**
     * @param categoria the categoria to set
     */
    public void setCategoria(List<CategoriaParse> categoria) {
        this.categoria = categoria;
    }

}
