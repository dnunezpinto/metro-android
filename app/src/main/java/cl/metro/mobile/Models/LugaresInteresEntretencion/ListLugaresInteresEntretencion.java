package cl.metro.mobile.Models.LugaresInteresEntretencion;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class ListLugaresInteresEntretencion {

    @SerializedName("lugares")
    private List<DatosLugaresInteresEntretencion> lugares = new ArrayList<DatosLugaresInteresEntretencion>();
    @SerializedName("eventos")
    private List<DatosLugaresInteresEntretencion> eventos = new ArrayList<DatosLugaresInteresEntretencion>();

    public List<DatosLugaresInteresEntretencion> getEventos() {
        return eventos;
    }

    public void setEventos(List<DatosLugaresInteresEntretencion> eventos) {
        this.eventos = eventos;
    }

    /**
     * @return The lugares
     */
    public List<DatosLugaresInteresEntretencion> getLugares() {
        return lugares;
    }

    /**
     * @param lugares The lugares
     */
    public void setLugares(List<DatosLugaresInteresEntretencion> lugares) {
        this.lugares = lugares;
    }


}
