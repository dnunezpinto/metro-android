package cl.metro.mobile.Models.TarifasServicio;

/**
 * Created by Daniel on 04-03-2015.
 */

import com.google.gson.annotations.Expose;

import java.util.ArrayList;
import java.util.List;

public class Horarios {

    @Expose
    private List<HorarioTarifa> horarios = new ArrayList<HorarioTarifa>();

    /**
     * @return The horarios
     */
    public List<HorarioTarifa> getHorarios() {
        return horarios;
    }

    /**
     * @param horarioTarifas The horarios
     */
    public void setHorarios(List<HorarioTarifa> horarioTarifas) {
        this.horarios = horarioTarifas;
    }

}