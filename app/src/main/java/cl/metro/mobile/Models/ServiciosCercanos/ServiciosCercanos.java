package cl.metro.mobile.Models.ServiciosCercanos;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Daniel on 13-03-2015.
 */
public class ServiciosCercanos {


    @SerializedName("getServiciosExternos")
    private ServiciosCercanosParse getServiciosCercanos;
    @SerializedName("getServiciosExternosLista")
    private ServiciosCercanosListaParse getServiciosCercanosLista;

    /**
     * @return The getServiciosCercanos
     */
    public ServiciosCercanosParse getServiciosCercanos() {
        return getServiciosCercanos;
    }

    /**
     * @param getServiciosCercanos The getServiciosCercanos
     */
    public void ServiciosCercanosParse(ServiciosCercanosParse getServiciosCercanos) {
        this.getServiciosCercanos = getServiciosCercanos;
    }

    /**
     * @return The getServiciosCercanosLista
     */
    public ServiciosCercanosListaParse getServiciosCercanosLista() {
        return getServiciosCercanosLista;
    }

    /**
     * @param getServiciosCercanosLista The getServiciosCercanosLista
     */
    public void ServiciosCercanosListaParse(ServiciosCercanosListaParse getServiciosCercanosLista) {
        this.getServiciosCercanosLista = getServiciosCercanosLista;
    }

}
