package cl.metro.mobile;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.io.File;

import cl.metro.mobile.Utilities.Utils;

public class BannerITT extends Activity {

    public static ImageLoader imageLoader = ImageLoader.getInstance();
    public static DisplayImageOptions options;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_banner_itt);

        ImageView imgITT = findViewById(R.id.imgBannerITT);
        final ProgressBar progressBar = findViewById(R.id.progressBar14);


        File cacheDir = new File(BannerITT.this.getCacheDir(), "imgcachedir");
        if (!cacheDir.exists())
            cacheDir.mkdir();

        options = new DisplayImageOptions.Builder()
                .resetViewBeforeLoading(true)
                .imageScaleType(ImageScaleType.IN_SAMPLE_INT)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .cacheOnDisk(true)
                .displayer(new FadeInBitmapDisplayer(300))
                .build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(BannerITT.this)
                .threadPriority(Thread.MIN_PRIORITY + 3)
                .denyCacheImageMultipleSizesInMemory()
                .memoryCache(new WeakMemoryCache())
                .defaultDisplayImageOptions(options)
                .build();
        ImageLoader.getInstance().init(config);

        String imgURL = Utils.BannerITTURL;

        imageLoader.displayImage(imgURL, imgITT, options, new SimpleImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {
                progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {

                Toast.makeText(BannerITT.this, "Erro al Cargar el Banner", Toast.LENGTH_LONG).show();

                Intent i = new Intent(BannerITT.this, MainActivity.class);
                finish();
                startActivity(i);
//                imgEvento.setScaleType(ImageView.ScaleType.FIT_XY);
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                progressBar.setVisibility(View.GONE);

                Thread timer = new Thread() {
                    public void run() {
                        try {
                            sleep(5000);
                        } catch (InterruptedException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } finally {
                            Intent i = new Intent(BannerITT.this, MainActivity.class);
                            finish();
                            startActivity(i);
                        }
                    }
                };
                timer.start();
            }
        });

        imgITT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Utils.linkBanner.isEmpty() || Utils.linkBanner != null || Utils.linkBanner != "" || !Utils.linkBanner.equals("")) {
                    Utils.openWebPage(Utils.linkBanner, BannerITT.this);
                }

            }
        });


    }
}
